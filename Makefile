build:
	@docker-compose pull && docker-compose build

start:
	@docker-compose up -d

stop:
	@docker-compose stop

clean:
	rm node_modules dist -rf && docker-compose down

logs:
	@docker-compose logs
