var Promise = require('es6-promise').Promise;
var exec = require('child_process').exec;
var path = require('path');
var del = require('del');
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var postcss = require('gulp-postcss');
var autoprefixer   = require('autoprefixer');
var postcssEasings = require('postcss-easings');
var express = require('express');
var exec = require('child_process').exec,
    path = require('path'),
    del = require('del'),
    gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    postcss = require('gulp-postcss'),
    autoprefixer   = require('autoprefixer'),
    postcssEasings = require('postcss-easings');
// set variable via $ gulp --type production
var environment = process.env.NODE_ENV || 'development',
    isProduction = environment === 'production',
    webpackConfig = require('./webpack.config.js').getConfig('production'),
    webpackServerConfig = require ('./webpack.server').getConfigServer('production')
var Proxy = require('gulp-connect-proxy');

var port = $.util.env.port || 9000;
var app = 'app/';
var dist = 'dist/';
var connect = require ('gulp-connect');
var http = require('http');
var expressServer = express();

// https://github.com/ai/autoprefixer
var autoprefixerBrowsers = [
  'ie >= 9',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 6',
  'opera >= 23',
  'ios >= 6',
  'android >= 4.4',
  'bb >= 10'
];




expressServer.use("/css", express.static(__dirname + '/dist/css'));
expressServer.use("/js", express.static(__dirname + '/dist/js'));
expressServer.use("/fonts", express.static(__dirname + '/dist/fonts'));
expressServer.use("/images", express.static(__dirname + '/dist/images'));
expressServer.use("/flags", express.static(__dirname + '/dist/flags'));



expressServer.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:9000');
  next();
});



expressServer.get('*', function(req, res) {

  console.log(JSON.stringify(__dirname));

//console.log(__dirname);

 res.sendFile(__dirname + '/dist/index.html');


});


gulp.task('bower', function (cb) {
  // Install bower dependencies
  exec('bower install', function (err, stdout, stderr) {
    if (stdout) console.log(stdout);
    if (stderr) console.log(stderr);
    cb(err);
  });
});

/* Font task
 *
 * Copies font files over to dest dir
 */
gulp.task('font', function () {
  return gulp.src(app + 'fonts/**/*.{eot,ttf,svg,woff}')
        .pipe(gulp.dest(dist + 'fonts/'))
        .pipe($.size({ title : 'fonts' }))
        .pipe(connect.reload());
});

gulp.task('scripts', function() {
  return gulp.src(webpackConfig.entry)
    .pipe($.webpack(webpackConfig))
    .pipe(isProduction ? $.uglify() : $.util.noop())
    .pipe(gulp.dest(dist + 'js/'))
    .pipe($.size({ title : 'js' }))
    .pipe(connect.reload());
});

gulp.task('render', function(){
    return gulp.src(webpackServerConfig.entry)
        .pipe($.webpack(webpackServerConfig))
        .pipe(isProduction ? $.uglify() : $.util.noop())
        .pipe(gulp.dest(dist))
        .pipe($.size({ title : 'js' }))
});

// copy html from app to dist
gulp.task('html', function() {
  return gulp.src(app + 'index.html')
    .pipe(gulp.dest(dist))
    .pipe($.size({ title : 'html' }))
    .pipe(connect.reload());
});

gulp.task('styles',function(cb) {
  var processors = [];
  // convert stylus to css

  processors.push(
      autoprefixer({
        browsers: autoprefixerBrowsers
      }),
      postcssEasings
  );

  return gulp.src(app + 'styles/main.scss')
    .pipe($.sass(
    //     {
    //   // only compress if we are in production
    //   compress: isProduction,
    //   // include 'normal' css into main.css
    //   'include css' : true
    // }

    ))
    .pipe(postcss(processors))
    // .pipe($.autoprefixer({browsers: autoprefixerBrowsers}))
    .pipe(gulp.dest(dist + 'css/'))
    .pipe($.size({ title : 'css' }))
    .pipe(connect.reload());

});


// add livereload on the given port
gulp.task('serve', function() {
  var option = {};
  console.log(process.env.NODE_ENV);
  connect.server({
    root: dist,
    port: port,
    livereload:{
      port:7777
    },
    middleware: function (connect, opt) {

      return [expressServer];

    }
  });
});

gulp.task('misc', function () {
  return gulp.src(app + 'misc/**/*.{ico,png,jpg}')
        .pipe(gulp.dest(dist))
        .pipe($.size({ title : 'misc' }));
});

gulp.task('flags', function () {
    return gulp.src(app + 'flags/**/*.{png,jpg,jpeg,gif,svg}')
        .pipe($.size({ title : 'flags' }))
        .pipe(gulp.dest(dist + 'flags/'));
});



// copy images
gulp.task('images', function(cb) {
  return gulp.src(app + 'images/**/*.{png,jpg,jpeg,gif,svg}')
    .pipe($.size({ title : 'images' }))
    .pipe(gulp.dest(dist + 'images/'));
});

// watch styl, html and js file changes
gulp.task('watch', function() {
  gulp.watch(app + 'styles/**/*.scss', ['styles']);
  gulp.watch(app + 'index.html', ['html']);
  gulp.watch(app + 'scripts/**/*.js', ['scripts']);
  gulp.watch(app + 'scripts/**/*.jsx', ['scripts']);
});

//custom server task


// remove bundels
gulp.task('clean', function(cb) {
  return del([dist+'/*'], cb);
});


// by default build project and then watch files in order to trigger livereload
gulp.task('default', ['font', 'images', 'html','scripts', 'styles', 'misc', 'flags', 'watch']);

gulp.task('init', ['bower']);
gulp.task('server', ['serve']);
gulp.task('local', ['local']);

// waits until clean is finished then builds the project
gulp.task('build', function(){
  gulp.start(['font', 'images', 'html','scripts', 'styles', 'misc', 'flags']);
});
