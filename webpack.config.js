var webpack = require('webpack');
var WebpackStrip = require('webpack-strip');
var path = require("path");


function isExternal(module) {
  var userRequest = module.userRequest;

  if (typeof userRequest !== 'string') {
    return false;
  }

  return userRequest.indexOf('bower_components') >= 0 ||
      userRequest.indexOf('node_modules') >= 0 ||
      userRequest.indexOf('libraries') >= 0;
}

module.exports.getConfig = function(type) {

  var isDev = type === 'development';
  process.env.NODE_ENV = 'production';

  var config = {
    cache: true,
     entry: ['./app/scripts/main.js'],
     //  entry: ['babel-polyfill','./app/scripts/main.js'],
      output: {
      path: __dirname,
      filename: 'main.js'
    },
    debug : false,
    plugins: [
      new webpack.DllReferencePlugin({
        context: path.join(__dirname, "dist"),
        manifest: require("./dll/vendor-manifest.json")
      }),
      new webpack.optimize.UglifyJsPlugin({
          compress: { warnings: false }
        }),
        new webpack.EnvironmentPlugin(['NODE_ENV']),
    ],
    module: {
      loaders: [{
        test: /\.jsx?$/,
        exclude: /node_modules/,
        cacheDirectory: true,
        include: [
          path.join(__dirname, "app") //important for performance!
        ],
        loader: 'babel',
        query: {
            // presets: ['react', 'es2015', 'stage-3']
            presets: ['stage-3', 'react', 'es2015','react-optimize']
        },
      },
        {
          test: /\.json$/,
          loader: 'json',
          exclude:/node_modules/
        },
        { test: /\.jsx$/, loader: WebpackStrip.loader('console.log', 'console.error', 'console.warn') },
        { test: /\.js$/, loader: WebpackStrip.loader('console.log', 'console.error', 'console.warn') }
      ],

    }
  };
  config.devtool = false;

  return config;
};