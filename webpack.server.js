var webpack = require('webpack');
var WebpackStrip = require('webpack-strip');
var path = require("path");
var fs = require('fs');
var nodeModules = {};


fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });




function isExternal(module) {
    var userRequest = module.userRequest;

    if (typeof userRequest !== 'string') {
        return false;
    }

    return userRequest.indexOf('bower_components') >= 0 ||
        userRequest.indexOf('node_modules') >= 0 ||
        userRequest.indexOf('libraries') >= 0;
}

module.exports.getConfigServer = function(type) {

    var isDev = type === 'development';
    process.env.NODE_ENV = 'production';

    var config = {
        cache: true,
        entry: ['./app/server.js'],
        output: {
            path: __dirname,
            filename: 'server.js'
        },
        externals: nodeModules,
        debug : false,
        module: {
            loaders: [{
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015'],
                    // presets: ['react', 'es2015','react-optimize', 'stage-0']
                },
            },

                {
                    test: /\.json$/,
                    loader: 'json',
                    exclude:/node_modules/
                },
                // { test: /\.jsx$/, loader: WebpackStrip.loader('console.log', 'console.error') },
                // { test: /\.js$/, loader: WebpackStrip.loader('console.log', 'console.error') }
            ],

        }
    };
    config.devtool = 'cheap-source-map'

    return config;
};