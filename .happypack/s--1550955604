'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RatexStats = function (_React$Component) {
    _inherits(RatexStats, _React$Component);

    function RatexStats(props) {
        _classCallCheck(this, RatexStats);

        var _this = _possibleConstructorReturn(this, (RatexStats.__proto__ || Object.getPrototypeOf(RatexStats)).call(this, props));

        _this.tooltipToggle = _this.tooltipToggle.bind(_this);
        _this.onActiveChange = _this.onActiveChange.bind(_this);
        _this.props = props;
        _this.state = {
            showActive: false,
            showTooltip: false
        };
        return _this;
    }

    _createClass(RatexStats, [{
        key: 'tooltipToggle',
        value: function tooltipToggle() {

            this.setState({
                showTooltip: !this.state.showTooltip
            });
        }
    }, {
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(nextProps, nextState) {
            var localeIsEqual = _underscore2.default.isEqual(this.props.locale.language.language, nextProps.locale.language.language);
            var dataIsEqual = _underscore2.default.isEqual(this.props.dashInitialState.data, nextProps.dashInitialState.data);
            var stateIsEqual = _underscore2.default.isEqual(this.state, nextState);
            if (!localeIsEqual) {
                return true;
            }
            if (!dataIsEqual) {
                return true;
            }
            if (!stateIsEqual) {
                return true;
            }

            //if nothing changed for this component - not re-rendering it
            return false;
        }
    }, {
        key: 'onActiveChange',
        value: function onActiveChange() {

            this.props.changeLimit(this.state.dropdownPlaceholder.value, !this.state.showActive);

            this.setState({
                showActive: !this.state.showActive
            });
        }
    }, {
        key: 'componentWillMount',
        value: function componentWillMount() {
            //this.props.loadRatexStats();
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            console.log('RATEX STATS TOTTALY UPDATED');
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var locale = this.props.locale.language;
            var iconStyle = {
                opacity: this.state.showTooltip ? 1 : 0,
                visibility: this.state.showTooltip ? 'visible' : 'hidden'
            };
            var tooltipStyle = {
                display: this.state.showTooltip ? 'block' : 'none'
            };
            var tooltip = _react2.default.createElement(
                'div',
                { className: 'tooltip', style: tooltipStyle },
                _react2.default.createElement('div', { className: 'tooltip-arrow' }),
                _react2.default.createElement(
                    'div',
                    { className: 'tooltip-inner' },
                    _react2.default.createElement(
                        'div',
                        { className: 'tooltip-title' },
                        '\u0423\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u0435 \u0432\u0438\u0434\u0436\u0435\u0442\u043E\u043C'
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'delete-widget', onClick: function onClick() {
                            _this2.props.deleteWidget(_this2.props.dataId);
                        } },
                    _react2.default.createElement(
                        'a',
                        { href: '#' },
                        '\u0443\u0434\u0430\u043B\u0438\u0442\u044C'
                    )
                )
            );

            if (this.props.dashInitialState.data != null) {
                var stats = this.props.dashInitialState.data.ratexStats.statistics;
                var totalCurrencies = stats.total_currencies;
                var totalCurrencyPairs = stats.total_currency_pairs;
                var totalExchangers = stats.total_exchangers;
                var statsRender = _react2.default.createElement(
                    'div',
                    { className: 'stats-wrap' },
                    _react2.default.createElement(
                        'div',
                        { className: 'left-part' },
                        _react2.default.createElement(
                            'ul',
                            null,
                            _react2.default.createElement(
                                'li',
                                null,
                                locale.t('_dash', { returnObjects: true }).ratex_stats.total_currencies
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                locale.t('_dash', { returnObjects: true }).ratex_stats.total_exchangers
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                locale.t('_dash', { returnObjects: true }).ratex_stats.total_directions
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'right-part' },
                        _react2.default.createElement(
                            'ul',
                            null,
                            _react2.default.createElement(
                                'li',
                                null,
                                totalCurrencies
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                totalExchangers
                            ),
                            _react2.default.createElement(
                                'li',
                                null,
                                totalCurrencyPairs
                            )
                        )
                    )
                );
                return _react2.default.createElement(
                    'div',
                    { 'data-item-id': this.props.dataId, className: 'card no-min-height' },
                    _react2.default.createElement(
                        'div',
                        { className: 'card-header' },
                        _react2.default.createElement(
                            'div',
                            { className: 'card-title' },
                            locale.t('_dash', { returnObjects: true }).ratex_stats.title,
                            '                            ',
                            _react2.default.createElement(
                                'div',
                                { className: 'setting-wrap', onClick: this.tooltipToggle },
                                _react2.default.createElement('span', { style: iconStyle, className: 'icon ru-settings' }),
                                tooltip
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'card-content no-min-height' },
                        statsRender
                    )
                );
            } else {
                return _react2.default.createElement(
                    'div',
                    { 'data-item-id': this.props.dataId, className: 'card no-min-height', style: { height: '200px!important' } },
                    _react2.default.createElement(
                        'div',
                        { className: 'card-header' },
                        _react2.default.createElement(
                            'div',
                            { className: 'card-title' },
                            _react2.default.createElement(
                                'div',
                                { className: 'setting-wrap', onClick: this.tooltipToggle },
                                _react2.default.createElement('span', { style: iconStyle, className: 'icon ru-settings' }),
                                tooltip
                            )
                        )
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'card-content no-min-height' },
                        _react2.default.createElement(
                            'div',
                            { className: 'preloader-cont' },
                            _react2.default.createElement(
                                'div',
                                { className: 'preloader' },
                                _react2.default.createElement('div', { className: 'circ1' }),
                                _react2.default.createElement('div', { className: 'circ2' }),
                                _react2.default.createElement('div', { className: 'circ3' })
                            )
                        )
                    )
                );
            }
        }
    }]);

    return RatexStats;
}(_react2.default.Component);

exports.default = RatexStats;