#!/usr/bin/env bash

cp etc/docker/.dockerignore .

docker build -f etc/docker/Dockerfile.prod -t 'paymaxi/ratex-frontend' .
docker push 'paymaxi/ratex-frontend'