#!/usr/bin/env bash

sudo chown ${USER}:${USER} . -R


if [[ ${CIRCLE_TAG} != '' ]]; then
    IMAGE_NAME="paymaxi/ratex-frontend:${CIRCLE_TAG}"

    cp etc/docker/.dockerignore .
    docker build -f etc/docker/Dockerfile.prod -t ${IMAGE_NAME} .
    docker push ${IMAGE_NAME}

    # update stack file with appropriate versions
    BACKEND_VERSION=`grep -oP "ratex-backend:(.+)$" docker-compose.backend.yml | awk -F':' '{print $2}'`

    if [[ ${BACKEND_VERSION} != '' ]]; then
        sed -i "s/CURRENT_VERSION/${CIRCLE_TAG}/g" docker-stack.yml
        sed -i "s/VERSION/${BACKEND_VERSION}/g" docker-stack.yml
        # create stack if not exist
        if [[ `docker-cloud stack ls |grep -c ${STACK}` -ne 1 ]]; then
            docker-cloud stack create -f docker-stack.yml -n ${STACK}
            docker-cloud stack start ${STACK}
        else
            docker-cloud stack update -f docker-stack.yml ${STACK}
            docker-cloud stack redeploy ${STACK}
        fi
    else
        echo "Can not determine what backend version you want to use."
        exit 1
    fi
fi
