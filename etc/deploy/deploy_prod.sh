#!/usr/bin/env bash

sudo chown ${USER}:${USER} . -R

if [[ ${CIRCLE_TAG} != '' ]]; then
    IMAGE_NAME="paymaxi/ratex-frontend:${CIRCLE_TAG}"
    IMAGE_NAME_LATEST="paymaxi/ratex-frontend:latest"

    cp etc/docker/.dockerignore .
    docker build -f etc/docker/Dockerfile.prod -t ${IMAGE_NAME_LATEST} .
    docker push ${IMAGE_NAME}
    docker push ${IMAGE_NAME_LATEST}
fi
