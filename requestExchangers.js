var http = require('https');
var express = require('express');
var app = express();

var options = {
    exchangers: {
        host: 'www.ratex.io',
        path: '/v1/exchangers',
        "rejectUnauthorized": false,
        headers : {
            'Authorization' : 'Basic ' + 'NTUwZTg0MDAtZTI5Yi00MWQ0LWE3MTYtNDQ2NjU1NDQwMDAwOg=='
        }
    },
    providers: {
        host:'www.ratex.io',
        path: '/v1/providers',
        "rejectUnauthorized": false,
        headers : {
            'Authorization' : 'Basic ' + 'NTUwZTg0MDAtZTI5Yi00MWQ0LWE3MTYtNDQ2NjU1NDQwMDAwOg=='
        }
    }

};

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

function request(response, options){

    var req = http.get(options, function(res){
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        var bodyChunks = [];

        res.on('data', function(chunk) {
            // You can process streamed parts here...
            bodyChunks.push(chunk);
        }).on('end', function() {
            var body = Buffer.concat(bodyChunks);
            response.send(body);
        })
    });


    req.on('error', function(e) {
        console.log('ERROR: ' + e.message);
    });

}




//ROUTINGS

app.get('/exchangers', function(req, res){

     request(res, options.exchangers);

});

app.get('/providers', function(req, res){

    request(res, options.providers);

});

app.listen(9001, function(){

    console.log('listening on port 9001');

});