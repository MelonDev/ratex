var resourses = {
  "en": {
    "translation": {
      "informer": "Informer",
      "_dash": {
        "general": {
          "delete": "delete",
          "widget_management": "Widget management",
          "buy": "buy",
          "sell": "sell",
          "no_data": "No data provided at the moment!",
          "widget_error": "This widget is broken! Delete it or reload the page"
        },
        "fast_exchange": {
          "title": "Fast exchange",
          "best_rate_from": "Best rate from",
          "with_fee": "with fee",
          "other_options": "Other options",
          "exchange": "Exchange",
          "another_rates": "Another rates",
          "exchanger": "Exchanger",
          "rating": "rating",
          "fee": "fee",
          "top_directions": "Top directions",
          "favorite_directions": "Favorite directions"
        },
        "ratex_stats": {
          "title": "Ratex statistics",
          "total_currencies": "Total currencies",
          "total_exchangers": "Total exchangers",
          "total_directions": "Total directions"
        },
        "national_bank_rates": {
          "title": "National bank rates",
          "amount": "Amount"
        },
        "single_exchanger": {
          "reserve": "Reserve",
          "age": "Age",
          "country": "Country",
          "responses": "Responses"
        },
        "top_exchangers": {
          "top": "Top",
          "exchangers": "exchangers",
          "name": "name",
          "total_rates": "total rates"
        },
        "news": {
          "title": "Financial news"
        },
        "black_market": {
          "title": "UAH rate on market",
          "offers": "offers",
          "total_amount": "total",
        },
        "calculator_pro": {
          "title": "Calculator PRO",
          "nbu": "NBU",
          "offers": "offers",
          "date": "Date",
          "rate": "rate",
          "total_amount": "total",
          "in_ukrainian_banks": "In ukrainian banks",
          "in_international_payment_systems": "In international payment systems"
        },
        "national_bank_fluctuations": {
          "title": "USD average rate fluctuation last month"
        }
      },
      "_currencies": {
        "name": "Name",
        "code": "Code",
        "type": "Type",
        "category": "category",
        "national": "National",
        "technical": "Technical",
        "metal": "Metal",
        "others": "Other",
        "cash": "Cash",
        "online_bankings": "Online bankings",
        "money_transfers": "Money transfers",
        "crypto_currencies": "Crypto currencies",
        "electronic": "Electronic",
        "vouchers": "Vouchers",
        "cards": "Cards",
        "digital": "Digital"
      },
      "_rates": {
        "rate": "Rate",
        "cash": "Cash",
        "cards": "Cards",
        "currency": "Currency",
        "p2p": "Black Market",
        "visa_mastercard": "Visa/Mastercard",
        "nbu": "NBU",
        "pat_kb_privatbank_card": "Privat24 Card",
        "pat_kb_privatbank_bank": "Privat Bank",
        "buy_sell": "buy/sell"
      },
      "_menu": {
        "exchangers": "Exchangers",
        "rates": "Rates",
        "market": "Market",
        "for_developers": "For developers",
        "about": "About",
        "directory": "Directory",
        "partnership": "Partnership",
        "in_development": "In development"
      },
      "_footer": {
        "services": "Services",
        "rates": "Rates",
        "exchangers": "Exchangers",
        "market": "Market",
        "information": "Information",
        "about": "About",
        "directory": "Directory",
        "for_developers": "For developers",
        "partnership": "Partnership",
        "social": "Social"
      },
      "_tooltip": {
        "currency": "currency",
        "name": "name",
        "type": "type",
        "offers_total": "Offers total",
        "popularity": "Popularity",
        "average_rate_from": "Average rate from",
        "best_rate_from": "best_rate_from"
      },
    }
  },
  "ru": {
    "translation": {
      "informer": "Informer",
      "_dash": {
        "general": {
          "delete": "удалить",
          "widget_management": "Управление виджетом",
          "buy": "покупка",
          "sell": "продажа",
          "no_data": "На данный момент отсутсвуют данные",
          "widget_error": "Виджет поломан! Удалите его или перезагрузите страницу."
        },
        "fast_exchange": {
          "title": "Быстрый обмен",
          "best_rate_from": "Лучший курс от",
          "with_fee": "c комиссией",
          "other_options": "Другие варианты",
          "exchange": "Обменять",
          "another_rates": "Другие курсы",
          "exchanger": "Обменник",
          "rating": "рейтинг",
          "fee": "комиссия",
          "top_directions": "Топ направлений",
          "favorite_directions": "Избранные направления"
        },
        "ratex_stats": {
          "title": "Статистика RATEX",
          "total_currencies": "Валют",
          "total_exchangers": "Обменников",
          "total_directions": "Направлений"
        },
        "national_bank_rates": {
          "title": "Курсы нацбанка",
          "amount": "Сумма"
        },
        "single_exchanger": {
          "reserve": "Сумма резервов",
          "age": "Возраст",
          "country": "Страна",
          "responses": "Отзывы"
        },
        "top_exchangers": {
          "top": "Топ",
          "exchangers": "обменников",
          "name": "название",
          "total_rates": "курсов"
        },
        "news": {
          "title": "Новости финансов"
        },
        "black_market": {
          "title": "Курс UAH на бирже Украины на сегодня",
          "offers": "заявки",
          "total_amount": "сумма",
        },
        "calculator_pro": {
          "title": "Калькулятор ПРО",
          "nbu": "НБУ",
          "offers": "заявки",
          "date": "Дата",
          "rate": "курс",
          "total_amount": "сумма",
          "in_ukrainian_banks": "В банках Украины",
          "in_international_payment_systems": "В международных платежных системах"
        },
        "national_bank_fluctuations": {
          "title": "График флуктуаций среднего курса USD за месяц"
        }
      },
      "_currencies": {
        "name": "Название",
        "code": "Код",
        "type": "Тип",
        "category": "Категория",
        "national": "Национальная",
        "technical": "Техническая",
        "metal": "Метал",
        "others": "Другое",
        "cash": "Наличные",
        "online_bankings": "Онлайн банкинг",
        "money_transfers": "Денежный перевод",
        "crypto_currencies": "Крипто-валюты",
        "electronic": "Електронные",
        "vouchers": "Ваучеры",
        "cards": "Карточки",
        "digital": "Цифровые"
      },
      "_rates": {
        "rate": "Курс валюты",
        "cash": "Наличные",
        "cards": "Карточные",
        "currency": "Валюта",
        "p2p": "Черный рынок",
        "visa_mastercard": "Visa/Mastercard",
        "nbu": "НБУ",
        "pat_kb_privatbank_card": "Privat24 Card",
        "pat_kb_privatbank_bank": "Privat Bank",
        "buy_sell": "покупка/продажа"
      },
      "_menu": {
        "exchangers": "Обменники",
        "rates": "Курсы",
        "market": "Биржа",
        "for_developers": "Для разработчиков",
        "about": "О проекте",
        "directory": "Справочник",
        "partnership": "Партнерская программа",
        "in_development": "В разработке"
      },
      "_footer": {
        "services": "Services",
        "rates": "Rates",
        "exchangers": "Exchangers",
        "market": "Market",
        "information": "Information",
        "about": "About",
        "directory": "Directory",
        "for_developers": "For developers",
        "partnership": "Partnership",
        "social": "Social"
      },
      "_tooltip": {
        "currency": "currency",
        "name": "name",
        "type": "type",
        "offers_total": "Offers total",
        "popularity": "Popularity",
        "average_rate_from": "Average rate",
        "best_rate_from": "Best rate",
        "symbol": "symbol"
      },
    }
  },
  "uk": {
    "translation": {
      "informer": "Informer",
      "_dash": {
        "fast_exchange": {
          "title": "Швидкий обмін",
          "best_rate_from": "Кращий курс від",
          "with_fee": "з комісією",
          "other_options": "Інші варінти",
          "exchange": "Обміняти",
          "another_rates": "Інші курси",
          "exchanger": "Обмінник",
          "rating": "рейтинг",
          "fee": "комісія",
          "top_directions": "Топ напрямків",
          "favorite_directions": "Обрані напрямки"
        },
        "ratex_stats": {
          "title": "Статистика RATEX",
          "total_currencies": "Валют",
          "total_exchangers": "Обмінників",
          "total_directions": "Напрямків"
        },
        "national_bank_rates": {
          "title": "Курси нацбанку",
          "amount": "Сума"
        },
        "single_exchanger": {
          "reserve": "Сума резервів",
          "age": "Вік",
          "country": "Країна",
          "responses": "Відгуки"
        },
        "top_exchangers": {
          "name": "назва",
          "total_rates": "курсів"
        },
        "news": {
          "title": "Новині фінансів"
        },
        "national_bank_fluctuations": {
          "title": "Графік флуктуацій середнього курсу USD за місяць"
        }
      },
      "_menu": {
        "exchangers": "Exchangers",
        "rates": "Rates",
        "market": "Market",
        "for_developers": "For developers",
        "about": "About",
        "directory": "Directory",
        "partnership": "Partnership",
        "in_development": "In development",
      },
      "_footer": {
        "services": "Services",
        "rates": "Rates",
        "exchangers": "Exchangers",
        "market": "Market",
        "information": "Information",
        "about": "About",
        "directory": "Directory",
        "for_developers": "For developers",
        "partnership": "Partnership",
        "social": "Social"
      },
      "_tooltip": {
        "currency": "валюта",
        "name": "название",
        "type": "тип",
        "offers_total": "Всего предложений",
        "popularity": "Популярность",
        "average_rate_from": "Средний курс",
        "best_rate_from": "Лучший курс от",
        "symbol": "символ"

      },
    }
  }
};
export default resourses;