export default function buildJsonApiQuery(entity, query) {
    var pageNumber = query.pageNumber || 1;
    var url = 'ajax/' + entity + '?page[number]=' + pageNumber + '&page[size]=30';
    //sorting string building
    if (typeof query.sort != 'undefined' && query.sort != '') {
        if (query.sortDirection == 'up') {
            url = url + '&sort=' + query.sort
        } else {
            url = url + '&sort=-' + query.sort
        }
    }
    if (typeof query.search != 'undefined' && query.search != '') {
        url = url + '&search=' + query.search
    }
    //filtering string building
    if (typeof query['filter[]'] != 'undefined' && typeof query['filter[]'] != 'string') {
        var queries = {};
        var queriesObjects = [];
        for (var i = 0; i < query['filter[]'].length; i++) {
            if (typeof queries[query['filter[]'][i].split(':')[0]] == 'undefined') {
                queries[query['filter[]'][i].split(':')[0]] = query['filter[]'][i].split(':')[1]
            } else {
                queries[query['filter[]'][i].split(':')[0]] = queries[query['filter[]'][i].split(':')[0]] + ',' + query['filter[]'][i].split(':')[1]
            }

        }
        for (var key in queries) {
            url = url + '&filter[' + key + ']=' + queries[key]
        }

    }
    if (typeof query['filter[]'] != 'undefined' && typeof query['filter[]'] == 'string') {

        url = url + '&filter[' + query['filter[]'].split(':')[0] + ']=' + query['filter[]'].split(':')[1]

    }

    if (typeof query['multifilter[]'] != 'undefined' && typeof query['multifilter[]'] != 'string') {
        for (var j = 0; j < query['multifilter[]'].length; j++) {

                url = url + '&filter' + query['multifilter[]'][j].split(':')[0] +'='+ query['multifilter[]'][j].split(':')[1]

        }

    }
    return url;
}