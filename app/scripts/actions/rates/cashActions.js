import axios from "axios";
import store from "../../stores/store";
// var $ = jQuery;
import config from "../../config";
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
    }
});


function buildJsonApiQuery(entity, query) {
    var pageNumber = query.pageNumber || 1;
    var url = 'ajax/' + entity + '?';
    //sorting string building
    if (typeof query.sort != 'undefined' && query.sort != '') {
        if (query.sortDirection == 'up') {
            url = url + '&sort=' + query.sort
        } else {
            url = url + '&sort=-' + query.sort
        }
    }
    if (typeof query.search != 'undefined' && query.search != '') {
        url = url + '&search=' + query.search
    }
    //filtering string building
    if (typeof query['filter[]'] != 'undefined' && typeof query['filter[]'] != 'string') {
        var queries = {};
        var queriesObjects = [];
        for (var i = 0; i < query['filter[]'].length; i++) {
            if (typeof queries[query['filter[]'][i].split(':')[0]] == 'undefined') {
                queries[query['filter[]'][i].split(':')[0]] = query['filter[]'][i].split(':')[1]
            } else {
                queries[query['filter[]'][i].split(':')[0]] = queries[query['filter[]'][i].split(':')[0]] + ',' + query['filter[]'][i].split(':')[1]
            }

        }
        for (var key in queries) {
            url = url + '&filter[' + key + ']=' + queries[key]
        }

    }
    if (typeof query['filter[]'] != 'undefined' && typeof query['filter[]'] == 'string') {

        url = url + '&filter[' + query['filter[]'].split(':')[0] + ']=' + query['filter[]'].split(':')[1]

    }
         url = url + '&filter[base_currencies]=USD,EUR,UAH,RUB,GBP,PLN,CAD,AUD';

    if (typeof query['multifilter[]'] != 'undefined' && typeof query['multifilter[]'] != 'string') {
        for (var j = 0; j < query['multifilter[]'].length; j++) {

            url = url + '&filter' + query['multifilter[]'][j].split(':')[0] +'='+ query['multifilter[]'][j].split(':')[1]

        }

    }
    return url;
}

export function loadRatesCash(query) {
    let params = query || store.getState().rates.cash.meta;
    return dispatch => {
        dispatch(
            {
                type: 'LOAD_RATES_CASH_START',
            }
        );
        axio.get(config.api_url + buildJsonApiQuery('aggregate-rates', params))
            .then((data) => {
                dispatch(
                    {
                        type: 'LOAD_RATES_CASH_OK',
                        data: data.data,
                        meta: Object.assign({}, data.data.meta, query)
                    }
                )
            })
            .catch((err) => {
                dispatch(
                    {
                        type: 'LOAD_RATES_CASH_ERR',
                    }
                )
            });

    }
}


export function filterRatesCash(params) {

    return dispatch => {

        dispatch(
            {
                type: 'FILTER_RATES_CASH_CHANGE',
                meta: params
            }
        );

        dispatch(loadRatesCash())

    }

}










