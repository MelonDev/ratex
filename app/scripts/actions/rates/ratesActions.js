import axios from "axios";
import store from "../../stores/store";
import config from "../../config";
import {filterRatesCash} from "./cashActions";
import {filterRatesCards} from "./cardsActions";
import {filterRatesOfficial} from "./officialActions";
import _ from "lodash";
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
    }
});

function buildJsonApiQuery(entity, query) {
    var pageNumber = query.pageNumber || 1;
    var url = 'ajax/' + entity + '?';
    //sorting string building
    if (typeof query.sort != 'undefined' && query.sort != '') {
        if (query.sortDirection == 'up') {
            url = url + '&sort=' + query.sort
        } else {
            url = url + '&sort=-' + query.sort
        }
    }
    if (typeof query.search != 'undefined' && query.search != '') {
        url = url + '&search=' + query.search
    }
    //filtering string building
    if (typeof query['filter[]'] != 'undefined' && typeof query['filter[]'] != 'string') {
        var queries = {};
        var queriesObjects = [];
        for (var i = 0; i < query['filter[]'].length; i++) {
            if (typeof queries[query['filter[]'][i].split(':')[0]] == 'undefined') {
                queries[query['filter[]'][i].split(':')[0]] = query['filter[]'][i].split(':')[1]
            } else {
                queries[query['filter[]'][i].split(':')[0]] = queries[query['filter[]'][i].split(':')[0]] + ',' + query['filter[]'][i].split(':')[1]
            }

        }
        for (var key in queries) {
            url = url + '&filter[' + key + ']=' + queries[key]
        }

    }
    if (typeof query['filter[]'] != 'undefined' && typeof query['filter[]'] == 'string') {

        url = url + '&filter[' + query['filter[]'].split(':')[0] + ']=' + query['filter[]'].split(':')[1]

    }

    if (typeof query['multifilter[]'] != 'undefined' && typeof query['multifilter[]'] != 'string') {
        for (var j = 0; j < query['multifilter[]'].length; j++) {

            url = url + '&filter' + query['multifilter[]'][j].split(':')[0] + '=' + query['multifilter[]'][j].split(':')[1]

        }

    }
    return url;
}

export function loadRates(query) {

    let params = query || store.getState().rates.meta;

    return dispatch => {
        dispatch(
            {
                type: 'RATES_LOAD_START',
            }
        );
        axio.get(config.api_url + buildJsonApiQuery('aggregate-rates', params))
            .then((data) => {
                dispatch(
                    {
                        type: 'RATES_LOAD_OK',
                        data: data.data.data,
                        meta: Object.assign({}, data.data.meta, query)
                    }
                )
            })
            .catch((err) => {
                dispatch(
                    {
                        type: 'RATES_LOAD_ERR',
                    }
                )
            });

    }

}

export function filterRates(params) {

    return dispatch => {
        dispatch(
            {
                type: 'RATES_FILTER_CHANGE',
                meta: params
            }
        );

        dispatch(loadRates())

    }

}

export function searchRates(param) {

    return dispatch => {
        dispatch(
            {
                type: 'RATES_SEARCH_CHANGE',
                meta: param
            }
        );
        dispatch(loadRates())
    }

}

export function changeTab(tab){
    return {
        type:'RATES_TAB_CHANGE',
        data: tab
    }
}


export function changeQouteCurrency(currency) {

    var state = store.getState().rates;
    var activeTab = state.currentTab;
    var filtersCash = state.cash.meta['filter[]'];
    var filtersCards = state.cards.meta['filter[]'];
    var filtersOfficial = state.official.meta['filter[]'];
    var filters = {
        filtersCash,
        filtersCards,
        filtersOfficial
    };
    for (var tab in filters){
        filters[tab] = filters[tab].filter((elem, index) => {
            if (_.includes(elem, 'quote_currency')) {
                return false;
            }
            return true;
        });
        filters[tab].push('quote_currency:' + currency);
    }


    return dispatch => {
        dispatch({
            type: 'RATES_CURRENCY_CHANGE',
            data: currency
        });
            dispatch(filterRatesCash(filters.filtersCash));
            dispatch(filterRatesCards(filters.filtersCards));
            dispatch(filterRatesOfficial(filters.filtersOfficial));
    }
}


