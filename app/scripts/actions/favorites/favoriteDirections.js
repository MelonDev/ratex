export function addFavoriteDirection(id) {
  return dispatch => {
    dispatch({
      type: 'ADD_FAVORITE_DIRECTION',
      data: id
    });
  };
}
export function removeFavoriteDirection(id) {
  return {
    type: 'REMOVE_FAVORITE_DIRECTION',
    data: id
  };
}
