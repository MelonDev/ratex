export function addFavoriteCurrency(id) {
  return dispatch => {
    dispatch({
      type: 'ADD_FAVORITE_CURRENCY',
      data: id
    });
  };
}
export function removeFavoriteCurrency(id) {
  return dispatch => {
    dispatch({
      type: 'REMOVE_FAVORITE_CURRENCY',
      data: id
    });
  };
}
