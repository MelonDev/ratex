
export function addFavoriteExchanger(id) {
    return dispatch => {
        dispatch({
            type: 'ADD_FAVORITE_EXCHANGER',
            data: id
        });

    }
}
export function removeFavoriteExchanger(id) {
    return dispatch => {
        dispatch({
            type: 'REMOVE_FAVORITE_EXCHANGER',
            data: id
        });

    }
}