export function sortDirectionsTable(column) {
  let actionType;
  switch (column) {
    case 'name':
      actionType = 'TABLE_DIRECTION_NAME';
      break;
    case 'reserves':
      actionType = 'TABLE_DIRECTION_RESERVES';
      break;
    case 'commission':
      actionType = 'TABLE_DIRECTION_COMMISSION';
      break;
    case 'rating':
      actionType = 'TABLE_DIRECTION_RATING';

  }
  return {
    namespace: 'EXCHANGER',
    tab: 'RATES',
    type: actionType,
  }
}
