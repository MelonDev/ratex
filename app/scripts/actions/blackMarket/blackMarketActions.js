import axios from 'axios';
import store from '../../stores/store';
import config from '../../config';
import buildJsonApiQuery from '../helpers/buildJsonApiQuery';

const axio = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  }
});

/**
 * A function for loading black market according to query params
 * @param {Object} query - query that will be passed to JSONApi jquery builder func
 * @returns {function(*)}
 */
export function loadBlackMarket(query) {
  const params = query || store.getState().blackMarket.meta;
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_LOAD_START',
      }
    );
    axio.get(config.api_url + buildJsonApiQuery('p2p/offers', params))
    .then((data) => {
      dispatch(
        {
          type: 'BLACK_MARKET_LOAD_OK',
          data: data.data.data,
          meta: Object.assign({}, data.data.meta, query)
        }
      );
    })
    .catch(() => {
      dispatch(
        {
          type: 'BLACK_MARKET_LOAD_ERR',
        }
      );
    });
  };
}

/**
 * Transits black market page
 * @param {Number} pageNumber - page number to transit
 * @returns {function(*)}
 */
export function paginateBlackMarket(pageNumber) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_PAGINATE',
        meta: pageNumber
      }
    );
    dispatch(loadBlackMarket());
  };
}

/**
 * Fiters Black market
 * @param {Object} param - filter params
 * @returns {function(*)}
 */
export function sortBlackMarket(param) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_SORT_CHANGE',
        meta: param
      }
    );
    dispatch(loadBlackMarket());
  };
}

/**
 * @param {String} city - new city
 * @param {Object} filter - filter Obj
 * @param {Object} multifilter - multifilter Obj
 * @returns {function(*)}
 */
export function changeCity(city, filter, multifilter) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_CITY_CHANGE',
        data: {
          city,
          filter,
          multifilter
        }
      }
    );
    dispatch(loadBlackMarket());
  };
}

/**
 * @param {String} operation - buy or sell
 * @param {Object} filter - filters Object
 * @param {Object} multifilter - multifilter Object
 * @returns {function(*)}
 */
export function changeOperation(operation, filter, multifilter) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_OPERATION_CHANGE',
        data: {
          operation,
          filter,
          multifilter
        }

      }
    );
    dispatch(loadBlackMarket());
  };
}

/**
 * @param {Object} currency
 * @param {Object} filter
 * @param {Object} multifilter
 * @returns {function(*)}
 */
export function changeBaseCurrency(currency, filter, multifilter) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_CURRENCY_CHANGE',
        data: {
          currencySelected: currency,
          filter,
          multifilter
        }
      }
    );
    dispatch(loadBlackMarket());
  };
}

/**
 * @param {Object} params
 * @returns {function(*)}
 */
export function filterBlackMarket(params) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_FILTER_CHANGE',
        meta: params
      }
    );
    dispatch(loadBlackMarket());
  };
}

/**
 * @param {Object} params
 * @returns {function(*)}
 */
export function multifilterBlackMarket(params) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_FILTER_VALUE_CHANGE',
        meta: params
      }
    );
    dispatch(loadBlackMarket());
  };
}
/**
 * @param {String} param
 * @returns {function(*)}
 */
export function searchBlackMarket(param) {
  return (dispatch) => {
    dispatch(
      {
        type: 'BLACK_MARKET_SEARCH_CHANGE',
        meta: param
      }
    );
    dispatch(loadBlackMarket());
  };
}
