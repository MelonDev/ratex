import axios from 'axios';
import config from '../../config';

const axio = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  }
});

/**
 * @param {String} id - id of currency pair
 * @returns {function(*)}
 */
export function getCurrPairTooltip(id) {
  return (dispatch) => {
    axio.get(`${config.api_url}ajax/tooltip/currency_pair/${id}`)
    .then((data) => {
      dispatch({
        type: 'LOAD_CURRPAIRTOOLTIP_OK',
        data: data.data
      });
    })
    .catch((error) => {
      dispatch({
        type: 'LOAD_CURRPAIRTOOLTIP_ERR',
        data: error
      });
    });
  };
}

/**
 * @param {String} id
 * @returns {function(*)}
 */
export function getCurrencyTooltip(id) {
  return (dispatch) => {
    axio.get(`${config.api_url}ajax/tooltip/currency/${id}`)
    .then((data) => {
      dispatch({
        type: 'LOAD_CURRENCYTOOLTIP_OK',
        data: data.data
      });
    })
    .catch((error) => {
      dispatch({
        type: 'LOAD_CURRENCYTOOLTIP_ERR',
        data: error
      });
    });
  };
}

/**
 * @param {String} id - exchangerId
 * @param {String} widgetId - widget id
 * @returns {function(*)}
 */
export function getExchanger(id, widgetId) {
  const requestJSON = {
    widgets: [{
      alias: 'exchanger_card',
      exchanger: {
        id
      }
    }]
  };
  return (dispatch) => {
    dispatch({
      type: 'LOAD_EXCHANGER_START',
      data: null,
      widgetId
    });
    axios.post(`${config.api_url}ajax/dashboard'`, requestJSON)
    .then((data) => {
      dispatch({
        type: 'LOAD_EXCHANGER_OK',
        data: data.data.widgets[0].exchanger,
        widgetId
      });
    }).catch((error) => {
      console.error(error);
      dispatch({
        type: 'LOAD_EXCHANGER_ERR',
        widgetId
      });
    });
  };
}

/**
 * @param {String} id
 * @returns {function(*)}
 */
export function getExchangerTooltip(id) {
  return (dispatch) => {
    axio.get(`${config.api_url}ajax/tooltip/exchanger/${id}`)
    .then((data) => {
      dispatch({
        type: 'LOAD_EXCHANGERTOOLTIP_OK',
        data:data.data
      });
    })
    .catch((error) => {
      dispatch({
        type: 'LOAD_EXCHANGERTOOLTIP_ERR',
        data: error
      });
    });
  };
}

/**
 * @param {Number} limit
 * @param {String} id - widget id
 * @returns {function(*)}
 */
export function changeDirectionsLimit(limit, id) {
  const json = {
    widgets: [
      {
        alias: 'top_currency_pairs',
        id,
        limit,
      }
    ]
  };
  return (dispatch) => {
    dispatch({
      family: 'WIDGET_ACTION',
      widget: 'TOP_CURRENCY_PAIRS',
      type: 'CHANGE_LIMIT_START',
      alias: 'top_currency_pairs',
      id,
      data: {
        ajaxLoading: true
      }
    });
    axio.post(`${config.api_url}ajax/dashboard`, json)
    .then((data) => {
      dispatch({
        family: 'WIDGET_ACTION',
        widget: 'TOP_CURRENCY_PAIRS',
        type: 'CHANGE_LIMIT_OK',
        alias: 'top_currency_pairs',
        id,
        data: data.data
      });
      pckry.shiftLayout();
    })
    .catch((err) => {
      console.log(err);
    });
  };
}

/**
 * @param {Integer} val
 * @param {String} id
 * @returns {function(*)}
 */
export function changeNationalBankInputAmount(val, id) {
  // TODO need to add val to request json!
  const json = {
    widgets: [
      {
        alias: 'national_bank_rates',
        quote_currency: 'UAH',
        id,
      }
    ]
  };
  return (dispatch) => {
    dispatch({
      family: 'WIDGET_ACTION',
      widget: 'NATIONAL_BANK_RATES',
      type: 'CHANGE_BASE_AMOUNT_START',
      alias: 'national_bank_rates',
      id,
      data: {
        ajaxLoading: true
      }
    });
    axio.post(`${config.api_url}ajax/dashboard`, json)
    .then((data) => {
      dispatch({
        family: 'WIDGET_ACTION',
        widget: 'NATIONAL_BANK_RATES',
        type: 'CHANGE_BASE_AMOUNT_OK',
        alias: 'national_bank_rates',
        id,
        data: data.data
      });
    })
    .catch((err) => {
      console.log(err);
    });
  };
}

/**
 * @param {String} id
 * @returns {function(*)}
 */
export function getCalculatorPro(id) {
  const json = {
    widgets: [
      {
        alias: 'national_bank_rates',
        quote_currency: 'UAH',
        id,
      }
    ]
  };

  return (dispatch) => {
    dispatch({
      family: 'WIDGET_ACTION',
      widget: 'CALCULATOR_PRO',
      type: 'LOAD_CALCULATOR_PRO_START',
      alias: 'calculator_pro',
      id,
      data: {
        ajaxLoading: true
      }
    });
    axio.post(`${config.api_url}ajax/dashboard`, json)
    .then((data) => {
      dispatch({
        family: 'WIDGET_ACTION',
        widget: 'CALCULATOR_PRO',
        type: 'LOAD_CALCULATOR_PRO_OK',
        alias: 'calculator_pro',
        id,
        data: data.data
      });
    })
    .catch((err) => {
      console.log(err);
    });
  };
}

/**
 * @param {Number} val
 * @param {String} id
 * @returns {function(*)}
 */
export function getFluctuationsWidget(val, id) {
  const json = {
    widgets: [
      {
        alias: 'national_bank_rates',
        quote_currency: 'UAH',
        id,
      }
    ]
  };
  return (dispatch) => {
    dispatch({
      family: 'WIDGET_ACTION',
      widget: 'NATIONAL_BANK_RATES',
      type: 'CHANGE_BASE_AMOUNT_START',
      alias: 'national_bank_rates',
      id,
      data: {
        ajaxLoading: true
      }
    });
    axio.post(`${config.api_url}ajax/dashboard`, json)
    .then((data) => {
      dispatch({
        family: 'WIDGET_ACTION',
        widget: 'NATIONAL_BANK_RATES',
        type: 'CHANGE_BASE_AMOUNT_OK',
        alias: 'national_bank_rates',
        id,
        data: data.data
      });
    })
    .catch((err) => {
      console.log(err);
    });
  };
}


// export function changeFluctuationsPeriod(val, id) {
//
//   var data = {
//     "widgets": [
//       {
//         "alias": "national_bank_rates",
//         "quote_currency": "UAH",
//         "id": id,
//
//       }
//     ]
//   };
//
//   return dispatch => {
//
//     dispatch({
//       family: 'WIDGET_ACTION',
//       widget: "NATIONAL_BANK_RATES",
//       type: 'CHANGE_BASE_AMOUNT_START',
//       alias: 'national_bank_rates',
//       id: id,
//       data: {
//         ajaxLoading: true
//       }
//     });
//
//     axio.post(config.api_url + 'ajax/dashboard', data)
//     .then((data) => {
//       dispatch({
//         family: 'WIDGET_ACTION',
//         widget: "NATIONAL_BANK_RATES",
//         type: 'CHANGE_BASE_AMOUNT_OK',
//         alias: 'national_bank_rates',
//         id: id,
//         data: data.data
//       })
//     })
//     .catch((err) => {
//       console.log(err);
//     })
//   }
//
// }

/**
 * @returns {function(*)}
 */
export function getFavoriteExchangers() {
  const favoritesArr = [];
  if (typeof localStorage !== 'undefined') {
    const favoritesHash = JSON.parse(localStorage.getItem('favoriteExchangers'));
    for (let key in favoritesHash) {
      favoritesArr.push(key);
    }
  }
  const sendJSON = {
    "favorites": favoritesArr
  };
  return (dispatch) => {
    axio.post(`${config.api_url}ajax/dashboard/widget/favorites_exchangers`, sendJSON)
    .then((data) => {
      dispatch({
        type: 'FAVORITE_EXCHANGERS_WIDGET_OK',
        data: data.data
      });
    })
    .catch((data, err) => {
      dispatch({
        type: 'FAVORITE_EXCHANGERS_WIDGET_ERR',
        data: err
      });
    });
  };
}

/**
 * @returns {function(*)}
 */
export function getFavoriteDirections() {
  const favoritesArr = [];
  if (typeof localStorage !== 'undefined') {
    const favoritesHash = JSON.parse(localStorage.getItem('favoriteDirections'));
    for (let key in favoritesHash) {
      favoritesArr.push(key);
    }

  }
  const sendJSON = {
    "favorites": favoritesArr
  };
  return (dispatch) => {
    axio.post(`${config.api_url}ajax/dashboard/widget/favorites_exchange_directions`, sendJSON)
    .then((data) => {
      dispatch({
        type: 'FAVORITE_DIRECTIONS_WIDGET_OK',
        data: data.data
      });
    })
    .catch((data, err) => {
      dispatch({
        type: 'FAVORITE_DIRECTIONS_WIDGET_ERR',
        data: err
      });
    });
  };
}

/**
 * @param {Number} limit
 * @param {Bool} returnActive
 * @param  {String} id
 * @returns {function(*)}
 */
export function changeTopExchangersLimit(limit, returnActive, id) {
  const json = {
    widgets: [
      {
        alias: 'top_exchangers',
        id,
        limit,
        return_active: returnActive
      }
    ]
  };
  return (dispatch) => {
    dispatch({
      family: 'WIDGET_ACTION',
      widget: 'TOP_EXCHANGERS',
      type: 'CHANGE_LIMIT_START',
      alias: 'top_exchangers',
      id: id,
      data: {
        ajaxLoading: true
      }
    });

    axio.post(`${config.api_url}ajax/dashboard`, json)
    .then((data) => {
      dispatch({
        family: 'WIDGET_ACTION',
        widget: 'TOP_EXCHANGERS',
        type: 'CHANGE_LIMIT_OK',
        alias: 'top_exchangers',
        id,
        data: data.data
      });
    })
    .catch((err) => {
      console.log(err);
    });
  };
}



export function getNationalBankRates(amount) {


  if (arguments.length == 0) {

    return dispatch => {
      $.ajax({
        dataType: 'json',
        url: config.api_url + 'ajax/dashboard/widget/national_bank_rates',
        beforeSend: function(xhr) {
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        },
        type: 'GET',
        success: function(data) {
          dispatch({
            type: "NATIONAL_BANK_RATES_OK",
            data: data
          });

        },
        error: function(data, error) {
          dispatch({
            type: "NATIONAL_BANK_RATES_ERR",
            data: error
          })
        }
      });
    }

  }

}

export function openMenu() {
  return {
    type: "MENU_OPENED"
  }
}

export function addWidget(data) {

  return {
    type: "WIDGET_ADDED",
    data: data
  }

}


export function getCurrencyPairs() {
  return dispatch => {

    dispatch({
      type: 'CURRENCY_PAIRS_START'
    });

    axio.get(config.api_url + 'ajax/list/currency_pairs?limit=100000')
    .then((data) => {
      console.log(data);
      dispatch({
        type: "CURRENCY_PAIRS_OK",
        data: data.data
      })
    })
    .catch((err) => {
      console.error(err);
      dispatch({
        type: "CURRENCY_PAIRS_ERR",
        data: err
      })
    })
  }

}

export function getCurrenciesList() {

  return dispatch => {
    dispatch({
      type: 'CURRENCIES_LIST_REQUESTED',
      data: null,
      loading: true
    });

    $.ajax({
      type: 'GET',
      dataType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      url: config.api_url + 'ajax/list/currencies?type=digital',
      beforeSend: function(xhr) {
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      },
      success: function(data) {
        dispatch({
          type: "CURRENCIES_LIST_OK",
          data: data,
          loading: false
        })
      },
      error: function(data, error) {
        dispatch({
          type: "CURRENCIES_LIST_ERR",
          data: error,
          loading: false
        })
      }
    });
  }

}

export function getCurrencies() {
  return dispatch => {
    dispatch({
      type: 'CURRENCIES_REQUESTED',
      data: null,
      loading: true
    });

    $.ajax({
      type: 'GET',
      dataType: 'json',
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      url: '/currencies.json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      },
      success: function(data) {
        dispatch({
          type: "CURRENCIES_OK",
          data: data,
          loading: false
        })
      },
      error: function(data, error) {
        dispatch({
          type: "CURRENCIES_ERR",
          data: error,
          loading: false
        })
      }
    });
  }
}


export function deleteWidget(id) {

  return {
    type: "WIDGET_DELETED",
    data: id
  }

}

export function getNews() {

  return dispatch => {
    //loading started event
    dispatch({
      type: 'LOAD_NEWS_START'
    });

    $.ajax({
      beforeSend: function(xhr) {
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      },
      type: 'GET',
      dataType: 'json',
      url: config.api_url + 'ajax/dashboard/widget/news',
      success: function(data) {
        dispatch({
          type: "LOAD_NEWS_OK",
          data: data
        })
      },
      error: function(data, error) {
        dispatch({
          type: "LOAD_NEWS_ERR",
          data: error
        })
      }
    });

  }

}


export function getWidgetList() {

  return dispatch => {

    axio.get(config.api_url + 'ajax/dashboard/widgets')
    .then((data) => {
      dispatch({
        type: "LOAD_WIDGET_LIST_OK",
        data: data.data
      })
    })
    .catch((data, err) => {
      dispatch({
        type: "LOAD_WIDGET_LIST_ERR",
        data: err
      })
    })

  }


  // return dispatch => {
  //     //loading started event
  //     dispatch({
  //         type: 'LOAD_WIDGET_LIST_START'
  //     });
  //
  //     $.ajax({
  //         type: 'GET',
  //         dataType: 'json',
  //         url: config.api_url + 'ajax/dashboard/widgets',
  //         beforeSend: function (xhr) {
  //             xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
  //         },
  //         success: function (data) {
  //             dispatch({
  //                 type: "LOAD_WIDGET_LIST_OK",
  //                 data: data
  //             })
  //         },
  //         error: function (data, error) {
  //             dispatch({
  //                 type: "LOAD_WIDGET_LIST_ERR",
  //                 data: error
  //             })
  //         }
  //     });
  // }

}

export function loadCurencyTooltip(id) {

  return dispatch => {
    dispatch({
      type: "LOAD_CURRENCYTOOLTIP_START",
      data: null
    });
    $.ajax({
      type: 'GET',
      dataType: 'json',
      url: config.api_url + 'ajax/tooltip/currency/' + id,
      beforeSend: function(xhr) {
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      },
      success: function(data) {

        dispatch({
          type: "LOAD_CURRENCYTOOLTIP_OK",
          data: data
        })

      },
      error: function(data, error) {

        dispatch({
          type: "LOAD_CURRENCYTOOLTIP_ERR",
          data: error
        })

      }
    });
  }

}

export function openWidgetMenu() {
  return {
    type: "ADD_WIDGET_MENU_OPENED"
  }
}

export function hideMenu() {
  return {
    type: "MENU_HIDDEN"
  }
}
export function hideWidgetMenu() {
  return {
    type: "ADD_WIDGET_MENU_HIDDEN"
  }
}


export function loadExchangers(paginationPos) {
  let domainUrl = config.api_url + 'ajax/',
    currentUrl = 'digital/exchangers?page[number]=1&page[size]=100000',
    url = domainUrl + currentUrl;
  return dispatch => {
    dispatch({
      type: 'LOAD_EXCHANGERS_REQUESTED'
    });
    if (paginationPos) {
      url = domainUrl + paginationPos;
    }
    $.ajax({
      type: 'GET',
      url: url,
      success: function(data) {
        dispatch({
          type: "LOAD_EXCHANGERS_OK",
          data: data
        })
      },
      error: function(data, error) {
        dispatch({
          type: "LOAD_EXCHANGERS_ERR",
          data: error
        })
      }
    });
  }
}


export function loadDashExchangers() {


  return dispatch => {

    dispatch({
      type: "LOAD_DASH_EXCHANGERS_START",
      data: null
    });

    axio.get('https://ratex.io/ajax/list/exchangers')
    .then((data) => {
      dispatch({
        type: "LOAD_DASH_EXCHANGERS_OK",
        data: data.data
      })
    })
    .catch((data, err) => {
      dispatch({
        type: "LOAD_DASH_EXCHANGERS_ERR",
        data: err
      })
    });

  }


  //     return dispatch => {
  //     dispatch({
  //         type: "LOAD_DASH_EXCHANGERS_START",
  //         data: null
  //     });
  //     $.ajax({
  //         type: 'GET',
  //         dataType: 'json',
  //         url: config.api_url + 'ajax/list/exchangers',
  //         beforeSend: function (xhr) {
  //             xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
  //         },
  //         success: function (data) {
  //
  //             dispatch({
  //                 type: "LOAD_DASH_EXCHANGERS_OK",
  //                 data: data
  //             })
  //
  //         },
  //         error: function (data, error) {
  //             dispatch({
  //                 type: "LOAD_DASH_EXCHANGERS_ERR",
  //                 data: error
  //             })
  //         }
  //     });
  // }


}


export function loadRatexStats() {

  return dispatch => {
    dispatch({
      type: "LOAD_RATEX_STATS_START",
      data: null
    });
    $.ajax({
      type: 'GET',
      dataType: 'json',
      url: config.api_url + 'ajax/dashboard/widget/ratex_stats',
      beforeSend: function(xhr) {
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      },
      success: function(data) {
        dispatch({
          type: "LOAD_RATEX_STATS_OK",
          data: data
        })
      },
      error: function(data, error) {
        dispatch({
          type: "LOAD_RATEX_STATS_ERR",
          data: error
        })
      }
    });
  }

}

export function changeLanguage(langFunction) {

  return dispatch => {
    dispatch({
      type: "CHANGE_LANGUAGE",
      payload: {
        language: langFunction
      }
    });
  }

}

export function loadFastExchange(currencyFrom, currencyTo, countFrom, countTo) {

  if (arguments.length > 0) {
    var requestJson;
    if (countFrom == null) {
      requestJson = {
        "count_to": countTo,
        "selected_direction": {
          "base_currency": { "id": currencyFrom },
          "quote_currency": { "id": currencyTo }
        }
      }
    }
    if (countTo == null) {
      requestJson = {
        "count_from": countFrom,
        "selected_direction": {
          "base_currency": { "id": currencyFrom },
          "quote_currency": { "id": currencyTo }
        }
      }
    }


    return dispatch => {
      dispatch({
        type: "LOAD_FAST_EXCHANGE_START",
        data: null
      });
      $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'applications/json',
        data: JSON.stringify(requestJson),
        url: config.api_url + 'ajax/dashboard/widget/fast_exchange',
        beforeSend: function(xhr) {
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        },
        success: function(data) {
          dispatch({
            type: "DIRECTION_DATA_CHANGE",
            data: data
          })
        },
        error: function(data, error) {
          dispatch({
            type: "DIRECTION_DATA_ERR",
            data: error
          })
        }
      });
    }
  } else {

    return dispatch => {
      dispatch({
        type: "LOAD_FAST_EXCHANGE_START",
        data: null
      });
      $.ajax({
        type: 'GET',
        dataType: 'json',
        url: config.api_url + 'ajax/dashboard/widget/fast_exchange',
        beforeSend: function(xhr) {
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        },
        success: function(data) {
          dispatch({
            type: "DIRECTION_DATA_CHANGE",
            data: data
          })
        },
        error: function(data, error) {
          dispatch({
            type: "DIRECTION_DATA_ERR",
            data: error
          })
        }
      });
    }

  }

}


export function loadExchanger(id) {

  return dispatch => {

    dispatch({
      type: 'LOAD_SINGLE_EXCHANGER_START'
    });

    axio.get(config.api_url + 'ajax/digital/exchangers/' + id)
    .then((data) => {
      var exchanger = data.data;
      dispatch({
        type: 'LOAD_SINGLE_EXCHANGER_OK',
        data: exchanger
      })
    })
    .catch((err) => {
      dispatch({
        type: 'LOAD_SINGLE_EXHCNAGER_ERR'
      });
      console.log(err);
    });

  }

}

export function loadExchangerReserves(id) {


  return dispatch => {

    dispatch({
      type: 'LOAD_SINGLE_EXCHANGER_RESERVES_START'
    });

    axio.get(config.api_url + 'ajax/digital/exchangers/' + id + '/live_reserves')
    .then((data) => {
      var exchanger = data.data;
      dispatch({
        type: 'LOAD_SINGLE_EXCHANGER_RESERVES_OK',
        data: exchanger.data
      })
    })
    .catch((err) => {
      dispatch({
        type: 'LOAD_SINGLE_EXCHANGER_RESERVES_ERR'
      });
      console.log(err);
    });

  }

}


export function loadExchangerRates(id) {

  return dispatch => {

    dispatch({
      type: 'LOAD_SINGLE_EXCHANGER_RATES_START'
    });

    axio.get(config.api_url + 'ajax/digital/exchangers/' + id + '/live_rates')
    .then((data) => {
      var exchanger = data.data;
      dispatch({
        type: 'LOAD_SINGLE_EXCHANGER_RATES_OK',
        data: exchanger.data
      });
      setTimeout(() => {
        pckry.shiftLayout();
        $('.preloader-wrap').fadeOut();
      }, 1000);
    })
    .catch((err) => {
      dispatch({
        type: 'LOAD_SINGLE_EXCHANGER_RATES_ERR'
      });
      console.log(err);
    });

  }

}

//main dashboard ajax when INIT Action

export function loadDash() {

  return dispatch => {

    dispatch({
      type: 'LOAD_DASH_START'
    });

    axio.get(config.api_url + 'ajax/dashboard')
    .then((data) => {
      dispatch({
        type: "LOAD_DASH_OK",
        data: data.data
      });

    })
    .catch((data, err) => {
      console.log(err);
      dispatch({
        type: "LOAD_DASH_ERR",
        data: err
      })
    });

  }

  // return dispatch => {
  //     //loading started event
  //     dispatch({
  //         type: 'LOAD_DASH_START'
  //     });
  //
  //     $.ajax({
  //         type: 'GET',
  //         url: config.api_url + 'ajax/dashboard',
  //         beforeSend: function (xhr) {
  //             xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
  //         },
  //         success: function (data) {
  //             dispatch({
  //                 type: "LOAD_DASH_OK",
  //                 data: data
  //             })
  //         },
  //         error: function (data, error) {
  //             dispatch({
  //                 type: "LOAD_DASH_ERR",
  //                 data: error
  //             })
  //         }
  //     });
  // }

}


export function loadMarket() {
  return dispatch => {
    dispatch({
      type: 'LOAD_MARKET_REQUESTED'
    });
    $.ajax({
      type: 'GET',
      url: config.api_url + 'v1/providers',
      success: function(data) {
        dispatch({
          type: "LOAD_MARKET_OK",
          data: data.data
        })
      },
      error: function(data, error) {
        dispatch({
          type: "LOAD_MARKET_ERR",
          data: error
        })
      }
    });
  }
}

export function loadProviders() {
  return dispatch => {
    dispatch({
      type: 'LOAD_PROVIDERS_REQUESTED'
    });
    $.ajax({
      type: 'GET',
      beforeSend: function(xhr) {
        xhr.setRequestHeader("Authorization", "Basic " + "NTUwZTg0MDAtZTI5Yi00MWQ0LWE3MTYtNDQ2NjU1NDQwMDAwOg==");
      },
      url: config.api_url + 'v1/providers',
      success: function(data) {
        dispatch({
          type: "LOAD_PROVIDERS_OK",
          data: data.data
        })
      },
      error: function(data, error) {
        dispatch({
          type: "LOAD_PROVIDERS_ERR",
          data: error
        })
      }
    });
  }
}
