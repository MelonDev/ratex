import axios from "axios";
import config from "../../config";
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
    }
});

export function loadTooltip(base_currency, quote_currency) {


    return dispatch => {
        dispatch(
            {
                type: 'CHART_TOOLTIP_LOAD_START',
            }
        )
        axio.get(config.api_url + 'ajax/tooltip/national_bank_rates_fluctuations?base_currency='+base_currency+'&quote_currency='+quote_currency)
            .then((data) => {
                dispatch(
                    {
                        type: 'CHART_TOOLTIP_LOAD_OK',
                        data: data.data,
                        currency:base_currency

                    }
                )
            })
            .catch((err) => {
                dispatch(
                    {
                        type: 'ORGANIZATIONS_LOAD_ERR',
                    }
                )
            });

    }

}