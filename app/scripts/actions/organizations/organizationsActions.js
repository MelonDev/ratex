import axios from "axios";
import store from "../../stores/store";
// var $ = jQuery;
import config from "../../config";
import buildJsonApiQuery from '../helpers/buildJsonApiQuery';
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
    }
});







export function loadOrganizations(query) {

    let params = query || store.getState().organizations.meta;

    return dispatch => {
        dispatch(
            {
                type: 'ORGANIZATIONS_LOAD_START',
            }
        )
        axio.get(config.api_url + buildJsonApiQuery('organizations', params))
            .then((data) => {
                dispatch(
                    {
                        type: 'ORGANIZATIONS_LOAD_OK',
                        data: data.data.data,
                        meta: Object.assign({}, data.data.meta, query)
                    }
                )
            })
            .catch((err) => {
                dispatch(
                    {
                        type: 'ORGANIZATIONS_LOAD_ERR',
                    }
                )
            });

    }

}

export function paginateOrganizations(pageNumber) {

    return dispatch => {
        dispatch(
            {
                type: 'ORGANIZATIONS_PAGINATE',
                meta: pageNumber
            }
        )

        dispatch(loadOrganizations())

    }

}

export function sortOrganizations(param) {


    return dispatch => {
        dispatch(
            {
                type: 'ORGANIZATIONS_SORT_CHANGE',
                meta: param
            }
        )

        dispatch(loadOrganizations())


    }


}

export function filterOrganizations(params) {

    return dispatch => {
        dispatch(
            {
                type: 'ORGANIZATIONS_FILTER_CHANGE',
                meta: params
            }
        )

        dispatch(loadOrganizations())

    }

}


export function searchOrganizations(param) {

    return dispatch => {
        dispatch(
            {
                type: 'ORGANIZATIONS_SEARCH_CHANGE',
                meta: param
            }
        )

        dispatch(loadOrganizations())

    }

}





