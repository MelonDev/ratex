import axios from 'axios';
import store from '../../stores/store';
import config from '../../config';
import buildJsonApiQuery from '../helpers/buildJsonApiQuery';

const axio = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  }
});

/**
 * @param {Object} query
 * @returns {function(*)}
 */
export function loadCurrencies(query) {
  const params = query || store.getState().currencies.meta;
  return (dispatch) => {
    dispatch(
      {
        type: 'CURRENCIES_LOAD_START',
      }
    );
    axio.get(config.api_url + buildJsonApiQuery('currencies', params))
    .then((data) => {
      dispatch(
        {
          type: 'CURRENCIES_LOAD_OK',
          data: data.data.data,
          meta: Object.assign({}, data.data.meta, query)
        }
      );
    })
    .catch(() => {
      dispatch(
        {
          type: 'CURRENCIES_LOAD_ERR',
        }
      );
    });
  };
}

/**
 * @param {String} pageNumber
 * @returns {function(*)}
 */
export function paginateCurrencies(pageNumber) {
  return (dispatch) => {
    dispatch(
      {
        type: 'CURRENCIES_PAGINATE',
        meta: pageNumber
      }
    );
    dispatch(loadCurrencies());
  };
}

/**
 * @param {Object} param
 * @returns {function(*)}
 */
export function sortCurrencies(param) {
  return (dispatch) => {
    dispatch(
      {
        type: 'CURRENCIES_SORT_CHANGE',
        meta: param
      }
    );
    dispatch(loadCurrencies());
  };
}

/**
 * @param {Object} params
 * @returns {function(*)}
 */
export function filterCurrencies(params) {
  return (dispatch) => {
    dispatch(
      {
        type: 'CURRENCIES_FILTER_CHANGE',
        meta: params
      }
    );
    dispatch(loadCurrencies());
  };
}

/**
 * @param {String} param
 * @returns {function(*)}
 */
export function searchCurrencies(param) {
  return (dispatch) => {
    dispatch(
      {
        type: 'CURRENCIES_SEARCH_CHANGE',
        meta: param
      }
    );
    dispatch(loadCurrencies());
  };
}
