import axios from "axios";
import store from "../../stores/store";
// var $ = jQuery;
import config from "../../config";
import buildJsonApiQuery from '../helpers/buildJsonApiQuery';
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
    }
});







export function loadCurrencies(query) {

    let params = query || store.getState().currencies.meta;

    return dispatch => {
        dispatch(
            {
                type: 'CURRENCIES_LOAD_START',
            }
        )
        axio.get(config.api_url + buildJsonApiQuery('currencies', params))
            .then((data) => {
                dispatch(
                    {
                        type: 'CURRENCIES_LOAD_OK',
                        data: data.data.data,
                        meta: Object.assign({}, data.data.meta, query)
                    }
                )
            })
            .catch((err) => {
                dispatch(
                    {
                        type: 'CURRENCIES_LOAD_ERR',
                    }
                )
            });

    }

}

export function paginateCurrencies(pageNumber) {

    return dispatch => {
        dispatch(
            {
                type: 'CURRENCIES_PAGINATE',
                meta: pageNumber
            }
        )

        dispatch(loadCurrencies())

    }

}

export function sortCurrencies(param) {


    return dispatch => {
        dispatch(
            {
                type: 'CURRENCIES_SORT_CHANGE',
                meta: param
            }
        )

        dispatch(loadCurrencies())


    }


}

export function filterCurrencies(params) {

    return dispatch => {
        dispatch(
            {
                type: 'CURRENCIES_FILTER_CHANGE',
                meta: params
            }
        )

        dispatch(loadCurrencies())

    }

}


export function searchCurrencies(param) {

    return dispatch => {
        dispatch(
            {
                type: 'CURRENCIES_SEARCH_CHANGE',
                meta: param
            }
        )

        dispatch(loadCurrencies())

    }

}





