/**
 * Created by alexdankov on 07.02.17.
 */
import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});


export function getDataForVisaRatesWidget(id){

    var json = {
        "widgets": [
            {
                "alias": "visa_rates",
                "id": id,
                "quote_currency": "UAH",
                "base_currencies": [
                    "EUR",
                    "USD",
                    "RUB"
                ]
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "VISA_RATES",
            type: 'DATA_START',
            alias: 'visa_rates',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "VISA_RATES",
                    type: 'DATA_OK',
                    alias: 'visa_rates',
                    id: id,
                    data: data.data
                });
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}