import axios from 'axios';
import config from '../../config';

export function getFavoriteExchangersWidget(id, favorites) {
  const req = {
    widgets: [
      {
        alias: "favorite_exchangers",
        id: id,
        favorites: [...favorites],
      }
    ]
  };
  return dispatch => {
    axios.post(`${config.api_url}/ajax/dashboard`, req).then((response) => {
      dispatch({
        family: 'WIDGET_ACTION',
        widget: "FAVORITE_EXCHANGERS",
        type: 'DATA_OK',
        alias: 'favorite_exchangers',
        id: id,
        data: response.data
      });
    }).catch((e) => {
      console.log(e);
    });
  }
}