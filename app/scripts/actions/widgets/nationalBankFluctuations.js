import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});





export function getDataForNationalBankFluctuationsWidget(id){
    var json = {
        "widgets": [
            {
                "alias": "national_bank_fluctuations",
                "id": id,
            }
        ]
    };
    return dispatch => {
        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "NATIONAL_BANK_FLUCTUATIONS",
                    type: 'DATA_OK',
                    alias: 'national_bank_fluctuations',
                    id: id,
                    data: data.data
                });
            })
            .catch((err) => {
                console.error(err);
            })
    }
}