
import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});


export function getTopDirections(id){

    var json = {
        "widgets": [
            {
                "alias": "top_currency_pairs",
                "id": id,
                "limit": 5
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "TOP_CURRENCY_PAIRS",
            type: 'LOAD_TOP_DIRECTIONS_START',
            alias: 'top_currency_pairs',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "TOP_CURRENCY_PAIRS",
                    type: 'LOAD_TOP_DIRECTIONS_OK',
                    alias: 'top_currency_pairs',
                    id: id,
                    data: data.data
                });
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}