import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});



export function blackMarketOverviewChangeDate(id) {

    var data = {
        "widgets": [
            {
                "alias": "black_market_rates",
                "id": id,
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "BLACK_MARKET_OVERVIEW",
            type: 'CHANGE_DATE',
            alias: 'black_market_rates',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', data)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "BLACK_MARKET_OVERVIEW",
                    type: 'CHANGE_DIRECTION_OK',
                    alias: 'fast_exchange',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

}






export function getDataForBlackMarketOverviewWidget(id){

    var json = {
        "widgets": [
            {
                "alias": "black_market_rates",
                "id": id,
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "BLACK_MARKET_OVERVIEW",
            type: 'DATA_START',
            alias: 'black_market_rates',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "BLACK_MARKET_OVERVIEW",
                    type: 'DATA_OK',
                    alias: 'black_market_rates',
                    id: id,
                    data: data.data
                });
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}
