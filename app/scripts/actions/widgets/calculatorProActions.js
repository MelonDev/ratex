import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});


export function calculatorProChangeDirection(value, id) {

    var data = {
        "widgets": [
            {
                "alias": "calculator_pro",
                "id": id,
                "amount": 1,
                "selected_direction": {
                    "base_code": value[0],
                    "quote_code": value[1]
                }
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "CALCULATOR_PRO",
            type: 'CHANGE_DIRECTION_START',
            alias: 'calculator_pro',
            id: id,
            data:value

        });

        axio.post(config.api_url + 'ajax/dashboard', data)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "CALCULATOR_PRO",
                    type: 'CHANGE_DIRECTION_OK',
                    alias: 'calculator_pro',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

}


export function calculatorProChangeDate(date, baseCurr, quoteCurr, id) {

    var data = {
        "widgets": [
            {
                "alias": "calculator_pro",
                "id": id,
                "amount": 1,
                "date": date,
                "selected_direction": {
                    "base_code": baseCurr,
                    "quote_code": quoteCurr
                }
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "CALCULATOR_PRO",
            type: 'CHANGE_DATE_START',
            alias: 'calculator_pro',
            id: id,
        });

        axio.post(config.api_url + 'ajax/dashboard', data)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "CALCULATOR_PRO",
                    type: 'CHANGE_DATE_OK',
                    alias: 'calculator_pro',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

}

export function countFromChangeCalculatorPro(id, baseCurr, quoteCurr, amount){



    var json = {
        "widgets": [
            {
                "alias": "calculator_pro",
                "id": id,
                "amount": amount,
                "selected_direction": {
                    "base_code":baseCurr,
                    "quote_code": quoteCurr
                },
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "CALCULATOR_PRO",
            type: 'CALCULATOR_PRO_LOADING_START',
            alias: 'calculator_pro',
            id: id,
            data: null,
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {

                console.warn(data);
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "CALCULATOR_PRO",
                    type: 'CALCULATOR_PRO_LOADING_OK',
                    alias: 'calculator_pro',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}





export function getDataForCalculatorPro(id){

    var json = {
        "widgets": [
            {
                "alias": "calculator_pro",
                "id": id,
                "amount": 1,
                "selected_direction": {
                    "base_code":"USD",
                    "quote_code": "UAH"
                },
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "CALCULATOR_PRO",
            type: 'CALCULATOR_PRO_LOADING_START',
            alias: 'calculator_pro',
            id: id,
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {

                console.warn(data);
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "CALCULATOR_PRO",
                    type: 'CALCULATOR_PRO_LOADING_OK',
                    alias: 'calculator_pro',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}