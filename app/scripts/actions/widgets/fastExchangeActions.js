import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});


export function fastExchangeChangeDirection(value, id) {

    var data = {
        "widgets": [
            {
                "alias": "fast_exchange",
                "id": id,
                "count_from": 1,
                "limit": 5,
                "selected_direction": {
                    "base_code": value[0],
                    "quote_code": value[1]
                }
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "FAST_EXCHANGE",
            type: 'CHANGE_DIRECTION_START',
            alias: 'fast_exchange',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', data)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "FAST_EXCHANGE",
                    type: 'CHANGE_DIRECTION_OK',
                    alias: 'fast_exchange',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }

}

export function countFromChangeFastExchange(id, baseCurr, quoteCurr, amount){
    debugger;
    var json = {
        "widgets": [
            {
                "alias": "fast_exchange",
                "id": id,
                "in_amount": parseFloat(amount),
                "out_amount": 0,
                "selected_direction": {
                    "base_code":baseCurr,
                    "quote_code": quoteCurr
                },
                "limit": 5
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "FAST_EXCHANGE",
            type: 'FAST_EXCHANGE_LOADING_START',
            alias: 'fast_exchange',
            id: id,
            data: null,
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {

                console.warn(data);
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "FAST_EXCHANGE",
                    type: 'FAST_EXCHANGE_LOADING_OK',
                    alias: 'fast_exchange',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}



export function countToChangeFastExchange(id, baseCurr, quoteCurr, amount){
    debugger;

    var json = {
        "widgets": [
            {
                "alias": "fast_exchange",
                "id": id,
                "out_amount": amount,
                "selected_direction": {
                    "base_code":baseCurr,
                    "quote_code": quoteCurr
                },
                "limit": 5
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "FAST_EXCHANGE",
            type: 'FAST_EXCHANGE_LOADING_START',
            alias: 'fast_exchange',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {

                console.warn(data);
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "FAST_EXCHANGE",
                    type: 'FAST_EXCHANGE_LOADING_OK',
                    alias: 'fast_exchange',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}


export function getDataForFastExchangeWidget(id){



    var json = {
        "widgets": [
            {
                "alias": "fast_exchange",
                "id": id,
                "out_amount": 1,
                "limit": 5
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "FAST_EXCHANGE",
            type: 'FAST_EXCHANGE_LOADING_START',
            alias: 'fast_exchange',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {

                console.warn(data);
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "FAST_EXCHANGE",
                    type: 'FAST_EXCHANGE_LOADING_OK',
                    alias: 'fast_exchange',
                    id: id,
                    data: data.data
                })
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}