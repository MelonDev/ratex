import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});





export function getDataForInterantionalPaymentSystemRatesWidget(id){
    var json = {
        "widgets": [
            {
                "alias": "card_rates",
                "id": id,
            }
        ]
    };
    return dispatch => {
        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "INTERNATIONAL_PAYMENT_SYSTEMS",
                    type: 'DATA_OK',
                    alias: 'card_rates',
                    id: id,
                    data: data.data
                });
            })
            .catch((err) => {
                console.error(err);
            })
    }
}