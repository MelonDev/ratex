import config from "../../config";
import axios from "axios";
import constructRequestJSON from "../../reusable/constructRequestJSON";
// var $ = jQuery;
var axio = axios.create({
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type':'application/json'
    }
});


export function getDataForAverageVisaMastercardRates(id){

    var json = {
        "widgets": [
            {
                "alias": "average_visa_mastercard_rates",
                "id": id,
                "quote_currency": "UAH",
                "base_currencies": [
                    "EUR",
                    "USD",
                    "RUB"
                ]
            }
        ]
    };

    return dispatch => {

        dispatch({
            family: 'WIDGET_ACTION',
            widget: "AVERAGE_VISA_MASTERCARD_RATES",
            type: 'DATA_START',
            alias: 'average_visa_mastercard_rates',
            id: id,
            data: {
                ajaxLoading: true
            }
        });

        axio.post(config.api_url + 'ajax/dashboard', json)
            .then((data) => {
                dispatch({
                    family: 'WIDGET_ACTION',
                    widget: "AVERAGE_VISA_MASTERCARD_RATES",
                    type: 'DATA_OK',
                    alias: 'average_visa_mastercard_rates',
                    id: id,
                    data: data.data
                });
            })
            .catch((err) => {
                // console.log(data);
                console.log(err);
            })
    }


}