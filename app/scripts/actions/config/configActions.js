/**
 * @param {Object} data
 * @returns {{type: string, data: *}}
 */
export function changeConfig(data) {
  return {
    type: 'CONFIG_CHANGE',
    data
  };
}
/**
 * @returns {{type: string}}
 */
export function resetConfig() {
  return {
    type: 'CONFIG_RESET'
  };
}
