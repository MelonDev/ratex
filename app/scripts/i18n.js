import i18n from 'i18next';
import resourses from './translations/en';

var defaultLanguage =  'ru';

i18n.init({
    lng: defaultLanguage,
    resources: resourses
});

export default i18n;