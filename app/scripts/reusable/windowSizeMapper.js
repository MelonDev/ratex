export default function windowSizeMapper(windowWidth) {
  if (windowWidth < 660) {
    return 'smallest';
  }
  if (windowWidth >= 660 && windowWidth < 960) {
    return 'small';
  }
  if (windowWidth >= 960 && windowWidth < 1260) {
    return 'medium';
  }
  if (windowWidth >= 1260 && windowWidth < 1560) {
    return 'big';
  }
  if (windowWidth > 1560) {
    return 'biggest';
  }
  return 'big'
}