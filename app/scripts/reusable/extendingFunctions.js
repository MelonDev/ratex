import jQuery from 'jquery';
var $ = jQuery;
import config from '../config';
export function numberWithSpaces(x) {

    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ").replace('.', ',');

}

export function addZeroes( num ) {
    var value = Number(num);
    num = num.toString();
    var res = num.split(".");
    if(num.indexOf('.') === -1) {
        value = value.toFixed(2);
        num = value.toString();
    } else if (res[1].length < 3) {
        value = value.toFixed(2);
        num = value.toString();
    }
    return num
}

export function getTooltipPlacement (elem) {

    var $elem = $(elem);
    var endingRight = ($(window).width() - ($elem.offset().left + $elem.outerWidth()));
    var endingLeft = $(elem).offset().left;

    if (endingRight < 0) {
       return 'left';
    }

    if (endingLeft < 0) {
        return 'right';
    }

    return 'right';

}

export function removeDrops() {

    $('.curr-pairs-select .selectize-wrap').addClass('hidden');
    $('.curr-pairs-select .selectize-wrap').removeClass('open');
    $('.curr-pairs-select .selectize-wrap .resizable-input').blur();
    $('.fast-exchange').remove('pop-up-active');
    $('.tooltip').removeClass('visible');
    $('.settings-wrap').removeClass('tooltip-visible');
    $('.dropdown').removeClass('visible');
    $('.dropdown-wrap').removeClass('open');
    $('.rates-table-filters-wrap').removeClass('active');


}

export function hideMenu() {

    $('.layout-header').removeClass('open');
    $('.layout-aside').removeClass('open');
    $('.welcome-guide-wrap').removeClass('visible');
    $('.settings-modal').removeClass('visible');
    $('body').css({
        overflow: 'visible'
    });

}

export function showWidgetPreloader(elem){

    $(elem).find('.setting-wrap').addClass('ajax-loading')

}

export function showSettingsModal(){

    $('.settings-modal').addClass('visible');

}

export function hideSettingsModal(){

    $('.settings-modal').removeClass('visible');

}

export function hideWidgetPreloader(elem){

    $(elem).find('.setting-wrap').removeClass('ajax-loading')

}

export function getCurrencieIconUrl(flag, currencie) {

    if(flag == null){

        return '/images/currency_placeholder.svg'

    }

    if(flag == 'p'){

        return config.api_url + 'img/currencies/' + currencie + '/icon_32.png'

    }

    if( flag == 's'){

        return config.api_url + 'img/currencies/' + currencie + '/icon.svg'

    }

}