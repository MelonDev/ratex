import jQuery from 'jquery';

var $ = jQuery;

export default function trackClick(id){
    $.ajax({
        url: config.api_url+'/ajax/track/product/'+id,
        type: 'GET'
    });

}