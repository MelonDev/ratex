import jQuery from 'jquery';
var $ = jQuery;


// Cleaning up after page mounting (UI).
// Arguments contains of elements to remove class open
// Can receive no arguments to simple hide header and menu


export default  function didMount (){
     $('.layout-aside').removeClass('open');
     $('.layout-header').removeClass('open');
       setTimeout(() => {
         $('.preloader-wrap').fadeOut();
       }, 300);
}
