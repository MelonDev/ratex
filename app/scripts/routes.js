import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import Application from './app.js';
import Dashboard from './pages/dashboard.jsx';
import NotFound from './pages/notFound.jsx';
import { Provider } from 'react-redux';
import store, { history } from './stores/store';
import Exchangers from './pages/exchangers.jsx';
import Exchanger from './pages/exchanger.jsx';
import Guilist from './pages/guilist.jsx';
import FlyAway from './pages/flyAway.jsx';
import Rates from './pages/ratex.jsx';
import AddExchanger from './pages/addExchanger.jsx';
import Currencies from './pages/currensies.jsx';
import About from './pages/about.jsx';
import BlackMarket from './pages/blackMarket.jsx';
import Organizations from './pages/organizations.jsx';

const routes = (
  <Provider store={store}>
      <Router history={history}>
          <Route path="/" component={Application} >
              <IndexRoute component={Dashboard} />
              <Route path="dashboard" component={Dashboard} />
              <Route path="exchangers" component={Exchangers} />
              <Route path="about" component={About} />
              <Route path="rates/:tab_alias" component={Rates} />
              <Route path="add_exchanger" component={AddExchanger} />
              <Route path="exchangers/:exchanger_alias" component={Exchanger} />
              <Route path="exchangers/:exchanger_alias/:tab" component={Exchanger} />
              <Route path="currencies" component={Currencies}>
                  <Route path="/currencies/:page_number" component={Currencies} />
              </Route>
              <Route path="organizations" component={Organizations}>
                  <Route path="/organizations/:page_number" component={Organizations} />
              </Route>
              <Route path="guilist" component={Guilist} />
              <Route path="black-market" component={BlackMarket} />
              <Route path="ajax/*" component={FlyAway} />
              <Route path="*" component={NotFound} />
          </Route>
      </Router>
  </Provider>
);

export default routes;