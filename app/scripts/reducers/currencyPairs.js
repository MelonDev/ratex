var defaultState = {
    data : null,
    loading : false
};

function currencyPairs ( state = defaultState, action ) {

    switch( action.type ) {

        case 'CURRENCY_PAIRS_START':
            return Object.assign( {}, state, { loading : true } );

        case 'CURRENCY_PAIRS_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'CURRENCY_PAIRS_ERR':
            return state;

        default:
            return state;

    }

}
export default currencyPairs;