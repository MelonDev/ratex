var defaultState = { open : false };

function addWidgetMenu ( state = defaultState , action ) {

    switch(action.type){

        case 'ADD_WIDGET_MENU_OPENED':

            return state = {
                open : true
            };

        case 'ADD_WIDGET_MENU_HIDDEN':

            return state = {
                open : false
            };

        default:

            return state

    }

}

export default addWidgetMenu;
