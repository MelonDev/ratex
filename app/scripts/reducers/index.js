import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import widgetsSet from "./widgetsSet";
import ajaxLoad from "./ajaxLoad";
import exchangersLoad from "./exchangersLoad";
import marketLoad from "./marketLoad";
import providersLoad from "./providersLoad";
import menu from "./menuState";
import addWidgetMenu from "./addWidgetMenu";
import widgets from "./widgets";
import widgetList from "./widgetList";
import favoriteCurrencies from './favorites/favoriteCurrencies';
import addWidget from "./addWidget";
import news from "./news";
import exchangeDirections from "./exchangeDirections";
import currencyPairs from "./currencyPairs";
import exchanger from "./exchanger";
import dashExchangers from "./dashExchangers";
import currPairTooltip from "./currPairTooltip";
import fastExchange from "./fastExchange";
import ratexStats from "./ratexStats";
import locale from "./locale";
import config from "./config";
import currencyTooltip from "./currencyTooltip";
import favoriteDirections from "./favorites/favoriteDirections";
import favoriteExchangers from "./favorites/favoriteExchangers";
import exchangerTooltip from "./exchangerTooltip";
import currpairHashTable from "./currpairHashTable";
import dashInitialStateReducer from "./dashInitialState";
import favoriteExchangersWidget from "./favoriteExchangersWidget";
import favoriteDirectionsWidget from "./favoriteDirectionsWidget";
import currencies from "./pages/currencies";
import currenciesList from './currenciesList';
import countries from './countries';
import blackMarket from './pages/blackMarket';
import { reducer as formReducer } from 'redux-form';
import currenciesHashTable from './currenciesHashTable';
import rates from './pages/rates/rates';
import organizations from './pages/organizations';
import chartTooltip from './chartTooltip';



const rootReducer = combineReducers({
    widgetsSet,
    ajaxLoad,
    exchangersLoad,
    marketLoad,
    providersLoad,
    menu,
    widgets,
    addWidgetMenu,
    widgetList,
    addWidget,
    news,
    exchangeDirections,
    currencyPairs,
    exchanger,
    dashExchangers,
    currPairTooltip,
    fastExchange,
    ratexStats,
    locale,
    dashInitialState: dashInitialStateReducer,
    config,
    currencyTooltip,
    favoriteDirections,
    favoriteExchangers,
    exchangerTooltip,
    currpairHashTable,
    rates,
    favoriteExchangersWidget,
    favoriteDirectionsWidget,
    countries,
    currencies,
    currenciesHashTable,
    currenciesList,
    favoriteCurrencies,
    organizations,
    blackMarket,
    chartTooltip,
    form: formReducer,
    routing: routerReducer
});


export default rootReducer;