

function chartTooltip ( state = {}, action ) {

    switch( action.type ) {

        case 'CHART_TOOLTIP_LOAD_START':
            return state;

        case 'CHART_TOOLTIP_LOAD_OK':
            //check for non duplicates in store...
            var newState = Object.assign({}, state, {[action.currency]: action.data});
            return newState;

        case 'CHART_TOOLTIP_LOAD_ERR':
            return state;

        default:
            return state;

    }

}
export default chartTooltip;
