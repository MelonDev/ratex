
var defaultState = {
    data : null,
    loading : false
};

function widgetList ( state = defaultState, action ) {

    switch( action.type ) {

        case 'LOAD_WIDGET_LIST_START':
            return Object.assign( {}, state, { loading : true } );

        case 'LOAD_WIDGET_LIST_OK':
            var data = action.data;
            delete data.favorite_exchangers;
            delete data.favorite_currency_pairs;
            return {
                data : data,
                loading : false
            };

        case 'LOAD_WIDGET_LIST_ERR':
            return state;

        default:
            return state;

    }

}
export default widgetList;
