

var defaultState = {
    language: ''
};

function locale ( state = defaultState, action ) {

    switch (action.type) {
        case 'CHANGE_LANGUAGE' :
            return Object.assign({}, state, {language: action.payload.language});
        default :
            return state;
    }

}
export default locale;
