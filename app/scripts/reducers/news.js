
var defaultState = {
    data : null,
    loading : false
};

function news ( state = defaultState, action ) {

    switch( action.type ) {

        case 'LOAD_NEWS_START':
            return Object.assign( {}, state, { loading : true } );

        case 'LOAD_NEWS_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'LOAD_NEWS_ERR':
            return state;

        default:
            return state;

    }

}
export default news;
