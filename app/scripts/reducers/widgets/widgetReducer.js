import fastExchangeReducer from './types/fastExchange';
import topExchangersReducer from './types/topExchangers';
import topCurrencyPairsReducer from './types/topCurrencyPairs';
import nationaBankRatesReducer from './types/nataionalBankRates';
import calculatorProReducer from './types/calculatorPro';
import blackMarketOverviewReducer from './types/blackMarketOverview';
import internationalPaymentSystemsReducer from './types/internationalPaymentSystems';
import fluctuationsReducer from './types/fluctuations';
import visaRatesReducer from './types/visaRates';
import mastercardRatesReducer from './types/mastercardRates';
import averageVisaMastercardRatesReducer from './types/averageVisaMastercardRates';
import privatBankRatesReducer from './types/privatBankRates';
import privatCardRatesReducer from './types/privatCardRates';
import favoriteExchangersReducer from './types/favoriteExchanger';


export default function widgetReducer(state = {}, action) {


  if (action.family == 'WIDGET_ACTION') {
    switch (action.widget) {
      case 'TOP_EXCHANGERS':
        var exchangers = topExchangersReducer(state, action);
        return exchangers;
        break;
      case 'TOP_CURRENCY_PAIRS':
        return topCurrencyPairsReducer(state, action);
        break;
      case 'FAST_EXCHANGE':
        return fastExchangeReducer(state, action);
        break;
      case 'NATIONAL_BANK_RATES':
        return nationaBankRatesReducer(state, action);
        break;
      case 'VISA_RATES':
        return visaRatesReducer(state, action);
        break
      case 'MASTERCARD_RATES':
        return mastercardRatesReducer(state, action);
        break
      case 'AVERAGE_VISA_MASTERCARD_RATES':
        return averageVisaMastercardRatesReducer(state, action);
        break
      case 'PRIVAT_BANK_RATES':
        return privatBankRatesReducer(state, action);
        break
      case 'PRIVAT_CARD_RATES':
        return privatCardRatesReducer(state, action);
        break
      case 'CALCULATOR_PRO':
        return calculatorProReducer(state, action);
        break
      case 'NATIONAL_BANK_FLUCTUATIONS':
        var newFluc = fluctuationsReducer(state, action);
        return newFluc;
        break;
      case 'BLACK_MARKET_OVERVIEW':
        return blackMarketOverviewReducer(state, action);
        break
      case 'INTERNATIONAL_PAYMENT_SYSTEMS':
        return internationalPaymentSystemsReducer(state, action);
        break;
      case 'FAVORITE_EXCHANGERS':
        return favoriteExchangersReducer(state, action);
        break;
      default:
        return state;
    }

  }

  switch (action.type) {

    case 'LOAD_DASH_OK':
      if (state.alias == "national_bank_fluctuations") {
        let data = state;
        let fluctuations = data.fluctuations;
        let ask = fluctuations.rates;
        let askFormatted = ask.map((elem) => {
          return parseFloat(elem);
        });
        let timestamp = fluctuations.dates;
        let timestamps = timestamp.map((elem, index) => {
          let time = elem;
          return (
            moment(time).format('DD.MM.YY')
          )
        });
        var newObj = Object.assign({}, state, {
          categories: timestamps,
          data: askFormatted,
          loading: false
        });
        return newObj;
      }
      return Object.assign({}, state, { loading: false });
      break;
    case 'TEST':
      return state;
      break;

    case 'LOAD_DASH_ERR':
      return state;

    default:
      return state;

  }

}