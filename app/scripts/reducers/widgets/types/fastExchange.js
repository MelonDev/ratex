export default function fastExchangeReducer (state = {}, action) {
    switch (action.type) {

        case 'CHANGE_DIRECTION_START':
            return Object.assign({}, state, {loading: true});

        case 'FAST_EXCHANGE_LOADING_START':

            return Object.assign({}, state, {loading: true});

        case 'CHANGE_DIRECTION_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false}));

        case 'FAST_EXCHANGE_LOADING_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false}));

        default:
            return state;

    }
}
