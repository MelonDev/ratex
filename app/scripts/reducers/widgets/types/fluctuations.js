import moment from 'moment';



export default function fluctuationsReducer (state = {}, action) {

    switch (action.type) {

        case 'WIDGET_LOAD_START':
            return Object.assign({}, state, {loading: true});
            break;
        case 'DATA_OK':
            let data = action.data.widgets[0];
            let fluctuations = data.fluctuations;
            let ask = fluctuations.rates;
            let askFormatted = ask.map((elem)=>{
                return parseFloat(elem);
            });
            let timestamp = fluctuations.dates;
            let timestamps = timestamp.map((elem, index)=>{
                let time = elem;
                return(
                    moment(time).format('DD.MM.YY')
                )
            });
            var newObj = Object.assign({}, state, {
                categories: timestamps,
                data: askFormatted,
                loading:false
            });
            return newObj;
            break;
        case 'CHANGE_PERIOD_START':
            return Object.assign({}, state, {loading: true});
            break;
        case 'CHANGE_PERIOD_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false}));
            break;
        default:
            return state;

    }

}