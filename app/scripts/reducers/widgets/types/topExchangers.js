
export default function topExchangersReducer (state = {}, action) {

    switch (action.type) {

        case 'CHANGE_LIMIT_START':
            return Object.assign({}, state, {loading: true});
        case 'CHANGE_LIMIT_OK':
            let newState = Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false, exchangers:action.data.widgets[0].exchangers}));
            return newState;
        default:
            return state;

    }

}