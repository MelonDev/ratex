
export default function nationaBankRatesReducer (state = {}, action) {

    switch (action.type) {

        case 'CHANGE_BASE_AMOUNT_START':
            return Object.assign({}, state, {loading: true});
        case 'CHANGE_BASE_AMOUNT_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false}));
        default:
            return state;

    }

}