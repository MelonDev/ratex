
export default function internationalPaymentSystemsReducer (state = {}, action) {

    switch (action.type) {
        case 'CHANGE_DATE':
            return Object.assign({}, state, {date: action.data});
        case 'DATA_OK':
            var newState = action.data.widgets[0];
            return newState;
        default:
            return state;
    }

}