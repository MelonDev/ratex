
export default function calculatorProReducer (state = {}, action) {

    switch (action.type) {

        case 'LOAD_CALCULATOR_PRO_START':
            return Object.assign({}, state, {loading: true});
        case 'CHANGE_DIRECTION_START':
            return Object.assign({}, state, {selected_direction:{base_code:action.data[0], quote_code:action.data[1]}});
        case  'CHANGE_DATE_OK':
            return Object.assign({}, state, action.data.widgets[0]);
        case 'CHANGE_DIRECTION_OK':
            return Object.assign({}, state, action.data.widgets[0]);
        case 'CALCULATOR_PRO_LOADING_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false}));
        default:
            return state;

    }

}