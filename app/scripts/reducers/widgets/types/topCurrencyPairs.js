
export default function topCurrencyPairsReducer (state = {}, action) {

    switch (action.type) {

        case 'CHANGE_LIMIT_START':
            return Object.assign({}, state, {loading: true});
        case 'LOAD_TOP_DIRECTIONS_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading: false}));
        case 'CHANGE_LIMIT_OK':
            return Object.assign({}, state, Object.assign({}, action.data.widgets[0], {loading:false}));
        default:
            return state;

    }

}
