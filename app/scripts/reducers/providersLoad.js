var defaultState = {loading:true, data:null, error:null};

function providersLoad( state = defaultState, action ){

    switch (action.type){
        case 'LOAD_PROVIDERS_REQUESTED':
            return Object.assign({}, state, {
                loading:true
            });
        case 'LOAD_PROVIDERS_OK':
            return  Object.assign({}, state, {
                loading:false,
                data: action.data
            });
        case 'LOAD_PROVIDERS_ERR':
            return state;
        default:
            return state;
    }

}
export default providersLoad;