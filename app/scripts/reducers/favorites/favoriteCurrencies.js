var defaultState = {
    data : null,
    loading : false
};

function favoriteCurrencies(state = defaultState, action){

    switch (action.type){

        case "ADD_FAVORITE_CURRENCY":
            let found = false;
            for ( var i = 0; i < state.length; i++ ) {
                if(state[i] == action.data){
                    found = true;
                }
            }
            if(!found){
                let obj = {};
                obj[action.data] = action.data;
                let newState = Object.assign({}, state, obj);
                localStorage.setItem('favoriteCurrencies', JSON.stringify(newState));
                return newState;
            }
            return state;
            break;
        case "REMOVE_FAVORITE_CURRENCY":
            let lastState = Object.assign({},state, {});
            delete lastState[action.data];
            localStorage.removeItem('favoriteCurrencies');
            localStorage.setItem('favoriteCurrencies', JSON.stringify(lastState));
            return lastState;
            break;

        default:
            return state;


    }


}


export default favoriteCurrencies;