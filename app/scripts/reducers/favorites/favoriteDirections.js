
var defaultState = {
    data : null,
    loading : false
};

function favoriteDirections ( state = [], action ) {

    switch( action.type ) {

        case 'ADD_FAVORITE_DIRECTION':
            let found = false;
            for ( var i = 0; i < state.length; i++ ) {
                if(state[i] == action.data){
                    found = true;
                }
            }
            if(!found){
                let obj = {};
                obj[action.data] = action.data;
                let newState = Object.assign({}, state, obj);
                debugger;
                localStorage.setItem('favoriteDirections', JSON.stringify(newState));
                return newState;

            }
            return state;

        case 'REMOVE_FAVORITE_DIRECTION':
            delete state[action.data];
            let lastState = state;
            localStorage.removeItem('favoriteDirections');
            localStorage.setItem('favoriteDirections', JSON.stringify(lastState));
            return lastState;

        default:
            return state;

    }

}
export default favoriteDirections;
