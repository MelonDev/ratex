
var defaultState = {
    data : null,
    loading : false
};

function favoriteExchangers ( state = {}, action ) {

    switch( action.type ) {

        case 'ADD_FAVORITE_EXCHANGER':
            let found = false;
            for ( var i = 0; i < state.length; i++ ) {
                if(state[i] == action.data){
                    found = true;
                }
            }
            if(!found){
                let obj = {};
                obj[action.data] = action.data;
                let newState = Object.assign({}, state, obj);
                localStorage.setItem('favoriteExchangers', JSON.stringify(newState));
                return newState;
            }
            return state;

        case 'REMOVE_FAVORITE_EXCHANGER':
            let lastState = Object.assign({},state, {});
            delete lastState[action.data];
            localStorage.removeItem('favoriteExchangers');
            localStorage.setItem('favoriteExchangers', JSON.stringify(lastState));
            return lastState;

        default:
            return state;

    }

}
export default favoriteExchangers;
