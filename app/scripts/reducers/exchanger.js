var defaultState = {
    loading: false,
    data: null,
    reserves: null,
    rates: null
};

function rates(state = {}, action) {
    //helper function for new state construction fro reducing code copying
    function sortRatesTable(sortingProp, state, column) {
        let reservesSorted,
            direction;
        if (state.rates.tableDirection == 'rand' || state.rates.tableDirection == 'down') {
            reservesSorted = state.rates.data.sort((a, b) => {
                if (a.attributes[sortingProp] > b.attributes[sortingProp]) {
                    return -1
                }
                if (a.attributes[sortingProp] < b.attributes[sortingProp]) {
                    return 1
                }
                return 0;
            });
            direction = 'up'
        } else {
            reservesSorted = state.rates.data.sort((a, b) => {
                if (a.attributes[sortingProp] > b.attributes[sortingProp]) {
                    return 1
                }
                if (a.attributes[sortingProp] < b.attributes[sortingProp]) {
                    return -1
                }
                return 0;
            });
            direction = 'down'
        }

        return {
            data: reservesSorted,
            tableDirection: direction,
            sortColumn: column
        }
    }

    let newState;
    switch (action.type) {
        case 'TABLE_DIRECTION_RESERVES':
            newState = sortRatesTable('reserve', state, 'reserves');
            return Object.assign(
                {},
                state,
                {
                    rates: newState
                });
            break;
        case 'TABLE_DIRECTION_NAME':
            newState = sortRatesTable('base_currency', state, 'name');
            return Object.assign(
                {},
                state,
                {
                    rates: newState
                });
            break;
        case 'TABLE_DIRECTION_COMMISSION':
           return state;
        case 'TABLE_DIRECTION_RATING':
            newState = sortRatesTable('currency_pair_rating', state, 'rating');
            return Object.assign(
                {},
                state,
                {
                    rates: newState
                });
            break;
        default:
            return state;

    }

}


function exchanger(state = {}, action) {


    switch (action.tab) {
        case 'RATES':
            return rates(state, action);
            break;

    }


    switch (action.type) {

        case 'LOAD_SINGLE_EXCHANGER_START':
            return Object.assign({}, state, {loading: true});
            break;
        case 'LOAD_SINGLE_EXCHANGER_OK':
            return Object.assign({}, state, {
                data: action.data,
                loading: false
            });
            break;
        case 'LOAD_SINGLE_EXCHANGER_ERR':
            return state;
            break;
        case 'LOAD_SINGLE_EXCHANGER_RESERVES_OK':
            return Object.assign({}, state, {reserves: action.data});
            break;
        case 'LOAD_SINGLE_EXCHANGER_RATES_OK':
            return Object.assign(
                {},
                state,
                {
                    rates: {
                        data: action.data,
                        tableDirection: 'rand'
                    }
                });
            break;
        default:
            return state;

    }

}
export default exchanger;
