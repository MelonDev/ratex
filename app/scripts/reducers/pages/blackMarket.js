var defaultState = {
    loading: false,
    data: null,
    error: null,
    citySelected: 'Киев',
    currencySelected: 'USD',
    operation: {value: 'buy', label: 'покупке'},
    citiesAvaliable: [
        {label: 'Харьков', value: 'Харьков'},
        {label: 'Черкассы', value: 'Черкассы'},
        {label: 'Киев', value: 'Киев'}
    ],
    meta: {
        pageNumber: 1,
        sort: '',
        'filter[]': ['base_currency:USD', 'operation:buy', 'city:Киев'],
        'multifilter[]': [],
        search: '',
        sortDirection: 'rand',
        operation: 'buy'
    }
};
function blackMarket(state = defaultState, action) {

    switch (action.type) {
        case 'BLACK_MARKET_LOAD_START':
            return Object.assign({}, state, {
                loading: true
            });
        case 'BLACK_MARKET_LOAD_OK':
            return Object.assign({}, state, {
                loading: false,
                data: action.data,
                meta: Object.assign({}, state.meta, action.meta)
            });
        case 'BLACK_MARKET_LOAD_ERR':
            return state;
            break;
        case 'BLACK_MARKET_PAGINATE':
            return Object.assign({}, state, {meta: Object.assign({}, state.meta, {pageNumber: action.meta})});
        case 'BLACK_MARKET_CITY_CHANGE':
            return Object.assign({}, state, {
                citySelected: action.data.city,
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'filter[]': action.data.filter,
                    'multifilter[]': action.data.multifilter
                })
            });
        case 'BLACK_MARKET_CURRENCY_CHANGE':
            return Object.assign({}, state, {
                currencySelected: action.data.currencySelected,
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'filter[]': action.data.filter,
                    'multifilter[]': action.data.multifilter
                })
            });
        case 'BLACK_MARKET_OPERATION_CHANGE':
            return Object.assign({}, state, {
                operation: action.data.operation,
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'filter[]': action.data.filter,
                    'multifilter[]': action.data.multifilter,
                    operation: action.data.operation.value
                })
            });
        case 'BLACK_MARKET_SORT_CHANGE':
            if (typeof state.meta.sortDirection != 'undefined' && state.meta.sortDirection == 'up') {
                return Object.assign({}, state, {
                    meta: Object.assign({}, state.meta, {
                        sort: action.meta,
                        sortDirection: 'down'
                    })
                });
            } else if (typeof state.meta.sortDirection != 'undefined' && state.meta.sortDirection == 'down') {
                return Object.assign({}, state, {
                    meta: Object.assign({}, state.meta, {
                        sort: action.meta,
                        sortDirection: 'up'
                    })
                });
            } else {
                return Object.assign({}, state, {
                    meta: Object.assign({}, state.meta, {
                        sort: action.meta,
                        sortDirection: 'down'
                    })
                });
            }
        case 'BLACK_MARKET_FILTER_CHANGE':
            return Object.assign({}, state, {
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'filter[]': action.meta
                })
            });
        case 'BLACK_MARKET_FILTER_VALUE_CHANGE':
            return Object.assign({}, state, {
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'multifilter[]': action.meta
                })
            });
        case 'BLACK_MARKET_SEARCH_CHANGE':
            return Object.assign({}, state, {meta: Object.assign({}, state.meta, {search: action.meta})});
        default:
            return state;
    }

}
export default blackMarket;