var defaultState = {
    loading: false,
    data: null,
    error: null,
    meta: {
        pageNumber: 1,
        sort: '',
        'filter[]': [],
        search:'',
        sortDirection: 'rand'
    }
};

function currencies(state = defaultState, action) {

    switch (action.type) {
        case 'CURRENCIES_LOAD_START':
            return Object.assign({}, state, {
                loading: true
            });
        case 'CURRENCIES_LOAD_OK':
            return Object.assign({}, state, {
                loading: false,
                data: action.data,
                meta: Object.assign({}, state.meta, action.meta)
            });
        case 'CURRENCIES_LOAD_ERR':
            return state;
            break;
        case 'CURRENCIES_PAGINATE':
            return Object.assign({}, state, {meta: Object.assign({}, state.meta, {pageNumber: action.meta})});
        case 'CURRENCIES_SORT_CHANGE':
            if (typeof state.meta.sortDirection != 'undefined' && state.meta.sortDirection == 'up'){
                return Object.assign({}, state, {meta: Object.assign({}, state.meta, {sort: action.meta, sortDirection: 'down'})});
            }else if(typeof state.meta.sortDirection != 'undefined' && state.meta.sortDirection == 'down'){
                return Object.assign({}, state, {meta: Object.assign({}, state.meta, {sort: action.meta, sortDirection: 'up'})});
            }else{
                return Object.assign({}, state, {meta: Object.assign({}, state.meta, {sort: action.meta, sortDirection: 'down'})});
            }
        case 'CURRENCIES_FILTER_CHANGE':
            return Object.assign({}, state, {
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'filter[]': action.meta
                })
            });
        case 'CURRENCIES_SEARCH_CHANGE':
            return Object.assign({}, state, {meta: Object.assign({}, state.meta, {search: action.meta})});
        default:
            return state;
    }
}
export default currencies;