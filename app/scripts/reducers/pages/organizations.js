var defaultState = {
    loading: false,
    data: null,
    error: null,
    meta: {
        pageNumber: 1,
        sort: '',
        'filter[]': [],
        search:'',
        sortDirection: 'rand'
    }
};

function organizations(state = defaultState, action) {

    switch (action.type) {
        case 'ORGANIZATIONS_LOAD_START':
            return Object.assign({}, state, {
                loading: true
            });
        case 'ORGANIZATIONS_LOAD_OK':
            return Object.assign({}, state, {
                loading: false,
                data: action.data,
                meta: Object.assign({}, state.meta, action.meta)
            });
        case 'ORGANIZATIONS_LOAD_ERR':
            return state;
            break;
        case 'ORGANIZATIONS_PAGINATE':
            return Object.assign({}, state, {meta: Object.assign({}, state.meta, {pageNumber: action.meta})});
        case 'ORGANIZATIONS_SORT_CHANGE':
            if (typeof state.meta.sortDirection != 'undefined' && state.meta.sortDirection == 'up'){
                return Object.assign({}, state, {meta: Object.assign({}, state.meta, {sort: action.meta, sortDirection: 'down'})});
            }else if(typeof state.meta.sortDirection != 'undefined' && state.meta.sortDirection == 'down'){
                return Object.assign({}, state, {meta: Object.assign({}, state.meta, {sort: action.meta, sortDirection: 'up'})});
            }else{
                return Object.assign({}, state, {meta: Object.assign({}, state.meta, {sort: action.meta, sortDirection: 'down'})});
            }
        case 'ORGANIZATIONS_FILTER_CHANGE':
            return Object.assign({}, state, {
                meta: Object.assign({}, state.meta, {
                    pageNumber: 1,
                    'filter[]': action.meta
                })
            });
        case 'ORGANIZATIONS_SEARCH_CHANGE':
            return Object.assign({}, state, {meta: Object.assign({}, state.meta, {search: action.meta})});
        default:
            return state;
    }
}
export default organizations;