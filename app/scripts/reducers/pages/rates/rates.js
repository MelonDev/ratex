import _ from 'lodash';



var defaultState = {
    loading: false,
    data: null,
    error: null,
    currencySelected: 'UAH',
    currentTab: 'cash',
    cash: {
        data: null,
        loading: true,
        meta: {
            'filter[]': ['quote_currency:UAH', 'aliases:p2p']
        }
    },
    cards: {
        data: null,
        loading: true,
        meta: {
            'filter[]': ['quote_currency:UAH', 'aliases:pat_kb_privatbank_bank', 'aliases:pat_kb_privatbank_card', 'aliases:p2p']
        }
    },
    official: {
        data: null,
        loading: true,
        meta: {
            'filter[]': ['quote_currency:UAH', 'aliases:nbu']
        }
    },
    meta: {
        pageNumber: 1,
        sort: '',
        'filter[]': ['quote_currency:UAH', 'aliases:pat_kb_privatbank_bank', 'aliases:pat_kb_privatbank_card', 'aliases:p2p'],
        'multifilter[]': [],
        search: '',
    }
};

function rates(state = defaultState, action) {

    switch (action.type) {

        case 'RATES_TAB_CHANGE':
            return Object.assign({}, state, {currentTab: action.data});
        case 'RATES_CURRENCY_CHANGE':
            return Object.assign({}, state, {currencySelected: action.data});
        case 'LOAD_RATES_CASH_OK':
            return Object.assign({}, state, {
                cash: {
                    data: action.data,
                    loading:false,
                    meta: state.cash.meta
                }
            });
        case 'FILTER_RATES_CASH_CHANGE':
            return Object.assign({}, state, {
                cash: {
                    data: state.cash.data,
                    meta: {
                        'filter[]': action.meta
                    }
                }
            });
        case 'LOAD_RATES_CARDS_OK':
            return Object.assign({}, state, {
                cards: {
                    data: action.data,
                    loading:false,
                    meta: state.cards.meta
                }
            });
        case 'FILTER_RATES_CARDS_CHANGE':
            return Object.assign({}, state, {
                cards: {
                    data: state.cards.data,
                    meta: {
                        'filter[]': action.meta
                    }
                }
            });
        case 'LOAD_RATES_OFFICIAL_OK':
            return Object.assign({}, state, {
                official: {
                    data: action.data,
                    loading:false,
                    meta: state.official.meta
                }
            });
        case 'FILTER_RATES_OFFICIAL_CHANGE':
            return Object.assign({}, state, {
                official: {
                    data: state.official.data,
                    meta: {
                        'filter[]': action.meta
                    }
                }
            });

        default:
            return state;

    }

}


export default rates;