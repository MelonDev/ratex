
var defaultState = {
    data : null,
    loading : false
};

function favoriteDirectionsWidget ( state = defaultState, action ) {

    switch( action.type ) {

        case 'FAVORITE_DIRECTIONS_WIDGET_START':
            return Object.assign( {}, state, { loading : true } );

        case 'FAVORITE_DIRECTIONS_WIDGET_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'FAVORITE_DIRECTIONS_WIDGET_ERR':
            return state;

        default:
            return state;

    }

}
export default favoriteDirectionsWidget;
