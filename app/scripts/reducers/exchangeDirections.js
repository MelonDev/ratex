/**
 * Created by alexdankov on 20.09.16.
 */
var defaultState = {
    data : null,
    loading : false };

function exchangeDirections ( state = defaultState, action ) {

    switch( action.type ) {

        case 'LOAD_TOP_DIRECTIONS_START':
            return Object.assign( {}, state, { loading : true } );

        case 'LOAD_TOP_DIRECTIONS_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'CHANGE_LIMIT_TOP_DIRECTIONS_OK':

            return {
                data : action.data,
                loading: false
            };

        case 'LOAD_TOP_DIRECTIONS_ERR':
            return state;

        default:
            return state;

    }

}
export default exchangeDirections;