
var defaultState = {
    data : null,
    loading : false
};

function dashExchangers ( state = defaultState, action ) {

    switch( action.type ) {

        case 'LOAD_DASH_EXCHANGERS_START':
            return Object.assign( {}, state, { loading : true } );

        case 'LOAD_DASH_EXCHANGERS_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'LOAD_DASH_EXCHANGERS_ERR':
            return state;

        default:
            return state;

    }

}
export default dashExchangers;
