/**
 * Created by alexdankov on 20.09.16.
 */
var defaultState = {
    data : null,
    loading : false };

function widgets ( state = defaultState, action ) {

    switch( action.type ) {

        case 'LOAD_DASH_START':
            return Object.assign( {}, state, { loading : true } );

        case 'LOAD_DASH_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'CHANGE_LIMIT_OK':

            return {
                data : action.data,
                loading: false
            };

        case 'LOAD_DASH_ERR':
            return state;

        default:
            return state;

    }

}
export default widgets;