var defaultState = { open : false };

function menu ( state = defaultState , action ) {

    switch(action.type){

        case 'MENU_OPENED':

            return state = {
                open : true
            };

        case 'MENU_HIDDEN':

            return state = {
                open : false
            };

        default:

            return state

    }

}
export default menu;