
var defaultState = {
    data : {
        avaliable_currencies:[],
        selected_currencies: {
            base_currency: "WMZ",
            quote_currency: "PB24UAH"
        },
        widget_data: {
            best_rate: null,
            rates: []
        }
    },
    loading : false
};



function fastExchange ( state = defaultState, action ) {

    switch( action.type ) {

        case 'DIRECTION_DATA_CHANGE':
            return Object.assign({}, state, {
                data: {
                    widget_data: {
                        best_rate: action.data.best_rate,
                        rates : action.data.rates
                    }
                }
            });

        case 'CURRENCIES_DATA_CHANGE':
            return Object.assign({}, state, {
                data: {
                    avaliable_currencies: action.data.avaliable_currencies
                }
            });


        default:
            return state;

    }

}
export default fastExchange;
