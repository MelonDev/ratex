import {combineReducers} from "redux";
import widgetReducer from './widgets/widgetReducer';
import _ from 'lodash';

var defaultState = null;

function dashData(state = {}, action) {

    if (action.family == "WIDGET_ACTION") {

        if(typeof state[action.alias] == 'undefined'){

            state[action.alias] = {};

        }

        let newState = Object.assign({}, state, {
            [action.alias]: Object.assign({}, state[action.alias], {
                [action.id]: widgetReducer(state[action.alias][action.id], action)
            })
        });
        return newState;

    }


    switch (action.type) {

        case 'LOAD_DASH_START':
            return Object.assign({}, state, {loading: true});

        case 'LOAD_DASH_OK':

            var reducersObj = {
                loading: false
            };

            for (var i = 0; i < action.data.widgets.length; i++) {

                reducersObj = Object.assign({}, reducersObj, {
                    [action.data.widgets[i].alias]: Object.assign({}, state[action.data.widgets[i].alias], {
                        [action.data.widgets[i].id]: widgetReducer(action.data.widgets[i], action),
                    })
                })
            }

            var newState = Object.assign({}, state, reducersObj);

            return newState;


        case 'LOAD_DASH_ERR':

            return state;

        default:
            return state;

    }

}


const dashInitialStateReducer = combineReducers({
    dash: dashData,
});

export default dashInitialStateReducer;
