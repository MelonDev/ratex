var defaultState = {loading:true, data:null, error:null};


function ajaxLoad( state = defaultState , action ){

    switch (action.type){
        case 'LOAD_INFO_REQUESTED':
            return Object.assign({}, state, {
                loading:true
            });
        case 'LOAD_INFO_OK':
            return  Object.assign({}, state, {
                loading:false,
                data: action.data
            });
        case 'LOAD_INFO_ERR':
            return state;
        default:
            return state;
    }
}


export default ajaxLoad;