import _ from 'underscore';


function widgetsSet( state=[], action ){
    const position = action.position;
    switch (action.type){
        case 'REPLACE_CARD':
            return state.map((widgetsSet, index)=>{
                return Object.assign({}, widgetsSet, {
                    id: position[index].id,
                    position: position[index].position,
                    type: position[index].type,
                    title:position[index].type,
                    size: position[index].size
                });
            });
        case 'WIDGET_ADDED':
             let newState = [...state, action.data];
             localStorage.setItem('widgets', JSON.stringify(newState));
             return newState;
        case 'WIDGET_DELETED':
            let widget = _.find(state, function(obj){return obj.id == action.data});
            let indexOfWidget = state.indexOf(widget);
            if(indexOfWidget != -1){
                let latestState = [...state.slice(0, indexOfWidget), ...state.slice(indexOfWidget+1)];
                localStorage.setItem('widgets', JSON.stringify(latestState));
                localStorage.removeItem('dragPositions');
                return latestState;

            }
            return state;
        default:
            return state;
    }
}
export default widgetsSet;