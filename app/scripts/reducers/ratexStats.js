
var defaultState = {
    data : null,
    loading : false
};

function ratexStats ( state = defaultState, action ) {

    switch( action.type ) {

        case 'LOAD_RATEX_STATS_START':
            return Object.assign( {}, state, { loading : true } );

        case 'LOAD_RATEX_STATS_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'LOAD_RATEX_STATS_START':
            return state;

        default:
            return state;

    }

}
export default ratexStats;

