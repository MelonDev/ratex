var defaultState = {loading: false, data: null, error: null};

function currenciesList(state = defaultState, action) {

    switch (action.type) {
        case 'CURRENCIES_LIST_REQUESTED':
            return Object.assign({}, state, {
                loading: true
            });
        case 'CURRENCIES_LIST_OK':
            return Object.assign({}, state, {
                loading: false,
                data: action.data
            });
        case 'CURRENCIES_LIST_ERR':
            return state;


        default:
            return state;
    }
}
export default currenciesList;