var defaultState = {loading:true, data:null, error:null};
function exchangersLoad( state = defaultState , action ){

    switch (action.type){
        case 'LOAD_EXCHANGERS_REQUESTED':
            return Object.assign({}, state, {
                loading:true
            });
        case 'LOAD_EXCHANGERS_OK':
            return  Object.assign({}, state, {
                loading:false,
                data: action.data
            });
        case 'LOAD_EXCHANGERS_ERR':
            return state;


        default:
            return state;
    }
}
export default exchangersLoad;