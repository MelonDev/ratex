var defaultState = {loading:true, data:null, error:null};
function countries( state = defaultState , action ){

    switch (action.type){
        case 'LOAD_COUNTRIES_REQUESTED':
            return Object.assign({}, state, {
                loading:true
            });
        case 'LOAD_COUNTRIES_OK':
            return  Object.assign({}, state, {
                loading:false,
                data: action.data
            });
        case 'LOAD_COUNTRIES_ERR':
            return state;


        default:
            return state;
    }
}
export default countries;