

function currPairTooltip ( state = [], action ) {

    switch( action.type ) {

        case 'LOAD_CURRPAIRTOOLTIP_START':
            return state;

        case 'LOAD_CURRPAIRTOOLTIP_OK':
            //check for non duplicates in store...
            let found = false;
            for ( var i = 0; i < state.length; i++ ) {
                if(state[i].id == action.data.id){
                    found = true;
                }
            }
            if(!found){

                return [...state, action.data];

            }
            return state;

        case 'LOAD_CURRPAIRTOOLTIP_ERR':
            return state;

        default:
            return state;

    }

}
export default currPairTooltip;
