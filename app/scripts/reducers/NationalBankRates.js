
var defaultState = {
    data : null,
    loading : false
};

function nationalBankRates ( state = defaultState, action ) {

    switch( action.type ) {

        case 'NATIONAL_BANK_RATES_START':
            return Object.assign( {}, state, { loading : true } );

        case 'NATIONAL_BANK_RATES_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'NATIONAL_BANK_RATES_ERR':
            return state;

        default:
            return state;

    }

}
export default nationalBankRates;
