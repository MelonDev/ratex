var defaultState = {};

function currpairHashTable ( state = {}, action ) {

    switch( action.type ) {

        case 'CURRENCY_PAIRS_OK':
            let obj = {};
            for (var i = 0; i < action.data.length; i++) {
                var fromCurrency = action.data[i].base_currency;
                var toCurrency = action.data[i].quote_currency;
                obj[fromCurrency+'-'+toCurrency] =  {
                    id: action.data[i].currency_pair_id,
                    iconFrom:  action.data[i].base_currency_icon_status != 'n' ?  action.data[i].base_currency_icon['32'] : '/images/currency_placeholder.svg',
                    iconTo:  action.data[i].quote_currency_icon_status != 'n' ?  action.data[i].quote_currency_icon['32'] : '/images/currency_placeholder.svg',
                };
            }
            return obj;

        case 'CURRENCY_PAIRS_ERR':
            return state;

        default:
            return state;

    }

}
export default currpairHashTable;