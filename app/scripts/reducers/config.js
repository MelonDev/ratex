var defaultState = {
    decimalsInNumbers : 2,
    quote_currency: 'UAH',
    refresh: 5,
    timezone: 'UTC+2'
};

function config ( state = defaultState, action ) {
   switch (action.type){
       case 'CONFIG_CHANGE':
           return action.data;
           break;
       case 'CONFIG_RESET':
           return defaultState;
           break;
       default:
           return state
   }
}

export default config;
