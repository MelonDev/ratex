/**
 * Created by alexdankov on 20.09.16.
 */
// var defaultState = {
//     data : null,
//     loading : false
// };

function topDirectionsWidget(state = [], action) {

    switch (action.type) {

        case 'LOAD_TOP_DIRECTIONS_WIDGET_START':
            return Object.assign({}, state, {
                [action.widgetId]: {
                    loading: true,
                    limit: action.limit,
                    data: null
                }
            });

        case 'CHANGE_LIMIT_TOP_DIRECTIONS_WIDGET_OK':

            return {
                data: action.data,
                limit: action.limit,
                loading: false
            };

        case 'LOAD_TOP_DIRECTIONS_WIDGET_ERR':
            return state;

        default:
            return state;

    }

}
export default exchangeDirections;