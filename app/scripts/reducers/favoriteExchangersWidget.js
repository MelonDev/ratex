
var defaultState = {
    data : null,
    loading : false,
};

function favoriteExchangersWidget ( state = defaultState, action ) {

    switch( action.type ) {

        case 'FAVORITE_EXCHANGERS_WIDGET_START':
            return Object.assign( {}, state, { loading : true } );

        case 'FAVORITE_EXCHANGERS_WIDGET_OK':
            return {
                data : action.data,
                loading : false
            };

        case 'FAVORITE_EXCHANGERS_WIDGET_ERR':
            return state;

        default:
            return state;

    }

}
export default favoriteExchangersWidget;
