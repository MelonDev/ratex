var defaultState = {loading:true, data:null, error:null};
function marketLoad( state = defaultState , action ){

    switch (action.type){
        case 'LOAD_MARKET_REQUESTED':
            return Object.assign({}, state, {
                loading:true
            });
        case 'LOAD_MARKET_OK':
            return  Object.assign({}, state, {
                loading:false,
                data: action.data
            });
        case 'LOAD_MARKET_ERR':
            return state;
        default:
            return state;
    }
}
export default marketLoad;