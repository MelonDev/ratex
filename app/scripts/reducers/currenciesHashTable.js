var defaultState = {};


function currenciesHashTable(state = defaultState, action) {

    switch (action.type) {
        case 'CURRENCIES_LIST_REQUESTED':
            return Object.assign({}, state, {
                loading: true
            });
        case 'CURRENCIES_LIST_OK':
            var currenciesHash = {};
            for(var i = 0; i < action.data.length; i++){

                currenciesHash[action.data[i].code] = {
                    icon: action.data[i].icon['32'] || '/images/currency_placeholder.svg',
                    icon_status: action.data[i].icon_status,
                    type: action.data[i].type
                }

            }
            return currenciesHash;

        case 'CURRENCIES_LIST_ERR':
            return state;


        default:
            return state;
    }
}

export default currenciesHashTable;