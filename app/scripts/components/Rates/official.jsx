import React from 'react';
import jQuery from "jquery";
import {push} from "react-router-redux";
import {connect} from "react-redux";
import * as ActionCreators from '../../actions/rates/officialActions';
import {bindActionCreators} from "redux";
import DateTime from "react-datetime";
import moment from "moment";
import _ from 'lodash';
var $ = jQuery;


class Official extends React.Component {

    constructor(props){
        moment.locale('ru');
        super(props);
        this.setCalendarDate = this.setCalendarDate.bind(this);
        this.addFilter = this.addFilter.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.state = {
            calendarDateText: moment().format('D MMMM')
        }
    }

    setCalendarDate(date) {

        var filters = this.props.rates.official.meta['filter[]'];
        var newFilter = filters.filter((elem, index) => {
            if (_.includes(elem, 'date')) {
                return false;
            }
            return true;
        });
        newFilter.push('date:'+moment(date).format('YYYY-MM-DD'));

        moment.locale('ru');

        this.setState({
            calendarDateText: moment(date).format('D MMMM')
        });

        this.hideCalendar();

        this.props.filterRatesOfficial(newFilter);

    }



    toggleFilters(e){

        e.stopPropagation();
        $('.rates-filter-dropdown').toggleClass('visible');

    }


    addFilter(key) {

        if (typeof this.props.rates.official.meta['filter[]'] != 'string' && typeof this.props.rates.official.meta['filter[]'] != 'undefined') {

            var newFilter =this.props.rates.official.meta['filter[]'];

            newFilter.push(key);
            var action = push(
                {
                    pathname: '/rates',
                    query: {
                        'filter[]': newFilter,
                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterRatesOfficial(newFilter);
        } else {
            var action = push(
                {
                    pathname: '/rates/official',
                    query: {
                        'filter[]': [key],

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterRatesOfficial([key]);
        }

    }

    contains(array, needle) {
        if (typeof array != 'undefined') {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == needle) {
                    return true;
                }
            }
            return false;
        }
        return false
    }

    removeFilter(filter){

        var filters = this.props.rates.official.meta['filter[]'];
        var newFilter = filters.filter((elem, index) => {
            if (elem == filter) {
                return false;
            }
            return true;
        });
        var action = push(
            {
                pathname: '/rates/official',
                query: {
                    'filter[]': newFilter,
                }
            }
        );
        this.props.dispatch(action);

        this.props.filterRatesOfficial(newFilter);

    }

    componentWillMount(){
        if(this.props.params.tab_alias == 'official'){
            var query = {
                'filter[]': this.props.location.query.filter || this.props.rates.official.meta['filter[]'],
            }
            this.props.loadRatesOfficial(query);
        }else{
            this.props.loadRatesOfficial();
        }

    }

    showCalendar(e) {

        e.stopPropagation();
        $('.calendar-dropdown').addClass('visible');

    }

    hideCalendar() {

        $('.calendar-dropdown').removeClass('visible');

    }

    render(){
        var self = this;
        if(this.props.rates.official.data != null){
            var locale = this.props.locale.language;

            var data = this.props.rates.official.data;
            var currencies = data.currencies;
            var rates = data.rates;
            var headers = [<th>{locale.t('_rates', {returnObjects: true}).currency}</th>,];
            var rows = [];
            var columnsLength = 0;
            var tableClass = 'directory-table exchangers-table';

            for( var alias in rates){
                headers.push(
                    <th>
                        {locale.t('_rates', {returnObjects: true})[rates[alias].alias]}
                        {
                            rates[alias].alias != 'nbu' ?
                                <div className="buy-sell">
                                    {locale.t('_rates', {returnObjects: true}).buy_sell}
                                </div> :
                                ''
                        }
                    </th>
                );
                columnsLength = Object.keys(rates).length;
            }

            for (var currency in currencies){
                var tds = [<td>{currency}</td>];
                let value;
                for (var vendor in rates){
                    if(rates[vendor].rate_type != 'fixed'){
                        if(typeof rates[vendor].rates[currency] != 'undefined'){
                            value = rates[vendor].rates[currency].value.buy.toFixed(2) + ' / ' + rates[vendor].rates[currency].value.sale.toFixed(2);
                        }else{
                            value = String.fromCharCode(8212);
                        }
                    }else{
                        if(typeof rates[vendor].rates[currency] != 'undefined'){
                            value = rates[vendor].rates[currency].value.value.toFixed(2);
                        }else{
                            value = String.fromCharCode(8212);
                        }
                    }
                    tds.push(<td>{value}</td>);
                }
                rows.push(
                    <tr>
                        {tds}
                    </tr>
                );
            }
            switch (columnsLength){
                case 1:
                    tableClass = tableClass + ' one-column';
                    break;
                case 2:
                    tableClass = tableClass + ' two-column';
                    break;
                case 3:
                    tableClass = tableClass + ' tree-column';
                    break;
                case 4:
                    tableClass = tableClass + ' four-column';
                    break;
                case 5:
                    tableClass = tableClass + ' five-column';
                    break;
            }
            return(
                <div className="rates-table-wrap">
                    <table id="ex-table" className={tableClass}>
                        <thead>
                        <tr>
                            {headers}
                        </tr>
                        </thead>
                        <tbody>
                        {rows}
                        </tbody>
                    </table>
                    <div className="rates-filters-and-calendar-wrap">
                        <div className="rates-table-calendar">
                            <div className="rates-table-calendar-wrap">
                                <span className="calendar-title">Дата</span>
                                <div className="calendar-text">
                                    {this.state.calendarDateText}
                                </div>
                                <div className="calendar-trigger ru-calendar" onClick={this.showCalendar}>
                                </div>

                                <div className="calendar-dropdown dropdown" onClick={this.stopPropagation}>
                                    <DateTime
                                        ref="table_date"
                                        open={true}
                                        viewMode={'days'}
                                        input={false}
                                        locale={'ru'}
                                        onChange={function (arg) {
                                            self.setCalendarDate(arg);
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="rates-table-filters">
                            <div className="rates-table-filters-wrap" onClick={this.toggleFilters}>
                                <div className="ru-settings"></div>
                                <div className="rates-filter-dropdown dropdown">
                                    <div className="dropdown-arrow"></div>
                                    <div className="parameters">
                                        <p>Колонки</p>
                                        <div className="form-group">
                                            <input type="checkbox"
                                                   id="nbu"
                                                   name="nbu"
                                                   checked={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:nbu')}
                                            />
                                            <label
                                                onClick={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:nbu') ? () => {
                                                    this.removeFilter('aliases:nbu');
                                                } : () => {
                                                    this.addFilter('aliases:nbu');
                                                }}
                                                className="checkbox-label"
                                                htmlFor="nbu">Нацбанк</label>
                                        </div>
                                        <div className="form-group">
                                            <input
                                                checked={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:pat_kb_privatbank_bank')}
                                                type="checkbox" id="pat_kb_privatbank_bank" name="pat_kb_privatbank_bank"/>
                                            <label
                                                onClick={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:pat_kb_privatbank_bank') ? () => {
                                                    this.removeFilter('aliases:pat_kb_privatbank_bank');
                                                } : () => {
                                                    this.addFilter('aliases:pat_kb_privatbank_bank');
                                                }}
                                                className="checkbox-label" htmlFor="pat_kb_privatbank_bank">Приват Банк</label>
                                        </div>
                                        <div className="form-group">
                                            <input
                                                checked={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:pat_kb_privatbank_card')}
                                                type="checkbox" id="pat_kb_privatbank_card" name="pat_kb_privatbank_card"/>
                                            <label
                                                onClick={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:pat_kb_privatbank_card') ? () => {
                                                    this.removeFilter('aliases:pat_kb_privatbank_card');
                                                } : () => {
                                                    this.addFilter('aliases:pat_kb_privatbank_card');
                                                }}
                                                className="checkbox-label" htmlFor="pat_kb_privatbank_card">Приват Банк Карточки</label>
                                        </div>
                                        <div className="form-group">
                                            <input
                                                checked={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:visa_mastercard')}
                                                type="checkbox" id="visa_mastercard" name="visa_mastercard"/>
                                            <label
                                                onClick={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:visa_mastercard') ? () => {
                                                    this.removeFilter('aliases:visa_mastercard');
                                                } : () => {
                                                    this.addFilter('aliases:visa_mastercard');
                                                }}
                                                className="checkbox-label" htmlFor="visa_mastercard">Visa/Mastercard</label>
                                        </div>
                                        <div className="form-group">
                                            <input
                                                checked={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:p2p')}
                                                type="checkbox" id="p2p" name="p2p"/>
                                            <label
                                                onClick={this.contains(this.props.rates.official.meta['filter[]'], 'aliases:p2p') ? () => {
                                                    this.removeFilter('aliases:p2p');
                                                } : () => {
                                                    this.addFilter('aliases:p2p');
                                                }}
                                                className="checkbox-label" htmlFor="p2p">Наличные</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <h3>Loading...</h3>
            )
        }


    }
}


function mapDispatchToProps(dispatch) {
    return {...bindActionCreators(ActionCreators, dispatch), dispatch};
}

function mapStateToProps(state) {
    return {
        locale: state.locale,
        rates: state.rates,
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Official);


