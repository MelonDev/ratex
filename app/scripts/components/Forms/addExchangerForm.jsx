import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import jQuery from 'jquery';
var $ = jQuery;

class ContactForm extends Component {

    constructor(){
        super();
        this.state = {
            formValid: false
        }
        this.getValues = this.getValues.bind(this);
        // this.validateForm = this.validateForm.bind(this);
    }


    getValues(e){
        var target = e.target;
        var inputs = $(target).closest('form').serializeArray();
        var inputsWithData = inputs.filter((elem, index)=>{
            if(elem.value.length > 0){
                return true;
            }
            else{
                return false
            }
        });
        if(inputsWithData.length == 9){
            this.setState({
                formValid: true
            });
        }else if(this.state.formValid && inputsWithData.length != 9){
            this.setState({
                formValid: false
            });
        }
    }

    showTooltip(e){
        var target = e.target;
        $(target).closest('.form-group').find('.tooltip-form').addClass('visible');
    }


    hideTooltip(e){
        var target = e.target;
        $(target).closest('.form-group').find('.tooltip-form').removeClass('visible');
    }



    render() {
        const { handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit} onChange={this.getValues}>

                <div className="form-group">
                    <Field   className="form-control" placeholder="Имя администратора" name="name" component="input" type="text"/>
                </div>
                <div className="form-group">
                    <Field   className="form-control" placeholder="Имя администратора" name="email" component="input" type="text"/>
                </div>
                <div className="form-group">
                    <Field   className="form-control" onFocus = {this.showTooltip} onBlur = {this.hideTooltip} placeholder="Имя администратора" name="phone" component="input" type="text"/>
                    <div className="tooltip-form">
                        Подсказка!
                    </div>
                </div>
                <div className="form-group">
                    <Field   className="form-control"  placeholder="Имя администратора" name="projectName" component="input" type="text"/>
                </div>
                <div className="form-group">
                    <Field  className="form-control"  placeholder="Имя администратора" name="siteUrl" component="input" type="text"/>
                </div>
                <div className="form-group">
                    <Field   className="form-control"  placeholder="Имя администратора" name="exportUrl" component="input" type="text"/>
                </div>
                <div className="form-group">
                    <Field  className="form-control" placeholder="Имя администратора" name="WMID" component="input" type="text"/>
                </div>
                <div className="form-group">
                    <Field   className="form-control" placeholder="Имя администратора" name="age" component="textarea" type="text"/>
                </div>
                <div className="form-group">
                    <Field   className="form-control" placeholder="Имя администратора" name="country" component="input" type="text"/>
                </div>
                <button disabled = {!this.state.formValid} className="btn" type="submit">Submit</button>
            </form>
        );
    }
}

ContactForm = reduxForm({
    form: 'contact' // a unique name for this form
})(ContactForm);

export default ContactForm;