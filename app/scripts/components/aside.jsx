import React from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import jQuery from 'jquery';
import { hideMenu } from '../reusable/extendingFunctions';
import SearchInput from '../components/searchInput.jsx';

const $ = jQuery;

export default class Aside extends React.Component {
  /**
   * Runs on CLient
   */
  componentDidMount() {
    $('.directory-trigger').on('click', function (e) {
      e.preventDefault();
      $(this).closest('nav').addClass('hidden');
      $('.directory').removeClass('hidden');
    });

    $('.back-from-directory').on('click', function () {
      $(this).closest('nav').addClass('hidden');
      $('.main-nav').removeClass('hidden');
    });
  };

  /**
   * Runs on server and Client
   * @returns {XML}
   */
  render() {
    const headerClasses = classnames('clearfix layout-aside', { notransition: true });
    const locale = this.props.locale.language;
    if (locale !== '') {
      return (
        <aside className={headerClasses}>
            <div className="container aside-container">
                <SearchInput />
                <nav className="clearfix main-nav">
                    <div id="rates-menu" className="aside-nav-item" onClick={() => {
                      hideMenu();
                    }}>
                        <Link to="/rates/cash/" activeClassName="test"><span
                          className="icon rm-currency-rate"></span><span
                          className="aside-link-text">{locale.t('_menu', { returnObjects: true }).rates}</span></Link>
                    </div>
                    <div id="exchangers-menu" className="aside-nav-item" onClick={() => {
                      hideMenu();
                    }}>
                        <Link to="/exchangers" activeClassName="test"><span
                          className="icon rm-exchangers"></span><span
                          className="aside-link-text">{locale.t('_menu', { returnObjects: true }).exchangers}</span></Link>
                    </div>
                    <div id="market-menu" className="aside-nav-item" onClick={() => {
                      hideMenu();
                    }}>
                        <Link to="/black-market" activeClassName="test"><span
                          className="icon rm-exchange-market"></span><span
                          className="aside-link-text">{locale.t('_menu', { returnObjects: true }).market}</span></Link>
                    </div>
                    <div id="for-developers-menu" className="aside-nav-item green">
                        <a href="https://docs.ratex.io" target="_blank" activeClassName="test"><span
                          className="icon rm-developers"></span><span
                          className="aside-link-text">{locale.t('_menu', { returnObjects: true }).for_developers}</span></a>
                    </div>
                    <div id="about-menu" className="aside-nav-item" onClick={() => {
                      hideMenu();
                    }}>
                        <Link to="/about" activeClassName="test"><span className="icon rm-about"></span><span
                          className="aside-link-text">{locale.t('_menu', { returnObjects: true }).about}</span></Link>
                    </div>
                    <div id="directory-menu" className="aside-nav-item" onClick={() => {
                      hideMenu();
                    }}>
                        <Link to="/currencies" className="directory-trigger"><span className="icon rm-info"></span><span
                          className="aside-link-text">Валюты</span></Link>
                    </div>
                    <div id="partnership-menu" className="aside-nav-item in-developing" onClick={() => {
                      hideMenu();
                    }}>
                        <div className="in-dev-block">{locale.t('_menu', { returnObjects: true }).in_development}</div>
                        <Link to="/guilist" activeClassName="test"><span
                          className="icon rm-cooperation"></span><span
                          className="aside-link-text">{locale.t('_menu', { returnObjects: true }).partnership}</span></Link>
                    </div>
                    <div id="powered-by-menu" className="aside-nav-item copyright-item">
                        <a href="https://www.paymaxi.com/" target="_blank"><span
                          className="icon rm-powered-by-paymaxi"></span><span className="aside-link-text">Powered by PayMaxi</span></a>
                    </div>
                </nav>
                <nav className="clearfix directory hidden">
                    <div className="aside-nav-item">
                        <a href="#" className="back-from-directory"><span className="icon ru-arrow"></span><span
                          className="aside-link-text">Назад</span></a>
                    </div>
                    <div className="aside-nav-item">
                        <Link to="exchangers" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Страны</span></Link>
                    </div>
                    <div className="aside-nav-item">
                        <Link to="dashboard" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Валюты</span></Link>
                    </div>
                    <div className="aside-nav-item">
                        <Link to="learn" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Вендоры</span></Link>
                    </div>
                    <div className="aside-nav-item">
                        <Link to="market" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Платежные системы</span></Link>
                    </div>
                    <div className="aside-nav-item">
                        <a href="#" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Банки</span></a>
                    </div>
                    <div className="aside-nav-item">
                        <Link to="guilist" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Обменники</span></Link>
                    </div>
                    <div className="aside-nav-item ">
                        <Link to="learn" activeClassName="test"><span className="icon rm-info"></span><span
                          className="aside-link-text">Мониторинги</span></Link>
                    </div>
                </nav>
            </div>
            <div className="background"></div>
        </aside>
      );
    } else {
      return (
        <div></div>
      );
    }
  };
}
