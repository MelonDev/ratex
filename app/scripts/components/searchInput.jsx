import React from 'react';
import ReactDOM from "react-dom";
import classnames from 'classnames';

class SearchInput extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="search-input-wrap">
                <input type="search" className="searchInput"/>
                <span className="icon-search"></span>
            </div>
        );
    }
}
export default SearchInput;