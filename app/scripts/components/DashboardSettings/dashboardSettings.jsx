import React from 'react';
import {SimpleSelect} from 'react-selectize';
import {hideSettingsModal} from '../../reusable/extendingFunctions';
import * as ActionCreators from "../../actions/config/configActions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";


class DashboardSettings extends React.Component{
    constructor(){
        super();
        this.changeProperty = this.changeProperty.bind(this);
        this.changeConfig = this.changeConfig.bind(this);
        this.settings = {};
        this.quoteCurrencies = ['UAH', 'USD'];
        this.refresh = [1, 5, 10];
        this.decimals = [1, 2, 3, 4, 5, 6, 7, 8 ];
        this.delimiters =  [',', '.'];
        this.timesone = ['utc+2'];
        this.defaultState = {
            decimalsInNumbers : 2,
            quote_currency: 'UAH',
            refresh: 5,
            timezone: 'UTC+2'
        };
        this.state = {
        }
    }


    changeProperty(elem, property){

        this.defaultState[property] = elem.value;

    }

    changeConfig(e){
        e.preventDefault();
        this.props.changeConfig(this.defaultState);
        hideSettingsModal();
    }



    render(){

       var currencies = this.quoteCurrencies.map((elem, index)=>{
           return {label: elem, value: elem};
       });
       var refresh = this.refresh.map((elem,index)=>{
           return {label:'через ' + elem + ' секунд', value: elem}
       });
       var decimals = this.decimals.map((elem, index)=>{
           return {label:elem.toString(), value: elem};
       });
       var timesones = this.timesone.map((elem, index)=>{
          return {label: elem.toString(), value: elem}
       });

        return(
            <div className='settings-modal'>
                <div className="backdrop">

                </div>

                    <div className="modal-cont">
                            <div className="modal-header">Персональные настройки
                                <div className="x-cont" onClick={hideSettingsModal}><span className="ru-menu-close"></span></div>
                            </div>
                            <div className="modal-descr">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam asperiores aspernatur, beatae consequuntur, corporis dignissimos ducimus harum inventore magnam maxime natus perferendis quaerat quisquam quos repellat soluta sunt temporibus voluptatum?
                            </div>
                            <div className="modal-content">
                                <form action="" onSubmit={this.changeConfig}>
                                    {/*<div className="form-group">*/}
                                        {/*<span className="label">Обновление</span>*/}
                                        {/*<SimpleSelect onValueChange={(value)=>{this.changeProperty(value, 'refresh')}}  options = {refresh}   defaultValue={{value:this.props.config.refresh, label:this.props.config.refresh }} />*/}
                                    {/*</div>*/}
                                    <div className="form-group">
                                        <span className="label">Дробных знаков</span>
                                        <SimpleSelect onValueChange={(value)=>{this.changeProperty(value, 'decimalsInNumbers')}}  options = {decimals}   defaultValue={{value:this.props.config.decimalsInNumbers, label:this.props.config.decimalsInNumbers }} />
                                    </div>
                                    <div className="btns-block">
                                        <button  className="btn btn-primary btn-gray">Сбросить настройки</button>
                                        <button type="submit" className="btn btn-primary btn-exchange ">Сохранить настройки</button>
                                    </div>
                                </form>

                            </div>
                    </div>
            </div>
        )


    }


}

function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};
}


function mapStateToProps(state, props) {


    return {
        locale: state.locale,
        config: state.config,

    }




}
export default connect(mapStateToProps, mapDispatchToProps)(DashboardSettings);