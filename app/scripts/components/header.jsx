import React from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import {showSettingsModal} from '../reusable/extendingFunctions';

export default class Header extends React.Component {

  constructor(props){

    super(props);
    this.state = {
      openUserDrop : false
    };
    this.openUserDrop = this.openUserDrop.bind(this);
    this.openMenu = this.openMenu.bind(this);

  }


  openMenu(){
    $('.burger').toggleClass('open');
    $('.layout-aside').toggleClass('open');
    $('.layout-header').toggleClass('open');
  }

  showTour(){
    $('.welcome-guide-wrap').addClass('visible');

  }

  openUserDrop(){

    if (!this.state.openUserDrop) {

      this.setState({
        openUserDrop : true
      });

    } else {

      this.setState({
        openUserDrop : false
      });

    }

  }


  render() {
    // let burgerClasses = classnames('burger', {open:this.props.menu.open});
    // let menuClasses = classnames('clearfix layout-header', {open:this.props.menu.open});
    // let profileClasses = classnames('profile-btn', {open:this.state.openUserDrop});
    return (
        <header className='clearfix layout-header'>
          <div className="container">
            <div className="left-block">
              <div className='burger' onClick={this.openMenu}>
                <div className="burger-icon"></div>
              </div>
            </div>
            <nav className="clearfix">
              <div className="nav-item logo-block">
                <Link to="/">
                  <div className="main-logo"><div className="icon-logo-gray"></div>
                    <div className="ru-tour" onClick={this.showTour}></div>
                    {/*<img src="images/icon_alpha.svg" alt=""/>*/}
                  </div>
                </Link>
              </div>
            </nav>
            <div className="settings-wrap" onClick={showSettingsModal}>
              <div className="ru-settings"></div>
            </div>

              {/* <div className='profile-btn' onClick = {this.openUserDrop}> */}
              {/*   <div className="icon-profile icon-user-top"></div> */}
              {/*<div className="notification-num">3</div>*/}
              {/*<div className="user-drop-menu">*/}
                {/*<ul>*/}
                  {/*<li>Настройки</li>*/}
                {/*</ul>*/}
              {/*</div>*/}
            {/*</div>*/}
          </div>
        </header>
    )
  }
};


