import React from "react";
import jQuery from "jquery";
import FastExchange from "./CardTypes/fastExchange.jsx";
import FluctuationsSchedule from "./CardTypes/fluctuationsSchedule.jsx";
import PopularDirections from "./CardTypes/popularDirections.jsx";
import TopExchangers from "./CardTypes/topExchangers.jsx";
import News from "./CardTypes/News.jsx";
import SingleExchanger from "./CardTypes/singleExchanger.jsx";
import RatexStats from "./CardTypes/ratexStats.jsx";
import FavoriteExchangers from "./CardTypes/favoriteExchangers.jsx";
import FavoriteDirections from "./CardTypes/favoriteDirections.jsx";
import BlackMarketOverview from "./CardTypes/blackMarketOverview.jsx";
import NationalBankRates from "./CardTypes/nationalBankRates.jsx";
import CalculatorPro from "./CardTypes/calculatorPro.jsx";
import InternationalPaymentSystemsRates from "./CardTypes/internationalPaymentSystemsRates.jsx";
import AverageVisaMastercardRates from "./CardTypes/averageVisaMastercardRates.jsx";
import PrivatBankRates from './CardTypes/privatBankRates.jsx';
import PrivatCardRates from './CardTypes/privatCardRatex.jsx';
import VisaRates from './CardTypes/visaRates.jsx';
import MastercardRates from './CardTypes/mastercardRates.jsx';


var $ = jQuery;

//Card types

class Card extends React.Component {
    constructor(props) {
        super();
        this.state = {
            show: true,
            packery: props.packery,
            title: props.title,
            size: props.size
        };
    }

    click() {
        const big = !this.state.big;
        this.setState({big});
    }

    deleteC() {
        const show = !this.state.show;
        this.setState({show});
    }

    contains(array, needle) {
        for (var i = 0; i < array.length; i++) {
            if (array[i].type == needle) {
                return true;
            }
        }
        return false;
    }

    componentWillReceiveProps(props) {
        this.state.packery = props.packery;
    }

    discoverType(type) {

        switch (type) {
            case 'fast_exchange':
                return <FastExchange dataId={this.props.dataId}
                                     countFromChangeFastExchange={this.props.countFromChangeFastExchange}
                                     countToChangeFastExchange={this.props.countToChangeFastExchange}
                                     getDataForFastExchangeWidget={this.props.getDataForFastExchangeWidget}
                                     getExchangerTooltip={this.props.getExchangerTooltip}
                                     fastExchangeChangeDirection={this.props.fastExchangeChangeDirection}
                                     addFavoriteExchanger={this.props.addFavoriteExchanger}
                                     removeFavoriteExchanger={this.props.removeFavoriteExchanger}
                                     getCurrenciesList={this.props.getCurrenciesList}
                                     deleteWidget={this.props.deleteWidget}/>;
                break;


            case 'top_currency_pairs':
                return <PopularDirections getCurrPairTooltip={this.props.getCurrPairTooltip}
                                          getCurrencyTooltip={this.props.getCurrencyTooltip} dataId={this.props.dataId}
                                          deleteWidget={this.props.deleteWidget}
                                          changeDirectionsLimit={this.props.changeDirectionsLimit}/>;
                break;

            case 'news':
                return <News dataId={this.props.dataId} deleteWidget={this.props.deleteWidget}/>;
                break;

            case 'average_visa_mastercard_rates':
                return <AverageVisaMastercardRates
                        deleteWidget={this.props.deleteWidget}
                        dataId={this.props.dataId}
                />;
                break;
            case 'ratex_stats':
                return <RatexStats deleteWidget={this.props.deleteWidget} dataId={this.props.dataId}/>;
                break;

            case 'national_bank_rates':
                // return (<div></div>);

                return <NationalBankRates locale={this.props.locale}
                                          deleteWidget={this.props.deleteWidget} dataId={this.props.dataId}
                                          changeNationalBankInputAmount={this.props.changeNationalBankInputAmount}
                                          getNationalBankRates={this.props.getNationalBankRates}
                                          config={this.props.config}/>;
                break;
            case 'national_bank_fluctuations':
                return <FluctuationsSchedule dashInitialState={this.props.dashInitialState} locale={this.props.locale}
                                             deleteWidget={this.props.deleteWidget} dataId={this.props.dataId}
                                             packeryProps={this.props.packeryProps}
                                             getNationalBankRates={this.props.getNationalBankRates}
                                             config={this.props.config}/>;
                break;

            case 'top_exchangers':
                return <TopExchangers addFavoriteExchanger={this.props.addFavoriteExchanger}
                                      getExchangerTooltip={this.props.getExchangerTooltip}
                                      deleteWidget={this.props.deleteWidget} dataId={this.props.dataId}
                                      changeTopExchangersLimit={this.props.changeTopExchangersLimit}/>;
                break;

            case 'exchanger_card':
                return <SingleExchanger getExchanger={this.props.getExchanger} dataId={this.props.dataId}/>;
                break;
            case 'calculator_pro':
                return <CalculatorPro deleteWidget={this.props.deleteWidget} dataId={this.props.dataId}
                                      getCalculatorPro={this.props.getCalculatorPro}
                />;
                break;
            case 'favorite_exchangers':
                return <FavoriteExchangers deleteWidget={this.props.deleteWidget} dataId={this.props.dataId} />;
                break;
            case 'black_market_rates':
                return <BlackMarketOverview dataId={this.props.dataId} deleteWidget={this.props.deleteWidget}/>;
                break;
            // case 'favorite_currency_pairs':
            //     return <FavoriteDirections  {...this.props} />;
            //     break;
            case 'privat_bank_rates':
                return <PrivatBankRates
                    dataId={this.props.dataId}
                    deleteWidget={this.props.deleteWidget}/>;
            case 'privat_card_rates':
                return <PrivatCardRates
                    dataId={this.props.dataId}
                    deleteWidget={this.props.deleteWidget}/>;
            case 'visa_rates':
                return <VisaRates
                    dataId={this.props.dataId}
                    deleteWidget={this.props.deleteWidget}/>;
            case 'mastercard_rates':
                return <MastercardRates
                    dataId={this.props.dataId}
                    deleteWidget={this.props.deleteWidget}/>;
            // case 'card_rates' :
            //     return <InternationalPaymentSystemsRates dataId={this.props.dataId}
            //                                              deleteWidget={this.props.deleteWidget}/>;
                break;

            default:
                return false;

        }

    }

    componentDidMount() {
        var self = this;
    }


    render() {
        let type = this.discoverType(this.props.type);

        if (this.props.type != '') {
            return (
                <div>
                    {type}
                </div>
            )
        } else {
            return (
                <div className="card" onClick={() => {
                    this.props.deleteWidget(this.props.dataId)
                }}>
                    {this.props.type}
                </div>
            )
        }

    }
}
export default Card;