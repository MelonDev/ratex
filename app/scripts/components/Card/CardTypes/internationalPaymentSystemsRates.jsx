import React from "react";
import jQuery from "jquery";
import * as ActionCreators from "../../../actions/widgets/internationalPaymentSystemsRatesActions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

var $ = jQuery;


class InternationalPaymentSystemsRates extends React.Component {
    constructor(props) {
        super(props);
        this.changeInputAmount = this.changeInputAmount.bind(this);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.state = {
            inputValue: 100,
            rates: null,
            showActive: false,
            showTooltip: false
        }

    }

    changeInputAmount(event) {

        let value = event.target.value.replace(/\D+/g, '');
        value = value.replace('.', "");
        let inputCharLength = value.length;
        let domElem = this.refs.input;


    }

    tooltipToggle(e) {

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
        $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');

    }


    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    componentDidMount() {

    }

    componentDidUpdate() {

    }

    componentWillMount() {

        this.props.getDataForInterantionalPaymentSystemRatesWidget(this.props.dataId);



    }

    render() {


        let locale = this.props.locale.language;
        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility: this.state.showTooltip ? 'visible' : 'hidden'
        };
        const tooltipStyle = {
            display: this.state.showTooltip ? 'block' : 'none'
        };
        const tooltip = (
            <div className="tooltip" style={tooltipStyle}>
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                    <div className="tooltip-title">Управление виджетом</div>
                </div>
                <div className="delete-widget" onClick={() => {
                    this.props.deleteWidget(this.props.dataId)
                }}>
                    <a href="#">удалить</a>
                </div>
            </div>
        );

        if (typeof this.props.cardRates != 'undefined' && typeof this.props.cardRates[this.props.dataId] != 'undefined') {
            var cardRates = this.props.cardRates[this.props.dataId],
                rates = cardRates.rates;

            var ratesMapped = rates.map((elem, index) => {
                return (
                    <tr key={index}>
                        <td className="gray-curr"><span className="gray-curr">{elem.base_currency}</span></td>
                        <td>{elem.buy}<span className={'delta negative'}>{elem.buy_delta ? Math.ceil(elem.buy_delta * 100) / 100 : ''}</span></td>
                        <td>{elem.total_buy}</td>
                        <td>{elem.amount_buy}</td>
                        <td>{elem.sale} <span className={'delta negative'}>{elem.sale_delta ? Math.ceil(elem.sale_delta * 100) / 100 : ''}</span></td>
                        <td>{elem.total_sale}</td>
                        <td>{elem.amount_sale}</td>
                    </tr>
                )
            });

            return (
                <div data-item-id={this.props.dataId} className='card national-bank-rates' style={ {width: '280px'} }>
                    <div className="card-header">
                        <div className="card-title">
                            Курсы в МПС
                            <div className="setting-wrap" onClick={this.tooltipToggle}>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div className="preloader-wrapped">
                                    <div className="preloader">
                                        <div className="circ1"></div>
                                        <div className="circ2"></div>
                                        <div className="circ3"></div>
                                    </div>
                                </div>
                                <div data-tooltip={this.props.dataId} className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={() => {
                                        this.props.deleteWidget(this.props.dataId)
                                    }}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-content">
                        <div className="national-bank-rates-wrap">
                            <div className="amount-result">
                                <table>
                                    <tbody>
                                    {ratesMapped}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {


            return (
                <div data-item-id={this.props.dataId} className='card national-bank-rates' style={ {width: '280px'} }>
                    <div className="card-header">
                        <div className="setting-wrap" onClick={this.tooltipToggle}>
                        </div>
                    </div>
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>
                </div>
            )


        }
    }

}
function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};
}


function mapStateToProps(state, props) {


        return {
            locale: state.locale,
            cardRates: state.dashInitialState.dash.card_rates,
            widgetsSet: state.widgetsSet
        }




}
export default connect(mapStateToProps, mapDispatchToProps)(InternationalPaymentSystemsRates);