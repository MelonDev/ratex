import React from 'react';
import * as ActionCreators from '../../../actions/widgets/calculatorProActions';
import CurrpairSelectize from '../../SelectComponents/currpairSelectize.jsx';
import { connect } from 'react-redux';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import DateTime from 'react-datetime';


class CalculatorPro extends React.Component {
  constructor(props) {
    moment.locale(props.locale.language.language);
    super(props);
    this.tooltipToggle = this.tooltipToggle.bind(this);
    this.showCalendar = this.showCalendar.bind(this);
    this.hideCalendar = this.hideCalendar.bind(this);
    this.changeInputValue = this.changeInputValue.bind(this);
    this.showCurPairs = this.showCurPairs.bind(this);
    this.hideCurPairs = this.hideCurPairs.bind(this);
    this.changeDirection = this.changeDirection.bind(this);
    this.changeDate = this.changeDate.bind(this);
    this.props = props;
    this.delayTimer = 0;
    this.currencyPairs = [
      {
        base_currency: 'USD',
        quote_currency: "UAH",
        currency_pair_id: 'USD/UAH',
        base_currency_icon_status: 'n',
        quote_currency_icon_status: 'n'
      },
      {
        base_currency: 'EUR',
        quote_currency: "UAH",
        currency_pair_id: 'EUR/UAH',
        base_currency_icon_status: 'n',
        quote_currency_icon_status: 'n'
      },
      {
        base_currency: 'RUB',
        quote_currency: "UAH",
        currency_pair_id: 'RUB/UAH',
        base_currency_icon_status: 'n',
        quote_currency_icon_status: 'n'
      },
      {
        base_currency: 'UAH',
        quote_currency: "USD",
        currency_pair_id: 'UAH/USD',
        base_currency_icon_status: 'n',
        quote_currency_icon_status: 'n'
      },
      {
        base_currency: 'EUR',
        quote_currency: "USD",
        currency_pair_id: 'EUR/USD',
        base_currency_icon_status: 'n',
        quote_currency_icon_status: 'n'
      },
      {
        base_currency: 'RUB',
        quote_currency: "USD",
        currency_pair_id: 'RUB/USD',
        base_currency_icon_status: 'n',
        quote_currency_icon_status: 'n'
      },
    ]
    this.currpairHashTable = {
      'USD-UAH': {},
      'EUR-UAH': {},
      'RUB-UAH': {},
      'UAH-USD': {},
      'EUR-USD': {},
      'RUB-USD': {},
    };
    this.currencyList = {
      data: [
        {
          code: 'USD',
          icon: 'us',
          icon_status: 'n',
          type: 'national'
        },
        {
          code: 'UAH',
          icon: 'ua',
          icon_status: 'n',
          type: 'national'
        },
        {
          code: 'RUB',
          icon: 'ru',
          icon_status: 'n',
          type: 'national'
        },
        {
          code: 'EUR',
          icon: 'eu',
          icon_status: 'n',
          type: 'national'
        },
      ]
    };
    this.state = {
      inputValue: 100,
      calendarDate: moment().format('D MMMM')
    }
  }

  tooltipToggle(e) {
    e.stopPropagation();
    let id = this.props.dataId;
    $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
    $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');
  }

  componentWillMount() {
    this.props.getDataForCalculatorPro(this.props.dataId);
  }

  componentDidUpdate() {
    //not implemented yet
  }


  showCurPairs(e) {
    var id = this.props.dataId;
    e.stopPropagation();
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap').toggleClass('hidden');
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap').addClass('open');
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap .resizable-input').focus();
    $('.fast-exchange[data-item-id="' + id + '"]').addClass('pop-up-active');
  }


  hideCurPairs() {
    var id = this.props.dataId;
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap').addClass('hidden');
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap').removeClass('open');
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap .resizable-input').blur();
    $('.fast-exchange[data-item-id="' + id + '"]').removeClass('pop-up-active');
  }


  changeInputValue(e) {
    var eventPersisted = e.target.value,
        test = /^([\d]*[,|\.]?[\d]*)$/.test(eventPersisted);
    if(!test || eventPersisted.toString().replace('.', '').replace(',', '').length > 4){
      return;
    }

    var calculatorPro = this.props.calculatorPro[this.props.dataId];
    var val = e.target.value;
    var baseCurrency = calculatorPro.selected_direction.base_code;
    var quoteCurrency = calculatorPro.selected_direction.quote_code;
    this.setState({
      inputValue: val
    });
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      this.props.countFromChangeCalculatorPro(this.props.dataId, baseCurrency, quoteCurrency, val.replace(',', '.'));

    }, 500);
  }

  changeDirection(values) {
    this.props.calculatorProChangeDirection(values, this.props.dataId);
  }

  changeDate(date) {

    var calculatorPro = this.props.calculatorPro[this.props.dataId];
    var baseCurrency = calculatorPro.selected_direction.base_code;
    var quoteCurrency = calculatorPro.selected_direction.quote_code;
    this.setState({
      calendarDate: moment(date).format('D MMMM')
    });
    var backendFormattedDate = moment(date).format('YYYY-MM-DD');

    this.props.calculatorProChangeDate(backendFormattedDate, baseCurrency, quoteCurrency, this.props.dataId);

  }

  showCalendar(e) {

    e.stopPropagation();
    $('.card[data-item-id="' + this.props.dataId + '"] .calendar-dropdown').addClass('visible');

  }

  hideCalendar() {

    $('.card[data-item-id="' + this.props.dataId + '"] .calendar-dropdown').removeClass('visible');

  }

  render() {
    let locale = this.props.locale.language;
    const iconStyle = {
      opacity: 0,
      visibility: 'hidden'
    };
    try {
      if (typeof this.props.calculatorPro != 'undefined' && typeof this.props.calculatorPro[this.props.dataId] != 'undefined' && typeof this.props.calculatorPro[this.props.dataId].rate != 'undefined') {
        var calculatorPro = this.props.calculatorPro[this.props.dataId];
        var baseCurrency = calculatorPro.selected_direction.base_code;
        var quoteCurrency = calculatorPro.selected_direction.quote_code;
        var rates = calculatorPro.rate;
        var decimals = this.props.config.decimalsInNumbers;
        var emptyValue = String.fromCharCode(8212);
        var rowsNational = (
          <tbody>
          <tr key={1}>
            <td>{locale.t('_dash', { returnObjects: true }).calculator_pro.nbu}</td>
            <td>{rates.nbu_rate != null ? rates.nbu_rate.value.toFixed(decimals) : emptyValue}</td>
            <td>{rates.nbu_rate != null ? rates.nbu_rate.out_amount.toFixed(decimals) : emptyValue}</td>
          </tr>
          <tr key={2}>
            <td>Средний курс</td>
            <td>{rates.black_market_rate != null ? rates.black_market_rate.buy : emptyValue} / {rates.black_market_rate != null ? rates.black_market_rate.sale.toFixed(decimals) : emptyValue}</td>
            <td>{rates.black_market_rate != null ? rates.black_market_rate.out_buy_amount.toFixed(decimals) : emptyValue} / { rates.black_market_rate != null ? rates.black_market_rate.out_sale_amount.toFixed(decimals) : emptyValue}</td>
          </tr>
          <tr key={4}>
            <td>Privat Bank</td>
            <td>{rates.bank_rate != null ? rates.bank_rate.buy.toFixed(decimals) : emptyValue} / {rates.bank_rate != null ? rates.bank_rate.sale.toFixed(decimals) : emptyValue}</td>
            <td>{rates.bank_rate != null ? rates.bank_rate.out_buy_amount.toFixed(decimals) : emptyValue} / {rates.bank_rate != null ? rates.bank_rate.out_sale_amount.toFixed(decimals) : emptyValue}</td>
          </tr>
          </tbody>

        );
        var rowsCards = (
          <tbody>
          <tr key={1}>
            <td>Visa</td>
            <td>{rates.visa_rate != null ? rates.visa_rate.buy.toFixed(decimals) : emptyValue} / { rates.visa_rate != null ? rates.visa_rate.sale.toFixed(decimals) : emptyValue}</td>
            <td>{rates.visa_rate != null ? rates.visa_rate.out_buy_amount.toFixed(decimals) : emptyValue} / {rates.visa_rate != null ? rates.visa_rate.out_sale_amount.toFixed(decimals) : emptyValue}</td>
          </tr>
          <tr key={2}>
            <td>MasterCard</td>
            <td>{ rates.mastercard_rate != null ? rates.mastercard_rate.buy.toFixed(decimals) : emptyValue} / { rates.mastercard_rate != null ? rates.mastercard_rate.sale.toFixed(decimals) : emptyValue}</td>
            <td>{ rates.mastercard_rate != null ? rates.mastercard_rate.out_buy_amount.toFixed(decimals) : emptyValue} / { rates.mastercard_rate != null ? rates.mastercard_rate.out_sale_amount.toFixed(decimals) : emptyValue}</td>
          </tr>
          <tr key={3}>
            <td>Visa/Mastercard</td>
            <td>{rates.visa_mastercard_average_rate != null ? rates.visa_mastercard_average_rate.buy.toFixed(decimals) : emptyValue} / {rates.visa_mastercard_average_rate != null ? rates.visa_mastercard_average_rate.sale.toFixed(decimals) : emptyValue}</td>
            <td>{rates.visa_mastercard_average_rate != null ? rates.visa_mastercard_average_rate.out_buy_amount.toFixed(decimals) : emptyValue} / {rates.visa_mastercard_average_rate != null ? rates.visa_mastercard_average_rate.out_sale_amount.toFixed(decimals) : emptyValue}</td>
          </tr>
          </tbody>
        );
        return (
          <div data-item-id={this.props.dataId} style={{ width: 580, minHeight: 561 }}
               className='card calculator-pro-widget fast-exchange no-min-height'>
            <div className="card-header">
              <div className="card-title">
                  {locale.t('_dash', { returnObjects: true }).calculator_pro.title}
                <div className="setting-wrap" onClick={this.tooltipToggle}>
                  <span style={iconStyle} className="icon ru-settings"></span>
                  <div data-tooltip={this.props.dataId} className="tooltip">
                    <div className="tooltip-arrow"></div>
                    <div className="tooltip-inner">
                      <div
                        className="tooltip-title"> {locale.t('_dash', { returnObjects: true }).general.widget_management}</div>
                    </div>
                    <div className="delete-widget" onClick={() => {
                      this.props.deleteWidget(this.props.dataId);
                    }}>
                      <a href="#"> {locale.t('_dash', { returnObjects: true }).general.delete}</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="card-content">
              <div className="direction-wrap">
                <div className="direction-block">
                  <div className="amount-input">
                    <input value={this.state.inputValue} onChange={this.changeInputValue}
                           ref="count_from_input" pattern="[0-9]+([\.][0-9]+)?" type="text"/>
                  </div>
                  <div className="direction-wrap" onClick={this.showCurPairs}>
                    <div ref="selectedCurrencyFrom" className="selected-currency-from">
                      <div className="clicked-area" onClick={this.showDropFrom}>
                        <span className="dotted">{baseCurrency}</span>
                      </div>
                    </div>
                    <div className="divider">
                      <div className="ru-arrow"></div>
                    </div>
                    <div ref="selectedCurrencyFrom" className="selected-currency-from"
                         onClick={this.showDropTo}><span
                      className="dotted">{quoteCurrency}</span>

                    </div>
                    <div className="curr-pairs-select" onClick={(e) => {
                      e.stopPropagation()
                    }}>
                      {/*<div id="fast-ex-direction-star"*/}
                      {/*className='favorite-star icon-star notransition'></div>*/}

                      <div
                        className='selectize-wrap derection-dropdown hidden'>
                        <CurrpairSelectize
                          currpairHashTable={this.currpairHashTable}
                          currenciePairs={this.currencyPairs}
                          changeDirection={ this.changeDirection }
                          favoriteCurrenciePairs={[]}
                          favoriteDirections={[]}
                          topCurrenciePairs={[]}
                          groupView={false}
                          hideCurrPairs={ this.hideCurPairs }
                          currenciesList={this.currencyList}
                          parentWidget={ this }
                          addFavoriteDirection={ this.props.addFavoriteDirection }
                          removeFavoriteDirection={ this.props.removeFavoriteDirection }
                          currencyType={'national'}
                        />
                      </div>
                      <div onClick={this.showCurPairs}
                           className='dropdown-arrow'></div>
                    </div>
                  </div>
                </div>
                <div className="calendar-block">
                  <div className="calendar-text">
                                    <span className="date-descr">
                                         {locale.t('_dash', { returnObjects: true }).calculator_pro.date}
                                    </span>
                    <span className="date-value">
                                        {this.state.calendarDate}
                                    </span>
                  </div>
                  <span className="calendar-trigger ru-calendar" onClick={this.showCalendar}>
                                </span>
                  <div className="calendar-dropdown dropdown" onClick={this.stopPropagation}>
                    <div className="tooltip-arrow"></div>
                    <DateTime
                      ref="calc_date"
                      open={true}
                      viewMode={'days'}
                      input={false}
                      locale={'ru'}
                      onChange={this.changeDate}
                    />
                  </div>
                </div>
              </div>
              <div className="calc-pro-table-wrap">
                <div
                  className="table-title"> {locale.t('_dash', { returnObjects: true }).calculator_pro.in_international_payment_systems}</div>
                <table className="calculator-pro-table">
                  <thead>
                  <tr>
                    <th></th>
                    <th>{locale.t('_dash', { returnObjects: true }).calculator_pro.rate}</th>
                    <th>{locale.t('_dash', { returnObjects: true }).calculator_pro.total_amount}</th>
                    <th></th>
                  </tr>
                  </thead>
                  {rowsNational}
                </table>
              </div>
              <div className="calc-pro-table-wrap">
                <div
                  className="table-title">{locale.t('_dash', { returnObjects: true }).calculator_pro.in_ukrainian_banks}</div>
                <table className="calculator-pro-table">
                  <thead>
                  <tr>
                    <th></th>
                    <th>{locale.t('_dash', { returnObjects: true }).calculator_pro.rate}</th>
                    <th>{locale.t('_dash', { returnObjects: true }).calculator_pro.total_amount}</th>
                    <th></th>
                  </tr>
                  </thead>
                  {rowsCards}
                </table>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <div data-item-id={this.props.dataId} style={{ width: 580, minHeight: 561 }} className='card'>
            <div className="card-header">
              <div className="card-title">
              </div>
            </div>
            <div className="card-content">
              <div className="preloader-cont">
                <div className="preloader">
                  <div className="circ1"></div>
                  <div className="circ2"></div>
                  <div className="circ3"></div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    }
    catch (e) {
      return (
        <div data-item-id={this.props.dataId} className='card national-bank-rates' style={{ width: 580, minHeight: 561 }}>
          <div className="card-header">
            <div className="card-title">
              Visa
              <div className="setting-wrap" onClick={this.tooltipToggle}>
                <span style={iconStyle} className="icon ru-settings"></span>
                <div data-tooltip={this.props.dataId} className="tooltip">
                  <div className="tooltip-arrow"></div>
                  <div className="tooltip-inner">
                    <div
                      className="tooltip-title">{locale.t('_dash', { returnObjects: true }).general.widget_management}</div>
                  </div>
                  <div className="delete-widget" onClick={() => {
                    this.props.deleteWidget(this.props.dataId);
                  }}>
                    <a href="#">{locale.t('_dash', { returnObjects: true }).general.delete}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="card-content">
            <p>Данный виджет поломан! Удалите его или перезагрузите страницу</p>
          </div>
        </div>
      )
    }

  }
}

function mapDispatchToProps(dispatch) {
  return { ...bindActionCreators({ ...ActionCreators }, dispatch), dispatch };
}

function mapStateToProps(state) {
  return {
    locale: state.locale,
    calculatorPro: state.dashInitialState.dash.calculator_pro,
    config: state.config
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(CalculatorPro);