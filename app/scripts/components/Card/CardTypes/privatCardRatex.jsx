import React from "react";
import jQuery from "jquery";
import * as ActionCreators from "../../../actions/widgets/privatCardRates";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

var $ = jQuery;


class PrivatCardRates extends React.Component {
    constructor(props) {
        super(props);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.state = {
            inputValue: 100,
            rates: null,
            showActive: false,
            showTooltip: false
        }

    }


    tooltipToggle(e) {

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
        $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');

    }


    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    componentDidMount() {

    }

    componentDidUpdate() {

    }

    componentWillMount() {

        this.props.getDataForPrivatCardRates(this.props.dataId);

    }

    render() {


        let locale = this.props.locale.language;
        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility: this.state.showTooltip ? 'visible' : 'hidden'
        };
        const tooltipStyle = {
            display: this.state.showTooltip ? 'block' : 'none'
        };
        const tooltip = (
            <div className="tooltip" style={tooltipStyle}>
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                    <div className="tooltip-title">Управление виджетом</div>
                </div>
                <div className="delete-widget" onClick={() => {
                    this.props.deleteWidget(this.props.dataId)
                }}>
                    <a href="#">удалить</a>
                </div>
            </div>
        );
        if (typeof this.props.privatCardRates != 'undefined' && typeof this.props.privatCardRates[this.props.dataId] != 'undefined' && typeof this.props.privatCardRates[this.props.dataId].rates != 'undefined') {
            var cardRates = this.props.privatCardRates[this.props.dataId],
                rates = cardRates.rates;

            var ratesMapped = rates.map((elem, index) => {
                let buyDeltaPositive = elem.buy_delta >= 0,
                    sellDeltaPositive = elem.sale_delta >= 0;
                return (
                    <tr key={index}>
                        <td className="gray-curr"><span className="gray-curr">{elem.base_currency}</span></td>
                        <td>{elem.buy.toFixed(this.props.config.decimalsInNumbers)}<span className={buyDeltaPositive ? 'delta positive' :'delta negative'}>{elem.buy_delta ? Math.ceil(elem.buy_delta * 100) / 100 : ''}</span></td>
                        <td>{elem.sale.toFixed(this.props.config.decimalsInNumbers)} <span className={sellDeltaPositive ? 'delta positive' :'delta negative'}>{elem.sale_delta ? Math.ceil(elem.sale_delta * 100) / 100 : ''}</span></td>
                    </tr>
                )
            });

            return (
                <div data-item-id={this.props.dataId} className='card national-bank-rates' style={ {width: '280px'} }>
                    <div className="card-header">
                        <div className="card-title">
                            Privat24 Card
                            <div className="setting-wrap" onClick={this.tooltipToggle}>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div className="preloader-wrapped">
                                    <div className="preloader">
                                        <div className="circ1"></div>
                                        <div className="circ2"></div>
                                        <div className="circ3"></div>
                                    </div>
                                </div>
                                <div data-tooltip={this.props.dataId} className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={() => {
                                        this.props.deleteWidget(this.props.dataId)
                                    }}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-content">
                        <div className="national-bank-rates-wrap">
                            <div className="amount-result">
                                <table className="rates-table-default">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>продажа</th>
                                        <th>покупка</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {ratesMapped}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {


            return (
                <div data-item-id={this.props.dataId} className='card national-bank-rates' style={ {width: '280px'} }>
                    <div className="card-header">
                        <div className="setting-wrap" onClick={this.tooltipToggle}>
                        </div>
                    </div>
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>
                </div>
            )


        }
    }

}
function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};
}


function mapStateToProps(state, props) {


    return {
        locale: state.locale,
        privatCardRates: state.dashInitialState.dash.privat_card_rates,
        widgetsSet: state.widgetsSet,
        config:state.config
    }




}
export default connect(mapStateToProps, mapDispatchToProps)(PrivatCardRates);