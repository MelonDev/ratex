import React from 'react';
import ReactDOM from "react-dom";
import jQuery from 'jquery';
import {getTooltipPlacement} from '../../../reusable/extendingFunctions';
import CurrPairTooltip from '../../Tooltips/currpairTooltip.jsx';
import CurrencyTooltip from '../../Tooltips/currencyTooltip.jsx';
import {showWidgetPreloader} from "../../../reusable/extendingFunctions";
import {hideWidgetPreloader} from "../../../reusable/extendingFunctions";
import config from "../../../config";
import Dropdown from 'react-dropdown';
import _ from 'underscore';



var $ = jQuery;


class FavoriteDirections extends React.Component {
    constructor(props) {
        super(props);
        this.showCurrPairTooltip = this.showCurrPairTooltip.bind(this);
        this.hideCurrpairTooltip = this.hideCurrpairTooltip.bind(this);
        this.onLimitChange = this.onLimitChange.bind(this);
        this.onActiveChange = this.onActiveChange.bind(this);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.changeLimit = this.changeLimit.bind(this);
        this.props = props;
        this.directionsCountOption = [{value: 5, label: 'Топ 5'}, {value: 10, label: 'Топ 10'}, {
            value: 15,
            label: 'Топ 15'
        }];
        this.state = {
            dropdownPlaceholder: {value: 5, label: 'Топ 5'},
            showActive: false,
            showTooltip: false,
            topDirections: {
                data: null
            }

        }
    }

    componentWillMount() {
        if (this.state.topDirections.data == null) {
            this.props.getTopDirections();
        }
    }

    componentDidMount() {

        if (this.state.topDirections.data == null && this.props.favoriteDirectionsWidget.data != null) {
            this.setState({
                topDirections: {
                    data: this.props.favoriteDirectionsWidget.data.favorites
                }
            })
        }


    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    componentDidUpdate() {

        if (this.props.favoriteDirectionsWidget.data != null) {
            this.setState({
                topDirections: {
                    data: this.props.favoriteDirectionsWidget.data.favorites
                }
            });

        }

    }

    changeLimit(limit) {
        var limit = limit.value;
        var requestJson = {
            "alias": "exchange_directions",
            "category": "information",
            "picture": "widget.png",
            "limit": limit,
        };
        this.setState({
            dropdownPlaceholder: {
                value: limit,
                label: 'Tоп ' + limit
            }
        });
        $.ajax({
            type: 'POST',
            contentType: 'applications/json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                showWidgetPreloader(self.refs[self.props.dataId]);

            },
            url: config.api_url + '/ajax/dashboard/widget/exchange_directions',
            data: JSON.stringify(requestJson),
            success: (data)=> {
                hideWidgetPreloader(self.refs[self.props.dataId]);
                this.setState({
                    topDirections: {
                        data: data
                    }
                });
                pckry.shiftLayout();
            },
            error: (data, error)=> {

            }
        });

    }

    showCurrPairTooltip(ref) {

        let elem = ReactDOM.findDOMNode(this.refs[ref]);
        var tooltip = $(elem).find('.currPairTooltip').selector;
        $(tooltip).addClass('right');
        var placement = getTooltipPlacement(tooltip);
        if (placement == 'right') {
            $('.currPairTooltip').removeClass('left');
            $('.currPairTooltip').addClass('right');
        }
        if (placement == 'left') {
            $('.currPairTooltip').removeClass('right');
            $('.currPairTooltip').addClass('left');
        }
        this.props.getCurrPairTooltip(ref);
        $(elem).addClass('tooltip-visible');

    }

    showCurrencyTooltip(id, ref) {

        let elem = ReactDOM.findDOMNode(this.refs[ref]);
        var tooltip = $(elem).find('.currencyTooltip').selector;
        $(tooltip).addClass('right');
        var placement = getTooltipPlacement(tooltip);
        if (placement == 'right') {
            $(tooltip).removeClass('left');
            $(tooltip).addClass('right');
        }
        if (placement == 'left') {
            $(tooltip).removeClass('right');
            $(tooltip).addClass('left');
        }
        this.props.getCurrencyTooltip(id);
        $(elem).addClass('tooltip-visible');

    }

    hideCurrencyTooltip(ref) {

        let elem = ReactDOM.findDOMNode(this.refs[ref]);
        $(elem).removeClass('tooltip-visible');

    }

    onLimitChange(value) {

        this.props.changeDirectionsLimit();

    }

    hideCurrpairTooltip(ref) {

        let elem = ReactDOM.findDOMNode(this.refs[ref]);
        $(elem).removeClass('tooltip-visible');

    }

    tooltipToggle(e){

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="'+ id +'"]').removeClass('visible');
        $('.tooltip[data-tooltip="'+ id +'"]').toggleClass('visible');

    }

    onActiveChange() {

        this.props.changeLimit(this.state.dropdownPlaceholder.value, !this.state.showActive);

        this.setState({
            showActive: !this.state.showActive
        });


    }


    render() {
        debugger;

        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility: this.state.showTooltip ? 'visible' : 'hidden'
        };
        const tooltipStyle = {
            display: this.state.showTooltip ? 'block' : 'none'
        };
        const tooltip = (
            <div className="tooltip" style={tooltipStyle}>
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                    <div className="tooltip-title">Управление виджетом</div>
                </div>
                <div className="delete-widget" onClick={()=> {
                    this.props.deleteWidget(this.props.dataId)
                }}>
                    <a href="#">удалить</a>
                </div>
            </div>
        );

        function findData(store, id) {

            for (var i = 0; i < store.length; i++) {

                if (store[i].id == id) {
                    return store[i];
                }

            }
            return null;

        }

        function contains(arr, needle) {

            for (var i = 0; i < arr.length; i++) {

                if (arr[i].id == needle) {
                    return true
                }

            }

            return false;

        }

        if (this.state.topDirections.data != null) {

            const directions = this.state.topDirections.data;
            var counter = 0;
            const directionsMapped = directions.map((elem, index)=> {
                var tooltipData = findData(this.props.currPairTooltip, elem.id);
                var fromRandom = elem.base_currency + counter;
                var toRandom = elem.quote_currency + counter;
                ++counter;
                return (
                    <div key={index} className="direction-item">
                        <div className="from">
                            <img src="images/currency_placeholder.svg" alt=""/>
                            <span ref={fromRandom}
                                  onMouseEnter={()=> {
                                      this.showCurrencyTooltip(elem.base_currency, fromRandom);
                                  }}
                                  onMouseLeave={()=> {
                                      this.hideCurrencyTooltip(fromRandom);
                                  }}>
                                {elem.base_currency}
                                <CurrencyTooltip locale={this.props.locale} from={elem.base_currency}
                                                 to={elem.quote_currency} id={elem.base_currency}
                                                 data={this.props.currencyTooltip}/>
                            </span>
                        </div>
                        <div ref={elem.id} className="divider"
                             onMouseEnter={()=> {
                                 this.showCurrPairTooltip(elem.id);
                             }}
                             onMouseLeave={()=> {
                                 this.hideCurrpairTooltip(elem.id);
                             }}
                        >
                            <CurrPairTooltip locale={this.props.locale} from={elem.base_currency}
                                             to={elem.quote_currency} id={elem.id} data={this.props.currPairTooltip}/>
                            <span className="ru-arrow"></span>
                        </div>
                        <div ref={toRandom} className="to">
                            <img src="images/currency_placeholder.svg" alt=""/>
                            <span ref={elem.quote_currency}
                                  onMouseEnter={()=> {
                                      this.showCurrencyTooltip(elem.quote_currency, toRandom);
                                  }}
                                  onMouseLeave={()=> {
                                      this.hideCurrencyTooltip(toRandom);
                                  }}>{elem.quote_currency}
                                <CurrencyTooltip locale={this.props.locale} from={elem.base_currency}
                                                 to={elem.quote_currency} id={elem.quote_currency}
                                                 data={this.props.currencyTooltip}/>
                            </span>
                            <div className="add" onClick={()=> {
                                this.props.addFavoriteDirection(elem.id)
                            }}></div>
                        </div>
                    </div>

                );

            });


            return (
                <div data-item-id={this.props.dataId} className='card top-directions favorite-directions'
                     data-size={this.props.size} title={this.props.title} type={this.props.type}
                     tabIndex={this.props.tabindex}>
                    <div className="card-header">
                        <div className="card-title">
                            Favorite directions
                            <div className="setting-wrap" onClick={this.tooltipToggle}>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div className="preloader-wrapped">
                                    <div className="preloader">
                                        <div className="circ1"></div>
                                        <div className="circ2"></div>
                                        <div className="circ3"></div>
                                    </div>
                                </div>
                                <div data-tooltip={this.props.dataId} className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-content">
                        {directionsMapped}
                    </div>
                </div>
            );
        } else {

            return (
                <div data-item-id={this.props.dataId} className='card top-directions' data-size={this.props.size}
                     title={this.props.title} type={this.props.type}
                     tabIndex={this.props.tabindex}>
                    <div className="card-header">

                    </div>
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>

                    <div className="card-content">
                    </div>
                </div>
            )

        }
    }
}
export default FavoriteDirections;