import React from 'react';
import jQuery from 'jquery';
import ChartTooltip from '../../Tooltips/chartTooltip.jsx';
import {getTooltipPlacement} from "../../../reusable/extendingFunctions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "../../../actions/tooltips/chartTooltipActions";


var $ = jQuery;
import config from '../../../config';


class NationalBankRates extends React.Component{
    constructor(props){
        super(props);
        this.changeInputAmount = this.changeInputAmount.bind(this);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.showChartTooltip = this.showChartTooltip.bind(this);
        this.hideChartTooltip = this.hideChartTooltip.bind(this);
        this.state = {
            inputValue: 100,
            rates:null,
            showTooltip: false
        }

    }

    changeInputAmount(event){

        let value = event.target.value.replace(/\D+/g, '');
        value = value.replace('.', "");
        let inputCharLength = value.length;
        let domElem = this.refs.input;

        if (event.target.value.toString().length < 9) {
            this.setState({
                inputValue: value
            });
            $(domElem).css({
                width: inputCharLength * 10.5 + 'px'
            });

            let requestJson = {
                "in_amount": value,
                "product": {
                    "alias": "nbu"
                },
                "currencies": [
                    "USD",
                    "EUR",
                    "RUB"
                ]
            };

            this.props.changeNationalBankInputAmount(value, this.props.dataId);
            // $.ajax({
            //     dataType: 'json',
            //     url:  config.api_url+'ajax/dashboard/widget/national_bank_rates',
            //     type: 'POST',
            //     contentType: 'applications/json',
            //     data: JSON.stringify(requestJson),
            //     success: (data)=> {
            //         this.setState({
            //             rates: data
            //         });
            //     },
            //     error: function (data, error) {
            //     }
            // });
        }

    }

    tooltipToggle(e){

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="'+ id +'"]').removeClass('visible');
        $('.tooltip[data-tooltip="'+ id +'"]').toggleClass('visible');

    }


    shouldComponentUpdate(nextProps, nextState){

        // if(nextState.inputValue != this.state.inputValue){
        //     return true;
        // }
        // if(this.state.rates == null){
        //     return true
        // }
        //
        // return false

        return true;

    }

    componentDidMount() {

    }
    componentDidUpdate(){

    }


    showChartTooltip(id) {
        var tooltip =  '.national-bank-rates[data-item-id="' + this.props.dataId + '"] #'+id+' .chartTooltip';
        var placement = getTooltipPlacement(tooltip);
        var $elem = $('.national-bank-rates[data-item-id="' + this.props.dataId + '"] #'+id+'');
        //get data for tooltip via ajax
        // this.props.getExchangerTooltip(id);

        //check the fitting of tooltip by adding primary class
        $elem.find('.chartTooltip').addClass('right');
        if (placement == 'right') {
            $(tooltip).removeClass('left');
            $(tooltip).addClass('right');
        }
        if (placement == 'left') {
            $(tooltip).removeClass('right');
            $(tooltip).addClass('left');
        }
        //show tooltip at the end of it
        $elem.addClass('tooltip-visible');
        $('.national-bank-rates[data-item-id="' + this.props.dataId + '"]').addClass('pop-up-active');
    }

    hideChartTooltip(id) {

        $('.national-bank-rates[data-item-id="' + this.props.dataId + '"]').removeClass('pop-up-active');
        $('.national-bank-rates[data-item-id="' + this.props.dataId + '"] #'+id+'').removeClass('tooltip-visible');

    }

    componentWillMount(){


        // $.ajax({
        //    dataType: 'json',
        //    url: config.api_url+'ajax/dashboard/widget/national_bank_rates',
        //    beforeSend: function(xhr) { xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest"); },
        //    type:'GET',
        //    success:(data)=>{
        //        this.setState({
        //            rates: data
        //        });
        //
        //    },
        //    error: function(data, error){
        //    }
        // });


    }

    render() {

        let locale = this.props.locale.language;
        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility : this.state.showTooltip ? 'visible' : 'hidden'
        };
        const tooltipStyle = {
            display: this.state.showTooltip ? 'block' : 'none'
        };
        const tooltip = (
            <div className="tooltip" style={tooltipStyle}>
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                    <div className="tooltip-title">Управление виджетом</div>
                </div>
                <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                    <a href="#">удалить</a>
                </div>
            </div>
        );

        if (typeof this.props.nationalBankRates != 'undefined' &&  this.props.nationalBankRates.hasOwnProperty(this.props.dataId) ) {

            var rates = this.props.nationalBankRates[this.props.dataId].rates;
            var baseCurrencies = this.props.nationalBankRates[this.props.dataId].base_currencies
            var ratesMapped = rates.map((elem, index)=>{
                if(elem == null){
                    return (
                        <tr key = {index}>
                            <td className="currency-td">God DAMN null from backend</td>
                            <td>God DAMN null from backend</td>
                        </tr>
                    )
                }

                return(
                    <tr key = {index}>
                        <td className="currency-td">{baseCurrencies[index]}</td>
                        <td>{elem.out_amount.toFixed(this.props.config.decimalsInNumbers)}</td>
                        <td id={baseCurrencies[index]}
                            className="chart-td"
                            onMouseEnter={()=>{
                                this.showChartTooltip(baseCurrencies[index]);
                                this.props.loadTooltip(baseCurrencies[index], this.props.config.quote_currency);
                            }}
                            onMouseLeave ={()=>{
                                this.hideChartTooltip(baseCurrencies[index]);
                            }}
                        ><span className="ru-graph"><ChartTooltip currency={baseCurrencies[index]}/></span></td>
                    </tr>
                )
            });

            return (
                <div data-item-id={this.props.dataId} className='card national-bank-rates national-bank-rates-input-less' style={ {width:'280px'} } data-size={this.props.size} title={this.props.title}
                     type={this.props.type} tabIndex={this.props.tabindex} >
                    <div className="card-header">
                        <div className="card-title">
                            {locale.t('_dash', {returnObjects:true}).national_bank_rates.title}
                            <div className="setting-wrap" onClick={this.tooltipToggle}>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div className="preloader-wrapped">
                                    <div className="preloader">
                                        <div className="circ1"></div>
                                        <div className="circ2"></div>
                                        <div className="circ3"></div>
                                    </div>
                                </div>
                                <div data-tooltip={this.props.dataId} className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-content">
                        <div className="national-bank-rates-wrap">
                            <div className="amount-result">
                                <table>
                                    <tbody>
                                    {ratesMapped}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }else{

            return(

                <div data-item-id={this.props.dataId} className='card national-bank-rates' style={ {width:'280px'} } data-size={this.props.size} title={this.props.title}
                     type={this.props.type} tabIndex={this.props.tabindex} >
                    <div className="card-header">
                        <div className="setting-wrap" onClick={this.tooltipToggle}>
                            <span style={iconStyle} className="icon ru-settings"></span>
                            {tooltip}
                        </div>
                    </div>
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>
                </div>

            )

        }
    }

}


function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};
}

function mapStateToProps(state, props) {

    return {
        config: state.config,
        locale: state.locale,
        nationalBankRates: state.dashInitialState.dash.national_bank_rates,
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(NationalBankRates);



