import React from 'react';
import jQuery from 'jquery';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ActionCreators from '../../../actions/widgets/blackMarketOverviewActions';
import PreloaderInner from '../Preloader/PreloaderInner.jsx';

var $ = jQuery;


class BlackMarketOverview extends React.Component {
  constructor(props) {
    super(props);
    this.tooltipToggle = this.tooltipToggle.bind(this);
    this.state = {
      inputValue: 100,
      rates: null,
      showActive: false,
      showTooltip: false
    }
  }

  shouldComponentUpdate(nextProps, nextState) {

    if (nextState.inputValue != this.state.inputValue) {
      return true;
    }
    if (this.state.rates == null) {
      return true
    }

    return false

  }

  componentDidMount() {

  }

  tooltipToggle(e) {
    e.stopPropagation();
    let id = this.props.dataId;
    //we have to find exactly this tooltip
    $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
    $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');
  }

  componentWillMount() {
    this.props.getDataForBlackMarketOverviewWidget(this.props.dataId);
  }

  render() {
    let locale = this.props.locale.language;
    if (typeof this.props.blackMarket != 'undefined' && typeof  this.props.blackMarket[this.props.dataId] != 'undefined' && typeof  this.props.blackMarket[this.props.dataId].rates != 'undefined') {
      var rates = this.props.blackMarket[this.props.dataId].rates;
      var ratesMapped = rates.map((elem, index) => {
        let buyDeltaPositive = elem.buy_delta > 0,
          sellDeltaPositive = elem.sale_delta > 0;
        return (
          <tr key={index}>
            <td className="gray-curr"><span className="gray-curr">{elem.currency}</span></td>
            <td>{elem.buy.toFixed(this.props.config.decimalsInNumbers)}<span
              className={buyDeltaPositive ? 'delta positive' : 'delta negative'}>{elem.buy_delta && elem.buy_delta.toFixed(this.props.config.decimalsInNumbers) != 0.00 ? Math.ceil(elem.buy_delta * 100) / 100 : ''}</span>
            </td>
            <td>{elem.total_buy}</td>
            <td>{elem.amount_buy}</td>
            <td>{elem.sale.toFixed(this.props.config.decimalsInNumbers)} <span
              className={sellDeltaPositive ? 'delta positive' : 'delta negative'}>{elem.sale_delta && elem.sale_delta.toFixed(this.props.config.decimalsInNumbers) != 0.00 ? Math.ceil(elem.sale_delta * 100) / 100 : ''}</span>
            </td>
            <td>{elem.total_sale}</td>
            <td>{elem.amount_sale}</td>
          </tr>
        )
      });
      return (
        <div data-item-id={this.props.dataId} style={ { 'width': 580 } } className='card no-min-height'>
          <div className="card-header">
            <div className="card-title">
              {locale.t('_dash', { returnObjects: true }).black_market.title}
              <div className="setting-wrap" onClick={this.tooltipToggle}>
                <span className="icon ru-settings"></span>
                <div className="preloader-wrapped">
                  <PreloaderInner />
                </div>
                <div data-tooltip={this.props.dataId} className="tooltip">
                  <div className="tooltip-arrow"></div>
                  <div className="tooltip-inner">
                    <div className="tooltip-title">{locale.t('_dash', { returnObjects: true }).general.widget_management}</div>
                  </div>
                  <div className="delete-widget" onClick={() => {
                    this.props.deleteWidget(this.props.dataId);
                  }}>
                    <a href="#">{locale.t('_dash', { returnObjects: true }).general.delete}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="card-content no-min-height">
            <table className="black-market-widget-table">
              <thead>
              <tr>
                <th></th>
                <th>{locale.t('_dash', { returnObjects: true }).general.buy}</th>
                <th>{locale.t('_dash', { returnObjects: true }).black_market.offers}</th>
                <th>{locale.t('_dash', { returnObjects: true }).black_market.total_amount}</th>
                <th>{locale.t('_dash', { returnObjects: true }).general.sell}</th>
                <th>{locale.t('_dash', { returnObjects: true }).black_market.offers}</th>
                <th>{locale.t('_dash', { returnObjects: true }).black_market.total_amount}</th>
              </tr>
              </thead>
              <tbody>
              {ratesMapped}
              </tbody>
            </table>
          </div>
        </div>
      );
    } else {
      return (
        <div data-item-id={this.props.dataId} className="card" style={{
          width: '580px',
        }}>
          <div className="card-header"></div>
          <div className="preloader-cont">
            <PreloaderInner />
          </div>
          <div className="card-content">
          </div>
        </div>
      );
    }
  }
}
function mapDispatchToProps(dispatch) {
  return { ...bindActionCreators({ ...ActionCreators }, dispatch), dispatch };
}

function mapStateToProps(state, props) {

  return {
    locale: state.locale,
    blackMarket: state.dashInitialState.dash.black_market_rates,
    config: state.config

  }

}
export default connect(mapStateToProps, mapDispatchToProps)(BlackMarketOverview);