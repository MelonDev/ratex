import React from 'react';
import ReactDOM from "react-dom";
import classnames from 'classnames';
import moment from 'moment';
import ReactTooltip from 'react-tooltip';
import { connect } from 'react-redux'



import _ from "underscore";

class News extends React.Component{

    constructor(props){
        super(props);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.props = props;
        this.state = {
            data:null,
            showActive:false,
            showTooltip: false
        }
    }

    tooltipToggle(e){

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="'+ id +'"]').removeClass('visible');
        $('.tooltip[data-tooltip="'+ id +'"]').toggleClass('visible');

    }

    componentDidUpdate(){
        setTimeout(() => {
          pckry.shiftLayout();
        }, 300);
    }

    componentDidMount(){
        window.moment = moment;
    }

    render(){

        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility : this.state.showTooltip ? 'visible' : 'hidden'
        };
        const tooltipStyle = {
            display: this.state.showTooltip ? 'block' : 'none'
        };
        const tooltip = (
            <div className="tooltip" style={tooltipStyle}>
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                    <div className="tooltip-title">Управление виджетом</div>
                </div>
                <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                    <a href="#">удалить</a>
                </div>
            </div>
        );

        if(typeof this.props.news != 'undefined'){

            try {
                let news = this.props.news[this.props.dataId].news;
                let locale = this.props.locale.language;
                var newsRender = news.map((elem, index)=>{
                    let date = moment(elem.pub_date).format('DD.MM.YYYY HH:mm');
                    return(
                        <div key={index} className="single-news">
                            <a target="_blank" href={elem.link}>{elem.title}</a>
                            <div className="time">{date}</div>
                        </div>
                    );
                });
                return(
                    <div data-item-id={this.props.dataId} className='card news' style={{width:580}}>
                        <div className="card-header">
                            <div className="card-title">
                                {locale.t('_dash', {returnObjects:true}).news.title}
                                <div className="setting-wrap" onClick={this.tooltipToggle}>
                                    <span style={iconStyle} className="icon ru-settings"></span>
                                    <div className="preloader-wrapped">
                                        <div className="preloader">
                                            <div className="circ1"></div>
                                            <div className="circ2"></div>
                                            <div className="circ3"></div>
                                        </div>
                                    </div>
                                    <div data-tooltip={this.props.dataId} className="tooltip">
                                        <div className="tooltip-arrow"></div>
                                        <div className="tooltip-inner">
                                            <div className="tooltip-title">Управление виджетом</div>
                                        </div>
                                        <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                                            <a href="#">удалить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card-content">
                            {newsRender}
                        </div>
                    </div>

                );
            } catch (e) {
                return(
                    <div data-item-id={this.props.dataId} className='card news' style={{width:580}}>
                        <div className="card-header">
                            <div className="card-title">
                                <div className="setting-wrap" htmlFor={this.props.dataId} data-tip data-event='click focus'>
                                    <span style={iconStyle} className="icon ru-settings"></span>
                                    <div className="preloader-wrapped">
                                        <div className="preloader">
                                            <div className="circ1"></div>
                                            <div className="circ2"></div>
                                            <div className="circ3"></div>
                                        </div>
                                    </div>
                                </div>
                                <ReactTooltip globalEventOff='click'  id={this.props.dataId} place="bottom" effect='solid' aria-haspopup='true'>
                                    <div className="tooltip">
                                        <div className="tooltip-arrow"></div>
                                        <div className="tooltip-inner">
                                            <div className="tooltip-title">Управление виджетом</div>
                                        </div>
                                        <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                                            <a href="#">удалить</a>
                                        </div>
                                    </div>
                                </ReactTooltip>
                            </div>
                        </div>
                        <div className="preloader-cont">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>

                        <div className="card-content">
                        </div>
                    </div>
                );
            }

        }else{
            return(
                <div data-item-id={this.props.dataId} className='card news' style={{width:580}}>
                    <div className="card-header">
                        <div className="card-title">
                            <div className="setting-wrap" htmlFor={this.props.dataId} data-tip data-event='click focus'>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div className="preloader-wrapped">
                                    <div className="preloader">
                                        <div className="circ1"></div>
                                        <div className="circ2"></div>
                                        <div className="circ3"></div>
                                    </div>
                                </div>
                            </div>
                            <ReactTooltip globalEventOff='click'  id={this.props.dataId.toString()} place="bottom" effect='solid' aria-haspopup='true'>
                                <div className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={()=>{this.props.deleteWidget(this.props.dataId)}}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </ReactTooltip>
                        </div>
                    </div>
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>

                    <div className="card-content">
                    </div>
                </div>
            );
        }

    }
}

function mapStateToProps(state) {
    return{
        locale: state.locale,
        news: state.dashInitialState.dash.news
    }
}

export default connect(mapStateToProps)(News);