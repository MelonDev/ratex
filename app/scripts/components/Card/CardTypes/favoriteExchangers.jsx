import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTooltipPlacement } from '../../../reusable/extendingFunctions';
import jQuery from 'jquery';
import trackClick from '../../../reusable/trackClick';
import { getFavoriteExchangersWidget } from '../../../actions/widgets/favoriteExchangersActions';

var $ = jQuery;


class FavoriteExchangers extends React.Component {


  constructor() {
    super();
    this.onLimitChange = this.onLimitChange.bind(this);
    this.onActiveChange = this.onActiveChange.bind(this);
    this.tooltipToggle = this.tooltipToggle.bind(this);
    this.showExchangerTooltip = this.showExchangerTooltip.bind(this);
    this.exchangersCountOption = [{ value: 5, label: 'Топ 5' }, { value: 10, label: 'Топ 10' }, {
      value: 15,
      label: 'Топ 15'
    }];
    this.state = {
      showTooltip: false,
      showActive: false,
      dropdownPlaceholder: this.exchangersCountOption[0],
      exchangersCount: 5
    }
  }

  showExchangerTooltip(ref) {
    let elem = ReactDOM.findDOMNode(this.refs[ref]);
    var tooltip = $(elem).find('.exTooltip').selector;
    $(tooltip).addClass('right');
    var placement = getTooltipPlacement(tooltip);
    if (placement == 'right') {
      $('.exTooltip').removeClass('left');
      $('.exTooltip').addClass('right');
    }
    if (placement == 'left') {
      $('.exTooltip').removeClass('right');
      $('.exTooltip').addClass('left');
    }
    this.props.getExchangerTooltip(ref);
    $(elem).parent().addClass('tooltip-visible');
  }

  hideExchangerTooltip(ref) {
    let elem = ReactDOM.findDOMNode(this.refs[ref]);
    $(elem).parent().removeClass('tooltip-visible');
  }


  onLimitChange(val) {

    this.setState({
      dropdownPlaceholder: val
    });
    this.props.changeLimit(val.value, this.state.showActive);


  }

  componentWillMount() {
    this.props.getFavoriteExchangersWidget(this.props.dataId, [...Object.keys(this.props.favoriteExchangers)]);
  }

  tooltipToggle(e) {

    e.stopPropagation();
    let id = this.props.dataId;
    //we have to find exactly this tooltip
    $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
    $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');

  }

  onActiveChange() {

    this.props.changeLimit(this.state.dropdownPlaceholder.value, !this.state.showActive);

    this.setState({
      showActive: !this.state.showActive
    });


  }

  componentDidUpdate() {

    // pckry.shiftLayout();

  }

  componentDidMount() {

  }


  render() {
      console.log(this.props.favoriteExchangersWidget);
    var self = this;
    let locale = this.props.locale.language;
    const iconStyle = {
      opacity: this.state.showTooltip ? 1 : 0,
      visibility: this.state.showTooltip ? 'visible' : 'hidden'
    };
    if (this.props.favoriteExchangersWidget && this.props.favoriteExchangersWidget[this.props.dataId] && this.props.favoriteExchangersWidget[this.props.dataId].data != null && Object.keys(this.props.favoriteExchangers).length > 0) {
        try {
          let data = this.props.favoriteExchangersWidget[this.props.dataId].data.favorites;
          let defaultExchangers = data;
          let defaultExLay = defaultExchangers.map(function(elem, index) {
            return (
              <tr key={index}>
                  <td ref={elem.id}>
                      <div className="ex-name-wrap" onMouseEnter={() => {
                        self.showExchangerTooltip(elem.id);
                      }}
                           onMouseLeave={() => {
                             self.hideExchangerTooltip(elem.id);
                           }}>
                          <a onClick={() => {
                            trackClick(elem.id)
                          }} href={elem.exchanger_site} target="_blank">
                              <img className="ex-logo-table" src="images/exchanger_icon.png" alt=""/>{elem.name}
                          </a>
                          <span className="topExchangerName"></span>
                      </div>
                  </td>
                {/* <td>{elem.reserves} <span className="gray-curr">USD</span></td> */}
                {/*<td>{elem.rates_count}</td>*/}
                {/*<td style={{*/}
                {/*paddingRight: 0*/}
                {/*}}>{elem.wmbl ? elem.wmbl : String.fromCharCode(8212)}</td>*/}
                {/* <td className="comments"><span className="positive">{elem.commentsCountPos}</span> / <span className="negative">{elem.commentsCountNeg}</span></td> */}
                {/* <td>s</td> */}
              </tr>
            );

          });

          console.log(defaultExLay);
          return (
            <div data-item-id={this.props.dataId} className='card top-exchangers favoriteExchangers'
                 style={ { 'width': 580 } }>
                <div className="card-header">
                    <div className="card-title">
                        <div className="widget-header-count-block">
                            Избранные обменники
                        </div>
                        <div className="setting-wrap" onClick={this.tooltipToggle}>
                            <span style={iconStyle} className="icon ru-settings"></span>
                            <div className="preloader-wrapped">
                                <div className="preloader">
                                    <div className="circ1"></div>
                                    <div className="circ2"></div>
                                    <div className="circ3"></div>
                                </div>
                            </div>
                            <div data-tooltip={this.props.dataId} className="tooltip">
                                <div className="tooltip-arrow"></div>
                                <div className="tooltip-inner">
                                    <div className="tooltip-title">Управление виджетом</div>
                                </div>
                                <div className="delete-widget" onClick={() => {
                                  this.props.deleteWidget(this.props.dataId)
                                }}>
                                    <a href="#">удалить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-content">
                    <div className="top-exchangers-wrap">
                        <table>
                            <thead>
                            <tr>
                                <th>asdasd</th>
                                <th>asasd</th>
                                <th>BL</th>
                            </tr>
                            </thead>
                            <tbody>
                            {defaultExLay}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          );
        }
        catch (e) {
            console.log(e);
          return (
            <div data-item-id={this.props.dataId} style={{ 'width': 580 }} className='card'
                 data-size={this.props.size} title={this.props.title} type={this.props.type}
                 tabIndex={this.props.tabindex}>
                <div className="card-header">
                </div>
                <div className="card-content">
                    <p>Нет избранных обменников</p>
                </div>
            </div>
          );
        }


    } else {
      return (
        <div data-item-id={this.props.dataId} style={{ 'width': 580 }} className='card'
             data-size={this.props.size} title={this.props.title} type={this.props.type}
             tabIndex={this.props.tabindex}>
            <div className="card-header">
            </div>
            <div className="card-content">
                <p>Нет избранных обменников</p>
            </div>

        </div>
      );
    }

  }
}

FavoriteExchangers = connect(
  state => ({
    locale: state.locale,
    favoriteExchangers: state.favoriteExchangers,
    favoriteExchangersWidget: state.dashInitialState.dash.favorite_exchangers
  }),
  dispatch => bindActionCreators({ getFavoriteExchangersWidget }, dispatch)
)(FavoriteExchangers);

export default FavoriteExchangers;