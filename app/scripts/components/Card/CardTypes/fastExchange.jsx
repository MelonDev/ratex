import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import { getTooltipPlacement } from '../../../reusable/extendingFunctions';
import * as ActionCreators from '../../../actions/widgets/fastExchangeActions';
import * as FavoriteExchangersActions from '../../../actions/favorites/favoriteExchangers';
import * as FavoriteDirectionsActions from '../../../actions/favorites/favoriteDirections';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'underscore';
import CurrpairSelectize from '../../SelectComponents/currpairSelectize.jsx';
import ExchangerTooltip from '../../Tooltips/exchangerTooltip.jsx';
let $ = jQuery;

class FastExchange extends React.Component {

  constructor(props) {
    super(props);
    this.showDropFrom = this.showDropFrom.bind(this);                      //Damn React bindings!!!
    this.hideDropFrom = this.hideDropFrom.bind(this);                      //Damn React bindings!!!
    this.showDropTo = this.showDropTo.bind(this);                          //Damn React bindings!!!
    this.hideDropTo = this.hideDropTo.bind(this);                          //Damn React bindings!!!
    this.showCurPairs = this.showCurPairs.bind(this);                      //Damn React bindings!!!
    this.hideCurPairs = this.hideCurPairs.bind(this);                      //Damn React bindings!!!
    this.showExTooltip = this.showExTooltip.bind(this);                    //Damn React bindings!!!
    this.hideExTooltip = this.hideExTooltip.bind(this);                    //Damn React bindings!!!
    this.countFromChange = this.countFromChange.bind(this);                //Damn React bindings!!!
    this.countToChange = this.countToChange.bind(this);                    //Damn React bindings!!!
    this.showAnotherExchangers = this.showAnotherExchangers.bind(this);    //Damn React bindings!!!
    this.resizeInput = this.resizeInput.bind(this);                        //Damn React bindings!!!
    this.changeDirection = this.changeDirection.bind(this);                //Damn React bindings!!!
    this.tooltipToggle = this.tooltipToggle.bind(this);                    //Damn React bindings!!!
    this.countToBlur = this.countToBlur.bind(this);                        //Damn React bindings!!!
    this.countFromBlur = this.countFromBlur.bind(this);                    //Damn React bindings!!!
    this.anotherExchangersCount = 0;
    this.selectedFrom = "WMZ";
    this.selectedTo = "P24UAH";
    this.delayTimer = 0;
    this.tooltipPlacement = null;
    this.currDropVisibility = false;
    this.countFromPersisted = null;
    this.countToPersisted = null;
    this.state = {
      hasError: false,
      currenciePairs: null,
      countFrom: 1,
      countTo: 1,
      fromDropVisible: false,
      toDropVisible: false,
      directionDropVisible: false,
      selectedFrom: {
        value: this.selectedFrom,
        label: this.selectedFrom,
        icon: 's'
      },
      selectedTo: {
        value: this.selectedTo,
        label: this.selectedTo,
        icon: null
      },
      currencyPairs: null,
      showAnotherExchangers: false,
      fromCurrenciesAvaliable: [],
      toCurrenciesAvaliable: [],
      bestRateCommission: null,
      noSuchDirection: false,
      showActive: false,
      showTooltip: false
    };
  }

  showExTooltip(id) {
    var widgetId = this.props.dataId;
    let elem = ReactDOM.findDOMNode(this.refs[id]),
      placement = getTooltipPlacement('.exTooltip'),
      exTooltip = '.exTooltip',
      $exTooltip = $('.exTooltip');

    //get data for tooltip via ajax
    this.props.getExchangerTooltip(id);

    //check the fitting of tooltip by adding primary class
    $(elem).find(exTooltip).addClass('right');
    if (placement == 'right') {
      $exTooltip.removeClass('left');
      $exTooltip.addClass('right');
    }
    if (placement == 'left') {
      $exTooltip.removeClass('right');
      $exTooltip.addClass('left');
    }
    //show tooltip at the end of it
    $(elem).addClass('tooltip-visible');
    $('.fast-exchange[data-item-id="' + widgetId + '"]').addClass('pop-up-active');


  }

  resizeInput() {


  }

  hideExTooltip(id) {

    let elem = ReactDOM.findDOMNode(this.refs[id]);
    $(elem).removeClass('tooltip-visible');
    $('.fast-exchange[data-item-id="' + id + '"]').removeClass('pop-up-active');


  }

  showAnotherExchangers() {

    this.setState({
      showAnotherExchangers: true
    });

    //recalculate Packery layout after resizing widget
    setTimeout(() => {
      pckry.layout();
    }, 10);//yes, I know setTimeout sucks in this case, but whatever...

  }


  changeDirection(value) {
    let self = this;
    if (event.target.value != '' && event.target.value != 0) {
      let requestJson = {
        "count_from": 1,
        "selected_direction": {
          "base_currency": { "id": value[0] },
          "quote_currency": { "id": value[1] }
        }
      };

      this.props.fastExchangeChangeDirection(value, this.props.dataId);

    }

  }

  showDropFrom() {

    $('.from-selectize-wrap').removeClass('hidden');
    $('.from-selectize-wrap').addClass('open');
    $('.from-selectize-wrap .resizable-input').focus();
    $('.fast-exchange').addClass('pop-up-active');


  }

  hideDropFrom() {

    $('.from-selectize-wrap').removeClass('open');
    $('.from-selectize-wrap').addClass('hidden');
    $('.fast-exchange').removeClass('pop-up-active');

  }

  showDropTo() {

    $('.to-selectize-wrap').removeClass('hidden');
    $('.to-selectize-wrap').addClass('open');
    $('.to-selectize-wrap .resizable-input').focus();
    $('.fast-exchange').addClass('pop-up-active');


  }

  hideDropTo() {

    $('.to-selectize-wrap').removeClass('open');
    $('.to-selectize-wrap').addClass('hidden');
    $('.fast-exchange').removeClass('pop-up-active');


  }

  showCurPairs(e) {

    var id = this.props.dataId;
    e.stopPropagation();
    if (this.props.currenciesList.data == null) {
      this.props.getCurrenciesList();
    }
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap').toggleClass('hidden');
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap').addClass('open');
    $('.fast-exchange[data-item-id="' + id + '"] .curr-pairs-select .selectize-wrap .resizable-input').focus();
    $('.fast-exchange[data-item-id="' + id + '"]').addClass('pop-up-active');

  }

  hideCurPairs() {

    $('.curr-pairs-select .selectize-wrap').addClass('hidden');
    $('.curr-pairs-select .selectize-wrap').removeClass('open');
    $('.fast-exchange').removeClass('pop-up-active');

  }

  countFromBlur(e) {

    if (e.target.value.length < 1 || e.target.value == 0) {
      this.setState({
        countFrom: this.countFromPersisted
      });
    }

  }

  countToBlur(e) {

    if (e.target.value.length < 1 || e.target.value == 0) {
      this.setState({
        countTo: this.countToPersisted
      });
    }

  }

  countFromChange(event) {

    let self = this,
      eventPersisted = event.target.value,
      testDot = /^([\d]*[,|\.]?[\d]*)$/.test(eventPersisted),
      domElemFrom = this.refs.count_from_input,
      domElemTo = this.refs.count_to_input,
      inputCharLength = $(domElemFrom).val().length;

    if (eventPersisted.length > 0 && eventPersisted != 0) {
      this.countFromPersisted = eventPersisted
    }
    //check for only digits and only one dot or coma...
    if (!testDot) return;

    if (event.target.value.toString().length < 12) {
      if (inputCharLength > 6) {
        $(domElemFrom).css({
          'font-size': '14px',
          width: inputCharLength * 8.9 + 'px'
        });
      } else {
        $(domElemFrom).css({
          'font-size': '16px',
          width: inputCharLength * 8.9 + 'px'
        });
      }
      this.setState({
        countFrom: eventPersisted
      });

      clearTimeout(this.delayTimer);
      this.delayTimer = setTimeout(() => {

        if (eventPersisted != '' && eventPersisted != 0) {
          let baseCurr = this.props.fastExchangeState[this.props.dataId].selected_direction.base_code;
          let quoteCurr = this.props.fastExchangeState[this.props.dataId].selected_direction.quote_code;
          this.props.countFromChangeFastExchange(this.props.dataId, baseCurr, quoteCurr, eventPersisted);
        }
      }, 300);

    }

  }

  countToChange(event) {
    let self = this,
      eventPersisted = event.target.value,
      test = /^([\d]*[,|\.]?[\d]*)$/.test(eventPersisted),
      domElemFrom = this.refs.count_from_input,
      domElemTo = this.refs.count_to_input,
      inputCharLength = $(domElemTo).val().length;

    //check for only digits and only one dot...
    if (!test) {
      return
    }
    if (inputCharLength > 12) {
      return
    }

    if (eventPersisted.length > 0 && eventPersisted != 0) {
      this.countToPersisted = eventPersisted
    }
    this.setState({
      countTo: eventPersisted
    });
    if (inputCharLength > 6) {
      $(domElemTo).css({
        'font-size': '14px',
        width: inputCharLength * 9 + 'px'
      });
    } else {
      $(domElemTo).css({
        'font-size': '16px',
        width: inputCharLength * 10.5 + 'px'
      });
    }
    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(() => {
      if (eventPersisted != '' && eventPersisted != 0) {
        let baseCurr = this.props.fastExchangeState[this.props.dataId].selected_direction.base_code;
        let quoteCurr = this.props.fastExchangeState[this.props.dataId].selected_direction.quote_code;
        this.props.countToChangeFastExchange(this.props.dataId, baseCurr, quoteCurr, eventPersisted);
      }
    }, 300);
  }


  componentWillMount() {
    this.props.getDataForFastExchangeWidget(this.props.dataId);
  }

  componentWillUpdate(props, state) {

    if (typeof this.props.fastExchangeState != 'undefined' && typeof this.props.fastExchangeState[this.props.dataId] != 'undefined' && typeof this.props.fastExchangeState[this.props.dataId].best_rate != 'undefined') {
      let currenValTo = this.props.fastExchangeState[this.props.dataId].best_rate.out_amount;
      let nextValTo = props.fastExchangeState[this.props.dataId].best_rate.out_amount;
      let currenValFrom = this.props.fastExchangeState[this.props.dataId].best_rate.in_amount;
      let nextValFrom = props.fastExchangeState[this.props.dataId].best_rate.in_amount;
      if (currenValTo != nextValTo) {
        this.state.countTo = nextValTo;

      }
      if (currenValFrom != nextValFrom) {
        this.state.countFrom = nextValFrom;
      }
    }
  }

  componentDidUpdate(props, state) {

  }

  tooltipToggle(e) {

    e.stopPropagation();
    let id = this.props.dataId;
    //we have to find exactly this tooltip
    $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
    $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');

  }

  shouldComponentUpdate(nextProps, nextState) {

    let stateIsEqual = _.isEqual(nextState, this.state);
    let propsIsEqual = _.isEqual(nextProps, this.props);

    if (!stateIsEqual) {
      return true
    }
    if (!propsIsEqual) {
      return true
    }
    else {
      return false
    }

  }


  componentDidMount() {


  }

  contains(array, needle) {

    for (var i = 0; i < array.length; i++) {
      if (array[i].id == needle) {
        return true;
      }
    }
    return false;

  }

  render() {

    let self = this;
    let locale = this.props.locale.language;
    const iconStyle = {
      opacity: this.state.showTooltip ? 1 : 0,
      visibility: this.state.showTooltip ? 'visible' : 'hidden'
    };
    const tooltipStyle = {
      display: this.state.showTooltip ? 'block' : 'none'
    };
    const tooltipSetting = (
      <div className="tooltip" style={tooltipStyle}>
          <div className="tooltip-arrow"></div>
          <div className="tooltip-inner">
              <div className="tooltip-title">Управление виджетом</div>
          </div>
          <div className="delete-widget" onClick={() => {
            this.props.deleteWidget(this.props.dataId);
          }}>
              <a href="#">удалить</a>
          </div>
      </div>
    );
    if (this.state.hasError) {
      return (
        <div data-item-id={this.props.dataId} className="card fast-exchange" style={{
          width: '580px',
        }}>
            <div className="card-header"></div>
            <h4>Виджет поломан.jpg</h4>
            <div className="card-content"></div>
        </div>
      )
    }
    if (this.props.currencyPairs.data != null && typeof this.props.fastExchangeState != 'undefined' && this.props.fastExchangeState[this.props.dataId] != 'undefined') {

      var fastExchangeData = this.props.fastExchangeState[this.props.dataId],
        currenciesHashTable = this.props.currenciesHashTable,
        bestRate = fastExchangeData.best_rate,
        bestExchangerName = bestRate.name,
        bestExchangerUrl = bestRate.url,
        bestExchangerId = bestRate.exchanger_id,
        countTo = bestRate.out_amount.toFixed(this.props.config.decimalsInNumbers),
        rates = fastExchangeData.rates,
        data = this.props.currencyPairs,
        topCurrpairs = this.props.currencyPairs.data.slice(0, 6),
        fromPlaceholderImgUrl = currenciesHashTable[fastExchangeData.selected_direction.base_code].icon,
        fromPlaceholderCode = fastExchangeData.selected_direction.base_code,
        toPlaceholderCode = fastExchangeData.selected_direction.quote_code,
        toPlaceholderImgUrl = currenciesHashTable[fastExchangeData.selected_direction.quote_code].icon,
        countFromStyle = () => {
          var length = this.state.countFrom.toString().length
          if (length > 6) {
            return {
              'font-size': '14px',
              width: length * 8.9 + 'px'
            }
          }
        },
        countToStyle = () => {
          var length = this.state.countTo.toString().length
          if (length > 6) {
            return {
              'font-size': '14px',
              width: length * 8.9 + 'px'
            }
          }
        };


      var currenciePairsMapped = topCurrpairs.map((elem, index) => {
        return (
          {
            groupId: "top",
            currPairId: elem.id,
            value: elem.base_currency.toUpperCase() + ' ' + elem.quote_currency.toUpperCase(),
            label: elem.base_currency.toUpperCase() + ' ' + elem.quote_currency.toUpperCase(),
            key: index
          }
        );

      });

      function getFavCurrpair(id) {
        for (var key in self.props.currpairHashTable) {
          if (self.props.currpairHashTable[key] == id) {
            return key.split('-');
          }
        }
      }

      try {
        var favoritePairsMapped = Object.keys(this.props.favoriteDirections).map((elem, index) => {
          return (
            {
              groupId: 'favorites',
              currPairId: elem,
              value: getFavCurrpair(elem)[0].toUpperCase() + ' ' + getFavCurrpair(elem)[1].toUpperCase(),
              label: getFavCurrpair(elem)[0].toUpperCase() + ' ' + getFavCurrpair(elem)[1].toUpperCase(),
              key: index
            }
          )
        });
      } catch (e) {
        var favoritePairsMapped = [];
      }

      currenciePairsMapped = currenciePairsMapped.concat(favoritePairsMapped);
      var ratesMapped = rates.map((elem, index) => {
        return (
          <tr key={index} ref={elem.exchanger_id}>
              <td>{elem.out_amount.toFixed(this.props.config.decimalsInNumbers)}</td>
              <td><span className="gray-currency"> {toPlaceholderCode}</span></td>
              <td onMouseEnter={() => {
                this.showExTooltip(elem.exchanger_id)
              }}
                  onMouseLeave={() => {
                    this.hideExTooltip(elem.exchanger_id)
                  }}>
                  <img src="images/exchanger_icon.png" alt=""/>{elem.name}<ExchangerTooltip
                addFavoriteExchanger={self.props.addFavoriteExchanger}
                favoriteExchangers={self.props.favoriteExchangers}
                data={self.props.exchangerTooltip} id={elem.exchanger_id}/></td>
              <td>{elem.exchanger_rating.toFixed(3)}
                {/*<span className="ru-arrow down"></span>*/}
              </td>
              <td>
                {elem.out_commission}
                  <span className="gray-currency">&ensp;{toPlaceholderCode}</span>
                  <span className={true ? "favorite-star icon-star notransition" : "favorite-star icon-star"}></span>
              </td>
          </tr>
        )
      });
      return (
        <div ref={this.props.dataId} data-item-id={this.props.dataId} className='card fast-exchange'
             style={ { 'width': 580 } }>
            <div className="card-header">
                <div className="card-title">
                  {locale.t('_dash', { returnObjects: true }).fast_exchange.title}
                    <div className="setting-wrap" onClick={this.tooltipToggle}>
                        <span style={iconStyle} className="icon ru-settings"></span>
                        <div className="preloader-wrapped">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>
                        <div data-tooltip={this.props.dataId} className="tooltip">
                            <div className="tooltip-arrow"></div>
                            <div className="tooltip-inner">
                                <div className="tooltip-title">Управление виджетом</div>
                            </div>
                            <div className="delete-widget" onClick={() => {
                              this.props.deleteWidget(this.props.dataId)
                            }}>
                                <a href="#">удалить</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="card-content">
                <div className="direction-block">
                    <div className="amount-input">
                        <input style={countFromStyle()} ref="count_from_input" pattern="[0-9]+([\.][0-9]+)?"
                               value={this.state.countFrom}
                               onChange={this.countFromChange} onBlur={this.countFromBlur} type="text"/>
                    </div>
                    <div className="direction-wrap" onClick={this.showCurPairs}>
                        <div ref="selectedCurrencyFrom" className="selected-currency-from">
                            <div className="clicked-area" onClick={this.showDropFrom}>
                                <img src={fromPlaceholderImgUrl} alt=""/> <span
                              className="dotted">{fromPlaceholderCode}</span>
                            </div>

                        </div>
                        <div className="divider">
                            <div className="ru-arrow"></div>
                        </div>
                        <div ref="selectedCurrencyFrom" className="selected-currency-from"
                             onClick={this.showDropTo}>
                            <img src={toPlaceholderImgUrl} alt=""/> <span
                          className="dotted">{toPlaceholderCode}</span>
                        </div>
                        <div className="curr-pairs-select" onClick={(e) => {
                          e.stopPropagation()
                        }}>
                            <div id="fast-ex-direction-star"
                                 className={
                                   this.props.favoriteDirections.hasOwnProperty(this.props.fastExchangeState[this.props.dataId].selected_direction.base_code + '/' + this.props.fastExchangeState[this.props.dataId].selected_direction.quote_code) ? 'favorite-star icon-star active notransition' : 'favorite-star icon-star notransition'
                                 }
                                 onClick={() => {
                                     debugger;
                                   if ($('#fast-ex-direction-star').hasClass('active')) {
                                     $('#fast-ex-direction-star').removeClass('active');
                                     this.props.removeFavoriteDirection(this.props.currpairHashTable[this.props.fastExchangeState[this.props.dataId].selected_direction.base_code + '-' + this.props.fastExchangeState[this.props.dataId].selected_direction.quote_code].id)
                                   } else {
                                     $('#fast-ex-direction-star').addClass('active');
                                     this.props.addFavoriteDirection(this.props.currpairHashTable[this.props.fastExchangeState[this.props.dataId].selected_direction.base_code + '-' + this.props.fastExchangeState[this.props.dataId].selected_direction.quote_code].id)
                                   }
                                 }}></div>

                            <div
                              className={this.state.directionDropVisible ? 'selectize-wrap derection-dropdown open currpair-select' : 'selectize-wrap derection-dropdown hidden'}>
                                <CurrpairSelectize
                                  favoriteDirections={ this.props.favoriteDirections }
                                  currpairHashTable={this.props.currpairHashTable}
                                  currenciePairs={this.props.currencyPairs}
                                  changeDirection={ self.changeDirection }
                                  hideCurrPairs={ self.hideCurPairs }
                                  parentWidget={ self }
                                  groupView={true}
                                  currenciesList={self.props.currenciesList}
                                  favoriteCurrenciePairs={ favoritePairsMapped }
                                  topCurrenciePairs={ currenciePairsMapped }
                                  addFavoriteDirection={ this.props.addFavoriteDirection }
                                  removeFavoriteDirection={ this.props.removeFavoriteDirection }
                                  currencyType='digital'
                                />
                            </div>
                            <div onClick={this.showCurPairs}
                                 className={this.state.directionDropVisible ? 'dropdown-arrow open' : 'dropdown-arrow'}></div>
                        </div>
                    </div>
                </div>
                <div style={this.state.noSuchDirection ? { display: 'none' } : { display: 'block' }}>
                    <div className="best-course">

                                <span
                                  className="bold">{locale.t('_dash', { returnObjects: true }).fast_exchange.best_rate_from}</span>
                        <div
                          className="best-exchanger"
                          ref={bestExchangerId}
                          onMouseEnter={() => {
                            this.showExTooltip(bestExchangerId)
                          }}
                          onMouseLeave={() => {
                            this.hideExTooltip(bestExchangerId)
                          }}><img src="images/exchanger_icon.png" alt="" ref="best_ex"/>{bestExchangerName}
                            <ExchangerTooltip addFavoriteExchanger={self.props.addFavoriteExchanger}
                                              favoriteExchangers={self.props.favoriteExchangers}
                                              data={self.props.exchangerTooltip} id={bestExchangerId}/>
                        </div>
                        <div
                          className={this.props.favoriteExchangers.hasOwnProperty(bestExchangerId) ? 'favorite-star icon-star active notransition' : 'favorite-star icon-star notransition'}
                          onClick={
                            () => {
                              if (!$('.best-course .favorite-star').hasClass('active')) {
                                this.props.addFavoriteExchanger(bestExchangerId);
                                $('.best-course .favorite-star').addClass('active');
                              } else {
                                this.props.removeFavoriteExchanger(bestExchangerId);
                                $('.best-course .favorite-star').removeClass('active');
                              }

                            }}></div>

                    </div>
                    <div className="result-block">
                        <div className="amount-input">
                            <input style={countToStyle()} ref="count_to_input" pattern="[0-9]+([\.][0-9]+)?"
                                   onChange={this.countToChange} onBlur={this.countToBlur}
                                   value={this.state.countTo} type="number"/>
                        </div>
                        <div className="currency"> {toPlaceholderCode}</div>

                        <button
                          className={this.state.showAnotherExchangers ? "btn btn-primary btn-exchange  btn-floated" : "hidden"}>{locale.t('_dash', { returnObjects: true }).fast_exchange.exchange}</button>
                    </div>
                    <div className="buttons-block">

                        <button
                          className={this.state.showAnotherExchangers || ratesMapped.length < 1 ? "hidden" : "btn btn-primary btn-exchange btn-gray"}
                          onClick={ this.showAnotherExchangers}>{locale.t('_dash', { returnObjects: true }).fast_exchange.other_options}</button>
                        <button type="submit" formtarget="_blank"
                                className={this.state.showAnotherExchangers ? "hidden" : "btn btn-primary btn-exchange"}>{locale.t('_dash', { returnObjects: true }).fast_exchange.exchange}</button>

                    </div>
                    <div
                      className={this.state.showAnotherExchangers ? 'more-ex-table' : 'more-ex-table hidden' }>
                        <h3>{locale.t('_dash', { returnObjects: true }).fast_exchange.another_rates}</h3>
                        <table>
                            <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>{locale.t('_dash', { returnObjects: true }).fast_exchange.exchanger}</th>
                                <th>{locale.t('_dash', { returnObjects: true }).fast_exchange.rating}</th>
                                <th>{locale.t('_dash', { returnObjects: true }).fast_exchange.fee}</th>
                            </tr>
                            </thead>
                            <tbody>
                            {ratesMapped}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div style={this.state.noSuchDirection ? { display: 'block' } : { display: 'none' }}>
                    Данное направление недоступно!
                </div>
            </div>
        </div>
      );
    }
    return (
      <div data-item-id={this.props.dataId} className="card fast-exchange" style={{
        width: '580px',
      }}>
          <div className="card-header"></div>
          <div className="preloader-cont">
              <div className="preloader">
                  <div className="circ1"></div>
                  <div className="circ2"></div>
                  <div className="circ3"></div>
              </div>
          </div>
          <div className="card-content">
          </div>
      </div>
    )

  }
}


function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({ ...ActionCreators, ...FavoriteExchangersActions, ...FavoriteDirectionsActions }, dispatch),
    dispatch
  };
}


function mapStateToProps(state, props) {

  return {
    locale: state.locale,
    fastExchangeState: state.dashInitialState.dash.fast_exchange,
    currencyPairs: state.currencyPairs,
    currenciesList: state.currenciesList,
    config: state.config,
    favoriteDirections: state.favoriteDirections,
    favoriteExchangers: state.favoriteExchangers,
    currpairHashTable: state.currpairHashTable,
    exchangerTooltip: state.exchangerTooltip,
    currenciesHashTable: state.currenciesHashTable
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(FastExchange);