import React from 'react';
import ReactDOM from "react-dom";
import classnames from 'classnames';
import moment from 'moment';
import _ from "underscore";
import ReactTooltip from 'react-tooltip'
import { connect } from 'react-redux'

class RatexStats extends React.Component{

    constructor(props){
        super(props);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.props = props;
    }

    tooltipToggle(e){
        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="'+ id +'"]').removeClass('visible');
        $('.tooltip[data-tooltip="'+ id +'"]').toggleClass('visible');
    }

    componentWillMount(){
        //not implemented yet
    }

    componentDidUpdate(){
        //not implemented yet
    }

    render(){
        let locale = this.props.locale.language;
        const iconStyle = {
            opacity: 0,
            visibility : 'hidden'
        };
        if(typeof this.props.ratexStats != 'undefined' &&  typeof this.props.ratexStats[this.props.dataId] != 'undefined'){
            let stats = this.props.ratexStats[this.props.dataId].statistics;
            let totalCurrencies = stats.total_currencies;
            let totalCurrencyPairs = stats.total_currency_pairs;
            let totalExchangers = stats.total_exchangers;
            var statsRender = (
                <div className="stats-wrap">
                    <div className="left-part">
                        <ul>
                            <li>{locale.t('_dash', {returnObjects:true}).ratex_stats.total_currencies}</li>
                            <li>{locale.t('_dash', {returnObjects:true}).ratex_stats.total_exchangers}</li>
                            <li>{locale.t('_dash', {returnObjects:true}).ratex_stats.total_directions}</li>
                        </ul>
                    </div>
                    <div className="right-part">
                        <ul>
                            <li>{totalCurrencies}</li>
                            <li>{totalExchangers}</li>
                            <li>{totalCurrencyPairs}</li>
                        </ul>
                    </div>
                </div>
            );
            return(
                <div data-item-id={this.props.dataId} className='card no-min-height'>
                    <div className="card-header">
                        <div className="card-title">
                            {locale.t('_dash', {returnObjects:true}).ratex_stats.title}
                            <div className="setting-wrap" onClick={this.tooltipToggle}>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div data-tooltip={this.props.dataId} className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={()=>{
                                        this.props.deleteWidget(this.props.dataId);
                                    }}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-content no-min-height">
                        {statsRender}
                    </div>
                </div>

            );
        } else {
            //preloader until data is loaded
            return(
                <div data-item-id={this.props.dataId} className='card' style={{height: '200px!important'}}>
                    <div className="card-header">
                        <div className="card-title">

                        </div>
                    </div>
                    <div className="card-content no-min-height">
                        <div className="preloader-cont">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

    }
}

//connect to specified redux state tree object
function mapStateToProps(state) {

    return {
        locale: state.locale,
        dashInitialState: state.dashInitialState,
        ratexStats: state.dashInitialState.dash.ratex_stats
    }

}

export default connect(mapStateToProps)(RatexStats);