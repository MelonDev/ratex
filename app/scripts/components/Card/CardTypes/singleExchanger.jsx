import React from "react";
import {SimpleSelect} from "react-selectize";
import trackClick from "../../../reusable/trackClick";
import moment from "moment";
import {connect} from "react-redux";


class SingleExchanger extends React.Component {

    constructor(props) {

        super(props);
        this.showExchangersDropdown = this.showExchangersDropdown.bind(this);
        this.hideExchangersDropdown = this.hideExchangersDropdown.bind(this);
        this.changeExchanger = this.changeExchanger.bind(this);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.onActiveChange = this.onActiveChange.bind(this);
        this.exchangerSelected = null;
        this.state = {

            data: null,
            dropdownVisibe: false,
            showActive: false,
            showTooltip: false

        }

    }

    componentWillMount() {

        this.props.getExchanger('exch_QEhSr7G4l2JH3JGF', this.props.dataId);

    }

    componentDidUpdate() {

        if (typeof this.props.exchanger[this.props.dataId] != 'undefined' && !this.props.exchanger[this.props.dataId].loading) {

            this.exchangerSelected = this.props.exchanger[this.props.dataId].exchanger.alias;

        }

    }

    showExchangersDropdown() {
        this.refs.drop.focus();

        this.setState({
            dropdownVisibe: true
        });

    }

    tooltipToggle(e) {

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
        $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');

    }

    onActiveChange() {

        this.props.changeLimit(this.state.dropdownPlaceholder.value, !this.state.showActive);

        this.setState({
            showActive: !this.state.showActive
        });

    }

    changeExchanger(id) {

        localStorage.setItem('selectedExchanger', id);

        this.props.getExchanger(id, this.props.dataId);

    }


    hideExchangersDropdown() {
        this.refs.drop.blur();

        this.setState({
            dropdownVisibe: false
        });

    }


    render() {
        var self = this;
        let locale = this.props.locale.language;
        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility: this.state.showTooltip ? 'visible' : 'hidden'
        };
        const tooltipStyle = {
            display: this.state.showTooltip ? 'block' : 'none'
        };
        const tooltip = (
            <div className="tooltip" style={tooltipStyle}>
                <div className="tooltip-arrow"></div>
                <div className="tooltip-inner">
                    <div className="tooltip-title">Управление виджетом</div>
                </div>
                <div className="delete-widget" onClick={() => {
                    this.props.deleteWidget(this.props.dataId)
                }}>
                    <a href="#">удалить</a>
                </div>
            </div>
        );

        if (typeof this.props.exchanger != 'undefined' && typeof this.props.exchanger[this.props.dataId] != 'undefined' && !this.props.exchanger[this.props.dataId].loading) {

            moment.locale(this.props.locale.language.language);
            moment.updateLocale(this.props.locale.language.language, {
                relativeTime: {
                    past: '%s'
                }
            });
            let data = this.props.exchanger[this.props.dataId].exchanger;
            if (data.id != null) {
                var link = data.link_website;
                var linkPrettypre = link.replace(/.*?:\/\//g, "");
                var linkPretty = linkPrettypre.replace('\/', '');
                var id = data.id;
                var name = data.name;
                var wmbl = data.wmbl || '-';
                var wmid = data.wmid || '-';
                var founded = data.founded || '';
                var foundedFormatted = () => {
                    return "-"
                };
                if (founded != '') {
                    foundedFormatted = () => {
                        if (founded == null) {
                            return '-'
                        } else {
                            return moment(founded * 1000).fromNow();
                        }
                    };
                }
                var responsesNegative = data.responses_negative || '-';
                var responsesPositive = data.responses_positive || '-';
                var isActive = data.is_active;
                var rating = data.rating || "-";
                var reserve = data.reserve || '-';
                var description = data.description || '-';
                var exchangersAviliableRaw = this.props.dashExchangers.data;
                var exchangersAvaliable = exchangersAviliableRaw.map((elem, index) => {
                    return {value: elem.id, label: elem.name, key: index}
                });
                return (
                    <div data-item-id={this.props.dataId} className='card single-exchanger-card'
                         style={{width: 580}}>
                        <div className="card-header">
                            <div className="card-title">
                                <div className="setting-wrap" onClick={this.tooltipToggle}>
                                    <span style={iconStyle} className="icon ru-settings"></span>
                                    {tooltip}
                                </div>
                            </div>
                        </div>
                        <div className="card-content">
                            <div className="left-widget-block">
                                <div className="exchanger-logo">
                                    <img src="images/exchanger_icon.png" alt=""/>
                                </div>
                            </div>
                            <div className="right-widget-block">
                                <div className="exchanger-info">
                                    <div className="name">
                                        <div className="widget-header-count-block">
                                            <div className="ex-drop-wrap">
                                                <div className="selected-ex" onClick={this.showExchangersDropdown}>
                                                    {this.exchangerSelected}
                                                </div>
                                                <div
                                                    className={this.state.dropdownVisibe ? 'selectize-wrap open' : 'selectize-wrap hidden'}>
                                                    <SimpleSelect
                                                        options={exchangersAvaliable}
                                                        placeholder="Выберите валюту"
                                                        open={true}
                                                        onBlur={this.hideExchangersDropdown}
                                                        ref="drop"
                                                        defaultValue={this.state.exchangerSelected}
                                                        renderNoResultsFound={() => {
                                                            return ' ';
                                                        }}
                                                        onValueChange={function (item) {
                                                            self.setState({
                                                                exchangerSelected: item
                                                            });
                                                            if (!!item && !!item.newOption) {
                                                                self.state.selectCurOptions.unshift({
                                                                    label: item.label,
                                                                    value: item.value
                                                                });
                                                                self.setState({selectedCurOptions: self.state.selectedCurOptions});
                                                            }
                                                            self.changeExchanger(item.value);
                                                            self.hideExchangersDropdown();
                                                        }}
                                                        renderOption={function (item) {
                                                            return <div className="simple-option" style={{
                                                                display: "flex",
                                                                alignItems: "center"
                                                            }}>
                                                                <div className="cur-icon" style={{
                                                                    backgroundImage: 'url("images/WMZ.svg")'
                                                                }}></div>
                                                                <div className="cur-value" style={{marginLeft: 10}}>
                                                                    {!!item.newOption ? "Add " + item.label + " ..." : item.label}
                                                                </div>
                                                            </div>
                                                        }}
                                                        renderValue={ function (item) {
                                                            return <div className="simple-value">
                                                            </div>
                                                        }}>
                                                    </SimpleSelect>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="exchanger-link">
                                        <a onClick={() => {
                                            trackClick(id)
                                        }} href={link} target="_blank">{linkPretty}</a>
                                    </div>
                                    <div className="exchanger-params">
                                        <div className="left-block">
                                            <span>Webmoney WMID</span>
                                            <span>Webmoney BL</span>
                                            {/*<span>{locale.t('_dash', {returnObjects: true}).single_exchanger.reserve}</span>*/}
                                            {/*<span>{locale.t('_dash', {returnObjects: true}).single_exchanger.age}</span>*/}
                                            {/*<span>{locale.t('_dash', {returnObjects: true}).single_exchanger.responses}</span>*/}
                                            <span>Резерв</span>
                                            <span>Возраст</span>
                                            <span>Отзывы</span>
                                        </div>
                                        <div className="right-block">
                                            <div>{wmid}</div>
                                            <div>{wmbl}</div>
                                            <div>{reserve} USD</div>
                                            <div>{foundedFormatted()}</div>
                                            <div>{responsesPositive} / {responsesNegative}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );

            } else {
                return (
                    <div data-item-id={this.props.dataId} className='card single-exchanger-card' style={{width: 580}}>
                        <div className="card-header">
                            <div className="card-title"></div>
                        </div>
                        <div className="card-content">
                            <p>Виджет поломан.jpg</p>
                        </div>
                    </div>
                )
            }
        }
        if (this.state.data == 'error') {
            return (
                <div data-item-id={this.props.dataId} className='card single-exchanger-card' style={{width: 580}}>
                    <div className="card-header">
                        <div className="card-title"></div>
                    </div>
                    <div className="card-content">
                        <p>Виджет поломан.jpg</p>
                    </div>
                </div>
            )
        }
        return (

            <div data-item-id={this.props.dataId} className='card single-exchanger-card' style={{width: 580}}>
                <div className="card-header">
                    <div className="card-title"></div>
                </div>
                <div className="preloader-cont">
                    <div className="preloader">
                        <div className="circ1"></div>
                        <div className="circ2"></div>
                        <div className="circ3"></div>
                    </div>
                </div>
                <div className="card-content"></div>
            </div>

        );
    }
}


function mapStateToProps(state) {
    return {
        locale: state.locale,
        exchanger: state.dashInitialState.dash.exchanger_card,
        dashExchangers: state.dashExchangers
    }
}

export default connect(mapStateToProps)(SingleExchanger);

