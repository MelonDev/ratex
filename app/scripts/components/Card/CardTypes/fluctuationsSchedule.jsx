import React from "react";
import ReactHighcharts from "react-highcharts";
import jQuery from "jquery";
import * as ActionCreators from "../../../actions/widgets/nationalBankFluctuations";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
var $ = jQuery;


class FluctuationsSchedule extends React.Component {
    constructor(props) {
        super(props);
        this.tooltipToggle = this.tooltipToggle.bind(this);
        this.onActiveChange = this.onActiveChange.bind(this);
        this.config = {
            chart: {
                height: 150
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, 'rgba(214,243,235, .9)'],
                            [1, 'rgba(214,243,235, 0)']
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }


            },
            series: [{
                name: '',
                type: 'area',
                lineColor: '#14B489',
                lineWidth: 1,
                showInLegend: false,
                animation: false,
                showLegend: false,
                showTitle: false,
                data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4, 29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4],

                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            fillColor: '#14B489',
                            lineWidth: 0,
                            lineColor: '#14B489',
                            width: 4,
                            height: 4
                        }
                    }
                },
                point: {
                    events: {
                        mouseOver: function () {
                            let xAxis = this.series.chart.xAxis[0],
                                index = this.index,
                                category = this.series.xAxis.options.categories[index],
                                lastLabel = (this.series.xAxis.options.categories.length) - 1;
                            if (index !== 0 && index !== lastLabel && index != 1 && index != 2 && index != lastLabel - 1 && index != lastLabel - 2) {
                                xAxis.labelGroup.element.children[index].innerHTML = category;
                            }
                        },
                        mouseOut: function () {
                            let xAxis = this.series.chart.xAxis[0],
                                index = this.index,
                                lastLabel = (this.series.xAxis.options.categories.length) - 1;
                            if (index !== 0 && index !== lastLabel) {
                                xAxis.labelGroup.element.children[index].innerHTML = '';
                            }

                        }
                    }
                }
            }],
            xAxis: {
                tickWidth: 0,
                lineWidth: 0,
                crosshair: {
                    width: 1,
                    color: 'rgba(182,191,202, 0.7)',
                    dashStyle: 'dot'
                },
                labels: {
                    step: 1,
                    formatter: function () {
                        if (this.isLast || this.isFirst) {
                            return this.value;
                        } else {
                            return '';
                        }
                        // return this.value;
                    }

                },
                categories: ['12.08', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', '13.09', '12.08', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', '13.09'],

            },
            yAxis: {
                labels: {
                    style: {
                        fontFamily: 'Open Sans',
                        fontSize: '12px',
                        color: '#314156'
                    },
                    enabled: true,
                    formatter: function () {
                        if (this.isLast || this.isFirst) {
                            return this.value;
                        } else {

                        }
                    }
                },
                title: {
                    text: null
                },
                gridLineDashStyle: "dot",
                showFirstLabel: true,
                showLastLabel: true,
                endOnTick: true

            },
            title: {
                style: {
                    display: 'none'
                }
            },
            tooltip: {
                style: {
                    color: '#314156',
                    fontSize: '12px',
                    fontFamily: 'Open Sans'
                },
                backgroundColor: '#FFFFFF',
                borderColor: '#B6BFCA',
                borderRadius: 0,
                borderWidth: 1,
                shadow: false,
                formatter: function () {
                    return '' + this.y + '';
                }
            }
        }

        this.state = {
            showActive: false,
            showTooltip: false,
            dataLoaded: false,
        }
    }

    componentDidMount() {

        //$.ajax({
        //    dataType: 'json',
        //    url: config.api_url+'ajax/dashboard/widget/national_bank_fluctuations',
        //    beforeSend: function(xhr) { xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest"); },
        //    type:'GET',
        //    success:(data) => {
        //        let fluctuations = data.fluctuations;
        //        let ask = fluctuations.ask;
        //        let askFormatted = ask.map((elem)=>{
        //            return parseFloat(elem);
        //        });
        //        let timestamp = fluctuations.timestamp;
        //        let timestamps = timestamp.map((elem, index)=>{
        //            let time = elem * 1000;
        //            return(
        //                moment(time).format('DD.MM')
        //            )
        //        });
        //        this.state.config.xAxis.categories = timestamps.slice(0,30);
        //        this.state.config.series[0].data = askFormatted.slice(0,30);
        //        this.forceUpdate();
        //    },
        //    error: function(data, error){
        //        console.log(error);
        //    }
        //});

    }

    componentWillMount() {

        this.props.getDataForNationalBankFluctuationsWidget(this.props.dataId);

    }

    componentDidUpdate() {

        if (this.props.dashInitialState.data != null && this.state.dataLoaded == false) {

            // let data = this.props.dashInitialState.data.nationalBankFluctuations;
            // let fluctuations = data.fluctuations;
            // let ask = fluctuations.ask;
            // let askFormatted = ask.map((elem)=>{
            //     return parseFloat(elem);
            // });
            // let timestamp = fluctuations.timestamp;
            // let timestamps = timestamp.map((elem, index)=>{
            //     let time = elem * 1000;
            //     return(
            //         moment(time).format('DD.MM.YY')
            //     )
            // });
            //
            // this.state.config.xAxis.categories = timestamps.slice(0,30);
            // this.state.config.series[0].data = askFormatted.slice(0,30);
            // this.setState({
            //     dataLoaded: true
            // });
            //this.forceUpdate();

        }

    }

    tooltipToggle(e) {

        e.stopPropagation();
        let id = this.props.dataId;
        //we have to find exactly this tooltip
        $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
        $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');

    }

    onActiveChange() {

        this.props.changeLimit(this.state.dropdownPlaceholder.value, !this.state.showActive);

        this.setState({
            showActive: !this.state.showActive
        });

    }


    render() {
        const iconStyle = {
            opacity: this.state.showTooltip ? 1 : 0,
            visibility: this.state.showTooltip ? 'visible' : 'hidden'
        };
        if (typeof this.props.fluctuations != 'undefined' && typeof this.props.fluctuations[this.props.dataId] != 'undefined' && typeof this.props.fluctuations[this.props.dataId].data != 'undefined') {
            let locale = this.props.locale.language;

            const tooltipStyle = {
                display: this.state.showTooltip ? 'block' : 'none'
            };
            const tooltip = (
                <div className="tooltip" style={tooltipStyle}>
                    <div className="tooltip-arrow"></div>
                    <div className="tooltip-inner">
                        <div className="tooltip-title">Управление виджетом</div>
                    </div>
                    <div className="delete-widget" onClick={() => {
                        this.props.deleteWidget(this.props.dataId)
                    }}>
                        <a href="#">удалить</a>
                    </div>
                </div>
            );
            this.config.series[0].data = this.props.fluctuations[this.props.dataId].data;
            this.config.xAxis.categories = this.props.fluctuations[this.props.dataId].categories;
            return (
                <div data-item-id={this.props.dataId} className='card chart-card' style={ {width: '580px'} }
                     data-size={this.props.size} title={this.props.title} type={this.props.type}
                     tabIndex={this.props.tabindex}>

                    <div className="card-header">
                        <div className="card-title">

                            {locale.t('_dash', {returnObjects: true}).national_bank_fluctuations.title}
                            <div className="setting-wrap" onClick={this.tooltipToggle}>
                                <span style={iconStyle} className="icon ru-settings"></span>
                                <div className="preloader-wrapped">
                                    <div className="preloader">
                                        <div className="circ1"></div>
                                        <div className="circ2"></div>
                                        <div className="circ3"></div>
                                    </div>
                                </div>
                                <div data-tooltip={this.props.dataId} className="tooltip">
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление виджетом</div>
                                    </div>
                                    <div className="delete-widget" onClick={() => {
                                        this.props.deleteWidget(this.props.dataId)
                                    }}>
                                        <a href="#">удалить</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-content">
                        <ReactHighcharts config={this.config} ref="chart"/>
                    </div>
                </div>
            )


        } else {
            return (
                <div data-item-id={this.props.dataId} className="card  chart-card" style={{
                    width: '580px',
                }}>
                    <div className="card-header"></div>
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>
                    <div className="card-content">

                    </div>
                </div>
            )
        }


    }

}

function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};
}


function mapStateToProps(state, props) {

    return {
        locale: state.locale,
        fluctuations: state.dashInitialState.dash.national_bank_fluctuations
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(FluctuationsSchedule);