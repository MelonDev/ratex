import React from 'react';
import ReactDOM from 'react-dom';
import Dropdown from 'react-dropdown';
import jQuery from 'jquery';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTooltipPlacement, showWidgetPreloader, hideWidgetPreloader } from '../../../reusable/extendingFunctions';
import trackClick from '../../../reusable/trackClick';
import config from '../../../config';
import * as FavoriteExchangersActions from '../../../actions/favorites/favoriteExchangers';
import ExchangerTooltip from '../../Tooltips/exchangerTooltip.jsx';

var $ = jQuery;

class TopExchangers extends React.Component {

  constructor(props) {
    super(props);
    this.exchangers = props.exchangers;
    this.onLimitChange = this.onLimitChange.bind(this);
    this.onActiveChange = this.onActiveChange.bind(this);
    this.tooltipToggle = this.tooltipToggle.bind(this);
    this.showExchangerTooltip = this.showExchangerTooltip.bind(this);
    this.exchangersCountOption = [{ value: 5, label: 'Топ 5' }, { value: 10, label: 'Топ 10' }, {
      value: 15,
      label: 'Топ 15'
    }];
    this.state = {
      data: null,
      showTooltip: false,
      showActive: false,
      dropdownPlaceholder: this.exchangersCountOption[0],
      exchangersCount: 5
    }
  }

  contains(arr, needle) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i].id == needle) {
        return true
      }
    }
    return false;
  }

  showExchangerTooltip(ref) {
    let elem = ReactDOM.findDOMNode(this.refs[ref]);
    var tooltip = $(elem).find('.exTooltip').selector;
    $(tooltip).addClass('right');
    var placement = getTooltipPlacement(tooltip);
    if (placement == 'right') {
      $('.exTooltip').removeClass('left');
      $('.exTooltip').addClass('right');
    }
    if (placement == 'left') {
      $('.exTooltip').removeClass('right');
      $('.exTooltip').addClass('left');
    }
    if (!this.contains(this.props.exchangerTooltip, ref)) {
      this.props.getExchangerTooltip(ref);
    }
    $(elem).parent().addClass('tooltip-visible');
    $('.top-exchangers').addClass('pop-up-active');
  }

  hideExchangerTooltip(ref) {
    let elem = ReactDOM.findDOMNode(this.refs[ref]);
    $(elem).parent().removeClass('tooltip-visible');
    $('.top-exchangers').removeClass('pop-up-active');
  }


  onLimitChange(val) {
    this.setState({
      dropdownPlaceholder: val
    });
    this.props.changeTopExchangersLimit(val.value, this.state.showActive, this.props.dataId);
  }

  tooltipToggle(e) {
    e.stopPropagation();
    let id = this.props.dataId;
    $('.tooltip').not('.tooltip[data-tooltip="' + id + '"]').removeClass('visible');
    $('.tooltip[data-tooltip="' + id + '"]').toggleClass('visible');
  }

  onActiveChange() {
    const self = this;
    const data = JSON.stringify({
      "return_active": !this.state.showActive,
      "limit": this.state.dropdownPlaceholder.value
    });
    this.setState({
      showActive: !this.state.showActive
    });
    $.ajax({
      dataType: 'json',
      url: config.api_url + 'ajax/dashboard/widget/exchangers',
      beforeSend: function(xhr) {
        showWidgetPreloader(self.refs[self.props.dataId]);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
      },
      type: 'POST',
      contentType: 'applications/json',
      data: data,
      success: function(data) {
        hideWidgetPreloader(self.refs[self.props.dataId]);
        self.setState({
          data: data.exchangers
        });
      },
      error: function(data, error) {
      }
    });
  }

  componentDidUpdate() {
    if (typeof window != 'undefined') {
      pckry.shiftLayout();
    }
  }


  componentWillMount() {
    this.props.changeTopExchangersLimit(5, false, this.props.dataId);
  }

  render() {
    var self = this;
    let locale = this.props.locale.language;
    let tooltipStyle = {
      display: this.state.showTooltip ? 'block' : 'none'
    };
    let iconStyle = {
      opacity: this.state.showTooltip ? 1 : 0,
      visibility: this.state.showTooltip ? 'visible' : 'hidden'
    };
    const tooltip = (
      <div className="tooltip">
        <div className="tooltip-arrow"></div>
        <div className="tooltip-inner">
          <div className="tooltip-title">Управление обменниками</div>
          <input onChange={this.onActiveChange} id="disable-non-active" type="checkbox"/>
          <label checked={this.state.showActive} htmlFor="disable-non-active" className="toggle-label">Исключить
            неактивные</label>
        </div>
        <div className="delete-widget" onClick={() => {
          this.props.deleteWidget(this.props.dataId)
        }}>
          <a href="#">удалить</a>
        </div>
      </div>
    );
    if (typeof this.props.topExchangers != 'undefined' && typeof this.props.topExchangers[this.props.dataId] != 'undefined' && typeof this.props.topExchangers[this.props.dataId].exchangers != 'undefined') {
      let currentProps = this.props.topExchangers[this.props.dataId];
      let ajaxLoading = false;
      if (typeof currentProps.loading != 'undefined') {
        ajaxLoading = currentProps.loading;
      }
      var exchangersCountOption = [
        { value: 5, label: locale.t('_dash', { returnObjects: true }).top_exchangers.top + ' 5' },
        { value: 10, label: locale.t('_dash', { returnObjects: true }).top_exchangers.top + ' 10' },
        { value: 15, label: locale.t('_dash', { returnObjects: true }).top_exchangers.top + ' 15' }
      ];

      this.exchangers = this.props.topExchangers[this.props.dataId];
      let placeholder = this.exchangers.limit == 5 ? exchangersCountOption[0] : this.exchangers.limit == 10 ? exchangersCountOption[1] : exchangersCountOption[2];
      let data = this.exchangers.exchangers;
      let defaultExchangers = data;
      let defaultExLay = defaultExchangers.map(function(elem, index) {
        return (
          <tr key={index}>
            <td ref={elem.id}>
              <div className="ex-name-wrap" onMouseEnter={() => {
                self.showExchangerTooltip(elem.id);
              }}
                   onMouseLeave={() => {
                     self.hideExchangerTooltip(elem.id);
                   }}>
                <a onClick={() => {
                  trackClick(elem.id)
                }} href={elem.exchanger_site} target="_blank">
                  <img className="ex-logo-table" src="images/exchanger_icon.png" alt=""/>{elem.name}
                </a>
                <span className="topExchangerName"><ExchangerTooltip
                  addFavoriteExchanger={self.props.addFavoriteExchanger}
                  favoriteExchangers={self.props.favoriteExchangers}
                  data={self.props.exchangerTooltip} id={elem.id}/></span>
              </div>
            </td>
            {/* <td>{elem.reserves} <span className="gray-curr">USD</span></td> */}
            <td style={{
              paddingRight: 0
            }}>{elem.wmbl ? elem.wmbl : String.fromCharCode(8212)}</td>
            <td>{elem.rating.toFixed(3)}</td>
            <td>
              <span
                className={self.props.favoriteExchangers.hasOwnProperty(elem.id) ? 'favorite-star icon-star notransition active' : 'favorite-star icon-star notransition'}
                onClick={() => {
                  if (self.props.favoriteExchangers.hasOwnProperty(elem.id)) {
                    self.props.removeFavoriteExchanger(elem.id);
                  } else {
                    self.props.addFavoriteExchanger(elem.id);
                  }
                }}
              ></span>
            </td>
          </tr>
        );

      });

      return (
        <div ref={this.props.dataId} data-item-id={this.props.dataId} className='card top-exchangers'
             style={ { 'width': 580 } } data-size={this.props.size} title={this.props.title}
             type={this.props.type} tabIndex={this.props.tabindex}>
          <div className="card-header">
            <div className="card-title">
              <div className="widget-header-count-block"><Dropdown onChange={this.onLimitChange}
                                                                   value={placeholder}
                                                                   options={exchangersCountOption}
                                                                   placeholder={this.state.dropdownPlaceholder}/>
              </div>
              {locale.t('_dash', { returnObjects: true }).top_exchangers.exchangers}


              <div className={ajaxLoading ? "setting-wrap ajax-loading" : 'setting-wrap'}
                   onClick={this.tooltipToggle}>
                <span style={iconStyle} className="icon ru-settings"></span>
                <div className="preloader-wrapped">
                  <div className="preloader">
                    <div className="circ1"></div>
                    <div className="circ2"></div>
                    <div className="circ3"></div>
                  </div>
                </div>
                <div data-tooltip={this.props.dataId} className="tooltip">
                  <div className="tooltip-arrow"></div>
                  <div className="tooltip-inner">
                    <div className="tooltip-title">Управление обменниками</div>
                    <input onChange={this.onActiveChange} id="disable-non-active" type="checkbox"/>
                    <label checked={this.state.showActive} htmlFor="disable-non-active"
                           className="toggle-label">Исключить неактивные</label>
                  </div>
                  <div className="delete-widget" onClick={() => {
                    this.props.deleteWidget(this.props.dataId)
                  }}>
                    <a href="#">удалить</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div className="card-content">
            <div className="top-exchangers-wrap">
              <table>
                <thead>
                <tr>
                  <th>{locale.t('_dash', { returnObjects: true }).top_exchangers.name}</th>
                  <th>BL</th>
                  <th>рейтинг</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                {defaultExLay}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div data-item-id={this.props.dataId} className='card' style={ { 'width': 580 } }
             data-size={this.props.size} title={this.props.title} type={this.props.type}
             tabIndex={this.props.tabindex}>
          <div className="card-header">
            <div className="card-title">
            </div>
          </div>
          <div className="preloader-cont">
            <div className="preloader">
              <div className="circ1"></div>
              <div className="circ2"></div>
              <div className="circ3"></div>
            </div>
          </div>
        </div>
      );
    }
  };
}

function mapDispatchToProps(dispatch) {
  return { ...bindActionCreators({ ...FavoriteExchangersActions }, dispatch), dispatch };
}


function mapStateToProps(state) {

  return {
    locale: state.locale,
    topExchangers: state.dashInitialState.dash.top_exchangers,
    exchangerTooltip: state.exchangerTooltip,
    favoriteExchangers: state.favoriteExchangers,
    dashLoading: state.dashInitialState.dash.loading
  }

}
export default connect(mapStateToProps, mapDispatchToProps)(TopExchangers);