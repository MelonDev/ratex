import React from 'react';

class PreloaderInner extends React.Component {

  render() {
    return (
        <div className="preloader">
          <div className="circ1"></div>
          <div className="circ2"></div>
          <div className="circ3"></div>
        </div>
    );
  }
}

export default PreloaderInner;