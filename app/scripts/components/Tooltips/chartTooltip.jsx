import React from 'react';
import ReactDOM from "react-dom";
import ReactHighcharts from 'react-highcharts';
import classnames from 'classnames';
import moment from 'moment';
import jQuery from 'jquery';
var $ = jQuery;
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getTooltipPlacement} from '../../reusable/extendingFunctions';
import * as ActionCreators from "../../actions/tooltips/chartTooltipActions";


class ChartTooltip extends React.Component{

    constructor(props) {
        super(props);
        this.hasDataForTooltip = this.hasDataForTooltip.bind(this);
        this.loadTooltip = this.loadTooltip.bind(this);
        this.contains = this.contains.bind(this);
        this.config = {
            chart: {
                height: 150
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, 'rgba(214,243,235, .9)'],
                            [1, 'rgba(214,243,235, 0)']
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }


            },
            series: [{
                name: '',
                type: 'area',
                lineColor: '#14B489',
                lineWidth: 1,
                showInLegend: false,
                animation: false,
                showLegend: false,
                showTitle: false,
                data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4, 29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 295.6, 454.4],

                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            fillColor: '#14B489',
                            lineWidth: 0,
                            lineColor: '#14B489',
                            width: 4,
                            height: 4
                        }
                    }
                },
                point: {
                    events: {
                        mouseOver: function () {
                          let xAxis = this.series.chart.xAxis[0],
                            index = this.index,
                            category = this.series.xAxis.options.categories[index],
                            lastLabel = (this.series.xAxis.options.categories.length) - 1;
                          if (index !== 0 && index !== lastLabel && index != 1 && index != 2 && index != lastLabel - 1 && index != lastLabel - 2) {
                            xAxis.labelGroup.element.children[index].innerHTML = category;
                          }
                        },
                        mouseOut: function () {
                            let xAxis = this.series.chart.xAxis[0],
                                index = this.index,
                                lastLabel = (this.series.xAxis.options.categories.length) - 1;
                            if (index !== 0 && index !== lastLabel) {
                                xAxis.labelGroup.element.children[index].innerHTML = '';
                            }

                        }
                    }
                }
            }],
            xAxis: {
                tickWidth: 0,
                lineWidth: 0,
                crosshair: {
                    width: 1,
                    color: 'rgba(182,191,202, 0.7)',
                    dashStyle: 'dot'
                },
                labels: {
                    step: 1,
                    formatter: function () {
                        if (this.isLast || this.isFirst) {
                            return this.value;
                        } else {
                            return '';
                        }
                        // return this.value;
                    }

                },
                categories: ['12.08', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', '13.09', '12.08', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', '13.09'],

            },
            yAxis: {
                labels: {
                    style: {
                        fontFamily: 'Open Sans',
                        fontSize: '12px',
                        color: '#314156'
                    },
                    enabled: true,
                    formatter: function () {
                        if (this.isLast || this.isFirst) {
                            return this.value;
                        } else {

                        }
                    }
                },
                title: {
                    text: null
                },
                gridLineDashStyle: "dot",
                showFirstLabel: true,
                showLastLabel: true,
                endOnTick: true

            },
            title: {
                style: {
                    display: 'none'
                }
            },
            tooltip: {
                style: {
                    color: '#314156',
                    fontSize: '12px',
                    fontFamily: 'Open Sans'
                },
                backgroundColor: '#FFFFFF',
                borderColor: '#B6BFCA',
                borderRadius: 0,
                borderWidth: 1,
                shadow: false,
                formatter: function () {
                    return '' + this.y + '';
                }
            }
        }

    }



    hasDataForTooltip(){

        var contains = false;
        var data = this.props.currencyTooltip;
        for(var i = 0; i < data; i++){

            if (data[i].id = this.props.id){
                contains = true;
            }

        }

        return contains;

    }

    componentDidUpdate(){

        let elem = ReactDOM.findDOMNode(this.refs.elem);


        $(elem).addClass('right');
        var placement = getTooltipPlacement(elem);
        if(placement == 'right'){
            $(elem).removeClass('left');
            $(elem).addClass('right');
        }
        if(placement == 'left'){
            $(elem).removeClass('right');
            $(elem).addClass('left');
        }


    }

    loadTooltip(){
        if(!this.hasDataForTooltip()){

            this.props.loadData(this.props.id);

        }

    }

    contains (arr, needle) {

        for(var i = 0; i < arr.length; i++){

            if(arr[i].id == needle){
                return true
            }

        }

        return false;

    }

    render(){
            if(this.props.chartTooltip.hasOwnProperty(this.props.currency)){
                let dates = this.props.chartTooltip[this.props.currency].dates;
                let rates = this.props.chartTooltip[this.props.currency].rates;
                let askFormatted = rates.map((elem)=>{
                    return parseFloat(elem);
                });
                let timestamps = dates.map((elem, index)=>{
                    let time = elem;
                    return(
                        moment(time).format('DD.MM.YY')
                    )
                });
                this.config.series[0].data = askFormatted;

                this.config.xAxis.categories = timestamps;
                return (

                    <div ref="elem" className="currPairTooltip chartTooltip">
                        <div className="currPairTooltipWrap chartTooltipWrap">
                            <ReactHighcharts  config={this.config} ref="chart"/>
                        </div>
                    </div>
                )
            }else{

                return(

                    <div ref="elem" className="currPairTooltip chartTooltip">
                        <div className="currPairTooltipWrap chartTooltipWrap">
                            <div className="preloader-cont">
                                <div className="preloader">
                                    <div className="circ1"></div>
                                    <div className="circ2"></div>
                                    <div className="circ3"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                );

            }


    }
}
function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};
}


function mapStateToProps(state, props) {

    return {
       chartTooltip: state.chartTooltip
    }

}
export default connect(mapStateToProps, mapDispatchToProps)(ChartTooltip);