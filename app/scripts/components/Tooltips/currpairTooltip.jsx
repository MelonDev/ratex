import React from 'react';
import ReactDOM from 'react-dom';
import { getTooltipPlacement } from '../../reusable/extendingFunctions';

class CurrPairTooltip extends React.Component {

  constructor(props) {
    super(props);
    this.hasDataForTooltip = this.hasDataForTooltip.bind(this);
    this.loadTooltip = this.loadTooltip.bind(this);
    this.contains = this.contains.bind(this);
    this.state = {

      currpairId: props.id

    }
  }

  hasDataForTooltip() {

    var contains = false;
    var data = this.props.currpairTooltip;
    for (var i = 0; i < data; i++) {

      if (data[i].id = this.props.id) {
        contains = true;
      }

    }

    return contains;

  }

  componentDidUpdate() {
    let elem = ReactDOM.findDOMNode(this.refs.elem);

    $(elem).addClass('right');
    var placement = getTooltipPlacement(elem);
    if (placement == 'right') {
      $(elem).removeClass('left');
      $(elem).addClass('right');
    }
    if (placement == 'left') {
      $(elem).removeClass('right');
      $(elem).addClass('left');
    }


  }

  loadTooltip() {
    if (!this.hasDataForTooltip()) {

      this.props.loadData(this.props.id);

    }

  }

  contains(arr, needle) {

    for (var i = 0; i < arr.length; i++) {

      if (arr[i].id == needle) {
        return true
      }

    }

    return false;

  }

  render() {
    if (this.props.data.length > 0 && this.contains(this.props.data, this.props.id)) {
      let locale = this.props.locale.language;
      let bestRateTemplate;
      for (var i = 0; i < this.props.data.length; i++) {
        var data = this.props.data;
        if (data[i].id == this.props.id) {
          var data = data[i];
          var averageRate = parseFloat(data.average_rate).toFixed(3);
          var bestRateRate = data.best_rate == null ? null : parseFloat(data.best_rate.rate).toFixed(3);
          var bestExchanger = data.best_rate == null ? null : data.best_rate.organization.name;
          var bestExchangerUrl = data.best_rate == null ? null : data.best_rate.organization.link_website;
          var id = data.id;
          var offers = data.offers;
          var popularity = data.popularity;
          if (bestRateRate == null || bestExchanger == null || bestExchangerUrl == null) {
            bestRateTemplate = (
              <span></span>
            )
          } else {
            bestRateTemplate = (
              <div>
                <div className="left-part no-float">
                  <div className="item">
                    {locale.t('_tooltip', { returnObjects: true }).best_rate_from}
                  </div>
                </div>
                <div className="left-part" style={{paddingTop: '3px'}}>
                  <a className="ex-link" href={bestExchangerUrl}>
                    {bestExchanger}
                  </a>
                </div>
                <div className="right-part">
                  <div className="r-item best-offer">
                    1
                    <span className="ru-arrow"></span>
                    {bestRateRate}
                  </div>
                </div>
              </div>
            )
          }
        }
      }
      return (
        <div ref="elem" className="currPairTooltip">
          <div className="currPairTooltipWrap">
            <div className="currpair-visual">
              <div className=" currpair-item">
                <img src={this.props.iconFrom ? this.props.iconFrom : '/images/currency_placeholder.svg'} alt=""/>
                {this.props.from}
              </div>
              <div className="ru-arrow"></div>
              <div className=" currpair-item">
                <img src={this.props.iconFrom ? this.props.iconTo : '/images/currency_placeholder.svg'} alt=""/>
                {this.props.to}
              </div>
            </div>
            <div className="info-block">
              <div className="left-part">
                <div className="item">
                  {locale.t('_tooltip', { returnObjects: true }).offers_total}
                </div>
                <div className="item">
                  {locale.t('_tooltip', { returnObjects: true }).popularity}
                </div>
                <div className="item">
                  {locale.t('_tooltip', { returnObjects: true }).average_rate_from}
                </div>
              </div>
              <div className="right-part">
                <div className="r-item offers">{offers}</div>
                <div className="r-item rating-fluctation">{popularity}</div>
                <div className="r-item average-rate"> 1 <span className="ru-arrow"></span> {averageRate}</div>
              </div>
            </div>

            <div className="best-offer-wrap">
              {bestRateTemplate}
            </div>
          </div>
        </div>
      )


    }

    return (

      <div ref="elem" className="currPairTooltip right">
        <div className="currPairTooltipWrap">
          <div className="preloader-cont">
            <div className="preloader">
              <div className="circ1"></div>
              <div className="circ2"></div>
              <div className="circ3"></div>
            </div>
          </div>
          <div className="tooltip-transparent">
            <div className="currpair-visual">
              <div className=" currpair-item">
                <img src="images/WMZ.svg" alt=""/>
                WMZ
              </div>
              <div className="ru-arrow"></div>
              <div className=" currpair-item">
                <img src="images/PBUAH.svg" alt=""/>
                YD
              </div>
            </div>
            <div className="info-block">
              <div className="left-part">
                <div className="item">
                  Предложений
                </div>
                <div className="item">
                  Популярность
                </div>
                <div className="item">
                  Средний курс от
                </div>
              </div>
              <div className="right-part">
                <div className="r-item offers">32</div>
                <div className="r-item rating-fluctation">+2,4%</div>
                <div className="r-item average-rate"> 1 <span className="ru-arrow"></span> 24,05</div>
              </div>
            </div>

            <div className="best-offer-wrap">
              <div className="left-part no-float">
                <div className="item">
                  Лучший курс от
                </div>
              </div>

              <div className="left-part">
                <a className="ex-link" href="#"><img src="images/ex-logo.png" alt=""/>Exhcanger</a>
              </div>
              <div className="right-part">
                <div className="r-item best-offer">1 <span className="ru-arrow"></span> 24,35</div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );

  }
}
export default CurrPairTooltip;