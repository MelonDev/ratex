import React from 'react';
import moment from 'moment';
import { FavoriteExchangers } from '../../app';


class ExchangerTooltip extends React.Component {

  constructor(props) {
    super(props);
    this.hasDataForTooltip = this.hasDataForTooltip.bind(this);
    this.loadTooltip = this.loadTooltip.bind(this);
    this.contains = this.contains.bind(this);
    this.state = {

      currpairId: props.id

    }
  }

  hasDataForTooltip() {

    var contains = false;
    var data = this.props.exchangerTooltip;
    for (var i = 0; i < data; i++) {

      if (data[i].id = this.props.id) {
        contains = true;
      }

    }

    return contains;

  }

  componentDidUpdate() {
    //let elem = ReactDOM.findDOMNode(this.refs.elem);
    //
    //
    //$(elem).addClass('right');
    //var placement = getTooltipPlacement(elem);
    //if(placement == 'right'){
    //    $(elem).removeClass('left');
    //    $(elem).addClass('right');
    //}
    //if(placement == 'left'){
    //    $(elem).removeClass('right');
    //    $(elem).addClass('left');
    //}


  }

  componentDidMount() {

  }

  loadTooltip() {
    if (!this.hasDataForTooltip()) {

      this.props.loadData(this.props.id);

    }

  }

  contains(arr, needle) {

    for (var i = 0; i < arr.length; i++) {

      if (arr[i].id == needle) {
        return true
      }

    }

    return false;

  }

  render() {
    if (this.props.data.length > 0 && this.contains(this.props.data, this.props.id)) {
      for (var i = 0; i < this.props.data.length; i++) {

        var data = this.props.data;
        if (data[i].id == this.props.id) {
          var data = data[i];
          var founded = data.founded;
          var id = data.id;
          var isActive = data.is_active;
          var rating = data.rating;
          var reserve = data.reserve;
          var resNeg = data.responses_negative;
          var resPos = data.responses_positive;
          var link = data.url;
          var linkPrettypre = link.replace(/.*?:\/\//g, "");
          var linkPretty = linkPrettypre.replace('\/', '');
          var wmbl = data.wmbl;
          var wmid = data.wmid;
          var name = data.name;
          var foundedFormatted = () => {
            if (founded == null) {
              return '-'
            } else {
              return moment(founded * 1000).fromNow();
            }
          };

        }

      }
      return (


        <div ref='elem' id={'ex-tooltip' + id} className="exTooltip">
            <div className="exTooltipWrap">
                <div className="left-tooltip-block">
                    <div className="exchanger-logo">
                        <img src="images/exchanger_icon.png" alt=""/>
                        <div className="name">{name}</div>
                        <div
                          className={this.props.favoriteExchangers.hasOwnProperty(id) ? 'favorite-star icon-star active notransition' : 'favorite-star icon-star notransition'}
                          onClick={
                            () => {
                              if (!$('#ex-tooltip' + id + ' .favorite-star').hasClass('active')) {
                                this.props.addFavoriteExchanger(id);
                                $('#ex-tooltip' + id + ' .favorite-star').addClass('active');
                              } else {
                                this.props.removeFavoriteExchanger(id);
                                $('#ex-tooltip' + id + ' .favorite-star').removeClass('active');
                              }

                            }}></div>
                    </div>
                </div>
                <div className="right-tooltip-block">
                    <div className="exchanger-info">
                        <div className="exchanger-link">
                            <a href={link} target="_blank">{linkPretty}</a>
                        </div>
                        <div className="exchanger-params">
                            <div>
                                <div className="left-block">
                                    <span>Webmoney WMID</span>
                                </div>
                                <div className="right-block">
                                    <div>{wmid}</div>
                                </div>
                            </div>
                            <div>
                                <div className="left-block">
                                    <span>Webmoney BL</span>
                                </div>
                                <div className="right-block">
                                    <div>{wmbl}</div>
                                </div>
                            </div>
                            <div>
                                <div className="left-block">
                                    <span>Сумма резервов</span>
                                </div>
                                <div className="right-block">
                                    <div>{reserve} USD</div>
                                </div>
                            </div>
                            <div>
                                <div className="left-block">
                                    <span>Возраст</span>
                                </div>
                                <div className="right-block">
                                    <div>{ founded ? moment(founded * 1000).fromNow() : '-'}</div>
                                </div>
                            </div>
                            <div>
                                <div className="left-block">
                                    <span>Отзывы</span>
                                </div>
                                <div className="right-block">
                                    <div><span className="color-red">{resNeg}</span> / <span
                                      className="color-green">{resPos}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      )

    }

    return (

      <div ref="elem" className="exTooltip">
          <div className="exTooltipWrap">
              <div className="preloader-cont">
                  <div className="preloader">
                      <div className="circ1"></div>
                      <div className="circ2"></div>
                      <div className="circ3"></div>
                  </div>
              </div>
              <div className="tooltip-transparent">
                  <div className="left-tooltip-block">
                      <div className="exchanger-logo">
                          <img src="images/ex-logo.png" alt=""/>
                          <div className="name">ExChanger</div>
                          <div className="icon icon-star"></div>
                      </div>
                  </div>
                  <div className="right-tooltip-block">
                      <div className="exchanger-info">
                          <div className="exchanger-link">
                              <a href="#" target="_blank">www.exchanger.net</a>
                          </div>
                          <div className="exchanger-params">
                              <div>
                                  <div className="left-block">
                                      <span>Webmoney WMID</span>
                                  </div>
                                  <div className="right-block">
                                      <div>232323123123123</div>
                                  </div>
                              </div>
                              <div>
                                  <div className="left-block">
                                      <span>Webmoney BL</span>
                                  </div>
                                  <div className="right-block">
                                      <div>3256</div>
                                  </div>
                              </div>
                              <div>
                                  <div className="left-block">
                                      <span>Сумма резервов</span>
                                  </div>
                                  <div className="right-block">
                                      <div>50 000 USD</div>
                                  </div>
                              </div>
                              <div>
                                  <div className="left-block">
                                      <span>Возраст</span>
                                  </div>
                                  <div className="right-block">
                                      <div>5 лет и 6 месяцев</div>
                                  </div>
                              </div>
                              <div>
                                  <div className="left-block">
                                      <span>Страна</span>
                                  </div>
                                  <div className="right-block">
                                      <div>Украина</div>
                                  </div>
                              </div>
                              <div>
                                  <div className="left-block">
                                      <span>Отзывы</span>
                                  </div>
                                  <div className="right-block">
                                      <div><span className="color-red">2</span> / <span
                                        className="color-green">109</span></div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <a href="#" className="details">Детальнее</a>
                  </div>
              </div>
          </div>
      </div>

    );

  }
}
export default ExchangerTooltip;