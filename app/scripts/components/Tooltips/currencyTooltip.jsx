import React from 'react';
import ReactDOM from "react-dom";
import classnames from 'classnames';
import moment from 'moment';
import jQuery from 'jquery';
var $ = jQuery;
import {getTooltipPlacement} from '../../reusable/extendingFunctions';

class CurrencyTooltip extends React.Component{

    constructor(props){
        super(props);
        this.hasDataForTooltip = this.hasDataForTooltip.bind(this);
        this.loadTooltip = this.loadTooltip.bind(this);
        this.contains = this.contains.bind(this);
        this.state = {

            currpairId : props.id

        }
    }

    hasDataForTooltip(){

        var contains = false;
        var data = this.props.currencyTooltip;
        for(var i = 0; i < data; i++){

            if (data[i].id = this.props.id){
                contains = true;
            }

        }

        return contains;

    }

    componentDidUpdate(){
        let elem = ReactDOM.findDOMNode(this.refs.elem);


        $(elem).addClass('right');
        var placement = getTooltipPlacement(elem);
        if(placement == 'right'){
            $(elem).removeClass('left');
            $(elem).addClass('right');
        }
        if(placement == 'left'){
            $(elem).removeClass('right');
            $(elem).addClass('left');
        }


    }

    loadTooltip(){
        if(!this.hasDataForTooltip()){

            this.props.loadData(this.props.id);

        }

    }

    contains (arr, needle) {

        for(var i = 0; i < arr.length; i++){

            if(arr[i].id == needle){
                return true
            }

        }

        return false;

    }

    render(){

        if(this.props.data.length > 0 && this.contains(this.props.data, this.props.id)){
            let locale = this.props.locale.language;

            for (var i = 0; i < this.props.data.length; i++){
                var data = this.props.data;

                if(data[i].id == this.props.id){

                    var data = data[i];
                    var id = data.id;
                    var type = data.type;
                    var symbol = data.symbol || String.fromCharCode(8212);

                }

            }

            return (

                <div ref="elem" className="currPairTooltip currencyTooltip">
                    <div className="currPairTooltipWrap">
                        <div className="info-block">
                            <div className="left-part">
                                <div className="item">
                                    {locale.t('_tooltip', {returnObjects:true}).name}
                                </div>
                                <div className="item">
                                    {locale.t('_tooltip', {returnObjects:true}).type}
                                </div>
                                <div className="item">
                                  {locale.t('_tooltip', {returnObjects:true}).symbol}
                                </div>
                            </div>
                            <div className="right-part">
                                <div className="r-item offers">{id}</div>
                                <div className="r-item offers">{type}</div>
                                <div className="r-item offers">{symbol}</div>
                            </div>
                        </div>
                    </div>
                </div>
            )




        }

        return(

            <div ref="elem" className="currPairTooltip currencyTooltip right">
                <div className="currPairTooltipWrap">
                    <div className="preloader-cont">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>
                    <div className="tooltip-transparent">
                        <div className="info-block">
                            <div className="left-part">
                                <div className="item">
                                    Валюта
                                </div>
                                <div className="item">
                                    Страна
                                </div>
                                <div className="item">
                                    Название
                                </div>
                            </div>
                            <div className="right-part">
                                <div className="r-item offers">32</div>
                                <div className="r-item rating-fluctation">+2,4%</div>
                                <div className="r-item average-rate"> 1 <span className="ru-arrow"></span> 24,05</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );

    }
}
export default CurrencyTooltip;