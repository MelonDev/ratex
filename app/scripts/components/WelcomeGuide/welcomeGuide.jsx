import React from "react";
import Slider from "react-slick";
import jQeury from 'jquery';
var $ = jQeury;


class WelcomeGuide extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: {
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,

            }
        }
    }


    shouldComponentUpdate(nextProps, nextState) {
        return true
    }

    componentDidMount() {

    }

    hideGuide(){
        $('.welcome-guide-wrap').removeClass('visible');

    }


    componentWillMount() {

    }

    render() {

        return (
            <div className="welcome-guide-wrap">
                <div className="x-cont" onClick={this.hideGuide}>
                    <span className="ru-menu-close"></span>
                </div>
                <div className="backdrop" onClick={this.hideGuide}></div>
                    <div className="left-arrow"></div>
                    <div className="right-arrow"></div>
                    <div className="slider">
                        <Slider {...this.state.settings}>
                            <div className="slide-item">
                                <div className="slide-item-content-wrap">
                                    <img src="images/welcome_guide/add_widget_light.png" alt=""/>
                                    <div className="slider-descr">
                                        <p>Можно добавлять и удалять виджеты из нашего набора,</p>
                                        <p>который постоянно пополняется</p>
                                    </div>
                                </div>
                            </div>
                            <div className="slide-item">
                                <div className="slide-item-content-wrap">
                                    <img src="images/welcome_guide/move_widget_light.png" alt=""/>
                                    <div className="slider-descr">
                                        <p>Виджеты можно перемещать,</p>
                                        <p>система запомнит их расположение</p>
                                    </div>
                                </div>
                            </div>
                            <div className="slide-item">
                                <div className="slide-item-content-wrap">
                                    <img src="images/welcome_guide/Ratex_api_ligh.png" alt=""/>
                                    <div className="slider-descr">
                                        <p>Мы предоставляем удобный API</p>
                                        <p>Для доступа к нашим данным</p>
                                    </div>
                                </div>
                            </div>
                            <div className="slide-item">
                                <div className="slide-item-content-wrap">
                                    <img src="images/welcome_guide/widget_modification_light.png" alt=""/>
                                    <div className="slider-descr">
                                         <p>Можно изменять настройки каждого виджета</p>
                                    </div>
                                </div>
                            </div>
                            <div className="slide-item">
                                <div className="slide-item-content-wrap">
                                    <img src="images/welcome_guide/widget_settings_light.png" alt=""/>
                                    <div className="slider-descr">
                                         <p>Можно модифицировать некоторые виджеты,</p>
                                        <p>что помогает видеть именно нужные данные</p>
                                    </div>
                                </div>
                            </div>
                        </Slider>
                    </div>
            </div>
        );

    }

}
export default WelcomeGuide;