import React from 'react';
import ReactDOM from "react-dom";
import classnames from 'classnames';
import jQuery from 'jquery';
var $ = jQuery;


class Success extends React.Component{

    constructor(){

        super();
        this.fadeOutFlash = this.fadeOutFlash.bind(this);
    }

    fadeOutFlash(){

        $('.success-message').fadeOut();

    }

    render(){
        return(
            <div className="flash-message success-message">
                    <div className="flash-icon"><span className="ru-check"></span></div>
                    <div className="flash-text">Отличная работа! Виджет <span className="name"></span> успешно добавлен</div>
                    <div className="flash-close" onClick={this.fadeOutFlash}><span className="ru-esc"></span></div>
            </div>
        );
    }
}
export default Success;