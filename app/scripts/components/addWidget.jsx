import React from 'react';
import classnames from 'classnames';
import SearchInput, { createFilter } from 'react-search-input';
import Highlight from 'react-highlighter';
import jQuery from 'jquery';

const $ = jQuery;


export default class AddWidget extends React.Component {
  constructor(props) {
    super(props);
    this.switchCategories = this.switchCategories.bind(this);
    this.resetCategory = this.resetCategory.bind(this);
    this.searchUpdated = this.searchUpdated.bind(this);
    this.countAddedWidgetsByType = this.countAddedWidgetsByType.bind(this);
    this.deleteUnuploadedImage = this.deleteUnuploadedImage.bind(this);
    this.data = this.props.widgetList.data;
    this.state = {
      selectedCategory: 'all',
      widgetCounter: 0,
      sorted: null,
      highlight: '',
      searchTerm: '',
      open: this.props.widgetMenuIsOpen,
    };
    this.sorted = [];
  };


  /**
   * @param term
   */
  searchUpdated(term) {
    this.setState({
      highlight: term,
      searchTerm: term
    });
  }

  deleteUnuploadedImage(ref) {
    const img = this.refs[ref];
    $(img).css({
      opacity: 0
    });
  }


  resetCategory() {
    $('.category-switcher').removeClass('active');
    $('.category-switcher[data-category="all"]').addClass('active');
    this.setState({
      selectedCategory: 'all'
    });
  }

  componentDidMount() {
    const self = this;
    $(document).keyup(function(e) {
      if (e.keyCode == 27 && self.props.addWidgetMenu.open) { // escape key maps to keycode `27`
        self.props.hideWidgetMenu();
      }
    });
  }

  countAddedWidgetsByType(type) {
    let counter = 0;
    for (var key in this.props.widgetsSet) {
      if (this.props.widgetsSet[key].type == type) {
        counter++;
      }
    }
    if (counter != 0) {
      return (<span className="counter">{counter}</span>);
    } else {
      return '';
    }

  }

  switchCategories(category) {
    $('.category-switcher').removeClass('active');
    $('.category-switcher[data-category="' + category + '"]').addClass('active');
    this.setState({
      selectedCategory: category
    });
  }


  render() {
    const KEYS_TO_FILTERS = ['name', 'description'];
    const PLACEHOLDER = '';
    const addWidgetClasses = classnames('clearfix add-widget-menu', { open: this.props.addWidgetMenu.open });

    function contains(array, needle) {
      for (var i = 0; i < array.length; i++) {
        if (array[i] == needle) {
          return true;
        }
      }
      return false;
    }

    if (this.props.widgetList.data != null) {
      let dataArr = [];
      let categoriesArr = [];
      let categoriesObj = {};
      var widgetCounter = 0;
      this.sorted = this.state.sorted || this.props.widgetList.data;
      for (var c in this.props.widgetList.data) {
        if (!contains(categoriesArr, this.props.widgetList.data[c].category)) {
          categoriesArr.push(this.props.widgetList.data[c].category);
        }
      }
      for (let k in this.sorted) {
        dataArr.push(this.sorted[k]);
      }
      let filteredData = dataArr.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));
      for (let cat in filteredData) {
        widgetCounter++;
        if (categoriesObj.hasOwnProperty(filteredData[cat].category)) {
          categoriesObj[filteredData[cat].category] = categoriesObj[filteredData[cat].category] + 1;
        } else {
          categoriesObj[filteredData[cat].category] = 1;
        }
      }
      let filteredRender = filteredData.map((data, index) => {
        let className;
        if (this.state.selectedCategory === 'all' || data.category === this.state.selectedCategory) {
          className = "widget-selection"
        } else {
          className = "widget-selection out-of-cat"
        }
        return (
          <div className={ className } data-category={data.category} key={index}>
              <div className="widget-img">
                  <img ref={data.alias} onError={() => {
                    this.deleteUnuploadedImage(data.alias)
                  }} src={'images/widgets/' + data.alias + '.png'} alt=""/>
              </div>
              <div className="widget-descr">
                  <div className="name"><Highlight search={this.state.highlight}>{data.name}</Highlight></div>
                  <div className="title"><Highlight search={this.state.highlight}>{data.category}</Highlight>
                  </div>
                  <div className="descr"><Highlight
                    search={this.state.highlight}>{data.description}</Highlight></div>
              </div>
              <div className="add-widget-action">{this.countAddedWidgetsByType(data.alias)}
                  <button href="" className="btn" onClick={() => {
                    this.props.addWidget({
                      type: data.alias,
                      id: this.props.widgetsSet[this.props.widgetsSet.length - 1].id + 1
                    });
                    this.props.hideWidgetMenu();
                    this.props.onWidgetAdded(data.name);
                  }}>Добавить
                  </button>
              </div>
          </div>
        )
      });
      let categoryRender = categoriesArr.map((data, index) => {
          if (typeof categoriesObj[data] != 'undefined') {
            return (
              <li key={index} className="category-switcher" value={data} data-category={data} onClick={() => {
                this.switchCategories(data)
              }}>{data}
                  <div className="num-avaliable">{categoriesObj[data]}</div>
              </li>
            );
          } else {
            return (<li key={index} className="category-switcher inactive" data-category={data} value={data}
                        onClick={() => {
                          this.switchCategories(data)
                        }}>{data}
                  <div className="num-avaliable">0</div>
              </li>
            );
          }
          ;
        }
      );
      return (

        <div className={addWidgetClasses}>

            <div className="container">

                <div className="add-widget-header">Добавить виджет
                    <div className="close-add-widget-btn" onClick={this.props.hideWidgetMenu}>&times;</div>
                </div>

            </div>

            <div className="container">

                <div className="modal-menu-w">
                    <div className="add-w-left">
                        <SearchInput className="search-input" throttle={0} placeholder={PLACEHOLDER}
                                     onChange={this.searchUpdated}/>
                        <div className="categories">
                            <h3>Категории</h3>
                            <ul>
                                <li data-category="all" className="category-switcher active" onClick={() => {
                                  this.resetCategory()
                                }}>All
                                    <div className="num-avaliable">{widgetCounter}</div>
                                </li>
                              {categoryRender}
                            </ul>
                        </div>
                    </div>
                    <div className="add-w-right">
                      {filteredRender}
                    </div>
                </div>

            </div>
            <div style={{
              position: 'fixed',
              left: 0,
              right: 0,
              bottom: 0,
              top: 0
            }} onClick={this.props.hideWidgetMenu} className="background"></div>
        </div>
      );
    } else {
      return (
        <span></span>
      )
    }


  };
}
