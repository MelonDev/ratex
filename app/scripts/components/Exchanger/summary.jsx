import React from 'react';

class Summary extends React.Component {

    constructor(props){
        super();
        this.state = {

        }
    }

    render(){

        var props = this.props,
            status = props.status,
            founded = props.founded,
            rating = props.rating,
            country = props.country,
            wmid = props.wmid,
            wmbl = props.wmbl,
            reserve = props.reserve,
            ratexResPos = props.ratexResPos,
            ratexResNeg = props.ratexResNeg,
            arbitrageResPositive = props.arbitrageResPositive,
            arbitrageResNegative = props.arbitrageResNegative,
            wmAdvisorResPositive = props.wmAdvisorResPositive,
            wmAdvisorResNegative = props.wmAdvisorResNegative;

        return(
            <div className="row">
                <div className="col-xs-4">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part">Статус</div>
                                <div className="right-part">
                                    <div className={status ? 'status active' : 'status'}> {status ? 'Доступен' : 'Недоступен'}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part">Возраст</div>
                                <div className="right-part">{founded}</div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part">Страна</div>
                                <div className="right-part">{country}
                                    <div className="icon flag-icon flag-icon-ua "></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part">Рейтинг</div>
                                <div className="right-part rating-wrap">
                                    {rating}
                                    {/*<div className="icon icon-star active"></div>*/}
                                    {/*<div className="icon icon-star active"></div>*/}
                                    {/*<div className="icon icon-star active"></div>*/}
                                    {/*<div className="icon icon-star"></div>*/}
                                    {/*<div className="icon icon-star"></div>*/}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xs-4">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part extended">Webmoney WMID</div>
                                <div className="right-part">{wmid}</div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part extended">Webmoney BL</div>
                                <div className="right-part">{wmbl}</div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part extended">TS</div>
                                <div className="right-part">-</div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item">
                                <div className="left-part extended">Сумма резервов</div>
                                <div className="right-part">{reserve} <span
                                    className="gray-curr">USD</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xs-4">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item response-item">
                                <div className="left-part bold">Отзывы</div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item response-item">
                                <div className="left-part extended">Ratex.io</div>
                                <div className="right-part">
                                    <span className="pos">{ratexResPos}</span> / <span className="neg">{ratexResNeg}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item response-item">
                                <div className="left-part extended">Арбитраж (WMID)</div>
                                <div className="right-part">
                                    <span className="pos">{arbitrageResPositive}</span> / <span className="neg">{arbitrageResNegative}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item response-item">
                                <div className="left-part extended">Арбитраж (сайт)</div>
                                <div className="right-part">
                                    <span className="pos">0</span> / <span className="neg">23</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="ex-info-item response-item">
                                <div className="left-part extended">WM Advisor</div>
                                <div className="right-part">
                                    <span className="pos">{wmAdvisorResPositive}</span> / <span className="neg">{wmAdvisorResNegative}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )


    }


}

export default Summary;