import React from "react";
import * as ActionCreators from "../../actions/exchangers/exchangerActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";


class Rates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        var rates = this.props.rates.data,
            ratesMapped = rates.map((elem, index) => {
                return (
                    <tr>
                        <td className="direction-td">
                            <div className="from">
                                <img src="/images/WMZ.svg" alt=""/>
                                {elem.attributes.base_currency}
                            </div>
                            <div className="divider">
                                <span className="ru-arrow"></span>
                            </div>
                            <div className="to">
                                <img src="/images/WMZ.svg" alt=""/>
                                {elem.attributes.quote_currency}
                            </div>
                        </td>
                        <td>
                            {elem.attributes.reserve}
                        </td>
                        <td>
                            <span className="gray-curr">USD</span>
                        </td>
                        <td>
                            {elem.attributes.comission || '-'}
                        </td>
                        <td>{elem.attributes.currency_pair_rating || '-'}</td>
                        <td>
                            <div className="icon icon-star"></div>
                        </td>
                    </tr>
                )
            });
        return (
            <table
                className="directory-table exchangers-table table-fixed direction-exchanger-table">
                <thead>
                <tr>
                    <th
                        className={this.props.rates.sortColumn == 'name' ? this.props.rates.tableDirection : ''}
                        onClick={() => {
                            this.props.sortDirectionsTable('name')
                        }}>
                        Направление <img className="filter-logo" src="/images/filter_logo.svg" alt=""/>
                    </th>
                    <th colSpan="2"
                        className={this.props.rates.sortColumn == 'reserves' ? this.props.rates.tableDirection : ''}
                        onClick={() => {
                            this.props.sortDirectionsTable('reserves')
                        }}>
                        Резерв <img className="filter-logo" src="/images/filter_logo.svg" alt=""/>
                    </th>
                    <th className={this.props.rates.sortColumn == 'commission' ? this.props.rates.tableDirection : ''}
                                 onClick={() => {
                                     this.props.sortDirectionsTable('commission')
                                 }}>

                        Комисия <img className="filter-logo" src="/images/filter_logo.svg" alt=""/>
                    </th>
                    <th className={this.props.rates.sortColumn == 'rating' ? this.props.rates.tableDirection : ''}
                        onClick={() => {
                            this.props.sortDirectionsTable('rating')
                        }}>
                        Рейтинг <img className="filter-logo" src="/images/filter_logo.svg" alt=""/>
                    </th>
                    <th>
                        {/*reserved for star*/}
                    </th>
                </tr>
                </thead>
                <tbody>
                {ratesMapped}
                </tbody>
            </table>
        )

    }


}


function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state, props) {
    return {
        locale: state.locale,
        rates: state.exchanger.rates
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Rates);