import React from 'react';
import ReactHighcharts from 'react-highcharts';
import { numberWithSpaces, addZeroes } from '../../reusable/extendingFunctions';
class Reserves extends React.Component {

  constructor(props) {
    super(props);
    this.pieChartConfig = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        height: 550,
        marginTop: 0,
        marginRight: 200,
        marginLeft: 0
      },
      credits: {
        enabled: false
      },
      colors: ['#E5324C', '#F5A623', '#DF674D', '#0FA989', '#B6BFCA', '#9A27BD', '#14B489', '#8013FE', '#1E7BE9', '#EFDF15', '#4A4A4A'],
      title: {

        text: ''
      },
      tooltip: {
        style: {
          color: '#314156',
          fontSize: '12px',
          fontFamily: 'Open Sans'
        },
        backgroundColor: '#FFFFFF',
        borderColor: '#B6BFCA',
        borderRadius: 0,
        borderWidth: 1,
        shadow: false,
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      legend: {
        backgroundColor: '#fff',
        layout: 'horizontal',
        floating: false,
        align: 'right',
        verticalAlign: 'middle',
        itemStyle: {
          "display": 'block',
          'font-family': 'Open Sans',
          'font-weight': 600,
          'font-size': 16
        },
        itemMarginTop: 10,
        shadow: false,
        width: 150,
        itemWidth: 150,
        border: 0,
        borderRadius: 0,
        borderWidth: 0,
        useHTML: true
      },
      plotOptions: {
        series: {
          states: {
            hover: {
              halo: {
                size: 0,
              }
            }
          }
        },
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          borderWidth: 0,
          slicedOffset: 0,
          dataLabels: {
            enabled: false,
            format: "{y}%",
            connectorWidth: 0,
            distance: 0,
            y: -5,
            inside: false,
            useHTML: true
          },
          showInLegend: true
        }
      },


    };
    this.state = {}

  }

  getSeries(reserves) {

    var total = 0,
      dataSeries,
      reservesSorted,
      dataSeriesFiltered,
      others = {
        name: 'Others',
        y: 0
      };

    for (var i = 0; i < reserves.length; i++) {
      total = total + reserves[i].attributes.in_usd;
    }

    reservesSorted = reserves.sort((a, b) => {
      if (a.attributes.in_usd > b.attributes.in_usd) {
        return -1
      }
      if (a.attributes.in_usd < b.attributes.in_usd) {
        return 1
      }
      return 0;
    });

    dataSeriesFiltered = reserves.filter((elem, index) => {

      if (index > 7) {
        others.y = others.y + (elem.attributes.in_usd * 100) / total
        return false;
      }
      return true;

    });
    dataSeries = dataSeriesFiltered.map((elem, index) => {
      var percents = (elem.attributes.in_usd * 100) / total,
        percentsFormatted = percents.toFixed(2);
      return {
        name: elem.attributes.issuer + ' ' + elem.attributes.parent_currency,
        y: parseFloat(percentsFormatted)
      }
    })
    dataSeries.push(others);

    return Object.assign({}, this.pieChartConfig,
      {
        series: [{
          name: 'Reserves',
          colorByPoint: true,
          data: dataSeries
        }]
      });

  }


  render() {


    var reserves = this.props.reserves,
      reservesMappped = reserves.map((elem, index) => {

        var attributes = elem.attributes;
        var numberWithZeroes = addZeroes(attributes.amount);
        var numberWithSpacesFormatted = numberWithSpaces(numberWithZeroes);

        return (
          <tr key={index}>
              <td>
                  <span className="cur-icon"><img src="/images/wmz.svg" alt=""/></span>
                  <span>{attributes.issuer} {attributes.parent_currency}</span>
              </td>
              <td>
                {numberWithSpacesFormatted}
              </td>
              <td>
                  <span className="gray-curr">{attributes.currency}</span>
              </td>
          </tr>
        )

      });

    return (

      <div className="row">
          <div className="col-xs-5">
              <table className="exchanger-table-reserves">
                  <thead>

                  <tr>
                      <th>Валюта <img className="filter-logo" src="/images/filter_logo.svg" alt=""/></th>
                      <th colSpan="2">Резервы</th>
                  </tr>
                  </thead>
                  <tbody>
                  {reservesMappped}
                  </tbody>
              </table>
          </div>
          <div className="col-xs-1"></div>
          <div className="col-xs-6">
              <div className="pie-chart-wrap">
                  <h4>Распределение резервов</h4>
                  <ReactHighcharts config={ this.getSeries(reserves) } ref="chart"/>
              </div>
          </div>
      </div>

    )

  }

}

export default Reserves;