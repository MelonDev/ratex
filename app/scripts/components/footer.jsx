import React from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import jQuery from 'jquery';
var $ = jQuery;

import i18next from 'i18next';
import resourses from '../translations/en';

export default class Footer extends React.Component {

    constructor(props){
        super(props);

    }

    changeLanguage(lang,){
        i18next.init({
            lng: lang,
            resources: resourses
        });
        // if(lang == 'ua'){
        //     $('.btn-add-widget').addClass('heroyam-slava');
        // }else{
        //     $('.btn-add-widget').removeClass('heroyam-slava');
        // }

        this.props.changeLanguage(i18next);
        $('.languages-b div').removeClass('active');
        $('.'+lang).addClass('active');

    }



    render() {
        let burgerClasses = classnames('burger', {open:this.props.menuIsOpen});
        let menuClasses = classnames('clearfix layout-footer');
        let locale = this.props.locale.language;

        if(this.props.locale.language != ''){
            return (
                <div className={menuClasses}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-3">
                                <div className="left-block">
                                    <div className="copyright">&copy; RATEX</div>
                                    <div className="languages-b">
                                        <div onClick={()=>{this.changeLanguage('ru')}} className="active ru">по-русски</div>
                                        <div onClick={()=>{this.changeLanguage('ua')}} className="ua">українською</div>
                                        <div onClick={()=>{this.changeLanguage('en')}} className="en">in english</div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-sm-2">
                                <div className="bold fli-title">{locale.t('_footer', {returnObjects:true}).services}</div>
                                <Link to="rates/cash" className="">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).rates}</div>
                                </Link>
                                <Link to="exchangers" className="">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).exchangers}</div>
                                </Link>
                                <Link to="black-market" className="">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).market}</div>
                                </Link>
                            </div>

                            <div className="col-sm-2">
                                <div className="bold fli-title">{locale.t('_footer', {returnObjects:true}).information}</div>
                                <Link to="about" className="">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).about}</div>
                                </Link>
                                <Link to="currencies" className="">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).directory}</div>
                                </Link>
                                <Link to="dashboard" className="disabled">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).partnership}</div>
                                </Link>
                            </div>

                            <div className="col-sm-2">
                                <div className="bold fli-title">{locale.t('_footer', {returnObjects:true}).for_developers}</div>
                                <Link to="dashboard" className="">
                                    <div className="footer-link-i">{locale.t('_footer', {returnObjects:true}).partnership}</div>
                                </Link>
                                <Link to="dashboard" className="">
                                    <div className="footer-link-i">API</div>
                                </Link>
                            </div>

                            <div className="col-sm-1"></div>
                            <div className="col-sm-2">
                                <div className="right-block">
                                    <div className="bold fli-title">{locale.t('_footer', {returnObjects:true}).social}</div>
                                    <div className="social-link-w">
                                        <a href="https://www.facebook.com/ratex.io" target="_blank" className="social-link"><span className="icon-facebook"></span></a>
                                        <a href="https://twitter.com/ratex_io" target="_blank" className="social-link"><span className="icon-twitter"></span></a>
                                        <a href="https://github.com/paymaxi" target="_blank" className="social-link"><span className="icon-github"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="bottom-block">
                                <div className="col-sm-10">
                                    <div className="paymaxi-logo"><a target="_blank" href="https://www.paymaxi.com/">Powered by <span  className="icon-paymaxi"></span> Paymaxi</a></div>
                                </div>
                                <div className="col-sm-2">
                                    <div className="popel-logo"><a href="http://popel.agency/" target="_blank">Designed by <span  className="icon-popel"></span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }else{
            return(
                <div>

                </div>
            );
        }
    }
};


