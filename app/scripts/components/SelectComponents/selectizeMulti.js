import React from "react";
import jQuery from "jquery";
var $ = jQuery;
import {SimpleSelect} from 'react-selectize';
import _ from 'underscore';
import config from '../../config';

class SelectizeMulti extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        var option = $('.option-wrapper');
    }


    shouldComponentUpdate(nextProps, nextState) {
        var notChanged = _.isEqual(this.props.data, nextProps.data);
        if (!notChanged) {
            return true
        }
        return false
    }

    render() {

        let groups = this.props.groups;
        let data = this.props.data;
        let self = this;


        return(
            <SimpleSelect
                groups={groups}
                options={data}
                placeholder=" "
                open={true}
                onBlur={this.props.onBlur}
                ref="cur_pairs"
                defaultValue={data[0]}
                renderNoResultsFound={()=> {
                    return ' ';
                }}
                onValueChange={this.props.onValueChange}
                renderOption={function (item) {
                    let valuesArr = item.value.split(' ');
                    return <div className="simple-option"
                                data-id = {item.label} style={{display: "flex", alignItems: "center"}}>

                        <div className="opt-wrap">
                            <div className="first-curr">
                                <div className="cur-icon" style={{
                                    backgroundImage: 'url("https://ratex.io/img/currencies/' + valuesArr[0] + '/icon")'
                                }}></div>
                                {valuesArr[0]}
                            </div>
                            <span className="ru-arrow"></span>
                            <div className="second-curr">
                                <div className="cur-icon" style={{
                                    backgroundImage: 'url("https://ratex.io/img/currencies/' + valuesArr[1] + '/icon")'
                                }}></div>
                                {valuesArr[1]}
                            </div>

                            <div
                                className={ self.props.favoriteDirections.hasOwnProperty(self.props.currpairHashTable[valuesArr[0] + '-' + valuesArr[1]]) ? 'favorite-star icon-star active notransition' : 'favorite-star icon-star notransition'}
                                 onClick={(e)=>{
                                self.props.addFavoriteDirection(item.currPairId);
                            }
                            }
                            ></div>
                        </div>


                    </div>
                }}
                renderValue={function (item) {
                    return <div className="simple-value">

                    </div>
                }}>
            </SimpleSelect>
            )



    }
}

export default SelectizeMulti;

