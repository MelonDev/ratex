import React from "react";
import jQuery from "jquery";
var $ = jQuery;
import {SimpleSelect} from 'react-selectize';
import _ from 'underscore';
import config from '../../config';

class Selectize extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        var option = $('.option-wrapper');
    }



    shouldComponentUpdate(nextProps, nextState){
        var notChanged = _.isEqual(this.props.data, nextProps.data);
        if(!notChanged){
            return true
        }
        return false
    }


    render() {
        let smst;
        let currenciesFrom = this.props.data;
        return (
            <SimpleSelect
                options={currenciesFrom}
                placeholder=" "
                open={true}
                ref="from"
                onBlur = {this.props.onBlur}
                onValueChange = {this.props.onValueChange}
                defaultValue = {{value:"WMZ", label:"WMZ"}}
                renderNoResultsFound = {()=>{
                    return ' ';
                }}
                renderOption={function(item){
                    var urlLink;
                    if(item.icon == null){
                        urlLink = '/images/currency_placeholder.svg'
                    }
                    if(item.icon == 'p') {
                        urlLink = config.api_url + 'img/currencies/' + item.value + '/icon_32.png';

                    }
                    if(item.icon == 's'){
                        urlLink = config.api_url + 'img/currencies/' + item.value + '/icon.svg';
                    }

                        return (
                            <div className={item.active ? 'simple-option active' : 'simple-option'} style={{display: "flex", alignItems: "center"}}>
                                <div className="cur-icon" style={{
                                    backgroundImage: 'url("'+ urlLink +'")'
                                }}></div>
                                {!!item.newOption ? "Add " + item.label + " ..." : item.label}
                            </div>
                        )
                }}
                renderValue={function(item){
                    return <div className="simple-value">

                    </div>
                }}>
            </SimpleSelect>
        );
    }
}
export default Selectize;




