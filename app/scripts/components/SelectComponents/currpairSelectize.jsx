import React from 'react';
import jQuery from 'jquery';
import { List } from 'react-virtualized';
import { createFilter } from 'react-search-input';
import _ from 'underscore';
import Highlight from 'react-highlighter';

const $ = jQuery;

export default class CurrpairSelectize extends React.Component {

  constructor(props) {
    super();
    this.buildList = this.buildList.bind(this);
    this.firstInputSearch = this.firstInputSearch.bind(this);
    this.secondInputSearch = this.secondInputSearch.bind(this);
    this.keyToFilter = ['name'];
    this.inputTimeout = 0;
    this.currenciesData = props.currpairHashTable;
    this.firstFiltered = null;
    this.secondFiltered = null;
    this.firstHighlight = '';
    this.secondHighlight = '';
    this.state = {
      firstInputSearch: false,
      secondInputSearch: false,
      groupsView: props.groupView,
      list: this.buildList(props),
      currHash: null,
      selectizeMode: true
    };
  };

  buildList(props) {
    _.intersectionObjects = function(array) {
      var slice = Array.prototype.slice; // added this line as a utility
      var rest = slice.call(arguments, 1);
      return _.filter(_.uniq(array), function(item) {
        return _.every(rest, function(other) {
          //return _.indexOf(other, item) >= 0;
          return _.any(other, function(element) {
            return _.isEqual(element, item);
          });
        });
      });
    };

    let keys = Object.keys(props.currpairHashTable);
    let list = [];

    for (var i = keys.length - 1; i >= 0; i--) {

      list.push({
        nameFrom: keys[i].split('-')[0],
        nameTo: keys[i].split('-')[1],
        name: keys[i],
        id: keys[i]

      });

    }

    this.dataForSelect = list;
    this.firstFiltered = list;
    this.secondFiltered = list;
    return list;

  }

  componentWillMount() {

    if (this.props.currenciesList.data != null && this.state.currHash == null) {

      let hashTable = {};
      for (var i = 0; i < this.props.currenciesList.data.length; i++) {
        hashTable[this.props.currenciesList.data[i].code] = {
          id: this.props.currenciesList.data[i].code
        };
        if(this.props.currencyType != 'national') {
          hashTable[this.props.currenciesList.data[i].code] = {
            icon: this.props.currenciesList.data[i].icon_status != null ? this.props.currenciesList.data[i].icon['32'] : '/images/currency_placeholder.svg',
          };
        }else {
          hashTable[this.props.currenciesList.data[i].code] = {
            icon: this.props.currenciesList.data[i].icon,
          };
        }
      }
      this.setState({
        currHash: hashTable
      });
    }

  }

  componentDidUpdate() {

    if (this.props.currenciesList.data != null && this.state.currHash == null) {
      let hashTable = {};
      for (var i = 0; i < this.props.currenciesList.data.length; i++) {
        hashTable[this.props.currenciesList.data[i].code] = {
          id: this.props.currenciesList.data[i].code
        };
        if(this.props.currencyType != 'national') {
          hashTable[this.props.currenciesList.data[i].code] = {
            icon: this.props.currenciesList.data[i].icon_status != null ? this.props.currenciesList.data[i].icon['32'] : '/images/currency_placeholder.svg',
          };
        }else {
          hashTable[this.props.currenciesList.data[i].code] = {
            icon: this.props.currenciesList.data[i].icon,
          };
        }
      }
      this.setState({
        currHash: hashTable
      });
    }

  }


  firstInputSearch(e) {
    let term = e.target.value;

    clearTimeout(this.inputTimeout);

    this.inputTimeout = setTimeout(() => {
      this.firstHighlight = term;
      let filteredData = this.dataForSelect.filter(createFilter(term, ['nameFrom']));
      this.firstFiltered = filteredData;
      let filteredFinal = _.intersectionObjects(this.firstFiltered, this.secondFiltered);
      this.setState({
        selectizeMode: true,
        list: filteredFinal,
      });
    }, 300);

  }

  secondInputSearch(e) {

    let term = e.target.value;

    clearTimeout(this.inputTimeout);
    this.inputTimeout = setTimeout(() => {

      this.secondHighlight = term;
      let filteredData = this.dataForSelect.filter(createFilter(term, ['nameTo']));
      this.secondFiltered = filteredData;

      let filteredFinal = _.intersectionObjects(this.firstFiltered, this.secondFiltered);

      this.setState({
        selectizeMode: true,
        list: filteredFinal,
      });
    }, 300);

  }


  render() {

    var self = this;
    const national = this.props.currencyType === 'national';

    function rowRenderer({
      key,         // Unique key within array of rows
      index,       // Index of row within collection
      isScrolling, // The List is currently being scrolled
      isVisible,   // This row is visible within the List (eg it is not an overscanned row)
      style        // Style object to be applied to row (to position it)
    }) {

      var iconFrom = self.state.currHash[self.state.list[index].nameFrom].icon,
        iconTo = self.state.currHash[self.state.list[index].nameTo].icon;


      return (
        <div
          key={key}
          style={style}
          className="currpair-item"
        >
            <div className="currpair-item-cont"
                 onClick={() => {
                   self.props.parentWidget.setState({
                     selectedFrom: {
                       value: self.state.list[index].nameFrom,
                       label: self.state.list[index].nameFrom,
                       icon: self.state.currHash[self.state.list[index].nameFrom].icon
                     },
                     selectedTo: {
                       value: self.state.list[index].nameTo,
                       label: self.state.list[index].nameTo,
                       icon: self.state.currHash[self.state.list[index].nameTo].icon
                     },
                   });
                   self.props.hideCurrPairs();
                   self.props.changeDirection([self.state.list[index].nameFrom, self.state.list[index].nameTo])
                 }}
            >
                <div className="curr first-curr">
                  {national ? <span className={"icon flag-icon flag-icon-" + iconFrom}></span> :
                    <img src={iconFrom} alt=""/>}
                    <Highlight search={self.firstHighlight}>{self.state.list[index].nameFrom}</Highlight>
                    <div className="gray-name"><Highlight
                      search={self.firstHighlight}>({self.state.list[index].nameFrom})</Highlight>
                    </div>
                </div>
                <div className="divider">
                    <div className="ru-arrow"></div>
                </div>
                <div className="curr second-curr">
                  {national ? <span className={"icon flag-icon flag-icon-" + iconTo}></span> :
                    <img src={iconTo} alt=""/>}
                    <Highlight search={self.secondHighlight}>{self.state.list[index].nameTo}</Highlight>
                    <div className="gray-name">(<Highlight
                      search={self.secondHighlight}>{self.state.list[index].nameTo}</Highlight>)
                    </div>
                </div>
                <div
                  className={typeof self.props.favoriteDirections[self.state.list[index].nameFrom + '/' + self.state.list[index].nameTo] == 'undefined' ? 'favorite-block' : 'favorite-block in-favorites'}
                  onClick={(e) => {
                    e.stopPropagation();
                    debugger;
                    if (typeof self.props.favoriteDirections[self.state.list[index].nameFrom + '/' + self.state.list[index].nameTo] == 'undefined') {
                      self.props.addFavoriteDirection(self.state.list[index].nameFrom + '/' + self.state.list[index].nameTo);
                    } else {
                      self.props.removeFavoriteDirection(self.state.list[index].nameFrom + '/' + self.state.list[index].nameTo);
                    }
                  }}>
                    <div
                      className={typeof self.props.favoriteDirections[self.state.list[index].nameFrom + '/' + self.state.list[index].nameTo] == 'undefined' ? 'icon-star' : 'icon-star active'}></div>
                </div>
            </div>
        </div>
      )
    }

    if (this.state.groupsView) {

      let favoritesMapped,
        topMapped;

      favoritesMapped = this.props.favorites

    }

    let listArea = <List
      width={580}
      height={300}
      rowCount={this.state.list.length}
      rowHeight={50}
      onScroll={function(a, b, c) {
        if (a.scrollTop > 0) {

          $('.input-block').addClass('scrolled')

        } else {

          $('.input-block').removeClass('scrolled')

        }
      }}
      rowRenderer={rowRenderer}
    />;

    let preloader = <div style={{ paddingLeft: 29 }}>
        Loading...
    </div>;


    let listRenderer;

    if (this.state.selectizeMode && this.state.currHash != null) {
      listRenderer = this.state.currHash == null ? preloader : listArea
    } else if (this.state.currHash != null) {
      //template for initial state with favoritex and top

      let favCurrPairs = this.props.favoriteDirections,
        topCurrPairs = this.props.topCurrenciePairs;
      let favCurrPairsMapped = favCurrPairs.map((elem, index) => {

        return (
          <div key={index} className="currpair-item" onClick={() => {

            self.props.parentWidget.setState({
              selectedFrom: {
                value: elem.value.split(' ')[0],
                label: elem.label.split(' ')[0],
                icon: self.state.currHash[elem.value.split(' ')[0]].icon
              },
              selectedTo: {
                value: elem.value.split(' ')[1],
                label: elem.label.split(' ')[1],
                icon: self.state.currHash[elem.value.split(' ')[1]].icon
              },
            });
            self.props.hideCurrPairs();
            self.props.changeDirection([elem.value.split(' ')[0], elem.value.split(' ')[1]])

          }}>
              <div className="currpair-item-cont">
                  <div className="curr first-curr">
                      <img
                        src={self.state.currHash[elem.value.split(' ')[0]].icon}
                        alt=""/>
                    {elem.value.split(' ')[0]}
                      <div className="gray-name">({elem.value.split(' ')[0]})</div>
                  </div>
                  <div className="divider">
                      <div className="ru-arrow"></div>
                  </div>
                  <div className="curr second-curr">
                      <img
                        src={self.state.currHash[elem.value.split(' ')[1]].icon}
                        alt=""/>
                    {elem.value.split(' ')[1]}
                      <div className="gray-name">({elem.value.split(' ')[1]})</div>
                  </div>
              </div>
          </div>

        );

      });
      let topCurrPairsMapped = topCurrPairs.map((elem, index) => {

        try {
          return (
            <div key={index} className="currpair-item" onClick={() => {

              self.props.parentWidget.setState({
                selectedFrom: {
                  value: elem.value.split(' ')[0],
                  label: elem.label.split(' ')[0],
                  icon: self.state.currHash[elem.value.split(' ')[0]].icon

                },
                selectedTo: {
                  value: elem.value.split(' ')[1],
                  label: elem.label.split(' ')[1],
                  icon: self.state.currHash[elem.value.split(' ')[1]].icon

                },
              });
              self.props.hideCurrPairs();
              self.props.changeDirection([elem.value.split(' ')[0], elem.value.split(' ')[1]])

            }}>
                <div className="currpair-item-cont">
                    <div className="curr first-curr">
                        <img
                          src={self.state.currHash[elem.value.split(' ')[0]].icon}
                          alt=""/>
                      {elem.value.split(' ')[0]}
                        <div className="gray-name">({elem.value.split(' ')[0]})</div>
                    </div>
                    <div className="divider">
                        <div className="ru-arrow"></div>
                    </div>
                    <div className="curr second-curr">
                        <img
                          src={self.state.currHash[elem.value.split(' ')[1]].icon}
                          alt=""/>
                      {elem.value.split(' ')[1]}
                        <div className="gray-name">({elem.value.split(' ')[1]})</div>
                    </div>
                </div>
            </div>
          );
        } catch (e) {
          console.error(e);
        }
      });
      listRenderer = (
        <div className="initial-selectize-wrapper">
            <div className="selectize-group-header" style={{ marginTop: 10 }}>
                Топ направлений
            </div>
          {topCurrPairsMapped}
            <div className="selectize-group-header">Избранные направления</div>
          {favCurrPairsMapped}
        </div>
      );

    } else {
      listRenderer = (<div style={{ paddingLeft: 29 }}>Loading...</div>)
    }
    return (
      <div className="curpair-selectize-wrap">
          <div className="input-block">
              <div className="input-wrap">
                  <div className="first-input">
                      <input type="text" onChange={this.firstInputSearch} placeholder="Любое направление"/>
                  </div>
                  <div className="divider">
                      <div className="ru-arrow"></div>
                  </div>
                  <div className="second-input">
                      <input type="text" onChange={this.secondInputSearch} placeholder="Любое направление"/>
                  </div>
              </div>
          </div>
        {listRenderer}
      </div>
    )

  }

}