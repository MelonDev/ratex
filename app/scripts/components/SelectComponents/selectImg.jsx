import React from 'react';
import Select from 'react-select';
import jQuery from 'jquery';
var $ = jQuery;


export class ImgOption extends React.Component{

    constructor(props){
        super(props);
        this.handleMouseMove = this.handleMouseMove.bind(this);
    }

    handleMouseEnter (event) {
        this.props.onFocus(this.props.option, event);
    }

    handleMouseMove (event) {
        if (this.props.isFocused) return;
        this.props.onFocus(this.props.option, event);
    }

    render () {
        let gravatarStyle = {
            borderRadius: 3,
            display: 'inline-block',
            marginRight: 10,
            position: 'relative',
            top: -2,
            verticalAlign: 'middle'
        };
        return (
            <div onMouseMove={this.handleMouseMove}>
                <img src="http://placehold.it/350x150"/>
                {this.props.children}
            </div>
        );
    }
}
export class ImgValue extends React.Component {

    constructor(props){
        super(props);
        this.handleMouseMove = this.handleMouseMove.bind(this);
    }


    handleMouseEnter (event) {
        this.props.onFocus(this.props.option, event);
    }

    handleMouseMove (event) {
        if (this.props.isFocused) return;
        this.props.onFocus(this.props.option, event);
    }

    render () {
        let gravatarStyle = {
            borderRadius: 3,
            display: 'inline-block',
            marginRight: 10,
            position: 'relative',
            top: -2,
            verticalAlign: 'middle'
        };
        return (
            <div onMouseMove={this.handleMouseMove}>
                <img  onMouseMove={this.handleMouseMove} src="http://placehold.it/350x150"/>
                {this.props.children}
            </div>
        );
    }
}