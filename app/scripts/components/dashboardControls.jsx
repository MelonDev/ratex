import React from "react";

class DashboardControls extends React.Component {
    constructor() {
        super();
        this.state = {"dark": false}

    }

    render() {
        return (
			<div className="dashboard-controls clearfix">
				<div className="add-widget">
					<span className="icon-atom"></span>
					Добавить виджет
				</div>
				<div className="right-side-controls">
					<div className="dcontrol-item color-toggle">
						<span className="icon-atom"></span>
					</div>
					<div className="dcontrol-item reload-toggle">
						<span className="icon-atom"></span>
					</div>
				</div>
			</div>
        );
    }
}
export default DashboardControls;