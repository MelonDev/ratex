const locally = false;

const config = {
  api_url: locally ? 'http://localhost:8080/' : 'https://ratex.io/',
  default_dragPositions_biggest: [{"attr":"8","x":0},{"attr":"13","x":0.4},{"attr":"11","x":0.8},{"attr":"14","x":0.4},{"attr":"25","x":0.8},{"attr":"19","x":0},{"attr":"0","x":0.8},{"attr":"12","x":0},{"attr":"27","x":0.8},{"attr":"7","x":0.4},{"attr":"24","x":0.6},{"attr":"10","x":0},{"attr":"16","x":0.8},{"attr":"17","x":0.4}],
  default_dragPositions_big: [{"attr":"8","x":0},{"attr":"13","x":0.4},{"attr":"11","x":0.8},{"attr":"14","x":0.4},{"attr":"25","x":0.8},{"attr":"19","x":0},{"attr":"0","x":0.8},{"attr":"12","x":0},{"attr":"27","x":0.8},{"attr":"7","x":0.4},{"attr":"24","x":0.6},{"attr":"10","x":0},{"attr":"16","x":0.8},{"attr":"17","x":0.4}],
  default_dragPositions_medium: [{"attr":"8","x":0},{"attr":"13","x":0.4},{"attr":"11","x":0.8},{"attr":"14","x":0.4},{"attr":"25","x":0.8},{"attr":"19","x":0},{"attr":"0","x":0.8},{"attr":"12","x":0},{"attr":"27","x":0.8},{"attr":"7","x":0.4},{"attr":"24","x":0.6},{"attr":"10","x":0},{"attr":"16","x":0.8},{"attr":"17","x":0.4}],
  default_dragPositions_small: [{"attr":"8","x":0},{"attr":"13","x":0.4},{"attr":"11","x":0.8},{"attr":"14","x":0.4},{"attr":"25","x":0.8},{"attr":"19","x":0},{"attr":"0","x":0.8},{"attr":"12","x":0},{"attr":"27","x":0.8},{"attr":"7","x":0.4},{"attr":"24","x":0.6},{"attr":"10","x":0},{"attr":"16","x":0.8},{"attr":"17","x":0.4}],
  default_dragPositions_smallest: [{"attr":"8","x":0},{"attr":"13","x":0.4},{"attr":"11","x":0.8},{"attr":"14","x":0.4},{"attr":"25","x":0.8},{"attr":"19","x":0},{"attr":"0","x":0.8},{"attr":"12","x":0},{"attr":"27","x":0.8},{"attr":"7","x":0.4},{"attr":"24","x":0.6},{"attr":"10","x":0},{"attr":"16","x":0.8},{"attr":"17","x":0.4}]
};

export default config;