import ReactDOM from 'react-dom';
import routes from './routes';
import EventEmitter from 'event-emitter';
import Raven from 'raven-js';
import { hideMenu } from './reusable/extendingFunctions';


//Raven.config('https://4c8fd502df0a41a8b41c0bd5ccbc61fa@sentry.io/99987').install();

import jQuery from 'jquery';
window.is_safari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > -1;
function detectRetina (){
    if (window.matchMedia) {
        var mq = window.matchMedia("only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen  and (min-device-pixel-ratio: 1.3), only screen and (min-resolution: 1.3dppx)");
        return (mq && mq.matches || (window.devicePixelRatio > 1));
    }
};
window.isRetina = detectRetina();

window.$ = jQuery;
window.jQuery = jQuery;
window.ee = new EventEmitter();


//Main render function
ReactDOM.render(routes, document.getElementById('app'));

//Make something this HEADER after certain("sDistanse") scroll
$(document).on('scroll', function(){
    var sElement = $('.layout-header'),
        sDistace = 1,
        sFromTop = $(window).scrollTop();
    if(sFromTop > sDistace) {
        sElement.addClass('scrolled');
    }
    else {
        sElement.removeClass('scrolled');
    }
});

var scrolledBtn = false;
var fullscreenmode = false

var raf = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    window.oRequestAnimationFrame;
function onScroll(){

    if(!fullscreenmode){
        var windowScrollTop = $(window).scrollTop(),
            windowHeight    = $(window).height(),
            documentHeight  = $(document).height(),
            footerOffset    = $('.layout-footer').offset().top,
            stickyElement   = $('.btn-add-widget'),
            stickyIframe    = $('iframe');

        var sElement = $('.layout-header'),
            sDistace = 1,
            sFromTop = $(window).scrollTop();
        // console.log(sFromTop);
        if(sFromTop > sDistace) {
            sElement.addClass('scrolled');
        }
        else {
            sElement.removeClass('scrolled');
        }

        var bottomOffset = 230 - (documentHeight - windowScrollTop - windowHeight);

        if((windowScrollTop + windowHeight - 2) >= footerOffset) {

            stickyElement.css({
                position: 'absolute',
                bottom:230
            });
            // stickyIframe.css({
            //     position: 'absolute',
            //     bottom:230
            // });
            if(!stickyIframe.hasClass('absoluted')){
                stickyIframe.addClass('absoluted');
            }

        }
        else{

            stickyElement.css({
                bottom: 50,
                position:'fixed',
                right: '50px'

            });
            stickyIframe.removeClass('absoluted');
            stickyIframe.css({
                bottom: 50,
                position:'fixed',
                right: '50px'
            });


        }

    }
    raf(onScroll);

}
onScroll();

$(document).keyup(function(e) {
    if (e.keyCode == 27) {
       hideMenu();
    }
});


$(document).ajaxStop( function(){
    setTimeout(()=>{
        pckry.shiftLayout();
        $('.preloader-wrap').fadeOut();
    }, 600);
});










