import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from './actions/ActionCreators';
import App from './pages/app.jsx';


/**
 * maps state to props
 * @param state
 * @returns {
 * {widgetsSet, dashInitialState, ajaxLoad: *, exchangersLoad: *, marketLoad: *, providersLoad: *, menu, addWidgetMenu: *, widgets, widgetList: *, exchangeDirections: *, news, currencyPairs: (*|null|Array), dashExchangers: *, exchanger, fastExchange: *, ratexStats: *, currPairTooltip: *, config, currencyTooltip: *, favoriteDirections: (*|favoriteDirections.favoriteDirections|{}|Array|favoriteDirections), favoriteExchangers, exchangerTooltip: *, currpairHashTable: *, favoriteExchangersWidget: *, favoriteDirectionsWidget: *, currencies: (*|Array), locale, countries: *, currenciesList: *}}
 */
function mapStateToProps(state) {
  return {
    widgetsSet: state.widgetsSet,
    dashInitialState: state.dashInitialState,
    exchangersLoad: state.exchangersLoad,
    marketLoad: state.marketLoad,
    providersLoad: state.providersLoad,
    addWidgetMenu: state.addWidgetMenu,
    widgets: state.widgets,
    widgetList: state.widgetList,
    exchangeDirections: state.exchangeDirections,
    news: state.news,
    currencyPairs: state.currencyPairs,
    dashExchangers: state.dashExchangers,
    exchanger: state.exchanger,
    fastExchange: state.fastExchange,
    ratexStats: state.ratexStats,
    currPairTooltip: state.currPairTooltip,
    config: state.config,
    currencyTooltip: state.currencyTooltip,
    favoriteDirections: state.favoriteDirections,
    favoriteExchangers: state.favoriteExchangers,
    exchangerTooltip: state.exchangerTooltip,
    currpairHashTable: state.currpairHashTable,
    favoriteExchangersWidget: state.favoriteExchangersWidget,
    favoriteDirectionsWidget: state.favoriteDirectionsWidget,
    currencies: state.currencies,
    locale: state.locale,
    countries: state.countries,
    currenciesList: state.currenciesList,
  };
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}


const Application = connect(mapStateToProps, mapDispatchToProps)(App);


export default Application;
