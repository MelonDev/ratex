import {createStore} from "redux";
 const applyMiddleware = typeof window == 'undefined' ? require('redux-universal') : require('redux').applyMiddleware;

// import applyMiddleware from "redux-wait";
import thunk from "redux-thunk";
import {syncHistoryWithStore, routerMiddleware} from "react-router-redux";
import {createMemoryHistory, browserHistory, useRouterHistory} from "react-router";
import createBrowserHistory from 'history/lib/createBrowserHistory'
import jQuery from "jquery";
import rootReducer from "../reducers/index.js";
import widgetsSet from "../data/vidgets";
var $ = jQuery;
import { stringify, parse } from 'qs'

var dash = {};
var localDash = JSON.parse(localStorage.getItem('dash'));

if (localDash != null) {
    dash = localDash;
}


var widgetsPersisted = widgetsSet;
var locaWidgetsSet = JSON.parse(localStorage.getItem('widgets'));
if(locaWidgetsSet != null){
    widgetsPersisted = locaWidgetsSet;
}

const widgetSet = {
    dashInitialState: dash,
    widgetsSet: widgetsPersisted
};

const favoriteDirections = {
    favoriteDirections: JSON.parse(localStorage.getItem('favoriteDirections')) || {}
};
const favoriteCurrencies = {
    favoriteCurrencies: JSON.parse(localStorage.getItem('favoriteCurrencies')) || {}
};
const favoriteExchangers = {
    favoriteExchangers: JSON.parse(localStorage.getItem('favoriteExchangers')) || {}
};

const persistedState = Object.assign({}, widgetSet, favoriteDirections, favoriteExchangers, favoriteCurrencies);


// localStorage.clear();
if (typeof __INITIAL_STATE__ == 'undefined') {

    const stringifyQuery = query => stringify(query, { arrayFormat: 'brackets' });
    const queryHistory = useRouterHistory(createBrowserHistory)({ parseQueryString: parse, stringifyQuery });
    var store = createStore(rootReducer, persistedState, applyMiddleware(thunk, routerMiddleware(queryHistory)));
    // var store = createStore(rootReducer, persistedState, applyMiddleware(thunk, routerMiddleware(browserHistory)));



} else {

    var store = createStore(rootReducer, __INITIAL_STATE__, applyMiddleware(thunk, routerMiddleware(createMemoryHistory())));

}


// store.subscribe(() => {
//     localStorage.setItem('widgets', JSON.stringify(store.getState().widgetsSet));
//     localStorage.setItem('dash', JSON.stringify(store.getState().dashInitialState))
//
// });

var historyNew;


if (typeof window == 'undefined') {
    historyNew = createMemoryHistory();
} else {
    const stringifyQuery = query => stringify(query, { arrayFormat: 'brackets' });
    const queryHistory = useRouterHistory(createBrowserHistory)({ parseQueryString: parse, stringifyQuery });
    historyNew = queryHistory
}

export const history = syncHistoryWithStore(historyNew, store);

export default store;


