import React, {Component} from "react";
import {Link} from "react-router";
import jQuery from "jquery";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "../actions/organizations/organizationsActions";
import {push} from "react-router-redux";
import _ from "underscore";
import ReactPaginate from "react-paginate";
var $ = jQuery;
import didMount from "../reusable/didMount";
class Organizations extends Component {

    constructor(props) {
        super(props);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
        this.searchTimeout = null;
        this.state = {}
    }

    componentWillMount() {

        var query = {
            pageNumber: this.props.location.query.pageNumber || 1,
            sort: this.props.location.query.sort || '',
            'filter[]': this.props.location.query.filter || [],
            search: this.props.location.query.search || '',
            sortDirection: this.props.location.query.sortDirection

        }

        this.props.loadOrganizations(query);

    }

    componentWillUpdate(nextProps, nextState) {


        if (!_.isEqual(this.props.location.query, nextProps.location.query) && this.props.organizations.data.length == nextProps.organizations.data.length) {

            var query = {
                search: nextProps.location.query.search,
                pageNumber: nextProps.location.query.pageNumber,
                sort: nextProps.location.query.sort,
                'filter[]': nextProps.location.query.filter,
                sortDirection: nextProps.location.query.sortDirection

            }

            this.props.loadOrganizations(query);

        }

    }


    toggleSettings(e) {

        e.stopPropagation();
        $('.rates-table-filters-wrap').toggleClass('active');

    }

    componentDidUpdate() {


    }


    handleFilter(key) {

        if (typeof this.props.organizations.meta['filter[]'] != 'string' && typeof this.props.organizations.meta['filter[]'] != 'undefined') {

            var newFilter = this.props.organizations.meta['filter[]'];
            newFilter.push(key);

            var action = push(
                {
                    pathname: '/organizations',
                    query: {
                        search: this.props.organizations.meta.search,
                        sort: this.props.organizations.meta.sort,
                        'filter[]': newFilter,
                        sortDirection: this.props.organizations.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterOrganizations(newFilter);
        } else {
            var action = push(
                {
                    pathname: '/oraganizations',
                    query: {
                        search: this.props.oraganizations.meta.search,
                        'filter[]': [key],
                        sort: this.props.oraganizations.meta.sort,
                        sortDirection: this.props.oraganizations.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterOrganizations([key]);
        }

    }

    removeFilter(filter) {

        var currenctFilter = this.props.organizations.meta['filter[]'];
        var newFilter = currenctFilter.filter((elem, index) => {
            if (elem == filter) {
                return false;
            }
            return true;
        });

        var action = push(
            {
                pathname: '/organizations',
                query: {
                    search: this.props.organizations.meta.search,
                    'filter[]': newFilter,
                    sort: this.props.organizations.meta.sort,
                    sortDirection: this.props.organizations.meta.sortDirection

                }
            }
        );
        this.props.dispatch(action);

        this.props.filterOrganizations(newFilter);

    }


    contains(array, needle) {
        if (typeof array != 'undefined') {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == needle) {
                    return true;
                }
            }
            return false;
        }
        return false
    }


    searchInputToggle() {

        $('.search-input').toggleClass('active');
        $('.search-input input').focus();

    }

    handlePagination(e) {

        var action = push(
            {
                pathname: '/oraganizations',
                query: {
                    pageNumber: parseInt(e.selected + 1),
                    search: this.props.organizations.meta.search,
                    'filter[]': this.props.organizations.meta['filter[]'] || [],
                    sort: this.props.organizations.meta.sort,
                    sortDirection: this.props.organizations.meta.sortDirection
                }
            }
        );
        this.props.dispatch(action);

        this.props.paginateOrganizations(parseInt(e.selected + 1));
    }


    searchFilter(val) {
        clearTimeout(this.searchTimeout);
        this.searchTimeout = setTimeout(() => {
            var inputValue = val;
            var action = push({
                pathname: '/organizations',
                query: {
                    search: val,
                    'filter[]': this.props.organizations.meta.filter,
                    sort: this.props.organizations.meta.sort,
                    sortDirection: this.props.organizations.meta.sortDirection
                }
            });
            this.props.dispatch(action);
            this.props.searchOrganizations(val);
        }, 500);

    }


    componentDidMount() {

        didMount();
        $(window).bind("scroll", function () {
            var tableOffset = $("#ex-table").offset().top;
            var $fixedHeader = $(".fixed-table-wrap");
            var offset = $(this).scrollTop() + 60;
            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });

        var naturalTh = $('#ex-table thead th');
        var fixedTh = $('.header-fixed thead th');

    }

    render() {
        let locale = this.props.locale.language;
        if (this.props.organizations.data != null) {
            let organizations = this.props.organizations.data,
                paginationLinksMapped = [],
                activeFilters;
            let organizationsMapped = organizations.map((elem, index) => {
                var iconUrl = 'images/ex-logo.png';
                return (
                    <tr>
                        <td>
                            {elem.attributes.name}
                        </td>
                    </tr>
                )
            });

            if (typeof this.props.organizations.meta['filter[]'] != 'undefined' && this.props.organizations.meta['filter[]'] != '') {


                if (typeof this.props.organizations.meta['filter[]'] == 'string') {
                    activeFilters = (
                        <div className="filter-item">
                            {this.props.organizations.meta['filter[]'].split(':')[1]}
                            <div className="delete-filter" onClick={() => {
                            }}>&times;</div>
                        </div>
                    )
                } else {

                    activeFilters = this.props.organizations.meta['filter[]'].map((elem, index) => {
                        return (
                            <div className="filter-item" key={index}>
                                {elem.split(':')[1]}
                                <div className="delete-filter" onClick={() => {
                                    this.removeFilter(elem);
                                }}>&times;</div>
                            </div>
                        )
                    })
                }

            } else {
                activeFilters = '';
            }
            return (
                <div>
                    <div className="fixed-table-wrap">
                        <div className="container">
                            <div className="table-wrapper">
                                <table className="directory-table exchangers-table header-fixed currencies-table">
                                    <thead>
                                    <tr>
                                        <th className={this.props.organizations.meta.sort == 'name' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/organizations',
                                                query: {
                                                    pageNumber: this.props.organizations.meta.pageNumber,
                                                    sort: 'name',
                                                    'filter[]': this.props.organizations.meta['filter[]'],
                                                    sortDirection: this.props.organizations.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortOrganizations('name')
                                            }}>Название</Link>
                                            <img className={'filter-logo ' + this.props.organizations.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div className="container market-container">
                        <div className="directory-wrap currencies-wrap">
                            <div className="directory-header-wrap">
                                <h2 className="directory-header">Список организаций</h2>
                                <div className="search-input"
                                     onClick={this.searchInputToggle}>
                                    <input
                                        className="search-input" onBlur={() => {
                                        $('.search-input').removeClass('active')
                                    }} onChange={(e) => {
                                        this.searchFilter(e.target.value)
                                    }}/>
                                </div>
                            </div>
                            <table id="ex-table" className="directory-table exchangers-table currencies-table">
                                <thead>
                                <tr>
                                    <th className={this.props.organizations.meta.sort == 'name' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/organizations',
                                            query: {
                                                pageNumber: this.props.organizations.meta.pageNumber,
                                                sort: 'name',
                                                'filter[]': this.props.organizations.meta['filter[]'],
                                                sortDirection: this.props.organizations.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortOrganizations('name')
                                        }}>Название</Link>
                                        <img className={'filter-logo ' + this.props.organizations.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {organizationsMapped}
                                </tbody>
                            </table>
                        </div>
                        <div className="pagination">
                            <ReactPaginate previousLabel={<span className="icon-arrow-big-left"></span>}
                                           nextLabel={<span className="ru-arrow"></span>}
                                           breakLabel={<a href="">...</a>}
                                           breakClassName={"break-me"}
                                           pageCount={this.props.organizations.meta.pages}
                                           marginPagesDisplayed={2}
                                           initialPage={parseInt(this.props.organizations.meta.pageNumber) - 1}
                                           pageRangeDisplayed={7}
                                           onPageChange={this.handlePagination}
                                           containerClassName={"pagination"}
                                           subContainerClassName={"pages pagination"}
                                           activeClassName={"active"}/>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className="container market-container">
                    <div className="directory-wrap loading-directory">
                        <div className="preloader-wrap">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }

}

function mapDispatchToProps(dispatch) {
    return {...bindActionCreators(ActionCreators, dispatch), dispatch};
}

function mapStateToProps(state) {
    return {
        locale: state.locale,
        organizations: state.organizations
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Organizations);


