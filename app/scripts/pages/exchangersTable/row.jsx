import React, {Component} from "react";
import {Link} from "react-router";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import jQuery from "jquery";
import { addFavoriteExchanger, removeFavoriteExchanger } from '../../actions/favorites/favoriteExchangers';
var $ = jQuery;


class Row extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {


    }


    componentWillUpdate(nextProps, nextState) {


    }

    componentDidUpdate() {


    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.name != this.props.name || Object.keys(this.props.favoriteExchangers).length != Object.keys(nextProps.favoriteExchangers).length) {
            console.log('should!');
            return true
        }

        return false;

    }

    sort(param) {
        let sorted = this.state.exchangersLoaded.data;

        if (this[param] == null || this[param] == 'up') {
            this[param] = 'down';
            sorted.sort((a, b) => {

                if (a.attributes[param] > b.attributes[param]) {
                    return 1;
                }
                if (a.attributes[param] < b.attributes[param]) {
                    return -1;
                }
                return 0;


            });
            this.setState({
                exchangersLoaded: {
                    data: sorted
                }
            });
            return
        }
        if (this[param] == 'down') {
            this[param] = 'up';
            sorted.sort((a, b) => {

                if (a.attributes[param] > b.attributes[param]) {
                    return -1;
                }
                if (a.attributes[param] < b.attributes[param]) {
                    return 1;
                }
                return 0;


            });
            this.setState({
                exchangersLoaded: {
                    data: sorted
                }
            });
            return
        }


    }


    componentDidMount() {


    }

    render() {

        let {reserves, rates, bl, name, id, rating} = this.props;
        function returnReserves(reserves){
            return(
                <span>
                    {reserves} <span className="gray-currency">USD</span>
                </span>
            )
        }
        return (
            <tr>
                <td className="status-td">
                    <div className={this.props.isActive ? 'status-mark' : 'status-mark non-active'}></div>
                </td>
                <td><Link to={'exchangers/' + id}><img className="logo-small" src="images/ex-logo.png" alt=""/>{ name }
                </Link></td>

                <td>{ reserves ? returnReserves(reserves) : String.fromCharCode(8212)} </td>
                {/*<td>{ rates == null ? String.fromCharCode(8212) : rates }</td>*/}
                <td>{ bl == null ? String.fromCharCode(8212) : bl }</td>
                {/*<td className="responses"><span className="neg">&mdash;</span> / <span className="pos">&mdash;</span>*/}
                {/*</td>*/}
                <td>{rating.toFixed(3) || String.fromCharCode(8212)}</td>
                <td>
                    <div
                        className={this.props.favoriteExchangers.hasOwnProperty(id) ? 'favorite-star icon-star active notransition' : 'favorite-star icon-star notransition'}
                        onClick={this.props.favoriteExchangers.hasOwnProperty(id) ? () => {this.props.removeFavoriteExchanger(id) } : () => {this.props.addFavoriteExchanger(id)} }
                    ></div>
                </td>
            </tr>
        )

    }


}


Row = connect(
  null,
  dispatch => bindActionCreators({addFavoriteExchanger, removeFavoriteExchanger}, dispatch)
)(Row);

export default Row;
