import React from 'react';
import jQuery from 'jquery';
var $ = jQuery;
import {Link} from 'react-router';

class FlyAway extends React.Component {
    constructor(props){
        super(props);
    }
    componentWillUnmount(){
        $('.profile-btn').show();
    }
    render(){
        $('.profile-btn').hide();
        return (
            <div>
                <div className="container">
                    <div className="centered-cont not-found-cont">
                        <h1>007</h1>
                        <p>You shouldn't be here...</p>
                        <p>It's not he page you are loking for...</p>
                        <div className="btn-cont">
                            <Link className="btn btn-primary btn-exchange" to="/">Back to Main Page</Link>
                        </div>
                    </div>
                </div>
                <div className="powered-by-not-found">
                    <a href="#">
                        <span>Povered by</span>
                        <span className="ru-paymaxi icon"></span>
                    </a>
                </div>
            </div>

        );
    }
}

export default FlyAway;