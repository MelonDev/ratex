import React from "react";
import ContactForm from '../components/Forms/addExchangerForm.jsx';
import didMount from "../reusable/didMount";

export default class AddExchanger extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        didMount();

    }

    componentWillMount() {


    }

    handleSubmit  (values)  {
        // Do something with the form values

    }



    render() {

        return (
            <div className="container add-exchanger">
                <div className="row">
                    <div className="col-xs-6">
                        <div className="ex-form">
                            <h2>Заявка на включение обменного пункта</h2>
                            <ContactForm onSubmit={this.handleSubmit}/>
                        </div>
                    </div>
                    <div className="col-xs-6">
                        <div className="terms">
                            <h2>Минимальные требования к обменному пункту</h2>
                            <ol>
                                <li>Очень серьезное требование!</li>
                                <li>Очень серьезное требование!</li>
                                <li>Очень серьезное требование!</li>
                                <li>Очень серьезное требование!</li>
                                <li>Очень серьезное требование!</li>
                                <li>Очень серьезное требование!</li>
                                <li>Очень серьезное требование!</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        )

    }


}