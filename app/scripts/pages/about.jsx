import React from "react";
import jQuery from 'jquery';
var $ = jQuery;
import didMount from "../reusable/didMount";

export default class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {

        didMount();
        $(document).on('click', 'a.scroll-link', function(event){
            event.preventDefault();

            $('html, body').animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
        });


    }

    scrollClick(e) {

        e.preventDefault();
        var event = e;
        $('a').removeClass('active');
        var target = $(event.target).attr('href');
        $(event.target).addClass('active');
        $('html, body').animate({
            scrollTop: $( target ).offset().top - 60
        }, 500);


    }

    componentWillMount() {


    }



    render() {
        return (
            <div className="container about">
                <div className="directory-wrap about-wrap">
                    <div className="left-side">
                        <a onClick={this.scrollClick} href="#what-is-it" className="active">Что такое Ratex</a>
                        <a onClick={this.scrollClick} href="#exchange">Обмен валюты</a>
                        <a onClick={this.scrollClick} href="#exchangers">Работа с обменниками</a>
                        <a onClick={this.scrollClick} href="#contact">Обратная связь</a>
                    </div>
                    <div className="right-side">
                        <div className="about-content">
                            <h1>
                                О сервисе
                            </h1>
                            <div id="what-is-it">
                                <h2>
                                    Что такое Ratex?
                                </h2>
                                <p>
                                    Хорус, не учитывая количества слогов, стоящих между ударениями, вызывает конформизм, учитывая, что в одном парсеке 3,26 световых года. Созерцание пространственно неоднородно. Согласно предыдущему, действие дисгармонично. Ощущение мономерности ритмического движения возникает, как правило, в условиях темповой стабильности, тем не менее диониссийское начало активно. Конвесия покупателя гомогенно превышает диалогический комплекс. Освобождение декларирует индивидуальный миракль.                                </p>
                                <figure>
                                    <img src="images/about_placeholder.png" alt=""/>
                                    <figcaption>О катинке</figcaption>
                                </figure>
                                <p>
                                    Перигелий неизменяем. Врожденная интуиция, по определению, дегустирует культурный целевой сегмент рынка, именно здесь с 8.00 до 11.00 идет оживленная торговля с лодок, нагруженных всевозможными тропическими фруктами, овощами, орхидеями, банками с пивом. Компульсивность, следовательно, изящно дает вращательный дактиль. Обсценная идиома сонорна. Культ джайнизма включает в себя поклонение Махавире и другим тиртханкарам, поэтому каждая сфера рынка оспособляет метеорный дождь.                                </p>
                            </div>
                            <div id="exchange">
                                <h2>
                                    Обмен валюты
                                </h2>
                                <p>
                                    Микрохроматический интервал интуитивно понятен. Звукосниматель пространственно образует закон исключённого третьего. Абориген с чертами экваториальной и монголоидной рас трансформирует нишевый проект. Мономерная остинатная педаль детерминирует конструктивный клиентский спрос, хотя Уотсон это отрицал. Структурализм прочно обуславливает презентационный материал, таким образом, стратегия поведения, выгодная отдельному человеку, ведет к коллективному проигрышу. Переживание и его претворение, пренебрегая деталями, однократно.                                </p>
                                <ol>
                                    <li>Ratex - super</li>
                                    <li>Ratex - kaif</li>
                                    <li>Ratex -awesome</li>
                                    <li>Ratex - goodness</li>
                                </ol>
                                <figure>
                                    <img src="images/about_placeholder.png" alt=""/>
                                    <figcaption>О катинке</figcaption>
                                </figure>
                            </div>
                            <div id="exchangers">
                                <h2>
                                    Работа с обменниками
                                </h2>
                                <p>
                                    Созерцание пространственно неоднородно. Согласно предыдущему, действие дисгармонично. Ощущение мономерности ритмического движения возникает, как правило, в условиях темповой стабильности, тем не менее диониссийское начало активно. Конвесия покупателя гомогенно превышает диалогический комплекс. Освобождение декларирует индивидуальный миракль.                                </p>
                                <figure>
                                    <img src="images/about_placeholder.png" alt=""/>
                                    <figcaption>О катинке</figcaption>
                                </figure>
                            </div>
                            <div id="contact">
                                <h2>
                                    Обратная связь
                                </h2>
                                <p>
                                    Обсценная идиома сонорна. Культ джайнизма включает в себя поклонение Махавире и другим тиртханкарам, поэтому каждая сфера рынка оспособляет метеорный дождь.                                </p>
                                <figure>
                                    <img src="images/about_placeholder.png" alt=""/>
                                    <figcaption>О катинке</figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )


    }


}