import React, {Component} from "react";
import jQuery from "jquery";
var $ = jQuery;


export default class Row extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {


    }


    componentWillUpdate(nextProps, nextState) {


    }

    componentDidUpdate() {


    }

    shouldComponentUpdate(nextProps) {

        if (nextProps.id != this.props.id) {
            return true
        }

        return false;

    }

    componentDidMount() {


    }

    render() {

        let {issuerName, code, type, category, parentCurrency, id, name} = this.props;
        return (
            <tr>
                <td>
                    { name }
                </td>
                <td>{ code }</td>
                <td>{ issuerName }</td>
                <td>{ type }</td>
                <td>{ category }</td>
                <td className="responses">
                    {parentCurrency}
                </td>
                <td>
                    {/*<div className={this.props.favoriteExchangers.hasOwnProperty(id) ? 'favorite-star icon-star active notransition' : 'favorite-star icon-star notransition'}></div>*/}
                </td>
            </tr>
        )

    }


}
