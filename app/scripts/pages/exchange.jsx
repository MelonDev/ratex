import React, { Component} from 'react';
import { Link } from 'react-router';
import jQuery from 'jquery';

import classnames from 'classnames';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import Checkbox from 'material-ui/lib/checkbox';
import RadioButton from 'material-ui/lib/radio-button';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
// import injectTapEventPlugin from 'react-tap-event-plugin';



export default class Exchange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exchangersLoaded: false,
            currencyValueFrom: 1,
            currencyValueTo: 2
        };
        this.styles = {
            select: {
                height: '45px',
                width: '40%',
                borderWidth: '1px',
                borderStyle: 'solid',
                borderColor: '#f6f6f6',
                backgroundColor: '#f6f6f6',
                padding: '0 10px',
                color: '#000',
                outline: 'none',
                display: 'block'
            },
            selectLeft: {
                float: 'left'
            },
            selectRight: {
                float: 'right'
            },
            switchersIcon: {
                fill: '#ccc'
            },
            switchers: {},
            radioButton: {}
        };
        this.ratesSample = [
            {
                sell: 1,
                get: 25.1
            },
            {
                sell:1,
                get: 25.8
            }

        ];
        this.middleRate = {
            sell:1,
            get: 25.4
        }

    }

    componentDidMount(){
        this.props.loadExchangers();

    }
    handleChange(field, event, index, value) {
        var nextState = {};
        nextState[field] = value;
        this.setState(nextState);

    }
    calculateRate(value){
        this.setState({
            currencyValueFrom: value
        });
    }
    switchCurrency(){
        this.setState({
            currencyValueFrom: this.state.currencyValueTo,
            currencyValueTo: this.state.currencyValueFrom
        });

        var fastExFromValue = this.refs.fastExInputFrom.value,
            fastExToValue = this.refs.fastExInputTo.value;
        this.refs.fastExInputFrom.value = fastExToValue;
        this.refs.fastExInputTo.value = fastExFromValue;
    }

    render(){
        var self = this;
        if(this.props.exchangersLoad.loading == false){
            let exchangers = this.props.exchangersLoad.data.exchangers;
            let exchangersMapped = exchangers.map((item, index)=>{
                return(
                    <tr key={index}>
                        <td>
                            {this.ratesSample[index].get * this.state.currencyValueFrom}
                        </td>
                        <td>
                            <Link to={'exchangers/'+ exchangers[index].vendor.id}>
                                {exchangers[index].vendor.name}
                            </Link>
                        </td>
                        <td>
                            {exchangers[index].reserveTotalUsd}
                        </td>
                        <td>
                            {exchangers[index].wmbl}
                        </td>
                    </tr>
                );
            });
            return(

                <div className="container market-container">
                    <h2>Обмен валют</h2>
                    <div className="form-group">
                        <input type="text" placeholder="1000"  defaultValue="2" onChange={function(){
                            self.calculateRate(self.refs.fastExInputFrom.value);
                        } } className="widget-input-big half-form left" ref="fastExInputFrom" />

                        <input type="text" value={this.middleRate.get * this.state.currencyValueFrom} placeholder="1000" className="widget-input-big half-form right" ref="fastExInputTo" />
                    </div>
                    <div className="form-group">
                        <SelectField value={this.state.currencyValueFrom} style={Object.assign({}, this.styles.select, this.styles.selectLeft)} className="selectLeft" onChange={this.handleChange.bind(this, 'currencyValueFrom')}>
                            <MenuItem value={1} primaryText="WMZ" />
                            <MenuItem value={2} primaryText="WMR" />
                            <MenuItem value={3} primaryText="BT" />
                            <MenuItem value={4} primaryText="EUR" />
                            <MenuItem value={5} primaryText="USD" />
                        </SelectField>
                    </div>
                    <table className="inner-table">
                        <thead>
                        <tr>
                            <td className="thead-cell time-coll">Получаете</td>
                            <td className="thead-cell course-coll">Обменник</td>
                            <td className="thead-cell sum-coll">резерв</td>
                            <td className="thead-cell comments-coll">Отзывы</td>
                        </tr>
                        </thead>
                        <tbody>
                        {exchangersMapped}
                        </tbody>
                    </table>

                </div>
            );

        }


        return(
            <div>Loading...</div>
        )

    }
}
