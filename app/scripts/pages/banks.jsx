import React, {Component} from "react";
import {Link} from "react-router";
import jQuery from "jquery";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "../actions/banks/banksActions";
import {push} from "react-router-redux";
import _ from "underscore";
import ReactPaginate from "react-paginate";
var $ = jQuery;
class Banks extends Component {

    constructor(props) {
        super(props);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
        this.searchTimeout = null;
        this.state = {}
    }

    componentWillMount() {

        var query = {
            pageNumber: this.props.location.query.pageNumber || 1,
            sort: this.props.location.query.sort || '',
            'filter[]': this.props.location.query.filter || [],
            search: this.props.location.query.search || '',
            sortDirection: this.props.location.query.sortDirection

        }

        this.props.loadBanks(query);

    }

    componentWillUpdate(nextProps, nextState) {
        if (!_.isEqual(this.props.location.query, nextProps.location.query) && this.props.currencies.data.length == nextProps.currencies.data.length) {

            var query = {
                search: nextProps.location.query.search,
                pageNumber: nextProps.location.query.pageNumber,
                sort: nextProps.location.query.sort,
                'filter[]': nextProps.location.query.filter,
                sortDirection: nextProps.location.query.sortDirection

            }

            this.props.loadBanks(query);

        }

    }


    toggleSettings(e) {

        e.stopPropagation();
        $('.rates-table-filters-wrap').toggleClass('active');

    }

    componentDidUpdate() {


    }


    handleFilter(key) {

        if (typeof this.props.currencies.meta['filter[]'] != 'string' && typeof this.props.currencies.meta['filter[]'] != 'undefined') {

            var newFilter = this.props.currencies.meta['filter[]'];
            newFilter.push(key);

            var action = push(
                {
                    pathname: '/currencies',
                    query: {
                        search: this.props.currencies.meta.search,
                        sort: this.props.currencies.meta.sort,
                        'filter[]': newFilter,
                        sortDirection: this.props.currencies.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterCurrencies(newFilter);
        } else {
            var action = push(
                {
                    pathname: '/currencies',
                    query: {
                        search: this.props.currencies.meta.search,
                        'filter[]': [key],
                        sort: this.props.currencies.meta.sort,
                        sortDirection: this.props.currencies.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterCurrencies([key]);
        }

    }

    removeFilter(filter) {

        var currenctFilter = this.props.currencies.meta['filter[]'];
        var newFilter = currenctFilter.filter((elem, index) => {
            if (elem == filter) {
                return false;
            }
            return true;
        });

        var action = push(
            {
                pathname: '/currencies',
                query: {
                    search: this.props.currencies.meta.search,
                    'filter[]': newFilter,
                    sort: this.props.currencies.meta.sort,
                    sortDirection: this.props.currencies.meta.sortDirection

                }
            }
        );
        this.props.dispatch(action);

        this.props.filterCurrencies(newFilter);

    }


    contains(array, needle) {
        if (typeof array != 'undefined') {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == needle) {
                    return true;
                }
            }
            return false;
        }
        return false
    }


    searchInputToggle() {

        $('.search-input').toggleClass('active');
        $('.search-input input').focus();

    }

    handlePagination(e) {

        var action = push(
            {
                pathname: '/currencies',
                query: {
                    pageNumber: parseInt(e.selected + 1),
                    search: this.props.currencies.meta.search,
                    'filter[]': this.props.currencies.meta['filter[]'] || [],
                    sort: this.props.currencies.meta.sort,
                    sortDirection: this.props.currencies.meta.sortDirection
                }
            }
        );
        this.props.dispatch(action);

        this.props.paginateCurrencies(parseInt(e.selected + 1));
    }


    searchFilter(val) {
        clearTimeout(this.searchTimeout);
        this.searchTimeout = setTimeout(() => {
            var inputValue = val;
            var action = push({
                pathname: '/currencies',
                query: {
                    search: val,
                    'filter[]': this.props.currencies.meta.filter,
                    sort: this.props.currencies.meta.sort,
                    sortDirection: this.props.currencies.meta.sortDirection
                }
            });
            this.props.dispatch(action);
            this.props.searchCurrencies(val);
        }, 500);

    }


    componentDidMount() {


        $(window).bind("scroll", function () {
            var tableOffset = $("#ex-table").offset().top;
            var $fixedHeader = $(".fixed-table-wrap");
            var offset = $(this).scrollTop() + 60;
            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });

        var naturalTh = $('#ex-table thead th');
        var fixedTh = $('.header-fixed thead th');

    }

    render() {
        let locale = this.props.locale.language;
        if (this.props.banks.data != null) {
            let banks = this.props.banks.data,
                paginationLinksMapped = [],
                activeFilters;
            let banksMapped = banks.map((elem, index) => {
                    var iconUrl = 'images/ex-logo.png';
                return (
                    <tr>
                        <td>
                            {elem.attributes.status}
                        </td>
                        <td>
                            <img src={iconUrl} alt=""/>
                            {elem.attributes.name}
                        </td>
                        <td>{elem.attributes.city}</td>
                        <td>{elem.attributes.street}</td>
                        <td>{elem.attributes.sort_code}</td>
                    </tr>
                )
            });

            if (typeof this.props.banks.meta['filter[]'] != 'undefined' && this.props.banks.meta['filter[]'] != '') {


                if (typeof this.props.banks.meta['filter[]'] == 'string') {
                    activeFilters = (
                        <div className="filter-item">
                            {this.props.currencies.meta['filter[]'].split(':')[1]}
                            <div className="delete-filter" onClick={() => {
                            }}>&times;</div>
                        </div>
                    )
                } else {

                    activeFilters = this.props.banks.meta['filter[]'].map((elem, index) => {
                        return (
                            <div className="filter-item" key={index}>
                                {elem.split(':')[1]}
                                <div className="delete-filter" onClick={() => {
                                    this.removeFilter(elem);
                                }}>&times;</div>
                            </div>
                        )
                    })
                }

            } else {
                activeFilters = '';
            }
            return (
                <div>
                    <div className="fixed-table-wrap">
                        <div className="container">
                            <div className="table-wrapper">
                                <table className="directory-table exchangers-table header-fixed currencies-table">
                                    <thead>
                                    <tr>
                                        <th className={this.props.banks.meta.sort == 'name' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/currencies',
                                                query: {
                                                    pageNumber: this.props.banks.meta.pageNumber,
                                                    sort: 'name',
                                                    'filter[]': this.props.banks.meta['filter[]'],
                                                    sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortBanks('name')
                                            }}>Название</Link>
                                            <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.banks.meta.sort == 'code' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/banks',
                                                query: {
                                                    pageNumber: this.props.banks.meta.pageNumber,
                                                    sort: 'code',
                                                    'filter[]': this.props.banks.meta['filter[]'],
                                                    sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortBanks('code')
                                            }}>Код</Link>
                                            <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>

                                        </th>
                                        <th className={this.props.banks.meta.sort == 'currency_type' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/banks',
                                                query: {
                                                    pageNumber: this.props.banks.meta.pageNumber,
                                                    sort: 'currency_type',
                                                    'filter[]': this.props.banks.meta['filter[]'],
                                                    sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortBanks('currency_type')
                                            }}>Тип</Link>
                                            <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.banks.meta.sort == 'category' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/banks',
                                                query: {
                                                    pageNumber: this.props.banks.meta.pageNumber,
                                                    sort: 'category',
                                                    'filter[]': this.props.banks.meta['filter[]'],
                                                    sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'
                                                }
                                            }} onClick={(e) => {
                                                this.props.sortBanks('category')
                                            }}>Категория</Link>
                                            <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th>

                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div className="container market-container">
                        <div className="directory-wrap currencies-wrap">
                            <div className="directory-header-wrap">
                                <h2 className="directory-header">Список валют</h2>
                                <div className="rates-table-filters">
                                    <div className="rates-table-filters-wrap">
                                        <div className="ru-settings" onClick={this.toggleSettings}></div>
                                        <div className="rates-filter-dropdown dropdown" onClick={(e) => {
                                            e.stopPropagation()
                                        }}>
                                            <div className="dropdown-arrow"></div>

                                        </div>
                                    </div>
                                </div>
                                <div className="filters-array">
                                    {activeFilters}
                                </div>
                                <div className="search-input"
                                     onClick={this.searchInputToggle}>
                                    <input
                                        className="search-input" onBlur={() => {
                                        $('.search-input').removeClass('active')
                                    }} onChange={(e) => {
                                        this.searchFilter(e.target.value)
                                    }}/>
                                </div>
                            </div>
                            <table id="ex-table" className="directory-table exchangers-table currencies-table">
                                <thead>
                                <tr>
                                    <th className={this.props.banks.meta.sort == 'name' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/banks',
                                            query: {
                                                pageNumber: this.props.banks.meta.pageNumber,
                                                sort: 'name',
                                                'filter[]': this.props.banks.meta['filter[]'],
                                                sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortBanks('name')
                                        }}>Название</Link>
                                        <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.banks.meta.sort == 'code' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/banks',
                                            query: {
                                                pageNumber: this.props.banks.meta.pageNumber,
                                                sort: 'code',
                                                'filter[]': this.props.banks.meta['filter[]'],
                                                sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortBanks('code')
                                        }}>Код</Link>
                                        <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>

                                    </th>
                                    <th className={this.props.banks.meta.sort == 'currency_type' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/banks',
                                            query: {
                                                pageNumber: this.props.banks.meta.pageNumber,
                                                sort: 'currency_type',
                                                'filter[]': this.props.banks.meta['filter[]'],
                                                sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortBanks('currency_type')
                                        }}>Тип</Link>
                                        <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.banks.meta.sort == 'category' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/banks',
                                            query: {
                                                pageNumber: this.props.banks.meta.pageNumber,
                                                sort: 'category',
                                                'filter[]': this.props.banks.meta['filter[]'],
                                                sortDirection: this.props.banks.meta.sortDirection == 'up' ? 'down' : 'up'
                                            }
                                        }} onClick={(e) => {
                                            this.props.sortBanks('category')
                                        }}>Категория</Link>
                                        <img className={'filter-logo ' + this.props.banks.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {banksMapped}
                                </tbody>
                            </table>
                        </div>
                        <div className="pagination">
                            <ReactPaginate previousLabel={<span className="icon-arrow-big-left"></span>}
                                           nextLabel={<span className="ru-arrow"></span>}
                                           breakLabel={<a href="">...</a>}
                                           breakClassName={"break-me"}
                                           pageCount={this.props.banks.meta.pages}
                                           marginPagesDisplayed={2}
                                           initialPage={parseInt(this.props.banks.meta.pageNumber) - 1}
                                           pageRangeDisplayed={7}
                                           onPageChange={this.handlePagination}
                                           containerClassName={"pagination"}
                                           subContainerClassName={"pages pagination"}
                                           activeClassName={"active"}/>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className="container market-container">
                    <div className="directory-wrap loading-directory">
                        <div className="preloader-wrap">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }

}

function mapDispatchToProps(dispatch) {
    return {...bindActionCreators(ActionCreators, dispatch), dispatch};
}

function mapStateToProps(state) {
    return {
        locale: state.locale,
        banks: state.banks
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Banks);


