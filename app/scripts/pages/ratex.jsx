import React, {Component} from "react";
import jQuery from "jquery";
import {Tab, Tabs} from "react-bootstrap";
import moment from "moment";
import Cash from "../components/Rates/cash.jsx";
import Cards from "../components/Rates/cards.jsx";
import Official from "../components/Rates/official.jsx";
import {connect} from "react-redux";
import {push} from "react-router-redux";
import * as ActionCreators from "../actions/rates/ratesActions";
import {bindActionCreators} from "redux";
import {SimpleSelect} from "react-selectize";
import didMount from "../reusable/didMount";


var $ = jQuery;


class Rates extends Component {
    constructor(props) {
        super(props);
        this.setCalendarDate = this.setCalendarDate.bind(this);
        this.changeQuoteCurrency = this.changeQuoteCurrency.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.dataForTable = null;
        this.reserves = null;
        this.keyToFilter = ['name'];
        this.searchTimeout = null;
        this.wmbl = null;
        this.name = null;
        this.state = {
            currentPage: 1,
            exchangers: null,
            searchTerm: '',
            showActive: false,
            calendarDateText: '',
            activeTab: props.params.tab_alias
        }
    }


    removeFilter(param) {

    }

    componentWillMount() {

    }

    toggleSettings(e) {

        e.stopPropagation();
        $('.settings-wrap').toggleClass('tooltip-visible');

    }

    componentWillUpdate(nextProps, nextState) {

        if (nextState.exchangersLoaded != null && this.state.exchangersLoaded == null) {

        }

    }

    componentDidUpdate() {

    }


    showCalendar(e) {

        e.stopPropagation();
        $('.calendar-dropdown').addClass('visible');

    }

    hideCalendar() {

        $('.calendar-dropdown').removeClass('visible');


    }

    changeQuoteCurrency(currency) {


        this.props.changeQouteCurrency(currency);


    }

    setCalendarDate(date) {

        moment.locale('ru');

        this.setState({
            calendarDateText: moment(date).format('D MMMM')
        });

        this.hideCalendar();

    }

    stopPropagation(e) {

        e.stopPropagation();

    }

    searchInputToggle() {

        $('.search-input').toggleClass('active');
        $('.search-input input').focus();

    }


    handleSelect(tabNumber) {
        var tab;
        switch (tabNumber) {
            case 1:
                tab = 'cash'
                break;
            case 2:
                tab = 'cards'
                break;
            case 3:
                tab = 'official'
                break;
            default:
                tab = 'cash'


        }
        var action = push(
            {
                pathname: '/rates/' + tab,
                query: {
                    'filter[]': [],
                }
            }
        );
        this.props.dispatch(action);
        this.props.changeTab(tab);
        this.setState({
            activeTab: tab
        })
    }

    componentDidMount() {
        didMount();
        // $(document).on('click', ()=>{
        //     $('.dropdown').removeClass('visible');
        // });

        $(window).bind("scroll", function () {
            var tableOffset = $("#ex-table").offset().top;
            var $fixedHeader = $(".header-fixed");
            var offset = $(this).scrollTop() + 60;

            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });

        var naturalTh = $('#ex-table thead th');
        var fixedTh = $('.header-fixed thead th');


        // didMount(this);

    }

    render() {

        var self = this;

        var activeKey = () => {
            switch (this.state.activeTab) {
                case 'cash':
                    return 1;
                case 'cards':
                    return 2;
                case 'official':
                    return 3;
            }
        }

        return (
            <div className="container market-container rates-container">
                <div className="directory-wrap">
                    <div className="directory-header-wrap">
                        <h2 className="directory-header">Курс валюты
                            <div className="city-dropdown dropdown-wrap" ref='currency_dropdown'
                                 onClick={(e) => {
                                     e.stopPropagation();
                                     $(this.refs.currency_dropdown).addClass('open');
                                     this.refs.currency_selectize.focus();
                                 }}>
                                &ensp;<span
                                className="underline-span">{this.props.rates.currencySelected}</span>
                                <div className="selectize-wrap selectize-dropdown">
                                    <SimpleSelect
                                        ref='currency_selectize'
                                        options={[{value: 'USD', label: 'USD'}, {value: 'UAH', label: 'UAH'}]}
                                        onValueChange={(item) => {
                                            this.changeQuoteCurrency(item.value);
                                            $(this.refs.currency_dropdown).removeClass('open');
                                        }}
                                        open={true}
                                    />
                                </div>
                            </div>
                        </h2>
                    </div>
                    <Tabs
                        defaultActiveKey={activeKey()}
                        activeKey={activeKey()}
                        id="uncontrolled-tab-example"
                        onSelect={this.handleSelect}
                        unmountOnExit={false}
                    >
                        <Tab eventKey={1} title="Наличные">
                            <Cash params={this.props.params} location={this.props.location}/>
                        </Tab>
                        <Tab eventKey={2} title="Карточные">
                            <Cards params={this.props.params} location={this.props.location}/>
                        </Tab>
                        <Tab eventKey={3} title="Официальные">
                            <Official params={this.props.params} location={this.props.location}/>
                        </Tab>
                    </Tabs>

                </div>
            </div>

        );

        // }

    }

}

function mapDispatchToProps(dispatch) {

    return {...bindActionCreators({...ActionCreators}, dispatch), dispatch};

}

function mapStateToProps(state) {
    return {
        locale: state.locale,
        rates: state.rates
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Rates);
