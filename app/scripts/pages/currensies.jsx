import React, {Component} from "react";
import {Link} from "react-router";
import jQuery from "jquery";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "../actions/currencies/currenciesActions";
import * as favoriteActions from "../actions/favorites/favoriteCurrencies";
import {push} from "react-router-redux";
import _ from "underscore";
import ReactPaginate from "react-paginate";
import didMount from "../reusable/didMount";
var $ = jQuery;
class Currencies extends Component {
    constructor(props) {
        super(props);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
        this.searchTimeout = null;
        this.state = {}
    }

    componentWillMount() {

        var query = {
            pageNumber: this.props.location.query.pageNumber || 1,
            sort: this.props.location.query.sort || '',
            'filter[]': this.props.location.query.filter || [],
            search: this.props.location.query.search || '',
            sortDirection: this.props.location.query.sortDirection

        };

        this.props.loadCurrencies(query);

    }

    componentWillUpdate(nextProps, nextState) {
        if (!_.isEqual(this.props.location.query, nextProps.location.query) && this.props.currencies.data.length == nextProps.currencies.data.length) {

            var query = {
                search: nextProps.location.query.search,
                pageNumber: nextProps.location.query.pageNumber,
                sort: nextProps.location.query.sort,
                'filter[]': nextProps.location.query.filter,
                sortDirection: nextProps.location.query.sortDirection

            }

            this.props.loadCurrencies(query);

        }

    }


    toggleSettings(e) {

        e.stopPropagation();
        $('.rates-table-filters-wrap').toggleClass('active');

    }

    componentDidUpdate() {


    }


    handleFilter(key) {

        if (typeof this.props.currencies.meta['filter[]'] != 'string' && typeof this.props.currencies.meta['filter[]'] != 'undefined') {

            var newFilter = this.props.currencies.meta['filter[]'];
            newFilter.push(key);

            var action = push(
                {
                    pathname: '/currencies',
                    query: {
                        search: this.props.currencies.meta.search,
                        sort: this.props.currencies.meta.sort,
                        'filter[]': newFilter,
                        sortDirection: this.props.currencies.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterCurrencies(newFilter);
        } else {
            var action = push(
                {
                    pathname: '/currencies',
                    query: {
                        search: this.props.currencies.meta.search,
                        'filter[]': [key],
                        sort: this.props.currencies.meta.sort,
                        sortDirection: this.props.currencies.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterCurrencies([key]);
        }

    }

    removeFilter(filter) {

        var currenctFilter = this.props.currencies.meta['filter[]'];
        var newFilter = currenctFilter.filter((elem, index) => {
            if (elem == filter) {
                return false;
            }
            return true;
        });

        var action = push(
            {
                pathname: '/currencies',
                query: {
                    search: this.props.currencies.meta.search,
                    'filter[]': newFilter,
                    sort: this.props.currencies.meta.sort,
                    sortDirection: this.props.currencies.meta.sortDirection

                }
            }
        );
        this.props.dispatch(action);

        this.props.filterCurrencies(newFilter);

    }


    contains(array, needle) {
        if (typeof array != 'undefined') {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == needle) {
                    return true;
                }
            }
            return false;
        }
        return false
    }


    searchInputToggle() {

        $('.search-input').toggleClass('active');
        $('.search-input input').focus();

    }

    handlePagination(e) {

        var action = push(
            {
                pathname: '/currencies',
                query: {
                    pageNumber: parseInt(e.selected + 1),
                    search: this.props.currencies.meta.search,
                    'filter[]': this.props.currencies.meta['filter[]'] || [],
                    sort: this.props.currencies.meta.sort,
                    sortDirection: this.props.currencies.meta.sortDirection
                }
            }
        );
        this.props.dispatch(action);

        this.props.paginateCurrencies(parseInt(e.selected + 1));
    }



    searchFilter(val) {
        clearTimeout(this.searchTimeout);
        this.searchTimeout = setTimeout(() => {
            var inputValue = val;
            var action = push({
                pathname: '/currencies',
                query: {
                    search: val,
                    'filter[]': this.props.currencies.meta.filter,
                    sort: this.props.currencies.meta.sort,
                    sortDirection: this.props.currencies.meta.sortDirection
                }
            });
            this.props.dispatch(action);
            this.props.searchCurrencies(val);
        }, 500);

    }


    componentDidMount() {

        didMount();
        $(window).bind("scroll", function () {
            var tableOffset = $("#ex-table").offset().top;
            var $fixedHeader = $(".fixed-table-wrap");
            var offset = $(this).scrollTop() + 60;
            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });

        var naturalTh = $('#ex-table thead th');
        var fixedTh = $('.header-fixed thead th');

    }

    render() {
        let locale = this.props.locale.language;
        if (this.props.currencies.data != null) {
            let currencies = this.props.currencies.data,
                paginationLinksMapped = [],
                activeFilters;
            let currenciesMapped = currencies.map((elem, index) => {
                if (elem.attributes.icon_status != null) {
                    var iconUrl = elem.attributes.icon['32'];
                    // var iconUrl = elem.attributes.icon_status == 's' ? config.api_url + 'img/currencies/' + elem.attributes.code + '/icon.svg' : config.api_url + 'img/currencies/' + elem.attributes.code + '/icon32.png';
                } else {
                    var iconUrl = '/images/currency_placeholder.svg';
                }
                try {
                    var favorite = this.props.favoriteCurrencies[elem.attributes.code] !== undefined;
                }
                catch (e) {
                    console.log(e);
                    var favorite = false;
                }
                return (
                    <tr>
                        <td>
                            <img src={iconUrl} alt=""/>
                            {elem.attributes.name}
                        </td>
                        <td>{elem.attributes.code}</td>
                        <td>{locale.t('_currencies', {returnObjects: true})[elem.attributes.currency_type]}</td>
                        <td>{locale.t('_currencies', {returnObjects: true})[elem.attributes.category]}</td>
                        <td>
                            <div
                                className={favorite ? 'favorite-star icon-star notransition active' : 'favorite-star icon-star notransition' }
                                onClick={
                                    () => {
                                        if (favorite) {
                                            this.props.removeFavoriteCurrency(elem.attributes.code)
                                        } else {
                                            this.props.addFavoriteCurrency(elem.attributes.code)
                                        }
                                    }
                                }>
                            </div>
                        </td>
                    </tr>
                )
            });

            if (typeof this.props.currencies.meta['filter[]'] != 'undefined' && this.props.currencies.meta['filter[]'] != '') {


                if (typeof this.props.currencies.meta['filter[]'] == 'string') {
                    activeFilters = (
                        <div className="filter-item">
                            {this.props.currencies.meta['filter[]'].split(':')[1]}
                            <div className="delete-filter" onClick={() => {
                            }}>&times;</div>
                        </div>
                    )
                } else {

                    activeFilters = this.props.currencies.meta['filter[]'].map((elem, index) => {
                        return (
                            <div className="filter-item" key={index}>
                                {locale.t('_currencies', {returnObjects: true})[elem.split(':')[1]]}
                                <div className="delete-filter" onClick={() => {
                                    this.removeFilter(elem);
                                }}>&times;</div>
                            </div>
                        )
                    })
                }

            } else {
                activeFilters = '';
            }
            return (
                <div>
                    <div className="fixed-table-wrap">
                        <div className="container">
                            <div className="table-wrapper">
                                <table className="directory-table exchangers-table header-fixed currencies-table">
                                    <thead>
                                    <tr>
                                        <th className={this.props.currencies.meta.sort == 'name' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/currencies',
                                                query: {
                                                    pageNumber: this.props.currencies.meta.pageNumber,
                                                    sort: 'name',
                                                    'filter[]': this.props.currencies.meta['filter[]'],
                                                    sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortCurrencies('name')
                                            }}>Название</Link>
                                            <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.currencies.meta.sort == 'code' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/currencies',
                                                query: {
                                                    pageNumber: this.props.currencies.meta.pageNumber,
                                                    sort: 'code',
                                                    'filter[]': this.props.currencies.meta['filter[]'],
                                                    sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortCurrencies('code')
                                            }}>Код</Link>
                                            <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>

                                        </th>
                                        <th className={this.props.currencies.meta.sort == 'currency_type' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/currencies',
                                                query: {
                                                    pageNumber: this.props.currencies.meta.pageNumber,
                                                    sort: 'currency_type',
                                                    'filter[]': this.props.currencies.meta['filter[]'],
                                                    sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortCurrencies('currency_type')
                                            }}>Тип</Link>
                                            <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.currencies.meta.sort == 'category' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/currencies',
                                                query: {
                                                    pageNumber: this.props.currencies.meta.pageNumber,
                                                    sort: 'category',
                                                    'filter[]': this.props.currencies.meta['filter[]'],
                                                    sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'
                                                }
                                            }} onClick={(e) => {
                                                this.props.sortCurrencies('category')
                                            }}>Категория</Link>
                                            <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th>

                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div className="container market-container">
                        <div className="directory-wrap currencies-wrap">
                            <div className="directory-header-wrap">
                                <h2 className="directory-header">Список валют</h2>
                                <div className="rates-table-filters">
                                    <div className="rates-table-filters-wrap">
                                        <div className="ru-settings" onClick={this.toggleSettings}></div>
                                        <div className="rates-filter-dropdown dropdown" onClick={(e) => {
                                            e.stopPropagation()
                                        }}>
                                            <div className="dropdown-arrow"></div>
                                            <div className="parameters">
                                                <p>{locale.t('_currencies', {returnObjects: true}).category}</p>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:others')}
                                                        type="checkbox"
                                                        id="others" name="others"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:others') ? () => {
                                                            this.removeFilter('category:others')
                                                        } : () => {
                                                            this.handleFilter('category:others')
                                                        }} className="checkbox-label"
                                                        htmlFor="others">{locale.t('_currencies', {returnObjects: true}).others}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:cash')}
                                                        type="checkbox"
                                                        id="cash" name="cash"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:cash') ? () => {
                                                            this.removeFilter('category:cash')
                                                        } : () => {
                                                            this.handleFilter('category:cash')
                                                        }} className="checkbox-label"
                                                        htmlFor="cash">{locale.t('_currencies', {returnObjects: true}).cash}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:online_bankings')}
                                                        type="checkbox"
                                                        id="online_bankings" name="online_bankings"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:online_bankings') ? () => {
                                                            this.removeFilter('category:online_bankings')
                                                        } : () => {
                                                            this.handleFilter('category:online_bankings')
                                                        }} className="checkbox-label"
                                                        htmlFor="online_bankings">{locale.t('_currencies', {returnObjects: true}).online_bankings}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:money_transfers')}
                                                        type="checkbox"
                                                        id="money_transfers" name="money_transfers"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:money_transfers') ? () => {
                                                            this.removeFilter('category:money_transfers')
                                                        } : () => {
                                                            this.handleFilter('category:money_transfers')
                                                        }} className="checkbox-label"
                                                        htmlFor="money_transfers">{locale.t('_currencies', {returnObjects: true}).money_transfers}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:crypto_currencies')}
                                                        type="checkbox"
                                                        id="crypto_currencies" name="crypto_currencies"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:crypto_currencies') ? () => {
                                                            this.removeFilter('category:crypto_currencies')
                                                        } : () => {
                                                            this.handleFilter('category:crypto_currencies')
                                                        }} className="checkbox-label"
                                                        htmlFor="crypto_currencies">{locale.t('_currencies', {returnObjects: true}).crypto_currencies}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:electronic')}
                                                        type="checkbox"
                                                        id="electronic" name="electronic"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:electronic') ? () => {
                                                            this.removeFilter('category:electronic')
                                                        } : () => {
                                                            this.handleFilter('category:electronic')
                                                        }} className="checkbox-label"
                                                        htmlFor="electronic">{locale.t('_currencies', {returnObjects: true}).electronic}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:vouchers')}
                                                        type="checkbox"
                                                        id="vouchers" name="vouchers"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:vouchers') ? () => {
                                                            this.removeFilter('category:vouchers')
                                                        } : () => {
                                                            this.handleFilter('category:vouchers')
                                                        }} className="checkbox-label"
                                                        htmlFor="vouchers">{locale.t('_currencies', {returnObjects: true}).vouchers}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'category:cards')}
                                                        type="checkbox"
                                                        id="cards" name="cards"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'category:cards') ? () => {
                                                            this.removeFilter('category:cards')
                                                        } : () => {
                                                            this.handleFilter('category:cards')
                                                        }} className="checkbox-label"
                                                        htmlFor="cards">{locale.t('_currencies', {returnObjects: true}).cards}</label>
                                                </div>
                                            </div>
                                            <div className="vendors">
                                                <p>{locale.t('_currencies', {returnObjects: true}).type}</p>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:digital')}
                                                        type="checkbox"
                                                        id="digital" name="digital"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:digital') ? () => {
                                                            this.removeFilter('currency_type:digital')
                                                        } : () => {
                                                            this.handleFilter('currency_type:digital')
                                                        }} className="checkbox-label"
                                                        htmlFor="digital">{locale.t('_currencies', {returnObjects: true}).digital}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:national')}
                                                        type="checkbox"
                                                        id="national" name="national"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:national') ? () => {
                                                            this.removeFilter('currency_type:national')
                                                        } : () => {
                                                            this.handleFilter('currency_type:national')
                                                        }} className="checkbox-label"
                                                        htmlFor="national">{locale.t('_currencies', {returnObjects: true}).national}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:technical')}
                                                        type="checkbox"
                                                        id="technical" name="technical"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:technical') ? () => {
                                                            this.removeFilter('currency_type:technical')
                                                        } : () => {
                                                            this.handleFilter('currency_type:technical')
                                                        }} className="checkbox-label"
                                                        htmlFor="technical">{locale.t('_currencies', {returnObjects: true}).technical}</label>
                                                </div>
                                                <div className="form-group">
                                                    <input
                                                        checked={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:metal')}
                                                        type="checkbox"
                                                        id="metal" name="metal"/>
                                                    <label
                                                        onClick={this.contains(this.props.currencies.meta['filter[]'], 'currency_type:metal') ? () => {
                                                            this.removeFilter('currency_type:metal')
                                                        } : () => {
                                                            this.handleFilter('currency_type:metal')
                                                        }} className="checkbox-label"
                                                        htmlFor="metal">{locale.t('_currencies', {returnObjects: true}).metal}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="filters-array">
                                    {activeFilters}
                                </div>
                                <div className="search-input"
                                     onClick={this.searchInputToggle}>
                                    <input
                                        defaultValue={this.props.location.query.search}
                                        onBlur={() => {
                                        $('.search-input').removeClass('active')
                                    }} onChange={(e) => {
                                        this.searchFilter(e.target.value)
                                    }}/>
                                </div>
                            </div>
                            <table id="ex-table" className="directory-table exchangers-table currencies-table">
                                <thead>
                                <tr>
                                    <th className={this.props.currencies.meta.sort == 'name' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/currencies',
                                            query: {
                                                pageNumber: this.props.currencies.meta.pageNumber,
                                                sort: 'name',
                                                'filter[]': this.props.currencies.meta['filter[]'],
                                                sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortCurrencies('name')
                                        }}>Название</Link>
                                        <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.currencies.meta.sort == 'code' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/currencies',
                                            query: {
                                                pageNumber: this.props.currencies.meta.pageNumber,
                                                sort: 'code',
                                                'filter[]': this.props.currencies.meta['filter[]'],
                                                sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortCurrencies('code')
                                        }}>Код</Link>
                                        <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>

                                    </th>
                                    <th className={this.props.currencies.meta.sort == 'currency_type' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/currencies',
                                            query: {
                                                pageNumber: this.props.currencies.meta.pageNumber,
                                                sort: 'currency_type',
                                                'filter[]': this.props.currencies.meta['filter[]'],
                                                sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortCurrencies('currency_type')
                                        }}>Тип</Link>
                                        <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.currencies.meta.sort == 'category' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/currencies',
                                            query: {
                                                pageNumber: this.props.currencies.meta.pageNumber,
                                                sort: 'category',
                                                'filter[]': this.props.currencies.meta['filter[]'],
                                                sortDirection: this.props.currencies.meta.sortDirection == 'up' ? 'down' : 'up'
                                            }
                                        }} onClick={(e) => {
                                            this.props.sortCurrencies('category')
                                        }}>Категория</Link>
                                        <img className={'filter-logo ' + this.props.currencies.meta.sortDirection}src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {currenciesMapped}
                                </tbody>
                            </table>
                        </div>
                        <div className="pagination">
                            <ReactPaginate previousLabel={<span className="icon-arrow-big-left"></span>}
                                           nextLabel={<span className="ru-arrow"></span>}
                                           breakLabel={<a href="">...</a>}
                                           breakClassName={"break-me"}
                                           pageCount={this.props.currencies.meta.pages}
                                           marginPagesDisplayed={2}
                                           initialPage={parseInt(this.props.currencies.meta.pageNumber) - 1}
                                           pageRangeDisplayed={7}
                                           onPageChange={this.handlePagination}
                                           containerClassName={"pagination"}
                                           subContainerClassName={"pages pagination"}
                                           activeClassName={"active"}/>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className="container market-container">
                    <div className="directory-wrap loading-directory">
                        <div className="preloader-wrap">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }


}

function mapDispatchToProps(dispatch) {
    return {...bindActionCreators({...ActionCreators, ...favoriteActions}, dispatch), dispatch};
}

function mapStateToProps(state) {
    return {
        locale: state.locale,
        favoriteCurrencies: state.favoriteCurrencies,
        currencies: state.currencies
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Currencies);


