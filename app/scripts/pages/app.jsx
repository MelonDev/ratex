import React from 'react';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import Aside from '../components/aside.jsx';
import AddWidget from '../components/addWidget.jsx';
import jQuery from 'jquery';
import Success from '../components/FlashMessages/success.jsx';
import {removeDrops} from '../reusable/extendingFunctions';

import i18next from '../i18n';

var $ = jQuery;


export default class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {

            openWidgetMenu: false,
            widgetSet:this.props.widgetSet
        };
    }
    openMenu() {

    }
    openWidgetMenu(){

        const openWidgetMenu = !this.state.openWidgetMenu;
        this.setState({openWidgetMenu});

    }

    componentWillMount(){

        this.props.changeLanguage(i18next);

    }

    componentDidMount(){

       $(document).on('keyup', (e )=> {
           if(e.keyCode == 27){
               this.props.hideMenu();
           }
       });

    }
    componentDidUpdate(){

    }
    onWidgetAdded(name){
        $('.flash-message.success-message .name').text(name);
        $('.flash-message.success-message').fadeIn();
        setTimeout(function(){

            $('.flash-message.success-message').fadeOut();

        }, 2000);
    }

    render(){
        let self = this;
        return(
            <div className="layout-wrap" onClick={removeDrops}>

                <Success />

                <Header  {...this.props}  openMenu={function(){self.props.openMenu()}} />

                <Aside {...this.props} openMenu={function(){self.props.openMenu()}} />

                <AddWidget  widgetsSet={self.props.widgetsSet} onWidgetAdded = {this.onWidgetAdded} addWidget = {this.props.addWidget} widgetList = {self.props.widgetList}  widgetSet = {self.props.widgetSet} getWidgetList={self.props.getWidgetList} openWidgetMenu = { self.props.openWidgetMenu } hideWidgetMenu = { self.props.hideWidgetMenu } addWidgetMenu = { self.props.addWidgetMenu }/>

                <div className="layout-content">
                    {React.cloneElement(this.props.children, this.props)}
                </div>

                <Footer {...this.props}/>



            </div>
        );
    }
};
