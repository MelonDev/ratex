import React from 'react';
import jQuery from 'jquery';
var $ = jQuery;
import {Link} from 'react-router';

class NotFound extends React.Component {
    constructor(props){
        super(props);
    }
    componentWillUnmount(){
        $('.profile-btn').show();
    }
    render(){
        // $('.profile-btn').hide();
        return (
            <div>
                <div className="container not-found-container">
                    <div className="centered-cont not-found-cont">
                        <h1>404</h1>
                        <p>Жаль не нашлась страница...</p>
                        <div className="btn-cont">
                            <Link className="btn btn-primary btn-exchange" to="/">Вернуться на главную</Link>
                        </div>
                    </div>
                    <div className="powered-by-not-found">
                        <a href="#">
                            <span>Povered by</span>
                            <span className="ru-paymaxi icon"></span>
                        </a>
                    </div>
                </div>

            </div>

        );
    }
}

export default NotFound;