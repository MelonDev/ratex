import React, {Component} from "react";
import {Tab, Tabs} from "react-bootstrap";
import Summary from "../components/Exchanger/summary.jsx";
import Reserves from "../components/Exchanger/reserves.jsx";
import Rates from "../components/Exchanger/rates.jsx";
import {push} from "react-router-redux";
import {connect} from "react-redux";
import didMount from "../reusable/didMount";



class Exchanger extends Component {

    constructor(props) {
        super(props);
        this.handleSelect = this.handleSelect.bind(this);
        this.checkData = this.checkData.bind(this);
        this.pieChartConfig = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {

                text: ''
            },
            tooltip: {
                style: {
                    color: '#314156',
                    fontSize: '12px',
                    fontFamily: 'Open Sans'
                },
                backgroundColor: '#FFFFFF',
                borderColor: '#B6BFCA',
                borderRadius: 0,
                borderWidth: 1,
                shadow: false,
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            legend: {
                backgroundColor: '#fff',
                layout: 'horizontal',
                floating: false,
                align: 'right',
                verticalAlign: 'middle',
                itemStyle: {
                    "display": 'block',
                    'font-family': 'Open Sans',
                    'font-weight': 600,
                    'font-size': 16
                },
                itemMarginTop: 10,
                shadow: false,
                width: 200,
                itemWidth: 200,
                border: 0,
                borderRadius: 0,
                borderWidth: 0,
                useHTML: true
            },

            plotOptions: {
                series: {
                    state:
                        {
                            hover:{
                                halo:{
                                    size:0
                                }
                            }
                        },
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: "{y}%",
                        connectorWidth: 0,
                        distance: 0,
                        y: -5,
                        inside: false,
                        useHTML: true
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,

                data: [{
                    name: 'Microsoft Internet Explorer',
                    y: 56.33
                }, {
                    name: 'Chrome',
                    y: 24.03,
                }, {
                    name: 'Firefox',
                    y: 10.38
                }, {
                    name: 'Safari',
                    y: 4.77 + 0.91
                }]
            }]
        };
        this.state = {
            activeTab:'initial'
        }
    }

    componentWillMount() {

        var id = this.props.params.exchanger_alias;
        var tab = this.props.params.tab;
        //loading main exchanger info by default
        this.props.loadExchanger(id);
        switch (tab) {
            case 'summary':
                this.activeTab = 1;
                break;
            case 'reserves':
                this.props.loadExchangerReserves(id);
                this.activeTab = 2;
                break;
            case 'directions':
                this.props.loadExchangerRates(id);
                this.activeTab = 3;
                break;
            default:
                return
        }

    }

    handleSelect(key) {

        var id = this.props.params.exchanger_alias;

        switch (key) {
            case 1:
                this.props.dispatch(push('/exchangers/' + this.props.params.exchanger_alias + '/summary'));
                this.setState({activeTab:1});
                break;
            case 2:
                this.props.dispatch(push('/exchangers/' + this.props.params.exchanger_alias + '/reserves'));
                this.setState({activeTab:2});
                break;
            case 3:
                this.props.dispatch(push('/exchangers/' + this.props.params.exchanger_alias + '/directions'));
                this.setState({activeTab:3});
                break;
            default:
                return;

        }


    }



    checkData(tabName){
        var id = this.props.params.exchanger_alias;
        switch (tabName){
            case 'reserves':
                this.props.loadExchangerReserves(id);
                break;
            case 'rates':
                this.props.loadExchangerRates(id);
                break;

        }

    }

    componentDidMount(){
        didMount();

    }

    componentWillUpdate(nextProps){
    }

    render() {
        if (this.props.exchanger.data == null) {
            return (
                <div className="container  exchanger">
                    <div className="directory-wrap single-exchanger-wrap">
                        <div className="preloader">Loading...</div>
                    </div>
                </div>
            )

        }
        let id = this.props.params.exchanger_alias;
        let tab = this.props.params.tab;
        let activeKey;
        if(this.state.activeTab == 'initial'){
            switch (tab) {
                case 'summary':
                    activeKey = 1;
                    break;
                case 'reserves':
                    activeKey = 2;
                    break;
                case 'directions':
                    activeKey = 3;
                    break;
                default:
                    activeKey = 1
            }
        }else{
            activeKey = this.state.activeTab;
        }


        var exchanger = this.props.exchanger.data.data,
            name = exchanger.attributes.name || 'Anonimus',
            status = exchanger.attributes.is_active || false,
            founded = exchanger.attributes.founded || 'нет данных',
            rating = exchanger.attributes.rating || 1,
            wmid = exchanger.attributes.wmid || 'отсутсвует',
            wmbl = exchanger.attributes.wmbl || 'отсутсвует',
            reserve = exchanger.attributes.reserve_in_usd || 'нет данных',
            ratexResPos = exchanger.attributes.ratex_res_pos || '-',
            ratexResNeg = exchanger.attributes.ratex_res_neg || '-',
            wmAdvisorResPositive = exchanger.attributes.wm_advisor_res_positive || '-',
            wmAdvisorResNegative = exchanger.attributes.wm_advisor_res_negative || '-',
            arbitrageResPositive = exchanger.attributes.arbitrage_res_positive || '-',
            arbitrageResNegative = exchanger.attributes.arbitrage_res_negative || '-',
            country = exchanger.attributes.country || 'нет данных',
            email = exchanger.attributes.email || 'нет данных',
            site = exchanger.attributes.web_site || ' ',
            phone = exchanger.attributes.phone || 'нет данных',
            exchangerId = this.props.params.exchanger_alias;

        return (
            <div className="container  exchanger" >
                <div className="directory-wrap single-exchanger-wrap">
                    <div className="exchanger-head-info">
                        <div className="row">
                            <div className="col-sm-8 col-xs-8">
                                <div className="exchanger-logo">
                                    <img src="/images/ex-logo-dir.png" alt=""/>
                                </div>
                                <div className="exchanger-main-text">
                                    <h3>{name} <span className="icon icon-star"></span></h3>
                                    <div className="status-and-link">
                                        <div className={status ? 'status active' : 'status'}>
                                            {status ? 'Доступен' : 'Недоступен'}
                                        </div>
                                        <a href={site} className="link-self">{site}</a>
                                    </div>
                                    <div className="phone-and-mail">
                                        <div className="phone">
                                            <img className="image-icon" src="/images/icon_phone.svg" alt=""/>
                                            {phone}
                                        </div>
                                        <div className="mail">
                                            <img className="image-icon" src="/images/icon_mail.svg" alt=""/>
                                            {email}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style={{display:'none'}} className="col-sm-4 col-xs-12">
                                <div className="exchanger-btn-block">
                                    <button className="btn btn-primary ">Добавить отзыв</button>
                                    <a href="#" className="responses-link">
                                        <div className="icon ru-user"></div>
                                        <span className="comments-count">48</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="exchanger-tabs">
                        <Tabs activeKey={activeKey} id="uncontrolled-tab-example"
                              onSelect={this.handleSelect}>
                            <Tab eventKey={1} title="Сводка">
                                <Summary
                                    status={status}
                                    founded={founded}
                                    rating={rating}
                                    country={country}
                                    wmid={wmid}
                                    wmbl={wmbl}
                                    reserve={reserve}
                                    ratexResPos={ratexResPos}
                                    ratexResNeg={ratexResNeg}
                                    arbitrageResPositive={arbitrageResPositive}
                                    arbitrageResNegative={arbitrageResNegative}
                                    wmAdvisorResPositive={wmAdvisorResPositive}
                                    wmAdvisorResNegative={wmAdvisorResNegative}
                                />
                            </Tab>
                            <Tab eventKey={2} title="Резервы" onEnter = {()=>{this.checkData('reserves')}}>
                                {
                                    this.props.exchanger.reserves == null ?
                                        'загрузка' :
                                        <Reserves reserves={this.props.exchanger.reserves}/>
                                }
                            </Tab>
                            <Tab eventKey={3} title="Направление обмена" onEnter = {()=>{this.checkData('rates')}}>
                                {
                                    this.props.exchanger.rates == null ?
                                        'загрузка' :
                                        <Rates rates={this.props.exchanger.rates}/>
                                }
                            </Tab>
                            {/*<Tab eventKey={4} title="Отзывы">*/}
                            {/*<h1>Отзывы</h1>*/}
                            {/*</Tab>*/}
                            {/*<Tab eventKey={5} title="Статистика">*/}
                            {/*<div className="pie-chart-wrap">*/}
                            {/*<h4>График обьема резервов обменного пункта Exchanger за 48 часов</h4>*/}
                            {/*<ReactHighcharts config={ this.pieChartConfig } ref="chart"/>*/}
                            {/*</div>*/}
                            {/*</Tab>*/}


                        </Tabs>
                    </div>
                </div>
            </div>
        );


    }
}

export default connect()(Exchanger);
