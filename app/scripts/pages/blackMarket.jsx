import React, {Component} from "react";
import {Link} from "react-router";
import jQuery from "jquery";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "../actions/blackMarket/blackMarketActions";
import {SimpleSelect} from "react-selectize";
import Dropdown from "react-dropdown";
import {push} from "react-router-redux";
import _ from "underscore";
import moment from "moment";
import ReactPaginate from "react-paginate";
import {numberWithSpaces, addZeroes} from "../reusable/extendingFunctions";
var $ = jQuery;
import didMount from "../reusable/didMount";

class BlackMarket extends Component {

    constructor(props) {

        super(props);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePagination = this.handlePagination.bind(this);
        this.changeOperation = this.changeOperation.bind(this);
        this.countQuoteCurrencyAmount = this.countQuoteCurrencyAmount.bind(this);
        this.changeCity = this.changeCity.bind(this);
        this.searchTimeout = null;
        this.state = {}

    }

    componentWillMount() {
        var query = {
            pageNumber: this.props.location.query.pageNumber || 1,
            sort: this.props.location.query.sort || '',
            'filter[]': this.props.location.query.filter || ['base_currency:USD', 'operation:buy', 'city:Киев'],
            'multifilter[]': this.props.location.query.multifilter || [],
            search: this.props.location.query.search || '',
            sortDirection: this.props.location.query.sortDirection
        }

        this.props.loadBlackMarket(query);
    }

    componentWillUpdate(nextProps, nextState) {


        if (!_.isEqual(this.props.location.query, nextProps.location.query) && this.props.blackMarket.data.length == nextProps.blackMarket.data.length) {

            var query = {
                search: nextProps.location.query.search,
                pageNumber: nextProps.location.query.pageNumber,
                sort: nextProps.location.query.sort,
                'filter[]': nextProps.location.query.filter,
                'multifilter[]': nextProps.location.query.multifilter,
                sortDirection: nextProps.location.query.sortDirection

            }

            this.props.loadBlackMarket(query);

        }

    }


    toggleSettings(e) {

        e.stopPropagation();
        $('.rates-table-filters-wrap').toggleClass('active');

    }

    componentDidUpdate() {


    }


    handleFilter(key) {

        if (typeof this.props.blackMarket.meta['filter[]'] != 'string' && typeof this.props.blackMarket.meta['filter[]'] != 'undefined') {

            var newFilter = this.props.blackMarket.meta['filter[]'];
            newFilter.push(key);

            var action = push(
                {
                    pathname: '/black-market',
                    query: {
                        search: this.props.blackMarket.meta.search,
                        sort: this.props.blackMarket.meta.sort,
                        'filter[]': newFilter,
                        'multifilter[]': this.props.blackMarket.meta['multifilter[]'],
                        sortDirection: this.props.blackMarket.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterBlackMarket(newFilter);
        } else {
            var action = push(
                {
                    pathname: '/black-market',
                    query: {
                        search: this.props.blackMarket.meta.search,
                        'filter[]': [key],
                        'multifilter[]': this.props.blackMarket.meta['multifilter[]'],
                        sort: this.props.blackMarket.meta.sort,
                        sortDirection: this.props.blackMarket.meta.sortDirection

                    }
                }
            );

            this.props.dispatch(action);

            this.props.filterBlackMarket([key]);
        }

    }


    handleBlackMarketFilter(elem) {

        var valuesArr = $(elem).serializeArray();
        var newFilters = [];
        for (var i = 0; i < valuesArr.length; i++) {
            if (valuesArr[i].value != '') {
                newFilters.push('[' + valuesArr[i].name + ']' + '[from]' + ':' + valuesArr[i].value);
            }
        }
        var action = push(
            {
                pathname: '/black-market',
                query: {
                    search: this.props.blackMarket.meta.search,
                    sort: this.props.blackMarket.meta.sort,
                    'filter[]': this.props.blackMarket.meta['filter[]'],
                    'multifilter[]': newFilters,
                    sortDirection: this.props.blackMarket.meta.sortDirection
                }
            }
        );
        this.props.dispatch(action);
        this.props.multifilterBlackMarket(newFilters);

    }

    removeFilter(filter) {

        var currenctFilter = this.props.blackMarket.meta['filter[]'];
        var newFilter = currenctFilter.filter((elem, index) => {
            if (elem == filter) {
                return false;
            }
            return true;
        });

        var action = push(
            {
                pathname: '/black-market',
                query: {
                    search: this.props.blackMarket.meta.search,
                    'filter[]': newFilter,
                    'multifilter[]': this.props.blackMarket.meta['multifilter[]'],
                    sort: this.props.blackMarket.meta.sort,
                    sortDirection: this.props.blackMarket.meta.sortDirection

                }
            }
        );
        this.props.dispatch(action);

        this.props.filterBlackMarket(newFilter);

    }


    contains(array, needle) {
        if (typeof array != 'undefined') {
            for (var i = 0; i < array.length; i++) {
                if (array[i] == needle) {
                    return true;
                }
            }
            return false;
        }
        return false
    }


    searchInputToggle() {

        $('.search-input').toggleClass('active');
        $('.search-input input').focus();

    }

    handlePagination(e) {

        var action = push(
            {
                pathname: '/black-market',
                query: {
                    pageNumber: parseInt(e.selected + 1),
                    search: this.props.blackMarket.meta.search,
                    'filter[]': this.props.blackMarket.meta['filter[]'] || [],
                    'multifilter[]': this.props.blackMarket.meta['multifilter[]'] || [],
                    sort: this.props.blackMarket.meta.sort,
                    sortDirection: this.props.blackMarket.meta.sortDirection
                }
            }
        );
        this.props.dispatch(action);

        this.props.paginateBlackMarket(parseInt(e.selected + 1));
    }


    changeCity(item) {


        var valuesArr = $(this.refs.market_form).serializeArray();
        var newFiltersMulti = [];
        var newFilters = [];
        var city = item.value;
        var currency = this.props.blackMarket.currencySelected;
        var operation = this.props.blackMarket.operation;
        for (var i = 0; i < valuesArr.length; i++) {
            if (valuesArr[i].value != '') {
                newFiltersMulti.push('[' + valuesArr[i].name + ']' + '[from]' + ':' + valuesArr[i].value);
            }
        }

        newFilters.push('city:' + city);
        newFilters.push('base_currency:' + currency);
        newFilters.push('operation:' + operation);

        var action = push(
            {
                pathname: '/black-market',
                query: {
                    pageNumber: this.props.blackMarket.meta.pageNumber,
                    search: this.props.blackMarket.meta.search,
                    'filter[]': newFilters,
                    'multifilter[]': newFiltersMulti,
                    sort: this.props.blackMarket.meta.sort,
                    sortDirection: this.props.blackMarket.meta.sortDirection
                }
            }
        );

        this.props.dispatch(action);
        this.props.changeCity(city, newFilters, newFiltersMulti);


    }

    changeOperation(operation) {

        var valuesArr = $(this.refs.market_form).serializeArray();
        var newFiltersMulti = [];
        var newFilters = [];
        var city = this.props.blackMarket.citySelected;
        var currency = this.props.blackMarket.currencySelected;
        var operation = operation;
        for (var i = 0; i < valuesArr.length; i++) {
            if (valuesArr[i].value != '') {
                newFiltersMulti.push('[' + valuesArr[i].name + ']' + '[from]' + ':' + valuesArr[i].value);
            }
        }
        newFilters.push('city:' + city);
        newFilters.push('base_currency:' + currency);
        newFilters.push('operation:' + operation.value);
        var action = push(
            {
                pathname: '/black-market',
                query: {
                    pageNumber: this.props.blackMarket.meta.pageNumber,
                    search: this.props.blackMarket.meta.search,
                    'filter[]': newFilters,
                    'multifilter[]': newFiltersMulti,
                    sort: this.props.blackMarket.meta.sort,
                    sortDirection: this.props.blackMarket.meta.sortDirection
                }
            }
        );
        this.props.dispatch(action);
        this.props.changeOperation(operation, newFilters, newFiltersMulti);
    }


    changeBaseCurrency(currency) {

        var valuesArr = $(this.refs.market_form).serializeArray();
        var newFiltersMulti = [];
        var newFilters = [];
        var city = this.props.blackMarket.citySelected;
        var currency = currency.value;
        var operation = this.props.blackMarket.operation;
        for (var i = 0; i < valuesArr.length; i++) {
            if (valuesArr[i].value != '') {
                newFiltersMulti.push('[' + valuesArr[i].name + ']' + '[from]' + ':' + valuesArr[i].value);
            }
        }
        newFilters.push('city:' + city);
        newFilters.push('base_currency:' + currency);
        newFilters.push('operation:' + operation);

        var action = push(
            {
                pathname: '/black-market',
                query: {
                    pageNumber: this.props.blackMarket.meta.pageNumber,
                    search: this.props.blackMarket.meta.search,
                    'filter[]': newFilters,
                    'multifilter[]': newFiltersMulti,
                    sort: this.props.blackMarket.meta.sort,
                    sortDirection: this.props.blackMarket.meta.sortDirection
                }
            }
        );

        this.props.dispatch(action);

        this.props.changeBaseCurrency(currency, newFilters, newFiltersMulti);

    }


    componentDidMount() {

        didMount();
        $(window).bind("scroll", function () {
            var tableOffset = $("#ex-table").offset().top;
            var $fixedHeader = $(".fixed-table-wrap");
            var offset = $(this).scrollTop() + 60;
            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });

        var naturalTh = $('#ex-table thead th');
        var fixedTh = $('.header-fixed thead th');

    }

    countQuoteCurrencyAmount(elem) {
        if (this.props.blackMarket.meta.operation == 'buy') {

            return elem.attributes.amount * elem.attributes.value;

        }

        return elem.attributes.amount / elem.attributes.value
    }

    render() {

        let locale = this.props.locale.language;
        let buy = this.props.blackMarket.meta.operation == 'buy';
        let outAmount = buy ? 'quote_currency' : 'base_currency';
        if (this.props.blackMarket.data != null) {
            let offers = this.props.blackMarket.data,
                paginationLinksMapped = [],
                activeFilters,
                operator;

            let offersMapped = offers.map((elem, index) => {
                return (
                    <tr key={index}>
                        <td>
                            {moment(elem.attributes.time).format('HH:mm')}
                        </td>
                        <td>{numberWithSpaces(elem.attributes.amount)} <span className="gray-currency">{buy ? elem.attributes.base_currency :  elem.attributes.quote_currency}</span></td>
                        <td>{numberWithSpaces(this.countQuoteCurrencyAmount(elem).toFixed(1)) + ' ' + elem.attributes[outAmount] } <div className="rates-descr">По курсу {elem.attributes.value.toFixed(2)}</div></td>
                        <td>{elem.attributes.comment}</td>
                        <td>
                            {numberWithSpaces(elem.attributes.phone)}
                        </td>
                    </tr>

                )
            });

            if (typeof this.props.blackMarket.meta['filter[]'] != 'undefined' && this.props.blackMarket.meta['filter[]'] != '') {
                activeFilters = '';
            }
            return (
                <div>
                    <div className="fixed-table-wrap blackmarket-fixed-table">
                        <div className="container">
                            <div className="table-wrapper">
                                <table className="directory-table exchangers-table header-fixed currencies-table">
                                    <thead>
                                    <tr>
                                        <th className={this.props.blackMarket.meta.sort == 'name' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/black-market',
                                                query: {
                                                    pageNumber: this.props.blackMarket.meta.pageNumber,
                                                    sort: 'time',
                                                    'filter[]': this.props.blackMarket.meta['filter[]'],
                                                    sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortBlackMarket('time')
                                            }}>Время</Link>
                                            <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.blackMarket.meta.sort == 'code' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/black-market',
                                                query: {
                                                    pageNumber: this.props.blackMarket.meta.pageNumber,
                                                    sort: 'amount',
                                                    'filter[]': this.props.blackMarket.meta['filter[]'],
                                                    sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {
                                                this.props.sortBlackMarket('amount')
                                            }}>Кол-во</Link>
                                            <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>

                                        </th>
                                        <th className={this.props.blackMarket.meta.sort == 'currency_type' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/black-market',
                                                query: {
                                                    pageNumber: this.props.blackMarket.meta.pageNumber,
                                                    sort: 'currency_type',
                                                    'filter[]': this.props.blackMarket.meta['filter[]'],
                                                    sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'

                                                }
                                            }} onClick={(e) => {

                                            }}>За сумму</Link>
                                            <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.blackMarket.meta.sort == 'category' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/black-market',
                                                query: {
                                                    pageNumber: this.props.blackMarket.meta.pageNumber,
                                                    sort: 'category',
                                                    'filter[]': this.props.blackMarket.meta['filter[]'],
                                                    sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'
                                                }
                                            }} onClick={(e) => {
                                            }}>Комментари</Link>
                                            <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                        <th className={this.props.blackMarket.meta.sort == 'category' ? 'active' : ''}>
                                            <Link to={{
                                                pathname: '/black-market',
                                                query: {
                                                    pageNumber: this.props.blackMarket.meta.pageNumber,
                                                    sort: 'category',
                                                    'filter[]': this.props.blackMarket.meta['filter[]'],
                                                    sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'
                                                }
                                            }} onClick={(e) => {
                                            }}>Контакт</Link>
                                            <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                                 src="/images/filter_logo.svg" alt=""/>
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div className="container market-container black-market-container">
                        <div className="directory-wrap currencies-wrap">
                            <div className="directory-header-wrap">
                                <h2 className="directory-header">Биржа по&ensp;
                                    <span className="underline-span">
                                        <Dropdown onChange={(item) => {
                                            this.changeOperation(item)
                                        }}
                                                  value={this.props.blackMarket.operation}
                                                  options={[{
                                                      value: 'buy',
                                                      label: 'покупке'
                                                  }, {value: 'sell', label: 'продаже'}]}
                                                  placeholder={this.props.blackMarket.operation}/>
                                    </span>
                                    &ensp;наличной валюты в городе
                                    <div className="city-dropdown dropdown-wrap" ref='city_dropdown'
                                         onClick={(e) => {
                                             e.stopPropagation();
                                             $(this.refs.city_dropdown).addClass('open');
                                             this.refs.city_selectize.focus();
                                         }}>
                                        &ensp;<span
                                        className="underline-span">{this.props.blackMarket.citySelected}</span>
                                        <div className="selectize-wrap selectize-dropdown">
                                            <SimpleSelect
                                                ref='city_selectize'
                                                options={this.props.blackMarket.citiesAvaliable}
                                                onValueChange={(item) => {
                                                    this.changeCity(item);
                                                    $(this.refs.city_dropdown).removeClass('open');
                                                }}
                                                open={true}
                                            />
                                        </div>
                                    </div>
                                </h2>
                            </div>
                            <div className="black-market-filters-wrap" onSubmit={(e) => {
                                e.preventDefault();
                                this.handleBlackMarketFilter(e.target)
                            }}>
                                <form action="" ref='market_form'>
                                    <p>Я хочу купить
                                        <input  defaultValue={1} name="amount" className="base-currency-value-input small-input" type="text"/>
                                        <div className="city-dropdown dropdown-wrap" ref="currency_dropdown"
                                        onClick={(e)=>{
                                            e.stopPropagation();
                                            $(this.refs.currency_dropdown).addClass('open');
                                            this.refs.currency_selectize.focus();
                                        }}><span className="underline-span currency-span">&ensp;{this.props.blackMarket.currencySelected}&ensp;</span>
                                            {/*<div className="selectize-wrap selectize-dropdown">*/}
                                                {/*<SimpleSelect*/}
                                                    {/*ref='currency_selectize'*/}
                                                    {/*options={[{value: 'USD', label: 'USD'}, {value: 'UAH', label: 'UAH'}]}*/}
                                                    {/*open={true}*/}
                                                    {/*onValueChange={(item) => {*/}
                                                        {/*this.changeBaseCurrency(item);*/}
                                                        {/*$(this.refs.currency_dropdown).removeClass('open');*/}
                                                    {/*}}*/}
                                                {/*/>*/}
                                            {/*</div>*/}
                                        </div>
                                        по курсу
                                        <input placeholder="Введите значение" name="value" className="quote-currency-value-input small-input rate-input" type="text"/>
                                        <button className="btn btn-green btn-primary" type="submit">Применить</button>
                                    </p>
                                </form>

                            </div>
                            <table id="ex-table" className="directory-table exchangers-table currencies-table black-market-table">
                                <thead>
                                <tr>
                                    <th className={this.props.blackMarket.meta.sort == 'name' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/black-market',
                                            query: {
                                                pageNumber: this.props.blackMarket.meta.pageNumber,
                                                sort: 'time',
                                                'filter[]': this.props.blackMarket.meta['filter[]'],
                                                sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortBlackMarket('time')
                                        }}>Время</Link>
                                        <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.blackMarket.meta.sort == 'code' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/black-market',
                                            query: {
                                                pageNumber: this.props.blackMarket.meta.pageNumber,
                                                sort: 'amount',
                                                'filter[]': this.props.blackMarket.meta['filter[]'],
                                                sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {
                                            this.props.sortBlackMarket('amount')
                                        }}>Кол-во</Link>
                                        <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>

                                    </th>
                                    <th className={this.props.blackMarket.meta.sort == 'currency_type' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/black-market',
                                            query: {
                                                pageNumber: this.props.blackMarket.meta.pageNumber,
                                                sort: 'currency_type',
                                                'filter[]': this.props.blackMarket.meta['filter[]'],
                                                sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'

                                            }
                                        }} onClick={(e) => {

                                        }}>За сумму</Link>
                                        <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.blackMarket.meta.sort == 'category' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/black-market',
                                            query: {
                                                pageNumber: this.props.blackMarket.meta.pageNumber,
                                                sort: 'category',
                                                'filter[]': this.props.blackMarket.meta['filter[]'],
                                                sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'
                                            }
                                        }} onClick={(e) => {
                                        }}>Комментари</Link>
                                        <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                    <th className={this.props.blackMarket.meta.sort == 'category' ? 'active' : ''}>
                                        <Link to={{
                                            pathname: '/black-market',
                                            query: {
                                                pageNumber: this.props.blackMarket.meta.pageNumber,
                                                sort: 'category',
                                                'filter[]': this.props.blackMarket.meta['filter[]'],
                                                sortDirection: this.props.blackMarket.meta.sortDirection == 'up' ? 'down' : 'up'
                                            }
                                        }} onClick={(e) => {
                                        }}>Контакт</Link>
                                        <img className={'filter-logo ' + this.props.blackMarket.meta.sortDirection}
                                             src="/images/filter_logo.svg" alt=""/>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {offersMapped}
                                </tbody>
                            </table>
                        </div>
                        <div className="pagination">
                            <ReactPaginate previousLabel={<span className="icon-arrow-big-left"></span>}
                                           nextLabel={<span className="ru-arrow"></span>}
                                           breakLabel={<a href="">...</a>}
                                           breakClassName={"break-me"}
                                           pageCount={this.props.blackMarket.meta.pages}
                                           marginPagesDisplayed={2}
                                           initialPage={parseInt(this.props.blackMarket.meta.pageNumber) - 1}
                                           pageRangeDisplayed={7}
                                           onPageChange={this.handlePagination}
                                           containerClassName={"pagination"}
                                           subContainerClassName={"pages pagination"}
                                           activeClassName={"active"}/>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div className="container market-container">
                    <div className="directory-wrap loading-directory">
                        <div className="preloader-wrap">
                            <div className="preloader">
                                <div className="circ1"></div>
                                <div className="circ2"></div>
                                <div className="circ3"></div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }


}

function mapDispatchToProps(dispatch) {

    return {...bindActionCreators(ActionCreators, dispatch), dispatch};

}

function mapStateToProps(state) {
    return {

        locale: state.locale,
        favoriteCurrencies: state.favoriteCurrencies,
        blackMarket: state.blackMarket

    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(BlackMarket);


