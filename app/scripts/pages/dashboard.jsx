import React from 'react';
import Card from '../components/Card/Card.jsx';
import axios from 'axios';
import windowSizeMapper from '../reusable/windowSizeMapper';
import didMount from '../reusable/didMount';
import WelcomeGuide from '../components/WelcomeGuide/welcomeGuide.jsx';
import DashboardSettings from '../components/DashboardSettings/dashboardSettings.jsx';
var axio = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  }
});
if (typeof window != 'undefined') {
  var Packery = require('packery');
  var Draggabilly = require('draggabilly');
}
var $ = jQuery;


class Dashboard extends React.Component {

  constructor(props) {
    super(props);

    this.widgetCount = 0;
    this.widgets = props.widgetsSet;

    this.state = {

      widgets: props.widgetsSet,
      loading: false,
      className: "layout"

    };

    this.openWidgetMenu = this.openWidgetMenu.bind(this);
  }

  resetLayout() {
    this.setState({ layouts: {} });
  }

  componentWillUnmount() {
    pckry.destroy();
  }

  componentWillMount() {
    if (this.props.currencyPairs.data == null) {
      this.props.getCurrencyPairs();
    }
    this.props.loadDash();
    this.props.getWidgetList();
    this.props.loadDashExchangers();
    this.props.getCurrenciesList();
    // this.props.getFavoriteExchangers();
    // this.props.getFavoriteDirections();

  }

  componentDidUpdate() {
    var cards = $('.card');
    var pckryElem = pckry.getItemElements();
    if (pckryElem.length == cards.length) {
      return
    }
    if (pckryElem.length < cards.length) {
      pckry.appended(cards[cards.length - 1]);
      var draggie = new Draggabilly(cards[cards.length - 1], {
        handle: '.card-header'
      });
      pckry.bindDraggabillyEvents(draggie);
      pckry.shiftLayout();
    }
    if (pckryElem.length > cards.length) {
      pckry.shiftLayout();
    }
  }


  componentDidMount() {

    didMount();
    Packery.prototype.getShiftPositions = function(attrName) {
      attrName = attrName || 'id';
      var _this = this;
      return this.items.map(function(item) {
        return {
          attr: item.element.getAttribute(attrName),
          x: item.rect.x / _this.packer.width
        }
      });
    };
    var scrollDiv = document.createElement("div");
    scrollDiv.className = "scrollbar-measure";
    document.body.appendChild(scrollDiv);

    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

    document.body.removeChild(scrollDiv);

    Packery.prototype.initShiftLayout = function(positions, attr) {
      if (!positions) {
        // if no initial positions, run packery layout
        this.layout();
        return;
      }
      // parse string to JSON
      if (typeof positions == 'string') {
        var windowWidth;
        if (scrollbarWidth > 0) {
          windowWidth = $(window).width() - scrollbarWidth;
        } else {
          windowWidth = $(window).width();
        }
        if (windowWidth != localStorage.getItem('windowWidth')) {
          this.layout();
          return
        }

        try {
          positions = JSON.parse(positions);
        } catch (error) {
          console.log('JSON parse error: ' + error);
          this.layout();
          return;
        }
      }
      attr = attr || 'id'; // default to id attribute
      this._resetLayout();
      // set item order and horizontal position from saved positions

      this.items = positions.map(function(itemPosition) {
        var selector = '[' + attr + '="' + itemPosition.attr + '"]';
        var itemElem = this.element.querySelector(selector);
        var item = this.getItem(itemElem);
        item.rect.x = itemPosition.x * this.packer.width;
        return item;
      }, this);
      this.shiftLayout();
    };

    if (this.widgetCount == 0) {
      this.widgetCount = $('.card').length;
    }
    this.setState({
      loading: false
    });

    var elem = document.querySelector('.dashboard-container');
    window.pckry = new Packery(elem, {
      itemSelector: '.card',
      gutter: 20,
      columnWidth: 280,
      initLayout: false
    });
    let windowWidth = $(window).width();
    var widgetDefaultSetPrefix = windowSizeMapper(windowWidth);
    // var initPositions = localStorage.getItem('dragPositions') || config[`default_dragPositions_${widgetDefaultSetPrefix}`]
    var initPositions = localStorage.getItem('dragPositions');


    pckry.initShiftLayout(initPositions, 'data-item-id');


    pckry.getItemElements().forEach(function(itemElem) {
      var draggie = new Draggabilly(itemElem, {
        handle: '.card-header'
      });
      pckry.bindDraggabillyEvents(draggie);
    });

    pckry.on('dragItemPositioned', function() {
      // save drag positions
      var positions = pckry.getShiftPositions('data-item-id');
      localStorage.setItem('windowWidth', JSON.stringify(windowWidth));
      localStorage.setItem('dragPositions', JSON.stringify(positions));
    });


    //welcome guide logic

    var firstTimeOnRatex = localStorage.getItem('visited');
    if (firstTimeOnRatex == null) {
      localStorage.setItem('visited', 'true');
      setTimeout(() => {
        $('.welcome-guide-wrap').addClass('visible');
      }, 1000);
    }


  }


  openWidgetMenu() {

    if (!this.props.addWidgetMenu.open) {
      this.props.openWidgetMenu();
      return
    }
    this.props.hideWidgetMenu();
  }


  onStatusChange(state) {
    this.setState(state);
  }

  render() {

    const cards = this.props.widgetsSet.map((item, index) => {
      return (
        <Card className="card-wrap" title={this.props.widgetsSet[index].id}
              node={this.props.widgetsSet[index].id} {...this.props}
              size={this.props.widgetsSet[index].id} type={this.props.widgetsSet[index].type} text='text'
              dataId={this.props.widgetsSet[index].id} key={this.props.widgetsSet[index].id}
              packeryProps={index}/>
      )
    });


    return (
      <div>
        <WelcomeGuide/>
        <DashboardSettings/>
        <div className="container">
          <div className="dashboard-container">
            {cards}
          </div>
        </div>
        <div className="btn-add-widget" onClick={this.openWidgetMenu}>+
        </div>
      </div>

    )
  }
}


export default Dashboard;