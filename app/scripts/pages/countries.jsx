import React, {Component} from "react";
import {browserHistory} from "react-router";
import jQuery from "jquery";
import didMount from "../reusable/didMount";
import Row from "./currenciesTable/row.jsx";
import {createFilter} from "react-search-input";

var $ = jQuery;


export default class Countries extends Component {
    constructor(props) {
        super(props);
        this.searchUpdated = this.searchUpdated.bind(this);
        this.sort = this.sort.bind(this);
        this.activeToggle = this.activeToggle.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.dataForTable = null;
        this.reserves = null;
        this.keyToFilter = ['name'];
        this.searchTimeout = null;
        this.wmbl = null;
        this.name = null;
        this.state = {
            currenciesLoaded: null,
            currentPage: 1,
            exchangers: null,
            searchTerm: '',
            showActive: false,
            typesOfCurrencies: []
        }
    }

    searchUpdated(term, value) {

        browserHistory.push({
            query: {
                search: term
            }
        });
        clearTimeout(this.searchTimeout);
        this.searchTimeout = setTimeout(() => {
            let filteredData = this.dataForTable.filter(createFilter(term, this.keyToFilter));
            this.setState({
                exchangersLoaded: filteredData
            });
        }, 70);


    }

    contains(array, needle) {

        for (var i = 0; i < array.length; i++) {
            if (array[i] == needle) {
                return true;
            }
        }
        return false;

    }

    removeFilter(param) {

        this.state[param] = false;

        this.forceUpdate();

    }

    componentWillMount() {

        if (this.props.currencies.data == null) {
            this.props.getCountries();
        }

    }

    toggleSettings(e) {

        e.stopPropagation();
        $('.settings-wrap').toggleClass('tooltip-visible');

    }

    componentWillUpdate(nextProps, nextState) {

        if (nextState.exchangersLoaded != null && this.state.exchangersLoaded == null) {



        }

    }

    componentDidUpdate() {

        if (this.state.currenciesLoaded == null && this.props.currencies.data != null) {
            let typesOfCurrencises = [];
            this.dataForTable = this.props.currencies.data.data.map((elem, index) => {

                if(!this.contains(typesOfCurrencises, elem.type)){
                    typesOfCurrencises.push(elem.type);
                }

                return ({
                    id: elem.id || '-',
                    type: elem.type || '-',
                    issuer_name: elem.attributes_issuer_name || '-'
                });
            });


            this.setState({
                typesOfCurrencies: typesOfCurrencises,
                currenciesLoaded: this.dataForTable,

            });
            $('.preloader-wrap').fadeOut();

            //removing preloader...

        }

    }


    sort(param) {
        let sorted = this.state.exchangersLoaded;
        let sortedRaw = this.dataForTable;

        if (this[param] == null || this[param] == 'up') {
            this[param] = 'down';
            sorted.sort((a, b) => {

                if (a[param] > b[param]) {
                    return 1;
                }
                if (a[param] < b[param]) {
                    return -1;
                }
                return 0;

            });
            sortedRaw.sort((a, b) => {

                if (a[param] > b[param]) {
                    return 1;
                }
                if (a[param] < b[param]) {
                    return -1;
                }
                return 0;

            });
            this.dataForTable = sortedRaw;
            this.setState({
                exchangersLoaded: sorted
            });
            return
        }
        if (this[param] == 'down') {
            this[param] = 'up';
            sorted.sort((a, b) => {

                if (a[param] > b[param]) {
                    return -1;
                }
                if (a[param] < b[param]) {
                    return 1;
                }
                return 0;

            });
            sortedRaw.sort((a, b) => {

                if (a[param] > b[param]) {
                    return -1;
                }
                if (a[param] < b[param]) {
                    return 1;
                }
                return 0;


            });
            this.dataForTable = sortedRaw;
            this.setState({
                exchangersLoaded: sorted

            });
            return
        }


    }

    activeToggle() {

        let exchangersData = this.dataForTable;

        this.setState({
            showActive: !this.state.showActive
        });


    }

    searchInputToggle() {

        $('.search-input').toggleClass('active');
        $('.search-input input').focus();

    }


    componentDidMount() {

        $(window).bind("scroll", function () {
            var tableOffset = $("#ex-table").offset().top;
            var $fixedHeader = $(".header-fixed");
            var offset = $(this).scrollTop() + 60;

            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });

        var naturalTh = $('#ex-table thead th');
        var fixedTh = $('.header-fixed thead th');

        didMount(this);

    }

    render() {

        if (this.state.currenciesLoaded != null) {
            var dataList = this.dataForTable;
            let currencies = this.state.currenciesLoaded;
            // let filterActive = this.state.exchangersLoaded.length != this.dataForTable.length;
            // let dataLength = this.state.exchangersLoaded.length;
            let currenciesMapped = currencies.map((elem, index) => {
                let id = elem.id,
                    name = elem.name || "–",
                    code = elem.id || '-',
                    issuerName = elem.issuer_name || "-",
                    type = elem.type || "-",
                    category = elem.category || "-",
                    parentCurrency = elem.parent_currency || "-";

                return (
                    <Row key={index} index={index} name={ name } code={code} issuerName={ issuerName }
                         type={ type } category={ category } parentCurrency={ parentCurrency } id={ id }
                    />
                );
            });
            let typesMapped = this.state.typesOfCurrencies.map((elem, index)=>{

                return(
                    <div className="select-wrap">
                        <input id={'type'+ index} type="checkbox"/>
                        <label id={'type'+ index}
                               className="toggle-label">{elem}</label>
                    </div>
                )

            });

            return (
                <div className="container market-container">
                    <div className="directory-wrap">
                        <div className="directory-header-wrap">
                            <h2 className="directory-header">Список валют
                                {/*<span className="total-items">{ dataList.length }</span> {filterActive ? 'найдено ' + dataLength : ''}*/}
                            </h2>
                            <div className="settings-wrap">
                                <span className="icon ru-settings" onClick={this.toggleSettings}></span>
                                <div className="tooltip" onClick={(e) => {
                                    e.stopPropagation()
                                }}>
                                    <div className="tooltip-arrow"></div>
                                    <div className="tooltip-inner">
                                        <div className="tooltip-title">Управление валютами</div>
                                        { typesMapped }
                                    </div>
                                </div>
                            </div>
                            <div className="filters-array">
                                <div className="filter-item"
                                     style={{display: this.state.showActive ? 'inline-block' : 'none'}}>
                                    активные
                                    <div className="delete-filter" onClick={() => {
                                        this.removeFilter('showActive')
                                    }}>&times;</div>
                                </div>
                            </div>
                            <div className="search-input" onClick={this.searchInputToggle}><input
                                className="search-input" onBlur={() => {
                                $('.search-input').removeClass('active')
                            }} onChange={(e) => {
                                this.searchUpdated(e.target.value)
                            }}/></div>
                        </div>
                        <table id="ex-table" className="directory-table exchangers-table currencies-table">
                            <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th onClick={() => {
                                    this.sort('name')
                                }}>
                                    Code
                                </th>
                                <th onClick={() => {
                                    this.sort('reserves')
                                }}>
                                    Issuer name
                                </th>
                                <th>
                                    Type
                                </th>
                                <th onClick={() => {
                                    this.sort('wmbl')
                                }}>
                                    Category
                                </th>
                                <th>
                                    Parent currency
                                </th>
                                <th>

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            { currenciesMapped }
                            </tbody>
                        </table>
                    </div>
                </div>

            );

        } else {
            return (
                <div className="container market-container">
                    <div className="directory-wrap">
                        <h2 className="directory-header">Список обменников</h2>
                        <table className="directory-table exchangers-table header-fixed">
                            <thead>
                            <tr>
                                <th>
                                    Статус
                                </th>
                                <th onClick={() => {
                                    this.sort('name')
                                }}>
                                    Обменник
                                </th>
                                <th onClick={() => {
                                    this.sort('reserves')
                                }}>
                                    Резервы
                                </th>
                                <th>
                                    Курсы
                                </th>
                                <th onClick={() => {
                                    this.sort('wmbl')
                                }}>
                                    BL
                                </th>
                                <th>
                                    Отзывы
                                </th>
                                <th>
                                    Рейтинг
                                </th>
                                <th>

                                </th>
                            </tr>
                            </thead>
                        </table>
                        <table id="ex-table" className="directory-table exchangers-table">
                            <thead>
                            <tr>
                                <th>
                                    Статус
                                </th>
                                <th onClick={() => {
                                    this.sort('name')
                                }}>
                                    Обменник
                                </th>
                                <th onClick={() => {
                                    this.sort('reserves')
                                }}>
                                    Резервы
                                </th>
                                <th>
                                    Курсы
                                </th>
                                <th onClick={() => {
                                    this.sort('wmbl')
                                }}>
                                    BL
                                </th>
                                <th>
                                    Отзывы
                                </th>
                                <th>
                                    Рейтинг
                                </th>
                                <th>

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        }

    }

}


