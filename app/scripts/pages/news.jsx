import React from "react";
import ReactDisqusThread from 'react-disqus-thread';

export default class NewsArchive extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {


    }

    componentWillMount() {




    }



    render() {

        return (
            <div className="container news-archive">
                <div className="directory-wrap archive-news-wrap">
                            <div className="news-categories">
                                <a href="#" className="active">Все</a>
                                <a href="#">Экономика и инвестиции</a>
                                <a href="#">Финансы и валюта</a>
                                <a href="#">Банки и страхование</a>
                                <a href="#">Собственность и товары</a>
                            </div>
                        <div className="news-list">
                                <div className="news-header">
                                    <h1>Новости</h1>
                                    <div className="calendar"></div>
                                </div>
                                <div className="news-table">
                                    <table className="table">
                                        <thead>
                                        <tr>
                                            <th>Дата</th>
                                            <th>Название</th>
                                            <th>Категория</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        <tr>
                                            <td>10:32</td>
                                            <td>
                                                <a href="#">Аграрии нашли еще один рынок сбыта для украинской
                                                    пшеницы</a>
                                                <span className="source">korrespondent.net</span>
                                            </td>
                                            <td>Экономика и инвестиции</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                        </div>
                </div>
                {/*<ReactDisqusThread*/}
                    {/*shortname="example"*/}
                    {/*identifier="something-unique-12345"*/}
                    {/*title="Example Thread"*/}
                    {/*url="http://localhost:9000/news"*/}
                    {/*category_id="123456"/>*/}
            </div>
        )

    }


}