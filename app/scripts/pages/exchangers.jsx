import React, { Component } from 'react';
import jQuery from 'jquery';
import didMount from '../reusable/didMount';
import Row from './exchangersTable/row.jsx';
import { createFilter } from 'react-search-input';

var $ = jQuery;


export default class Exchangers extends Component {
  constructor(props) {
    super(props);
    this.searchUpdated = this.searchUpdated.bind(this);
    this.sort = this.sort.bind(this);
    this.activeToggle = this.activeToggle.bind(this);
    this.removeFilter = this.removeFilter.bind(this);
    this.dataForTable = null;
    this.reserves = null;
    this.keyToFilter = ['name'];
    this.searchTimeout = null;
    this.wmbl = null;
    this.name = null;
    this.state = {
      exchangersLoaded: null,
      currentPage: 1,
      exchangers: null,
      searchTerm: '',
      showActive: false
    }
  }

  searchUpdated(term, value) {

    // browserHistory.push({
    //     query: {
    //         search: term
    //     }
    // });
    clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => {
      let filteredData = this.dataForTable.filter(createFilter(term, this.keyToFilter));
      this.setState({
        exchangersLoaded: filteredData
      });
    }, 70);


  }

  removeFilter(param) {

    this.state[param] = false;

    this.forceUpdate();

  }

  componentWillMount() {

    if (this.props.exchangersLoad.data == null) {
      this.props.loadExchangers();
    }
    if (this.state.exchangersLoaded == null && this.props.exchangersLoad.data != null) {
      this.dataForTable = this.props.exchangersLoad.data.data.map((elem, index) => {
        return ({
          name: elem.attributes.name,
          reserves: elem.attributes.reserve_in_usd,
          rates: elem.attributes.ratesCount,
          bl: elem.attributes.wmbl,
          status: elem.attributes.status,
          id: elem.id,
          isActive: elem.attributes.is_active,
          rating: elem.attributes.rating

        });
      });
      // if (typeof this.props.location.query.search != 'undefined') {
      //     let filteredData = this.dataForTable.filter(createFilter(this.props.location.query.search, this.keyToFilter));
      //
      //     this.setState({
      //
      //         exchangersLoaded: filteredData,
      //
      //     });
      //
      //     return
      //
      // }
      this.setState({

        exchangersLoaded: this.dataForTable,

      });


    }

  }

  toggleSettings(e) {

    e.stopPropagation();
    $('.settings-wrap').toggleClass('tooltip-visible');

  }

  componentWillUpdate(nextProps, nextState) {

    if (nextState.exchangersLoaded != null && this.state.exchangersLoaded == null) {

    }

  }

  componentDidUpdate() {
    if (this.state.exchangersLoaded == null && this.props.exchangersLoad.data != null) {
      this.dataForTable = this.props.exchangersLoad.data.data.map((elem, index) => {
        return ({
          name: elem.attributes.name,
          reserves: elem.attributes.reserve_in_usd,
          rates: elem.attributes.ratesCount,
          bl: elem.attributes.wmbl,
          status: elem.attributes.is_active,
          id: elem.id,
          isActive: elem.attributes.is_active,
          rating: elem.attributes.rating

        });
      });
      if (typeof this.props.location.query.search != 'undefined') {
        let filteredData = this.dataForTable.filter(createFilter(this.props.location.query.search, this.keyToFilter));

        this.setState({

          exchangersLoaded: filteredData,

        });

        return

      }
      this.setState({

        exchangersLoaded: this.dataForTable,

      }, ()=> {this.sort('reserves');this.sort('reserves');});



      // $('.preloader-wrap').fadeOut();

      //removing preloader...

    }
    // if (this.state.exchangersLoaded != null) {
    //   var naturalTh = $('#ex-table thead th');
    //   var fixedTh = $('.header-fixed thead th');
    //
    //   setTimeout(() => {
    //     $.each(fixedTh, (index, elem) => {
    //
    //       if (index == 1) {
    //         $(elem).css({
    //           width: '391px',
    //           'padding-left': '7%'
    //         })
    //         return
    //       } else if (index == 2) {
    //           $(elem).css({
    //               width: $(naturalTh[index]).width() + 10,
    //               'text-align' : 'left'
    //           })
    //       }
    //       $(elem).css({
    //         width: $(naturalTh[index]).width() + 10
    //       })
    //     });
    //   }, 50);
    //
    // }


  }


  sort(param) {
    let sorted = this.state.exchangersLoaded;
    let sortedRaw = this.dataForTable;

    if (this[param] == null || this[param] == 'up') {
      this[param] = 'down';
      sorted.sort((a, b) => {

        if (a[param] > b[param]) {
          return 1;
        }
        if (a[param] < b[param]) {
          return -1;
        }
        return 0;


      });
      sortedRaw.sort((a, b) => {

        if (a[param] > b[param]) {
          return 1;
        }
        if (a[param] < b[param]) {
          return -1;
        }
        return 0;


      });
      this.dataForTable = sortedRaw;
      this.setState({
        exchangersLoaded: sorted
      });
      return
    }
    if (this[param] == 'down') {
      this[param] = 'up';
      sorted.sort((a, b) => {

        if (a[param] > b[param]) {
          return -1;
        }
        if (a[param] < b[param]) {
          return 1;
        }
        return 0;


      });
      sortedRaw.sort((a, b) => {

        if (a[param] > b[param]) {
          return -1;
        }
        if (a[param] < b[param]) {
          return 1;
        }
        return 0;


      });
      this.dataForTable = sortedRaw;
      this.setState({
        exchangersLoaded: sorted

      });
      return
    }


  }

  activeToggle() {

    let exchangersData = this.dataForTable;

    this.setState({
      showActive: !this.state.showActive
    });


  }

  searchInputToggle() {

    $('.search-input').toggleClass('active');
    $('.search-input input').focus();

  }


  componentDidMount() {
    $(window).bind("scroll", function() {
      var tableOffset = $("#ex-table").offset().top;
      var $fixedHeader = $(".header-fixed");
      var $fixedTableWrap = $('.fixed-table-wrap');
      var offset = $(this).scrollTop() + 60;

      if (offset >= tableOffset) {
        $fixedHeader.show();
        $fixedTableWrap.show();
      }
      else if (offset < tableOffset) {
        $fixedHeader.hide();

        $fixedTableWrap.hide();
      }
    });
    var naturalTh = $('#ex-table thead th');
    var fixedTh = $('.header-fixed thead th');

    didMount();
  }

  render() {

    if (this.state.exchangersLoaded != null) {


      var dataList = this.dataForTable;
      let exchangers = this.state.exchangersLoaded;
      let filterActive = this.state.exchangersLoaded.length != this.dataForTable.length;
      let dataLength = this.state.exchangersLoaded.length;
      let exchangersMapped = exchangers.map((elem, index) => {
        if (this.state.showActive && !elem.isActive) {
          return (<span></span>)
        }
        let name = elem.name,
          reserves = elem.reserves,
          rates = elem.rates,
          bl = elem.bl,
          status = elem.status,
          id = elem.id,
          isActive = elem.isActive,
          rating = elem.rating;

        return (
          <Row key={index}
               index={index}
               name={ name }
               isActive={isActive}
               reserves={ reserves }
               rates={ rates }
               bl={ bl }
               status={ status }
               rating={ rating }
               id={ id }
               favoriteExchangers={ this.props.favoriteExchangers }
          />
        );
      });

      return (
        <div>
          <div className="fixed-table-wrap">
            <div className="container">
              <div className="table-wrapp" style={{display:'block!important', paddingLeft: '30px', paddingRight: '30px', background: '#fff'}}>
                <table className="directory-table exchangers-table header-fixed currencies-table exchangers-fixed-width">
                  <thead>
                  <tr>
                    <th>
                      Статус
                    </th>
                    <th onClick={() => {
                      this.sort('name')
                    }}>
                      Обменник
                    </th>
                    <th onClick={() => {
                      this.sort('reserves')
                    }}>
                      Резервы
                    </th>
                    <th onClick={() => {
                      this.sort('wmbl')
                    }}>
                      BL
                    </th>
                    <th>
                      Рейтинг
                    </th>
                    <th>

                    </th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div>
          <div className="container market-container">
            <div className="directory-wrap">
              <div className="directory-header-wrap">

                <h2 className="directory-header">Список обменников <span
                  className="total-items">{ dataList.length }</span> {filterActive ? 'найдено ' + dataLength : ''}
                </h2>

                <div className="settings-wrap">
                  <span className="icon ru-settings" onClick={this.toggleSettings}></span>
                  <div className="tooltip" onClick={(e) => {
                    e.stopPropagation()
                  }}>
                    <div className="tooltip-arrow"></div>
                    <div className="tooltip-inner">
                      <div className="tooltip-title">Управление обменниками</div>
                      <input onChange={this.activeToggle} id="disable-non-active" type="checkbox"/>
                      <label checked={this.state.showActive} htmlFor="disable-non-active"
                             className="toggle-label">Исключить неактивные</label>
                    </div>
                  </div>
                </div>
                <div className="filters-array">
                  <div className="filter-item"
                       style={{ display: this.state.showActive ? 'inline-block' : 'none' }}>
                    активные
                    <div className="delete-filter" onClick={() => {
                      this.removeFilter('showActive')
                    }}>&times;</div>
                  </div>
                </div>
                <div className="search-input" onClick={this.searchInputToggle}><input
                  className="search-input" onBlur={() => {
                  $('.search-input').removeClass('active')
                }} onChange={(e) => {
                  this.searchUpdated(e.target.value)
                }}/></div>

              </div>

              <table id="ex-table" className="directory-table exchangers-table">
                <thead>
                <tr>
                  <th>
                    Статус
                  </th>
                  <th onClick={() => {
                    this.sort('name')
                  }}>
                    Обменник
                  </th>
                  <th onClick={() => {
                    this.sort('reserves')
                  }}>
                    Резервы
                  </th>
                  <th onClick={() => {
                    this.sort('wmbl')
                  }}>
                    BL
                  </th>
                  <th>
                    Рейтинг
                  </th>
                  <th>

                  </th>
                </tr>
                </thead>
                <tbody>
                { exchangersMapped }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      );

    } else {
      return (
        <div className="container market-container">
            <div className="directory-wrap">
                <div className="directory-wrap loading-directory">
                    <div className="preloader-wrap">
                        <div className="preloader">
                            <div className="circ1"></div>
                            <div className="circ2"></div>
                            <div className="circ3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      )
    }

  }

}


