import React from 'react';
import Select from 'react-select';
import jQuery from 'jquery';
import didMount from '../reusable/didMount'
import DateTime from 'react-datetime';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import WelcomeGuide from '../components/WelcomeGuide/welcomeGuide.jsx';
var $ = jQuery;
import { ImgOption, ImgValue } from '../components/SelectComponents/selectImg.jsx';
import Selectize from '../components/SelectComponents/selectize';
 class Guilist extends React.Component {
    constructor(props){
        super(props);
        this.changeValueOfSelectize = this.changeValueOfSelectize.bind(this); //binding context for inside-function usage of React.Class functions
    }

    changeValueOfSelectize(val){
        this.setState({
            select: val
        })
    }

    conponentWillMount() {
        this.props.getCurrencyPairs();
    }

    componentDidMount(){
        $('.preloader-wrap').fadeOut();
        didMount(this);

    }

    render(){
        return(
            <div className="container">
                <div className="another-div">
                    <DateTime
                        open={false}
                        viewMode = {'years'}
                        onChange = {function(arg){
                        }}
                    />
                </div>
                <div className="bootstrap-picker">
                    {/*<DateRangePicker alwaysShowCalendars={true} >*/}
                        {/*<h1>DATEPICKER TRIGGER</h1>*/}
                    {/*</DateRangePicker>*/}
                </div>
                <div className="slider">
                    <WelcomeGuide/>
                </div>
            </div>
        )
    }
}

export default Guilist;