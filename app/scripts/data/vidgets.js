
const widgetsSet =[
    {
        id:8,
        type:"fast_exchange" ,
    },
    {
        id:7,
        type:"ratex_stats" ,
    },
    {
        id:12,
        type:"top_exchangers",
    },

    // {
    //     id:15,
    //     position: 10,
    //     type:"exchanger_card" ,
    //     title:"Официальный курс валют",
    //     size:"h_small_v_small"
    // },
    {
        id:10,
        type:"news" ,
    },
    {
        id:16,
        type:"top_currency_pairs" ,
    },

    {
        id:0,
        type:"national_bank_rates" ,
    },

    {
        id:14,
        type:"calculator_pro",
    },
    {
        id:13,
        type:"national_bank_fluctuations",
    },
    {
        id:19,
        type:"black_market_rates",
    },
    {
        id: 18,
        type: 'card_rates'
    },
    {
        id: 11,
        type: 'average_visa_mastercard_rates'
    },
    {
        id: 24,
        type: 'privat_bank_rates'
    },
    {
        id: 17,
        type: 'privat_card_rates'
    },
    {
        id: 25,
        type: 'visa_rates'
    },
    {
        id: 27,
        type: 'mastercard_rates'
    }


];



export default widgetsSet;


