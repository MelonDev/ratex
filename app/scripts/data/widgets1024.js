
const widgetsSet1024 =[
    {
        id:1,
        position: 10,
        type:"fast_exchange" ,
        title:"Официальный курс валют",
        size:"h_small_v_small"
    },
    {
        id:2,
        position: 10,
        type:"ratex_stats" ,
        title:"Официальный курс валют",
        size:"h_small_v_small"
    },
    {
        id:10,
        position:6,
        type:"exchangers",
        title:"Топ-10 обменников",
        size:"h_small_v_small"
    },

    //{
    //    id:14,
    //    position: 10,
    //    type:"TopExchangers" ,
    //    title:"Официальный курс валют",
    //    size:"h_small_v_small"
    //},
    //{
    //    id:15,
    //    position: 10,
    //    type:"Informer" ,
    //    title:"Официальный курс валют",
    //    size:"h_small_v_small"
    //},
    // {
    //     id:16,
    //     position: 10,
    //     type:"exchanger_card" ,
    //     title:"Официальный курс валют",
    //     size:"h_small_v_small"
    // },
    {
        id:17,
        position: 10,
        type:"news" ,
        title:"Официальный курс валют",
        size:"h_small_v_small"
    },
    //
    {
        id:18,
        position: 10,
        type:"exchange_directions" ,
        title:"Официальный курс валют",
        size:"h_small_v_small"
    },

    {
        id:19,
        position: 10,
        type:"national_bank_rates" ,
        title:"Официальный курс валют",
        size:"h_small_v_small"
    },
    {
        id:20,
        position: 10,
        type:"national_bank_fluctuations",
        title:"Официальный курс валют",
        size:"h_small_v_small"
    },
    {
       id:21,
       position: 10,
       type:"favorites_exchangers",
       title:"Официальный курс валют",
       size:"h_small_v_small"
    },

    //{
    //    id:22,
    //    position: 10,
    //    type:"favorites_exchange_directions",
    //    title:"Официальный курс валют",
    //    size:"h_small_v_small"
    //}
    {
        id:23,
        type:"black_market_overview",
    },

];



export default widgetsSet1024;


