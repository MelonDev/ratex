var express = require('express');
var React = require('react');
var path = require('path');
import {match, RouterContext, createMemoryHistory} from "react-router";
import {renderToString} from "react-dom/server";
import {createStore, applyMiddleware} from "redux";
import { syncHistoryWithStore, push } from 'react-router-redux';
import thunkMiddleware from "redux-thunk";
import {Provider} from "react-redux";
import routes from "./scripts/routes";
import rootReducer from "./scripts/reducers/index";
import widgetsSet from './scripts/data/vidgets';
import axios from 'axios';
const app = express();

app.use("/css", express.static(__dirname + 'Users/alexdankov/Desktop/paycore_team/prod_test/dist/css'));
app.use("/js", express.static(__dirname + 'Users/alexdankov/Desktop/paycore_team/prod_test/dist/js'));
app.use("/fonts", express.static(__dirname + 'Users/alexdankov/Desktop/paycore_team/prod_test/dist/fonts'));
app.use("/images", express.static(__dirname + 'Users/alexdankov/Desktop/paycore_team/prod_test/dist/images'));


app.get('*', (req, res, next) => {

    match({routes: routes, location: req.url}, (err, redirectLocation, renderProps) => {
        if (err) return next(err)

        if (redirectLocation) {
            return res.redirect(302, redirectLocation.pathname + redirectLocation.search)
        }

        if (!renderProps) {
            return next(new Error('Missing render props'))
        }

        const components = renderProps.components;

        // If the component being shown is our 404 component, then set appropriate status
        if (components.some((c) => c && c.displayName === 'error-404')) {
            res.status(404)
        }



        const Comp = components[components.length - 1].WrappedComponent
        const fetchData = (Comp && Comp.fetchData) || (() => Promise.resolve())

        const widgets = {widgetsSet: widgetsSet};


        axios.get('http://localhost:9000/exchangers.json')
            .then((data)=>{
                var exchangersLoad = {exchangersLoad: data.data};
                var persistedState = Object.assign({}, exchangersLoad, widgets);
                const store = createStore(rootReducer,persistedState,applyMiddleware(thunkMiddleware));
                const memHistory = syncHistoryWithStore(createMemoryHistory(req.originalUrl), store);
                const {location, params,} = renderProps;
                var persist =JSON.stringify(store.getState());


                fetchData({store, location, params, memHistory})
                    .then(() => {
                        const body = renderToString(
                            <Provider store={store}>
                                <RouterContext  history={memHistory} {...renderProps}/>
                            </Provider>
                        )

                        const state = store.getState()

                        res.send(`<!DOCTYPE html>
                              <html>
                                <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <meta name=”fragment” content=”!” />
                                <style>
                                .preloader-wrap {
                                    position: fixed;
                                    left: 0;
                                    right: 0;
                                    bottom: 0;
                                    top: 0;
                                    background: #fff;
                                    z-index: 112221;
                                }
                        
                                .preloader {
                                    margin: 0;
                                    text-align: center;
                                    font-size: 10px;
                                    position: absolute;
                                    left: 50%;
                                    right: 0;
                                    bottom: 0;
                                    top: 50%;
                                    transform: translate(-50%, -50%);
                                    width: 67px;
                                    height: 67px;
                                    z-index: 1111;
                                }
                        
                                .preloader > div {
                                    background-color: #314156;
                                    height: 6px;
                                    width: 10px;
                                    margin-bottom: 24px;
                                    display: block;
                                    -webkit-animation: stretchdelay 0.7s infinite linear;
                                    animation: stretchdelay 0.7s infinite linear;
                                }
                        
                                .preloader .circ2 {
                                    -webkit-animation-delay: -0.6s;
                                    animation-delay: -0.6s;
                                }
                        
                                .preloader .circ3 {
                                    -webkit-animation-delay: -0.5s;
                                    animation-delay: -0.5s;
                                }
                        
                                .preloader .circ4 {
                                    -webkit-animation-delay: -0.4s;
                                    animation-delay: -0.4s;
                                }
                        
                                .preloader .circ5 {
                                    -webkit-animation-delay: -0.3s;
                                    animation-delay: -0.3s;
                                }
                        
                                @-webkit-keyframes stretchdelay {
                                    0%, 40%, 100% {
                                        width: 10px;
                                    }
                                    20% {
                                        width: 100%;
                                    }
                                }
                        
                                @keyframes stretchdelay {
                                    0%, 100% {
                                        width: 20px;
                                    }
                                    40% {
                                        width: 50px;
                                    }
                                    20% {
                                        width: 100%;
                                    }
                                }
                            </style>
                                                        <title>Ratex.io</title>
                                <link rel="stylesheet" href="/css/main.css">
                            
                                
                                    <meta name="theme-color" content="#ffffff">
                                
                                
                                    <meta name="msapplication-TileColor" content="#d10d3f">
                                    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
                                    <meta name="msapplication-config" content="browserconfig.xml">
                                </head>
                                <body>
                            </div>
                             <div id="app">${body}</div>
                                  <div id="root"></div>
                                  <script>window.__INITIAL_STATE__ = ${persist};</script>
                                  <script src="/js/dll.vendor.js"></script>
                                   <script src="/js/main.js"></script>
                                </body>
                              </html>`)
                    })
                    .catch((err) => next(err))
            })
            .catch((err)=>{
                console.log(err);
            })


    })
})


const server = app.listen(3000, () => console.log('Server started', server.address()))
