var path = require("path");
var webpack = require("webpack");
process.env.NODE_ENV = 'production';


module.exports = {
    entry: {
        vendor: [path.join(__dirname,  "vendors.js")]
    },
    output: {
        path: path.join(__dirname, "dist", "js"),
        filename: "dll.[name].js",
        library: "[name]"
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['react', 'es2015',]
            },
        }]
        },
    plugins: [
        new webpack.DllPlugin({
            path: path.join(__dirname, "dll", "[name]-manifest.json"),
            name: "[name]",
            context: path.resolve(__dirname, "app")
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.EnvironmentPlugin(['NODE_ENV']),

    ],
    resolve: {
        root: path.resolve(__dirname, "app"),
        modulesDirectories: ["node_modules"]
    }
};

